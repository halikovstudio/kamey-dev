<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 **/
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = "N";
$APPLICATION->ShowIncludeStat = false;
$placement = $_REQUEST['PLACEMENT'];
$options = json_decode($_REQUEST['PLACEMENT_OPTIONS'], true);
$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
if ($request->get('DOMAIN') != 'corp.ufa.kamey.ru') {
    die('Access denied');
}

?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
    <link rel="stylesheet" href="/local/templates/cameo-v3/css/bonus.css?v2">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<?
\Bitrix\Main\Loader::includeModule('sale');
$contactId = null;
switch ($placement) {
    case 'CRM_DEAL_DETAIL_TAB':
        $deal = B24Sync::component()->Send('crm.deal.get', ['id' => $options['ID']]);
        $contactId = $deal['result']['CONTACT_ID'];
        $orderId = $deal['result']['ORIGIN_ID'];

        if($orderId) {
            $order = \Bitrix\Sale\Order::load($orderId);
            $userId = $order->getUserId();
        }
        break;
    case 'CRM_CONTACT_DETAIL_TAB':
        $contactId = $options['ID'];
        $contact = $deal = B24Sync::component()->Send('crm.contact.get', ['id' => $options['ID']]);
        $originIdArray = explode('#', $contact['result']['ORIGIN_ID']);
        $userId = $originIdArray[0];
        break;
    default:
        die('Access denied');
}
if (!$contactId) {
    die('Не удалось определить идентификатор контакта');
}
$contact = B24Sync::component()->Send('crm.contact.get', ['id' => $contactId]);
$phone = $_GET['phone'];
if(!$phone && $userId){
    $user = CUser::GetByID($userId)->Fetch();
    if($user['PERSONAL_PHONE']){
        $phone = $user['PERSONAL_PHONE'];
    }
}
if (!$phone) {
    foreach ($contact['result']['PHONE'] as $item) {
        if ($item['VALUE']) {
            $phone = $item['VALUE'];
            break;
        }
    }
}

BonusCard::$guid = BonusCard::findGuidByPhone($phone);
if (!BonusCard::$guid) {
    ?>
    <script>
        $(function(){
          $('[type=tel]').inputmask('+7(999)999-99-99')
        })
    </script>
    <br>
    <div class="ui container">

        <div class="ui error message">
            Карта по номеру телефона <?= $phone ?> не найдена
        </div>
    <form class="ui form">
        <?
        foreach ($_REQUEST as $key => $value) {
            ?>
            <input type="hidden" name="<?=$key?>" value='<?=$value?>'>
            <?
        }
        ?>
        <div class="field">
            <label>Номер телефона</label>
            <input type="tel" name="phone" value="<?=$phone?>">
        </div>
        <button class="ui submit button">Найти</button>
    </form>
    </div>
    <?
    return;
}
?>
<div class="ui container">
    <? $APPLICATION->IncludeFile("/bonus-system/bonus_card_detail.php",['IS_CRM'=>true]); ?>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
