<?php

use Bitrix\Main\Loader;

class CatalogCIBlockElement extends CIBlockElement {
    public function MkFilter($arFilter, &$arJoinProps, &$arAddWhereFields, $level = 0, $bPropertyLeftJoin = false)
    {
        global $DB, $USER;

        $catalogIncluded = Loader::includeModule('catalog');
        $catalogFields = array();

        $arSqlSearch = Array();
        $permSQL = "";

        $arSectionFilter = Array(
          "LOGIC" => "",
          "BE" => array(),
          "BS" => array(),
        );

        $strSqlSearch = "";

        if (!is_array($arFilter))
            $arFilter = array();

        foreach($arFilter as $key=>$val)
        {
            $key = strtoupper($key);
            $p = strpos($key, "PROPERTY_");
            if($p!==false && ($p<4))
            {
                $arFilter[substr($key, 0, $p)."PROPERTY"][substr($key, $p+9)] = $val;
                unset($arFilter[$key]);
            }
        }

        if (isset($arFilter["LOGIC"]) && $arFilter["LOGIC"] == "OR")
        {
            $Logic = "OR";
            unset($arFilter["LOGIC"]);
            $bPropertyLeftJoin = true;
        }
        else
        {
            $Logic = "AND";
        }

        if ($Logic === "AND" && $level === 0)
        {
            $f = new \Bitrix\Iblock\PropertyIndex\QueryBuilder($arFilter["IBLOCK_ID"]);
            if ($f->isValid())
            {
                $arJoinProps["FC"] = $f->getFilterSql($arFilter, $arSqlSearch);
                $arJoinProps["FC_DISTINCT"] = $f->getDistinct();
            }
        }
        foreach($arFilter as $orig_key => $val)
        {
            $res = CIBlock::MkOperationFilter($orig_key);
            $key = $res["FIELD"];
            $cOperationType = $res["OPERATION"];

            //it was done before $key = strtoupper($key);

            switch($key."")
            {
                case "ACTIVE":
                case "DETAIL_TEXT_TYPE":
                case "PREVIEW_TEXT_TYPE":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "string_equal", $bFullJoinTmp, $cOperationType);
                    break;
                case "NAME":
                case "XML_ID":
                case "TMP_ID":
                case "DETAIL_TEXT":
                case "PREVIEW_TEXT":
                case "CODE":
                case "TAGS":
                case "WF_COMMENTS":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "string", $bFullJoinTmp, $cOperationType);
                    break;
                case "SEARCHABLE_CONTENT":
                    if ($DB->IndexExists("b_iblock_element", array("SEARCHABLE_CONTENT")))
                    {
                        $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "fulltext", $bFullJoinTmp, $cOperationType);
                    }
                    else
                    {
                        if ($cOperationType == "FT")
                            $cOperationType = "FTL";
                        elseif ($cOperationType == "FTI")
                            $cOperationType = "E";
                        $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "string", $bFullJoinTmp, $cOperationType);
                    }
                    break;
                case "ID":
                    if(is_object($val))
                    {
                        /** @var CIBlockElement $val */
                        $val->prepareSql(array($val->strField), $val->arFilter, false, false);
                        $arSqlSearch[] = 'BE.'.$key.(substr($cOperationType, 0, 1) == "N"? ' NOT': '').' IN  (
						SELECT '.$val->sSelect.'
						FROM '.$val->sFrom.'
						WHERE 1=1
							'.$val->sWhere.'
						)'
                        ;
                    }
                    else
                    {
                        $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "number", $bFullJoinTmp, $cOperationType);
                    }
                    break;
                case "SHOW_COUNTER":
                case "WF_PARENT_ELEMENT_ID":
                case "WF_STATUS_ID":
                case "SORT":
                case "CREATED_BY":
                case "MODIFIED_BY":
                case "PREVIEW_PICTURE":
                case "DETAIL_PICTURE":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "IBLOCK_ID":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "IBLOCK_SECTION_ID":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "TIMESTAMP_X":
                case "DATE_CREATE":
                case "SHOW_COUNTER_START":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.".$key, $val, "date", $bFullJoinTmp, $cOperationType);
                    break;
                case "EXTERNAL_ID":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.XML_ID", $val, "string", $bFullJoinTmp, $cOperationType);
                    break;
                case "IBLOCK_TYPE":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("B.IBLOCK_TYPE_ID", $val, "string", $bFullJoinTmp, $cOperationType);
                    break;
                case "CHECK_PERMISSIONS":
                    if ($val == 'Y')
                    {
                        $permissionsBy = null;
                        if (isset($arFilter['PERMISSIONS_BY']))
                        {
                            $permissionsBy = (int)$arFilter['PERMISSIONS_BY'];
                            if ($permissionsBy < 0)
                                $permissionsBy = null;
                        }
                        if ($permissionsBy !== null)
                            $permSQL = self::_check_rights_sql($arFilter["MIN_PERMISSION"], $permissionsBy);
                        elseif (!is_object($USER) || !$USER->IsAdmin())
                            $permSQL = self::_check_rights_sql($arFilter["MIN_PERMISSION"]);
                        unset($permissionsBy);
                    }
                    break;
                case "CHECK_BP_PERMISSIONS":
                    if(IsModuleInstalled('bizproc') && (!is_object($USER) || !$USER->IsAdmin()))
                    {
                        if(is_array($val))
                        {
                            $MODULE_ID = $DB->ForSQL($val["MODULE_ID"]);
                            $ENTITY = $DB->ForSQL($val["ENTITY"]);
                            $PERMISSION = $DB->ForSQL($val["PERMISSION"]);
                            $arUserGroups = array();
                            if(is_array($val["GROUPS"]))
                            {
                                $USER_ID = intval($val["USER_ID"]);
                                foreach($val["GROUPS"] as $GROUP_ID)
                                {
                                    $GROUP_ID = intval($GROUP_ID);
                                    if($GROUP_ID)
                                        $arUserGroups[$GROUP_ID] = $GROUP_ID;
                                }
                            }
                            else
                            {
                                $USER_ID = 0;
                            }
                        }
                        else
                        {
                            $MODULE_ID = "iblock";
                            $ENTITY = "CIBlockDocument";
                            $PERMISSION = $val;
                            $arUserGroups = false;
                            $USER_ID = 0;
                        }

                        if($PERMISSION == "read" || $PERMISSION == "write")
                        {
                            if(!is_array($arUserGroups) && is_object($USER))
                            {
                                $USER_ID = intval($USER->GetID());
                                $arUserGroups = $USER->GetUserGroupArray();
                            }

                            if(!is_array($arUserGroups) || count($arUserGroups) <= 0)
                                $arUserGroups = array(2);

                            $arSqlSearch[] = "EXISTS (
							SELECT S.DOCUMENT_ID_INT
							FROM
							b_bp_workflow_state S
							INNER JOIN b_bp_workflow_permissions P ON S.ID = P.WORKFLOW_ID
							WHERE
								S.DOCUMENT_ID_INT = BE.ID
								AND S.MODULE_ID = '$MODULE_ID'
								AND S.ENTITY = '$ENTITY'
								AND P.PERMISSION = '$PERMISSION'
								AND (
									P.OBJECT_ID IN ('".implode("', '", $arUserGroups)."')
									OR (P.OBJECT_ID = 'Author' AND BE.CREATED_BY = $USER_ID)
									OR (P.OBJECT_ID = ".$DB->Concat("'USER_'", "'$USER_ID'").")
								)
						)";
                        }
                    }
                    break;
                case "CHECK_BP_TASKS_PERMISSIONS":
                    if(
                      IsModuleInstalled('bizproc')
                      && CModule::IncludeModule("socialnetwork")
                      && (!is_object($USER) || !$USER->IsAdmin())
                    )
                    {
                        $val = explode("_", $val);

                        $taskType = $val[0];
                        if (!in_array($taskType, array("user", "group")))
                            $taskType = "user";

                        $ownerId = intval($val[1]);

                        $val = $val[2];
                        if (!in_array($val, array("read", "write", "comment")))
                            $val = "write";

                        $userId = is_object($USER)? intval($USER->GetID()): 0;

                        $arUserGroups = array();
                        if ($taskType == "group")
                        {
                            $r = CSocNetFeaturesPerms::CanPerformOperation(
                              $userId,
                              SONET_ENTITY_GROUP,
                              $ownerId,
                              "tasks",
                              (($val == "write") ? "edit_tasks" : "view_all")
                            );
                            if ($r)
                                break;

                            $arUserGroups[] = SONET_ROLES_ALL;
                            $r = CSocNetUserToGroup::GetUserRole($userId, $ownerId);
                            if (strlen($r) > 0)
                                $arUserGroups[] = $r;
                        }
                        else
                        {
//						$arUserGroups[] = SONET_RELATIONS_TYPE_ALL;
//						if (CSocNetUserRelations::IsFriends($userId, $ownerId))
//							$arUserGroups[] = SONET_RELATIONS_TYPE_FRIENDS;
//						elseif (CSocNetUserRelations::IsFriends2($userId, $ownerId))
//							$arUserGroups[] = SONET_RELATIONS_TYPE_FRIENDS2;
                        }

                        $arSqlSearch[] = "EXISTS (
						SELECT S.DOCUMENT_ID_INT
						FROM
						b_bp_workflow_state S
						INNER JOIN b_bp_workflow_permissions P ON S.ID = P.WORKFLOW_ID
						WHERE
							S.DOCUMENT_ID_INT = BE.ID
							AND S.MODULE_ID = 'intranet'
							AND S.ENTITY = 'CIntranetTasksDocument'
							AND P.PERMISSION = '".$val."'
							AND (
								".(($taskType == "group") ? "P.OBJECT_ID IN ('".implode("', '", $arUserGroups)."') OR" : "")."
								(P.OBJECT_ID = 'author' AND BE.CREATED_BY = ".$userId.")
								OR (P.OBJECT_ID = 'responsible' AND ".$userId." IN (
									SELECT SFPV0.VALUE_NUM
									FROM b_iblock_element_property SFPV0
										INNER JOIN b_iblock_property SFP0 ON (SFPV0.IBLOCK_PROPERTY_ID = SFP0.ID)
									WHERE ".CIBlock::_Upper("SFP0.CODE")."='TASKASSIGNEDTO'
										AND SFP0.IBLOCK_ID = BE.IBLOCK_ID
										AND SFPV0.IBLOCK_ELEMENT_ID = BE.ID
								))
								OR (P.OBJECT_ID = 'trackers' AND ".$userId." IN (
									SELECT SFPV0.VALUE_NUM
									FROM b_iblock_element_property SFPV0
										INNER JOIN b_iblock_property SFP0 ON (SFPV0.IBLOCK_PROPERTY_ID = SFP0.ID)
									WHERE ".CIBlock::_Upper("SFP0.CODE")."='TASKTRACKERS'
										AND SFP0.IBLOCK_ID = BE.IBLOCK_ID
										AND SFPV0.IBLOCK_ELEMENT_ID = BE.ID
								))
								OR (P.OBJECT_ID = '".("USER_".$userId)."')
							)
					)";
                    }
                    break;
                case "CHECK_BP_VIRTUAL_PERMISSIONS":
                    if (
                      IsModuleInstalled('bizproc')
                      && (!is_object($USER) || !$USER->IsAdmin())
                    )
                    {
                        if (!in_array($val, array("read", "create", "admin")))
                            $val = "admin";

                        $userId = is_object($USER)? (int)$USER->GetID(): 0;
                        $arUserGroups = is_object($USER)? $USER->GetUserGroupArray(): false;

                        if (!is_array($arUserGroups) || empty($arUserGroups))
                            $arUserGroups = array(2);

                        $arSqlSearch[] = "EXISTS (
						SELECT S.DOCUMENT_ID_INT
						FROM b_bp_workflow_state S
							INNER JOIN b_bp_workflow_permissions P ON S.ID = P.WORKFLOW_ID
						WHERE S.DOCUMENT_ID_INT = BE.ID
							AND S.MODULE_ID = 'bizproc'
							AND S.ENTITY = 'CBPVirtualDocument'
							AND
								(P.PERMISSION = '".$val."'
								AND (
									P.OBJECT_ID IN ('".implode("', '", $arUserGroups)."')
									OR (P.OBJECT_ID = 'Author' AND BE.CREATED_BY = ".$userId.")
									OR (P.OBJECT_ID = ".$DB->Concat("'USER_'", "'".$userId."'").")
								)
							)
					)";
                    }
                    break;
                case "TASKSTATUS":
                    if(IsModuleInstalled('bizproc'))
                    {
                        $arSqlSearch[] = ($cOperationType == "N" ? "NOT " : "")."EXISTS (
						SELECT S.DOCUMENT_ID_INT
						FROM
						b_bp_workflow_state S
						WHERE
							S.DOCUMENT_ID_INT = BE.ID
							AND S.MODULE_ID = 'intranet'
							AND S.ENTITY = 'CIntranetTasksDocument'
							AND S.STATE = '".$DB->ForSql($val)."'
					)";
                    }
                    break;
                case "LID":
                case "SITE_ID":
                case "IBLOCK_LID":
                case "IBLOCK_SITE_ID":
                    $flt = CIBlock::FilterCreateEx("SITE_ID", $val, "string_equal", $bFullJoinTmp, $cOperationType);
                    if ($flt !== '')
                        $arSqlSearch[] = ($cOperationType == "N" ? "NOT " : "")."EXISTS (
						SELECT IBLOCK_ID FROM b_iblock_site WHERE IBLOCK_ID = B.ID
						AND ".$flt."
					)";
                    break;
                case "DATE_ACTIVE_FROM":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.ACTIVE_FROM", $val, "date", $bFullJoinTmp, $cOperationType);
                    break;
                case "DATE_ACTIVE_TO":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.ACTIVE_TO", $val, "date", $bFullJoinTmp, $cOperationType);
                    break;
                case "IBLOCK_ACTIVE":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("B.ACTIVE", $val, "string_equal", $bFullJoinTmp, $cOperationType);
                    break;
                case "IBLOCK_CODE":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("B.CODE", $val, "string", $bFullJoinTmp, $cOperationType);
                    break;
                case "ID_ABOVE":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.ID", $val, "number_above", $bFullJoinTmp, $cOperationType);
                    break;
                case "ID_LESS":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.ID", $val, "number_less", $bFullJoinTmp, $cOperationType);
                    break;
                case "ACTIVE_FROM":
                    $val = (string)$val;
                    if($val !== '')
                        $arSqlSearch[] = "(BE.ACTIVE_FROM ".($cOperationType=="N"?"<":">=").$DB->CharToDateFunction($DB->ForSql($val), "FULL").($cOperationType=="N"?"":" OR BE.ACTIVE_FROM IS NULL").")";
                    break;
                case "ACTIVE_TO":
                    $val = (string)$val;
                    if($val !== '')
                        $arSqlSearch[] = "(BE.ACTIVE_TO ".($cOperationType=="N"?">":"<=").$DB->CharToDateFunction($DB->ForSql($val), "FULL").($cOperationType=="N"?"":" OR BE.ACTIVE_TO IS NULL").")";
                    break;
                case "ACTIVE_DATE":
                    $val = (string)$val;
                    if($val !== '')
                        $arSqlSearch[] = ($cOperationType=="N"?" NOT":"")."((BE.ACTIVE_TO >= ".$DB->GetNowFunction()." OR BE.ACTIVE_TO IS NULL) AND (BE.ACTIVE_FROM <= ".$DB->GetNowFunction()." OR BE.ACTIVE_FROM IS NULL))";
                    break;
                case "DATE_MODIFY_FROM":
                    $val = (string)$val;
                    if($val !== '')
                        $arSqlSearch[] = "(BE.TIMESTAMP_X ".
                          ( $cOperationType=="N" ? "<" : ">=" ).$DB->CharToDateFunction($DB->ForSql($val), "FULL").
                          ( $cOperationType=="N" ? ""  : " OR BE.TIMESTAMP_X IS NULL").")";
                    break;
                case "DATE_MODIFY_TO":
                    $val = (string)$val;
                    if($val !== '')
                        $arSqlSearch[] = "(BE.TIMESTAMP_X ".
                          ( $cOperationType=="N" ? ">" : "<=" ).$DB->CharToDateFunction($DB->ForSql($val), "FULL").
                          ( $cOperationType=="N" ? ""  : " OR BE.TIMESTAMP_X IS NULL").")";
                    break;
                case "WF_NEW":
                    if($val=="Y" || $val=="N")
                        $arSqlSearch[] = CIBlock::FilterCreateEx("BE.WF_NEW", "Y", "string_equal", $bFullJoinTmp, ($val=="Y"?false:true), false);
                    break;
                case "MODIFIED_USER_ID":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.MODIFIED_BY", $val, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "CREATED_USER_ID":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.CREATED_BY", $val, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "RATING_USER_ID":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("RVV.USER_ID", $val, "number", $bFullJoinTmp, $cOperationType);
                    $arJoinProps["RVV"] = array(
                      "bFullJoin" => $bFullJoinTmp,
                    );
                    break;
                case "WF_STATUS":
                    $arSqlSearch[] = CIBlock::FilterCreateEx("BE.WF_STATUS_ID", $val, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "WF_LOCK_STATUS":
                    if(strlen($val)>0)
                    {
                        $USER_ID = is_object($USER)? intval($USER->GetID()): 0;
                        $arSqlSearch[] = " if(BE.WF_DATE_LOCK is null, 'green', if(DATE_ADD(BE.WF_DATE_LOCK, interval ".COption::GetOptionInt("workflow", "MAX_LOCK_TIME", 60)." MINUTE)<now(), 'green', if(BE.WF_LOCKED_BY=".$USER_ID.", 'yellow', 'red'))) = '".$DB->ForSql($val)."'";
                    }
                    break;
                case "WF_LAST_STATUS_ID":
                    $arSqlSearch[] = "exists (
					select
						history.ID
					from
						b_iblock_element history
					where
						history.WF_PARENT_ELEMENT_ID = BE.ID
						and history.WF_STATUS_ID = ".intval($val)."
						and history.ID = (
							select max(history0.ID) LAST_ID
							from b_iblock_element history0
							where history0.WF_PARENT_ELEMENT_ID = BE.ID
						)
				)
				";
                    break;
                case "SECTION_ACTIVE":
                    if($arFilter["INCLUDE_SUBSECTIONS"]==="Y")
                        $arSectionFilter["BS"][] = "BSubS.ACTIVE = 'Y'";
                    else
                        $arSectionFilter["BS"][] = "BS.ACTIVE = 'Y'";
                    break;
                case "SECTION_GLOBAL_ACTIVE":
                    if($arFilter["INCLUDE_SUBSECTIONS"]==="Y")
                        $arSectionFilter["BS"][] = "BSubS.GLOBAL_ACTIVE = 'Y'";
                    else
                        $arSectionFilter["BS"][] = "BS.GLOBAL_ACTIVE = 'Y'";
                    break;
                case "SUBSECTION":
                    if (!is_array($val))
                        $val=Array($val);
                    //Find out margins of sections
                    $arUnknownMargins = array();
                    foreach($val as $i=>$section)
                    {
                        if(!is_array($section))
                            $arUnknownMargins[intval($section)] = intval($section);
                    }
                    if (!empty($arUnknownMargins))
                    {
                        $rs = $DB->Query("SELECT ID, LEFT_MARGIN, RIGHT_MARGIN FROM b_iblock_section WHERE ID in (".implode(", ", $arUnknownMargins).")");
                        while($ar = $rs->Fetch())
                        {
                            $arUnknownMargins[intval($ar["ID"])] = array(
                              intval($ar["LEFT_MARGIN"]),
                              intval($ar["RIGHT_MARGIN"]),
                            );
                        }
                        foreach($val as $i=>$section)
                        {
                            if(!is_array($section))
                                $val[$i] = $arUnknownMargins[intval($section)];
                        }
                    }
                    //Now sort them out
                    $arMargins = array();
                    foreach($val as $i=>$section)
                    {
                        if(is_array($section) && (count($section) == 2))
                        {
                            $left = intval($section[0]);
                            $right = intval($section[1]);
                            if($left > 0 && $right > 0)
                                $arMargins[$left] = $right;
                        }
                    }
                    ksort($arMargins);
                    //Remove subsubsections of the sections
                    $prev_right = 0;
                    foreach($arMargins as $left => $right)
                    {
                        if($right <= $prev_right)
                            unset($arMargins[$left]);
                        else
                            $prev_right = $right;
                    }

                    if(isset($arFilter["INCLUDE_SUBSECTIONS"]) && $arFilter["INCLUDE_SUBSECTIONS"] === "Y")
                        $bsAlias = "BSubS";
                    else
                        $bsAlias = "BS";

                    $res = "";
                    foreach($arMargins as $left => $right)
                    {
                        if($res!="")
                            $res .= ($cOperationType=="N"?" AND ":" OR ");
                        $res .= ($cOperationType == "N"? " NOT ": " ")."($bsAlias.LEFT_MARGIN >= ".$left." AND $bsAlias.RIGHT_MARGIN <= ".$right.")\n";;
                    }

                    if($res!="")
                        $arSectionFilter["BS"][] = "(".$res.")";
                    break;
                case "SECTION_ID":
                    if(!is_array($val))
                        $val = array($val);

                    $arSections = array();
                    foreach($val as $section_id)
                    {
                        $section_id = (int)$section_id;
                        $arSections[$section_id] = $section_id;
                    }

                    if($cOperationType=="N")
                    {
                        if(array_key_exists(0, $arSections))
                        {
                            $arSectionFilter["BE"][] = "BE.IN_SECTIONS<>'N'";
                            $arSectionFilter["LOGIC"] = "AND";
                            unset($arSections[0]);
                            if(!empty($arSections))
                                $arSectionFilter["BS"][] = "BS.ID NOT IN (".implode(", ", $arSections).")";
                        }
                        elseif(!empty($arSections))
                        {
                            $arSectionFilter["BE"][] = "BE.IN_SECTIONS='N'";
                            $arSectionFilter["LOGIC"] = "OR";
                            $arSectionFilter["BS"][] = "BS.ID NOT IN (".implode(", ", $arSections).")";
                        }
                    }
                    else
                    {
                        if(array_key_exists(0, $arSections))
                        {
                            $arSectionFilter["BE"][] = "BE.IN_SECTIONS='N'";
                            $arSectionFilter["LOGIC"] = "OR";
                            unset($arSections[0]);
                        }
                        if(!empty($arSections))
                            $arSectionFilter["BS"][] = "BS.ID IN (".implode(", ", $arSections).")";
                    }
                    break;
                case "SECTION_CODE":
                    if(!is_array($val))
                        $val = array($val);

                    $arSections = array();
                    foreach($val as $section_code)
                    {
                        $section_code = $DB->ForSql($section_code);
                        $arSections[$section_code] = $section_code;
                    }

                    if($cOperationType=="N")
                    {
                        if(array_key_exists("", $arSections))
                        {
                            $arSectionFilter["BE"][] = "BE.IN_SECTIONS<>'N'";
                            $arSectionFilter["LOGIC"] = "AND";
                            unset($arSections[""]);
                            if(!empty($arSections))
                                $arSectionFilter["BS"][] = "BS.CODE NOT IN ('".implode("', '", $arSections)."')";
                        }
                        elseif(!empty($arSections))
                        {
                            $arSectionFilter["BE"][] = "BE.IN_SECTIONS='N'";
                            $arSectionFilter["LOGIC"] = "OR";
                            $arSectionFilter["BS"][] = "BS.CODE NOT IN ('".implode("', '", $arSections)."')";
                        }
                    }
                    else
                    {
                        if(array_key_exists("", $arSections))
                        {
                            $arSectionFilter["BE"][] = "BE.IN_SECTIONS='N'";
                            $arSectionFilter["LOGIC"] = "OR";
                            unset($arSections[""]);
                        }
                        if(!empty($arSections))
                            $arSectionFilter["BS"][] = "BS.CODE IN ('".implode("', '", $arSections)."')";
                    }
                    break;
                case "PROPERTY":
                    foreach($val as $propID=>$propVAL)
                    {
                        $res = CIBlock::MkOperationFilter($propID);
                        $res["LOGIC"] = $Logic;
                        $res["LEFT_JOIN"] = $bPropertyLeftJoin;
                        if(preg_match("/^([^.]+)\\.([^.]+)$/", $res["FIELD"], $arMatch))
                        {
                            $db_prop = CIBlockProperty::GetPropertyArray($arMatch[1], CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"], $arFilter["~IBLOCK_ID"], $arFilter["~IBLOCK_CODE"]));
                            if(is_array($db_prop) && $db_prop["PROPERTY_TYPE"] == "E")
                            {
                                $res["FIELD"] = $arMatch;
                                self::MkPropertyFilter($res, $cOperationType, $propVAL, $db_prop, $arJoinProps, $arSqlSearch);
                            }
                        }
                        else
                        {
                            if($db_prop = CIBlockProperty::GetPropertyArray($res["FIELD"], CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"], $arFilter["~IBLOCK_ID"], $arFilter["~IBLOCK_CODE"])))
                                self::MkPropertyFilter($res, $cOperationType, $propVAL, $db_prop, $arJoinProps, $arSqlSearch);
                        }
                    }
                    break;
                default:
                    if(is_numeric($orig_key))
                    {
                        if (is_array($val))
                        {
                            //Here is hint for better property resolution:
                            if (!isset($val["~IBLOCK_ID"]))
                            {
                                if (isset($arFilter["IBLOCK_ID"]))
                                    $val["~IBLOCK_ID"] = $arFilter["IBLOCK_ID"];
                                elseif (isset($arFilter["~IBLOCK_ID"]))
                                    $val["~IBLOCK_ID"] = $arFilter["~IBLOCK_ID"];
                            }
                            if (!isset($val["~IBLOCK_CODE"]))
                            {
                                if (isset($arFilter["IBLOCK_CODE"]))
                                    $val["~IBLOCK_CODE"] = $arFilter["IBLOCK_CODE"];
                                elseif (isset($arFilter["~IBLOCK_CODE"]))
                                    $val["~IBLOCK_CODE"] = $arFilter["~IBLOCK_CODE"];
                            }
                            //Subfilter process
                            $arSubSqlSearch = self::MkFilter($val, $arJoinProps, $arAddWhereFields, $level + 1, $bPropertyLeftJoin);
                            if (strlen(trim($arSubSqlSearch[0], "\n\t")))
                                $arSqlSearch[] = str_replace("\n\t\t\t", "\n\t\t\t\t", $arSubSqlSearch[0]);
                        }
                    }
                    elseif ($catalogIncluded && \CProductQueryBuilder::isValidField($key))
                    {
                        $catalogFields[$orig_key] = $val;
                        $arAddWhereFields[$orig_key] = $val;
                    }
                    break;
            }
        }
        if ($catalogIncluded && !empty($catalogFields))
        {
            $catalogQueryResult = \CProductQueryBuilder::makeFilter($catalogFields);
            // catalog join set in \CIBlockElement::prepareSql
            if (!empty($catalogQueryResult) && !empty($catalogQueryResult['filter']))
                $arSqlSearch = array_merge($arSqlSearch, $catalogQueryResult['filter']);
            unset($catalogQueryResult);
        }
        unset($catalogFields);

        //SECTION sub filter
        $sWhere = "";
        foreach($arSectionFilter["BS"] as $strFilter)
        {
            if ($strFilter !== '')
            {
                if ($sWhere !== '')
                    $sWhere .= " ".$Logic." ";
                $sWhere .= "(".$strFilter.")";
            }
        }

        $bINCLUDE_SUBSECTIONS = isset($arFilter["INCLUDE_SUBSECTIONS"]) && $arFilter["INCLUDE_SUBSECTIONS"] === "Y";

        if($sWhere !== '')
        {
            $sectionScope = "";
            if (isset($arFilter["SECTION_SCOPE"]))
            {
                if ($arFilter["SECTION_SCOPE"] == "IBLOCK")
                    $sectionScope = "AND BSE.ADDITIONAL_PROPERTY_ID IS NULL";
                elseif ($arFilter["SECTION_SCOPE"] == "PROPERTY")
                    $sectionScope = "AND BSE.ADDITIONAL_PROPERTY_ID IS NOT NULL";
                elseif (preg_match("/^PROPERTY_(\\d+)\$/", $arFilter["SECTION_SCOPE"], $match))
                    $sectionScope = "AND BSE.ADDITIONAL_PROPERTY_ID = ".$match[1];
            }

            //Try to convert correlated subquery to join subquery
            if($level == 0 && $Logic == "AND" && !count($arSectionFilter["BE"]))
            {
                $arJoinProps["BES"] .= " INNER JOIN (
					SELECT DISTINCT BSE.IBLOCK_ELEMENT_ID
					FROM b_iblock_section_element BSE
					".($bINCLUDE_SUBSECTIONS? "
					INNER JOIN b_iblock_section BSubS ON BSE.IBLOCK_SECTION_ID = BSubS.ID
					INNER JOIN b_iblock_section BS ON (BSubS.IBLOCK_ID=BS.IBLOCK_ID
						AND BSubS.LEFT_MARGIN>=BS.LEFT_MARGIN
						AND BSubS.RIGHT_MARGIN<=BS.RIGHT_MARGIN)
					" : "
					INNER JOIN b_iblock_section BS ON BSE.IBLOCK_SECTION_ID = BS.ID
					")."
					WHERE (".$sWhere.")$sectionScope
					) BES ON BES.IBLOCK_ELEMENT_ID = BE.ID\n";
            }
            else
            {
                $arSqlSearch[] = "(".(count($arSectionFilter["BE"])? implode(" ".$arSectionFilter["LOGIC"]." ", $arSectionFilter["BE"])." ".$arSectionFilter["LOGIC"]: "")." EXISTS (
					SELECT BSE.IBLOCK_ELEMENT_ID
					FROM b_iblock_section_element BSE
					".($bINCLUDE_SUBSECTIONS? "
					INNER JOIN b_iblock_section BSubS ON BSE.IBLOCK_SECTION_ID = BSubS.ID
					INNER JOIN b_iblock_section BS ON (BSubS.IBLOCK_ID=BS.IBLOCK_ID
						AND BSubS.LEFT_MARGIN>=BS.LEFT_MARGIN
						AND BSubS.RIGHT_MARGIN<=BS.RIGHT_MARGIN)
					" : "
					INNER JOIN b_iblock_section BS ON BSE.IBLOCK_SECTION_ID = BS.ID
					")."
					WHERE BSE.IBLOCK_ELEMENT_ID = BE.ID
					AND (".$sWhere.")$sectionScope
					))";
            }
        }
        elseif(!empty($arSectionFilter["BE"]))
        {
            foreach($arSectionFilter["BE"] as $strFilter)
                $arSqlSearch[] = $strFilter;
        }

        $sWhere = "";
        foreach($arSqlSearch as $strFilter)
        {
            if(trim($strFilter, "\n\t") !== '')
            {
                if ($sWhere !== '')
                    $sWhere .= "\n\t\t\t\t".$Logic." ";
                else
                    $sWhere .= "\n\t\t\t\t";
                $sWhere .= "(".$strFilter.")";
            }
        }

        $arSqlSearch = array("\n\t\t\t".$sWhere."\n\t\t\t");

        $SHOW_BP_NEW = "";
        $SHOW_NEW = isset($arFilter["SHOW_NEW"]) && $arFilter["SHOW_NEW"]=="Y"? "Y": "N";

        if(
          $SHOW_NEW == "Y"
          && isset($arFilter["SHOW_BP_NEW"])
          && is_array($arFilter["SHOW_BP_NEW"])
          && IsModuleInstalled('bizproc')
          && (!is_object($USER) || !$USER->IsAdmin())
        )
        {

            $MODULE_ID = $DB->ForSQL($arFilter["SHOW_BP_NEW"]["MODULE_ID"]);
            $ENTITY = $DB->ForSQL($arFilter["SHOW_BP_NEW"]["ENTITY"]);
            $PERMISSION = $DB->ForSQL($arFilter["SHOW_BP_NEW"]["PERMISSION"]);
            $arUserGroups = array();
            if(is_array($arFilter["SHOW_BP_NEW"]["GROUPS"]))
            {
                $USER_ID = intval($arFilter["SHOW_BP_NEW"]["USER_ID"]);
                foreach($arFilter["SHOW_BP_NEW"]["GROUPS"] as $GROUP_ID)
                {
                    $GROUP_ID = intval($GROUP_ID);
                    if($GROUP_ID)
                        $arUserGroups[$GROUP_ID] = $GROUP_ID;
                }
            }
            else
            {
                $USER_ID = false;
                $arUserGroups = false;
            }

            if($PERMISSION == "read" || $PERMISSION == "write")
            {
                if(!is_array($arUserGroups))
                {
                    $USER_ID = is_object($USER)? (int)$USER->GetID(): 0;
                    if(is_object($USER))
                        $arUserGroups = $USER->GetUserGroupArray();
                }
                if(!is_array($arUserGroups) || empty($arUserGroups))
                    $arUserGroups = array(2);

                $SHOW_BP_NEW = " AND EXISTS (
					SELECT S.DOCUMENT_ID_INT
					FROM
					b_bp_workflow_state S
					INNER JOIN b_bp_workflow_permissions P ON S.ID = P.WORKFLOW_ID
					WHERE
						S.DOCUMENT_ID_INT = BE.ID
						AND S.MODULE_ID = '$MODULE_ID'
						AND S.ENTITY = '$ENTITY'
						AND P.PERMISSION = '$PERMISSION'
						AND (
							P.OBJECT_ID IN ('".implode("', '", $arUserGroups)."')
							OR (P.OBJECT_ID = 'Author' AND BE.CREATED_BY = $USER_ID)
							OR (P.OBJECT_ID = ".$DB->Concat("'USER_'", "'$USER_ID'").")
						)
				)";
            }
        }

        if(!isset($arFilter["SHOW_HISTORY"]) || $arFilter["SHOW_HISTORY"] != "Y")
            $arSqlSearch[] = "((BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL)".($SHOW_NEW == "Y"? " OR (BE.WF_NEW='Y'".$SHOW_BP_NEW.")": "").")";

        if($permSQL)
            $arSqlSearch[] = $permSQL;

        if(isset($this) && is_object($this) && isset($this->subQueryProp))
        {
            //Subquery list value should not be null
            $this->MkPropertyFilter(
              CIBlock::MkOperationFilter("!".substr($this->strField, 9))
              ,"NE"
              ,false
              ,$this->subQueryProp, $arJoinProps, $arSqlSearch
            );
        }

        return $arSqlSearch;
    }

    public static function GetList($arOrder=array("SORT"=>"ASC"), $arFilter=array(), $arGroupBy=false, $arNavStartParams=false, $arSelectFields=array())
    {
        global $DB;

        $el = new self();
        $el->prepareSql($arSelectFields, $arFilter, $arGroupBy, $arOrder);

        if($el->bOnlyCount)
        {
            $res = $DB->Query("
				SELECT ".$el->sSelect."
				FROM ".$el->sFrom."
				WHERE 1=1 ".$el->sWhere."
				".$el->sGroupBy."
			");
            $res = $res->Fetch();
            return $res["CNT"];
        }

        if(!empty($arNavStartParams) && is_array($arNavStartParams))
        {
            $nTopCount = (isset($arNavStartParams["nTopCount"]) ? (int)$arNavStartParams["nTopCount"] : 0);
            $nElementID = (isset($arNavStartParams["nElementID"]) ? (int)$arNavStartParams["nElementID"] : 0);

            if($nTopCount > 0)
            {
                $strSql = "
					SELECT ".$el->sSelect."
					FROM ".$el->sFrom."
					WHERE 1=1 ".$el->sWhere."
					".$el->sGroupBy."
					".$el->sOrderBy."
					LIMIT ".$nTopCount."
				";
                $res = $DB->Query($strSql);
            }
            elseif(
              $nElementID > 0
              && $el->sGroupBy == ""
              && $el->sOrderBy != ""
              && strpos($el->sSelect, "BE.ID") !== false
              && !$el->bCatalogSort
            )
            {
                $nPageSize = (isset($arNavStartParams["nPageSize"]) ? (int)$arNavStartParams["nPageSize"] : 0);

                if($nPageSize > 0)
                {
                    $DB->Query("SET @rank=0");
                    $DB->Query("
						SELECT @rank:=el1.rank
						FROM (
							SELECT @rank:=@rank+1 AS rank, el0.*
							FROM (
								SELECT ".$el->sSelect."
								FROM ".$el->sFrom."
								WHERE 1=1 ".$el->sWhere."
								".$el->sGroupBy."
								".$el->sOrderBy."
								LIMIT 18446744073709551615
							) el0
						) el1
						WHERE el1.ID = ".$nElementID."
					");
                    $DB->Query("SET @rank2=0");

                    $res = $DB->Query("
						SELECT *
						FROM (
							SELECT @rank2:=@rank2+1 AS RANK, el0.*
							FROM (
								SELECT ".$el->sSelect."
								FROM ".$el->sFrom."
								WHERE 1=1 ".$el->sWhere."
								".$el->sGroupBy."
								".$el->sOrderBy."
								LIMIT 18446744073709551615
							) el0
						) el1
						WHERE el1.RANK between @rank-$nPageSize and @rank+$nPageSize
					");
                }
                else
                {
                    $DB->Query("SET @rank=0");
                    $res = $DB->Query("
						SELECT el1.*
						FROM (
							SELECT @rank:=@rank+1 AS RANK, el0.*
							FROM (
								SELECT ".$el->sSelect."
								FROM ".$el->sFrom."
								WHERE 1=1 ".$el->sWhere."
								".$el->sGroupBy."
								".$el->sOrderBy."
								LIMIT 18446744073709551615
							) el0
						) el1
						WHERE el1.ID = ".$nElementID."
					");
                }
            }
            else
            {
                if ($el->sGroupBy == "")
                {
                    $res_cnt = $DB->Query("
						SELECT COUNT(".($el->bDistinct? "DISTINCT BE.ID": "'x'").") as C
						FROM ".$el->sFrom."
						WHERE 1=1 ".$el->sWhere."
						".$el->sGroupBy."
					");
                    $res_cnt = $res_cnt->Fetch();
                    $cnt = $res_cnt["C"];
                }
                else
                {
                    $res_cnt = $DB->Query("
						SELECT 'x'
						FROM ".$el->sFrom."
						WHERE 1=1 ".$el->sWhere."
						".$el->sGroupBy."
					");
                    $cnt = $res_cnt->SelectedRowsCount();
                }

                $strSql = "
					SELECT ".$el->sSelect."
					FROM ".$el->sFrom."
					WHERE 1=1 ".$el->sWhere."
					".$el->sGroupBy."
					".$el->sOrderBy."
				";
                $res = new CDBResult();
                $res->NavQuery($strSql, $cnt, $arNavStartParams);
            }
        }
        else//if(is_array($arNavStartParams))
        {
            $strSql = "
				SELECT ".$el->sSelect."
				FROM ".$el->sFrom."
				WHERE 1=1 ".$el->sWhere."
				".$el->sGroupBy."
				".$el->sOrderBy."
			";
            $res = $DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
        }

        $res = new CIBlockResult($res);
        $res->SetIBlockTag($el->arFilterIBlocks);
        $res->arIBlockMultProps = $el->arIBlockMultProps;
        $res->arIBlockConvProps = $el->arIBlockConvProps;
        $res->arIBlockAllProps  = $el->arIBlockAllProps;
        $res->arIBlockNumProps = $el->arIBlockNumProps;
        $res->arIBlockLongProps = $el->arIBlockLongProps;

        return $res;
    }

    function PrepareGetList(
      &$arIblockElementFields,
      &$arJoinProps,

      &$arSelectFields,
      &$sSelect,
      &$arAddSelectFields,

      &$arFilter,
      &$sWhere,
      &$sSectionWhere,
      &$arAddWhereFields,

      &$arGroupBy,
      &$sGroupBy,

      &$arOrder,
      &$arSqlOrder,
      &$arAddOrderByFields
    ){
        if(
          is_array($arSelectFields)
          && (in_array("DETAIL_PAGE_URL", $arSelectFields) || in_array("CANONICAL_PAGE_URL", $arSelectFields))
          && !in_array("LANG_DIR", $arSelectFields)
        )
        {
            $arSelectFields[] = "LANG_DIR";
        }

        $catalogIncluded = Loader::includeModule('catalog');

        global $DB, $USER;

        if((!is_array($arSelectFields) && $arSelectFields=="") || count($arSelectFields)<=0 || $arSelectFields===false)
            $arSelectFields = Array("*");

        if(is_bool($arGroupBy) && $arGroupBy!==false)
            $arGroupBy = Array();

        if(is_array($arGroupBy) && count($arGroupBy)==0)
            $this->bOnlyCount = true;

        $iPropCnt = 0;
        $arJoinProps = Array(
          "FP" => array(
              //CNT
              //bFullJoin
          ),
          "FPV" => array(
              //CNT
              //IBLOCK_ID
              //MULTIPLE
              //VERSION
              //JOIN
              //bFullJoin
          ),
          "FPS" => array(
              //
          ),
          "FPEN" => array(
              //CNT
              //MULTIPLE
              //VERSION
              //ORIG_ID
              //JOIN
              //bFullJoin
          ),
          "BE" => array(
              //CNT
              //MULTIPLE
              //VERSION
              //ORIG_ID
              //JOIN
              //bJoinIBlock
              //bJoinSection
          ),
          "BE_FP" => array(
              //CNT
              //JOIN
              //bFullJoin
          ),
          "BE_FPV" => array(
              //CNT
              //IBLOCK_ID
              //MULTIPLE
              //VERSION
              //JOIN
              //BE_JOIN
              //bFullJoin
          ),
          "BE_FPS" => array(
              //CNT
              //JOIN
          ),
          "BE_FPEN" => array(
              //CNT
              //MULTIPLE
              //VERSION
              //ORIG_ID
              //JOIN
              //bFullJoin
          ),
          "BES" => "",
          "RV" => false,
          "RVU" => false,
          "RVV" => false,
          "FC" => "",
        );

        $this->arIBlockMultProps = Array();
        $this->arIBlockAllProps = Array();
        $this->arIBlockNumProps = Array();
        $bWasGroup = false;

        //********************************ORDER BY PART***********************************************
        $arSqlOrder = Array();
        $arAddOrderByFields = Array();
        $iOrdNum = -1;
        if(!is_array($arOrder))
            $arOrder = Array();
        foreach($arOrder as $by=>$order)
        {
            $by_orig = $by;
            $by = strtoupper($by);
            //Remove aliases
            if($by == "EXTERNAL_ID") $by = "XML_ID";
            elseif($by == "DATE_ACTIVE_FROM") $by = "ACTIVE_FROM";
            elseif($by == "DATE_ACTIVE_TO") $by = "ACTIVE_TO";

            if(array_key_exists($by, $arSqlOrder))
                continue;

            if ($catalogIncluded && \CProductQueryBuilder::isValidField($by))
            {
                $iOrdNum++;
                $arAddOrderByFields[$iOrdNum] = Array($by=>$order);
                //Reserve for future fill
                $arSqlOrder[$iOrdNum] = false;
            }
            else
            {
                if($by == "ID") $arSqlOrder[$by] = CIBlock::_Order("BE.ID", $order, "desc", false);
                elseif($by == "NAME") $arSqlOrder[$by] = CIBlock::_Order("BE.NAME", $order, "desc", false);
                elseif($by == "STATUS") $arSqlOrder[$by] = CIBlock::_Order("BE.WF_STATUS_ID", $order, "desc");
                elseif($by == "XML_ID") $arSqlOrder[$by] = CIBlock::_Order("BE.XML_ID", $order, "desc");
                elseif($by == "CODE") $arSqlOrder[$by] = CIBlock::_Order("BE.CODE", $order, "desc");
                elseif($by == "TAGS") $arSqlOrder[$by] = CIBlock::_Order("BE.TAGS", $order, "desc");
                elseif($by == "TIMESTAMP_X") $arSqlOrder[$by] = CIBlock::_Order("BE.TIMESTAMP_X", $order, "desc");
                elseif($by == "CREATED") $arSqlOrder[$by] = CIBlock::_Order("BE.DATE_CREATE", $order, "desc");
                elseif($by == "CREATED_DATE") $arSqlOrder[$by] = CIBlock::_Order($DB->DateFormatToDB("YYYY.MM.DD", "BE.DATE_CREATE"), $order, "desc");
                elseif($by == "IBLOCK_ID") $arSqlOrder[$by] = CIBlock::_Order("BE.IBLOCK_ID", $order, "desc");
                elseif($by == "MODIFIED_BY") $arSqlOrder[$by] = CIBlock::_Order("BE.MODIFIED_BY", $order, "desc");
                elseif($by == "CREATED_BY") $arSqlOrder[$by] = CIBlock::_Order("BE.CREATED_BY", $order, "desc");
                elseif($by == "ACTIVE") $arSqlOrder[$by] = CIBlock::_Order("BE.ACTIVE", $order, "desc");
                elseif($by == "ACTIVE_FROM") $arSqlOrder[$by] = CIBlock::_Order("BE.ACTIVE_FROM", $order, "desc");
                elseif($by == "ACTIVE_TO") $arSqlOrder[$by] = CIBlock::_Order("BE.ACTIVE_TO", $order, "desc");
                elseif($by == "SORT") $arSqlOrder[$by] = CIBlock::_Order("BE.SORT", $order, "desc");
                elseif($by == "IBLOCK_SECTION_ID") $arSqlOrder[$by] = CIBlock::_Order("BE.IBLOCK_SECTION_ID", $order, "desc");
                elseif($by == "SHOW_COUNTER") $arSqlOrder[$by] = CIBlock::_Order("BE.SHOW_COUNTER", $order, "desc");
                elseif($by == "SHOW_COUNTER_START") $arSqlOrder[$by] = CIBlock::_Order("BE.SHOW_COUNTER_START", $order, "desc");
                elseif($by == "RAND") $arSqlOrder[$by] = CIBlockElement::GetRandFunction(true);
                elseif($by == "SHOWS") $arSqlOrder[$by] = CIBlock::_Order(CIBlockElement::GetShowedFunction(), $order, "desc", false);
                elseif($by == "HAS_PREVIEW_PICTURE") $arSqlOrder[$by] = CIBlock::_Order(CIBlock::_NotEmpty("BE.PREVIEW_PICTURE"), $order, "desc", false);
                elseif($by == "HAS_DETAIL_PICTURE") $arSqlOrder[$by] = CIBlock::_Order(CIBlock::_NotEmpty("BE.DETAIL_PICTURE"), $order, "desc", false);
                elseif($by == "RATING_TOTAL_VALUE")
                {
                    $arSqlOrder[$by] = CIBlock::_Order("RV.TOTAL_VALUE", $order, "desc");
                    $arJoinProps["RV"] = true;
                }
                elseif($by == "CNT")
                {
                    if(is_array($arGroupBy) && count($arGroupBy) > 0)
                        $arSqlOrder[$by] = " CNT ".$order." ";
                }
                elseif(substr($by, 0, 9) == "PROPERTY_")
                {
                    $propID = strtoupper(substr($by_orig, 9));
                    if(preg_match("/^([^.]+)\\.([^.]+)$/", $propID, $arMatch))
                    {
                        $db_prop = CIBlockProperty::GetPropertyArray($arMatch[1], CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"]));
                        if(is_array($db_prop) && $db_prop["PROPERTY_TYPE"] == "E")
                            CIBlockElement::MkPropertyOrder($arMatch, $order, false, $db_prop, $arJoinProps, $arSqlOrder);
                    }
                    else
                    {
                        if($db_prop = CIBlockProperty::GetPropertyArray($propID, CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"])))
                            CIBlockElement::MkPropertyOrder($by, $order, false, $db_prop, $arJoinProps, $arSqlOrder);
                    }
                }
                elseif(substr($by, 0, 13) == "PROPERTYSORT_")
                {
                    $propID = strtoupper(substr($by_orig, 13));
                    if(preg_match("/^([^.]+)\\.([^.]+)$/", $propID, $arMatch))
                    {
                        $db_prop = CIBlockProperty::GetPropertyArray($arMatch[1], CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"]));
                        if(is_array($db_prop) && $db_prop["PROPERTY_TYPE"] == "E")
                            CIBlockElement::MkPropertyOrder($arMatch, $order, true, $db_prop, $arJoinProps, $arSqlOrder);
                    }
                    else
                    {
                        if($db_prop = CIBlockProperty::GetPropertyArray($propID, CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"])))
                            CIBlockElement::MkPropertyOrder($by, $order, true, $db_prop, $arJoinProps, $arSqlOrder);
                    }
                }
                else
                {
                    $by = "ID";
                    if(!array_key_exists($by, $arSqlOrder))
                        $arSqlOrder[$by] = CIBlock::_Order("BE.ID", $order, "desc");
                }

                //Check if have to add select field in order to correctly sort
                if(is_array($arSqlOrder[$by]))
                {
                    if(is_array($arGroupBy) && count($arGroupBy)>0)
                        $arGroupBy[] = $arSqlOrder[$by][1];
                    else
                        $arSelectFields[] = $arSqlOrder[$by][1];
                    //                        COLUMN ALIAS         COLUMN EXPRESSION
                    $arIblockElementFields[$arSqlOrder[$by][1]] = $arSqlOrder[$by][0];
                    //                  ORDER EXPRESSION
                    $arSqlOrder[$by] = $arSqlOrder[$by][2];
                }
            }

            //Add order by fields to the select list
            //in order to avoid sql errors
            if(is_array($arGroupBy) && count($arGroupBy)>0)
            {
                if ($by == "STATUS")
                {
                    $arGroupBy[] = "WF_STATUS_ID";
                }
                elseif ($by == "CREATED")
                {
                    $arGroupBy[] = "DATE_CREATE";
                }
                elseif ($by == "SHOWS")
                {
                    $arGroupBy[] = "SHOW_COUNTER";
                    $arGroupBy[] = "SHOW_COUNTER_START_X";
                }
                else
                {
                    $arGroupBy[] = $by;
                }
            }
            else
            {
                if ($by == "STATUS")
                {
                    $arSelectFields[] = "WF_STATUS_ID";
                }
                elseif ($by == "CREATED")
                {
                    $arSelectFields[] = "DATE_CREATE";
                }
                elseif ($by == "SHOWS")
                {
                    $arSelectFields[] = "SHOW_COUNTER";
                    $arSelectFields[] = "SHOW_COUNTER_START_X";
                }
                else
                {
                    $arSelectFields[] = $by;
                }
            }
        }

        //*************************GROUP BY PART****************************
        $sGroupBy = "";
        if(is_array($arGroupBy) && count($arGroupBy)>0)
        {
            $oldSelectFields = $arSelectFields;
            $arSelectFields = $arGroupBy;
            $bWasGroup = true;
            foreach($arSelectFields as $key=>$val)
            {
                $val = strtoupper($val);
                if(array_key_exists($val, $arIblockElementFields))
                {
                    $sGroupBy.=",".preg_replace("/(\s+AS\s+[A-Z_]+)/i", "", $arIblockElementFields[$val]);
                }
                elseif(substr($val, 0, 9) == "PROPERTY_")
                {
                    $PR_ID = strtoupper(substr($val, 9));
                    if($db_prop = CIBlockProperty::GetPropertyArray($PR_ID, CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"])))
                        $sGroupBy .= CIBlockElement::MkPropertyGroup($db_prop, $arJoinProps);
                }
                elseif(substr($val, 0, 13) == "PROPERTYSORT_")
                {
                    $PR_ID = strtoupper(substr($val, 13));
                    if($db_prop = CIBlockProperty::GetPropertyArray($PR_ID, CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"])))
                        $sGroupBy .= CIBlockElement::MkPropertyGroup($db_prop, $arJoinProps, true);
                }
            }
            if($sGroupBy!="")
                $sGroupBy = " GROUP BY ".substr($sGroupBy, 1)." ";
            $arSelectFields = $oldSelectFields;
        }

        //*************************SELECT PART****************************
        $arAddSelectFields = Array();
        if($this->bOnlyCount)
        {
            $sSelect = "COUNT(%%_DISTINCT_%% BE.ID) as CNT ";
        }
        else
        {
            $sSelect = "";
            $arDisplayedColumns = array();
            $bStar = false;
            foreach($arSelectFields as $key=>$val)
            {
                $val = strtoupper($val);
                if(array_key_exists($val, $arIblockElementFields))
                {
                    if(isset($arDisplayedColumns[$val]))
                        continue;
                    $arDisplayedColumns[$val] = true;
                    $arSelectFields[$key] = $val;
                    $sSelect.=",".$arIblockElementFields[$val]." as ".$val;
                }
                elseif($val == "PROPERTY_*" && !$bWasGroup)
                {
                    //We have to analyze arFilter IBLOCK_ID and IBLOCK_CODE
                    //in a way to be shure we will get properties of the ONE IBLOCK ONLY!
                    $arPropertyFilter = array(
                      "ACTIVE"=>"Y",
                      "VERSION"=>2,
                    );
                    if(array_key_exists("IBLOCK_ID", $arFilter))
                    {
                        if(is_array($arFilter["IBLOCK_ID"]) && count($arFilter["IBLOCK_ID"])==1)
                            $arPropertyFilter["IBLOCK_ID"] = $arFilter["IBLOCK_ID"][0];
                        elseif(!is_array($arFilter["IBLOCK_ID"]) && intval($arFilter["IBLOCK_ID"])>0)
                            $arPropertyFilter["IBLOCK_ID"] = $arFilter["IBLOCK_ID"];
                    }
                    if(!array_key_exists("IBLOCK_ID", $arPropertyFilter))
                    {
                        if(array_key_exists("IBLOCK_CODE", $arFilter))
                        {
                            if(is_array($arFilter["IBLOCK_CODE"]) && count($arFilter["IBLOCK_CODE"])==1)
                                $arPropertyFilter["IBLOCK_CODE"] = $arFilter["IBLOCK_CODE"][0];
                            elseif(!is_array($arFilter["IBLOCK_CODE"]) && strlen($arFilter["IBLOCK_CODE"])>0)
                                $arPropertyFilter["IBLOCK_CODE"] = $arFilter["IBLOCK_CODE"];
                            else
                                continue;
                        }
                        else
                            continue;
                    }

                    $rs_prop = CIBlockProperty::GetList(array("sort"=>"asc"), $arPropertyFilter);
                    while($db_prop = $rs_prop->Fetch())
                        $this->arIBlockAllProps[]=$db_prop;
                    $iblock_id = false;
                    foreach($this->arIBlockAllProps as $db_prop)
                    {
                        if($db_prop["USER_TYPE"]!="")
                        {
                            $arUserType = CIBlockProperty::GetUserType($db_prop["USER_TYPE"]);
                            if(array_key_exists("ConvertFromDB", $arUserType))
                                $this->arIBlockConvProps["PROPERTY_".$db_prop["ID"]] = array(
                                  "ConvertFromDB"=>$arUserType["ConvertFromDB"],
                                  "PROPERTY"=>$db_prop,
                                );
                        }
                        $db_prop["ORIG_ID"] = $db_prop["ID"];
                        if($db_prop["MULTIPLE"]=="Y")
                            $this->arIBlockMultProps["PROPERTY_".$db_prop["ID"]] = $db_prop;
                        $iblock_id = $db_prop["IBLOCK_ID"];
                    }
                    if($iblock_id!==false)
                    {
                        if(!array_key_exists($iblock_id, $arJoinProps["FPS"]))
                            $arJoinProps["FPS"][$iblock_id] = count($arJoinProps["FPS"]);
                        $iPropCnt = $arJoinProps["FPS"][$iblock_id];

                        $sSelect .= ", FPS".$iPropCnt.".*";
                    }
                }
                elseif(substr($val, 0, 9) == "PROPERTY_")
                {
                    $PR_ID = strtoupper($val);
                    if(isset($arDisplayedColumns[$PR_ID]))
                        continue;
                    $arDisplayedColumns[$PR_ID] = true;
                    $PR_ID = substr($PR_ID, 9);

                    if(preg_match("/^([^.]+)\\.([^.]+)$/", $PR_ID, $arMatch))
                    {
                        $db_prop = CIBlockProperty::GetPropertyArray($arMatch[1], CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"]));
                        if(is_array($db_prop) && $db_prop["PROPERTY_TYPE"] == "E")
                            $this->MkPropertySelect($arMatch, $db_prop, $arJoinProps, $bWasGroup, $sGroupBy, $sSelect);
                    }
                    else
                    {
                        if($db_prop = CIBlockProperty::GetPropertyArray($PR_ID, CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"])))
                            $this->MkPropertySelect($PR_ID, $db_prop, $arJoinProps, $bWasGroup, $sGroupBy, $sSelect);
                    }
                }
                elseif(substr($val, 0, 13) == "PROPERTYSORT_")
                {
                    $PR_ID = strtoupper($val);
                    if(isset($arDisplayedColumns[$PR_ID]))
                        continue;
                    $arDisplayedColumns[$PR_ID] = true;
                    $PR_ID = substr($PR_ID, 13);

                    if(preg_match("/^([^.]+)\\.([^.]+)$/", $PR_ID, $arMatch))
                    {
                        $db_prop = CIBlockProperty::GetPropertyArray($arMatch[1], CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"]));
                        if(is_array($db_prop) && $db_prop["PROPERTY_TYPE"] == "E")
                            $this->MkPropertySelect($arMatch, $db_prop, $arJoinProps, $bWasGroup, $sGroupBy, $sSelect, true);
                    }
                    else
                    {
                        if($db_prop = CIBlockProperty::GetPropertyArray($PR_ID, CIBlock::_MergeIBArrays($arFilter["IBLOCK_ID"], $arFilter["IBLOCK_CODE"])))
                            $this->MkPropertySelect($PR_ID, $db_prop, $arJoinProps, $bWasGroup, $sGroupBy, $sSelect, true);
                    }
                }
                elseif($val == "*")
                {
                    $bStar = true;
                }
                elseif ($catalogIncluded && \CProductQueryBuilder::isValidField($val))
                {
                    $arAddSelectFields[] = $val;
                }
                elseif(
                  $val == "RATING_TOTAL_VALUE"
                  || $val == "RATING_TOTAL_VOTES"
                  || $val == "RATING_TOTAL_POSITIVE_VOTES"
                  || $val == "RATING_TOTAL_NEGATIVE_VOTES"
                )
                {
                    if(isset($arDisplayedColumns[$val]))
                        continue;
                    $arDisplayedColumns[$val] = true;
                    $arSelectFields[$key] = $val;
                    $sSelect.=",".preg_replace("/^RATING_/", "RV.", $val)." as ".$val;
                    $arJoinProps["RV"] = true;
                }
                elseif($val == "RATING_USER_VOTE_VALUE")
                {
                    if(isset($arDisplayedColumns[$val]))
                        continue;
                    $arDisplayedColumns[$val] = true;
                    $arSelectFields[$key] = $val;
                    if(isset($USER) && is_object($USER))
                    {
                        $sSelect.=",".$DB->IsNull('RVU.VALUE', '0')." as ".$val;
                        $arJoinProps["RVU"] = true;
                    }
                    else
                    {
                        $sSelect.=",0 as ".$val;
                    }
                }
            }

            if($bStar)
            {
                foreach($arIblockElementFields as $key=>$val)
                {
                    if(isset($arDisplayedColumns[$key]))
                        continue;
                    $arDisplayedColumns[$key] = true;
                    $arSelectFields[]=$key;
                    $sSelect.=",".$val." as ".$key;
                }
            }
            else
            {
                //Try to add missing fields for correct URL translation (only then no grouping)
                if(isset($arDisplayedColumns["DETAIL_PAGE_URL"]))
                    $arAddFields = array("LANG_DIR", "ID", "CODE", "EXTERNAL_ID", "IBLOCK_SECTION_ID", "IBLOCK_TYPE_ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_EXTERNAL_ID", "LID");
                elseif(isset($arDisplayedColumns["CANONICAL_PAGE_URL"]))
                    $arAddFields = array("LANG_DIR", "ID", "CODE", "EXTERNAL_ID", "IBLOCK_SECTION_ID", "IBLOCK_TYPE_ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_EXTERNAL_ID", "LID");
                elseif(isset($arDisplayedColumns["SECTION_PAGE_URL"]))
                    $arAddFields = array("LANG_DIR", "ID", "CODE", "EXTERNAL_ID", "IBLOCK_SECTION_ID", "IBLOCK_TYPE_ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_EXTERNAL_ID", "LID");
                elseif(isset($arDisplayedColumns["LIST_PAGE_URL"]))
                    $arAddFields = array("LANG_DIR", "IBLOCK_TYPE_ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_EXTERNAL_ID", "LID");
                else
                    $arAddFields = array();

                //Try to add missing fields for correct PREVIEW and DETAIL text formatting
                if(isset($arDisplayedColumns["DETAIL_TEXT"]))
                    $arAddFields[] = "DETAIL_TEXT_TYPE";
                if(isset($arDisplayedColumns["PREVIEW_TEXT"]))
                    $arAddFields[] = "PREVIEW_TEXT_TYPE";

                foreach($arAddFields as $key)
                {
                    if(isset($arDisplayedColumns[$key]))
                        continue;
                    $arDisplayedColumns[$key] = true;
                    $arSelectFields[]=$key;
                    $sSelect.=",".$arIblockElementFields[$key]." as ".$key;
                }
            }

            if($sGroupBy!="")
                $sSelect = substr($sSelect, 1).", COUNT(%%_DISTINCT_%% BE.ID) as CNT ";
            elseif($sSelect !== '')
                $sSelect = "%%_DISTINCT_%% ".substr($sSelect, 1)." ";
        }

        //*********************WHERE PART*********************
        $arAddWhereFields = Array();
        if(is_array($arFilter) && isset($arFilter["CATALOG"]))
        {
            $arAddWhereFields = $arFilter["CATALOG"];
            unset($arFilter["CATALOG"]);
        }

        $arSqlSearch = self::MkFilter($arFilter, $arJoinProps, $arAddWhereFields);
        $this->bDistinct = false;
        $sSectionWhere = "";

        $sWhere = "";
        foreach ($arSqlSearch as $condition)
            if (trim($condition, "\n\t") !== '')
                $sWhere .= "\n\t\t\tAND (".$condition.")";
    }

    function MkPropertyFilter($res, $cOperationType, $propVAL, $db_prop, &$arJoinProps, &$arSqlSearch)
    {
        global $DB;

        if($res["OPERATION"]!="E")
            $cOperationType = $res["OPERATION"];

        //Tables counters
        if($db_prop["VERSION"] == 2 && $db_prop["MULTIPLE"]=="N")
        {
            if(!array_key_exists($db_prop["IBLOCK_ID"], $arJoinProps["FPS"]))
                $iPropCnt = count($arJoinProps["FPS"]);
            else
                $iPropCnt = $arJoinProps["FPS"][$db_prop["IBLOCK_ID"]];
        }
        else
        {
            if(!array_key_exists($db_prop["ID"], $arJoinProps["FPV"]))
                $iPropCnt = count($arJoinProps["FPV"]);
            else
                $iPropCnt = $arJoinProps["FPV"][$db_prop["ID"]]["CNT"];
        }

        if(!is_array($res["FIELD"]) && (substr(strtoupper($res["FIELD"]), -6) == '_VALUE'))
            $bValueEnum = true;
        else
            $bValueEnum = false;

        if($db_prop["PROPERTY_TYPE"] == "L" && $bValueEnum)
        {
            if(!array_key_exists($db_prop["ID"], $arJoinProps["FPEN"]))
                $iFpenCnt = count($arJoinProps["FPEN"]);
            else
                $iFpenCnt = $arJoinProps["FPEN"][$db_prop["ID"]]["CNT"];
        }
        else
        {
            $iFpenCnt = false;
        }

        if(is_array($res["FIELD"]))
        {
            if(!array_key_exists($db_prop["ID"], $arJoinProps["BE"]))
                $iElCnt = count($arJoinProps["BE"]);
            else
                $iElCnt = $arJoinProps["BE"][$db_prop["ID"]]["CNT"];
        }
        else
        {
            $iElCnt = false;
        }

        $bFullJoin = false;
        $r = "";

        if(is_array($res["FIELD"]))
        {
            switch($res["FIELD"][2]."")
            {
                case "ACTIVE":
                case "DETAIL_TEXT_TYPE":
                case "PREVIEW_TEXT_TYPE":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".".$res["FIELD"][2], $propVAL, "string_equal", $bFullJoinTmp, $cOperationType);
                    break;
                case "EXTERNAL_ID":
                    $res["FIELD"][2] = "XML_ID";
                case "NAME":
                case "XML_ID":
                case "TMP_ID":
                case "DETAIL_TEXT":
                case "SEARCHABLE_CONTENT":
                case "PREVIEW_TEXT":
                case "CODE":
                case "TAGS":
                case "WF_COMMENTS":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".".$res["FIELD"][2], $propVAL, "string", $bFullJoinTmp, $cOperationType);
                    break;
                case "ID":
                case "SHOW_COUNTER":
                case "WF_PARENT_ELEMENT_ID":
                case "WF_STATUS_ID":
                case "SORT":
                case "CREATED_BY":
                case "MODIFIED_BY":
                case "PREVIEW_PICTURE":
                case "DETAIL_PICTURE":
                case "IBLOCK_ID":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".".$res["FIELD"][2], $propVAL, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "TIMESTAMP_X":
                case "DATE_CREATE":
                case "SHOW_COUNTER_START":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".".$res["FIELD"][2], $propVAL, "date", $bFullJoinTmp, $cOperationType);
                    break;
                case "DATE_ACTIVE_FROM":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".ACTIVE_FROM", $propVAL, "date", $bFullJoinTmp, $cOperationType);
                    break;
                case "DATE_ACTIVE_TO":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".ACTIVE_TO", $propVAL, "date", $bFullJoinTmp, $cOperationType);
                    break;
                case "ACTIVE_FROM":
                    if(strlen($propVAL)>0)
                        $r = "(BE".$iElCnt.".ACTIVE_FROM ".($cOperationType=="N"?"<":">=").$DB->CharToDateFunction($DB->ForSql($propVAL), "FULL").($cOperationType=="N"?"":" OR BE".$iElCnt.".ACTIVE_FROM IS NULL").")";
                    break;
                case "ACTIVE_TO":
                    if(strlen($propVAL)>0)
                        $r = "(BE".$iElCnt.".ACTIVE_TO ".($cOperationType=="N"?">":"<=").$DB->CharToDateFunction($DB->ForSql($propVAL), "FULL").($cOperationType=="N"?"":" OR BE".$iElCnt.".ACTIVE_TO IS NULL").")";
                    break;
                case "ACTIVE_DATE":
                    if(strlen($propVAL)>0)
                        $r = ($cOperationType=="N"?" NOT":"")."((BE".$iElCnt.".ACTIVE_TO >= ".$DB->GetNowFunction()." OR BE".$iElCnt.".ACTIVE_TO IS NULL) AND (BE".$iElCnt.".ACTIVE_FROM <= ".$DB->GetNowFunction()." OR BE".$iElCnt.".ACTIVE_FROM IS NULL))";
                    break;
                case "DATE_MODIFY_FROM":
                    if(strlen($propVAL)>0)
                        $r = "(BE".$iElCnt.".TIMESTAMP_X ".
                          ( $cOperationType=="N" ? "<" : ">=" ).$DB->CharToDateFunction($DB->ForSql($propVAL), "FULL").
                          ( $cOperationType=="N" ? ""  : " OR BE".$iElCnt.".TIMESTAMP_X IS NULL").")";
                    break;
                case "DATE_MODIFY_TO":
                    if(strlen($propVAL)>0)
                        $r = "(BE".$iElCnt.".TIMESTAMP_X ".
                          ( $cOperationType=="N" ? ">" : "<=" ).$DB->CharToDateFunction($DB->ForSql($propVAL), "FULL").
                          ( $cOperationType=="N" ? ""  : " OR BE".$iElCnt.".TIMESTAMP_X IS NULL").")";
                    break;
                case "MODIFIED_USER_ID":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".MODIFIED_BY", $propVAL, "number", $bFullJoinTmp, $cOperationType);
                    break;
                case "CREATED_USER_ID":
                    $r = CIBlock::FilterCreateEx("BE".$iElCnt.".CREATED_BY", $propVAL, "number", $bFullJoinTmp, $cOperationType);
                    break;
            }
        }
        else
        {
            if (is_object($propVAL) && ($db_prop["PROPERTY_TYPE"]=="G" || $db_prop["PROPERTY_TYPE"]=="E")) {
                // SUBQUERY CHANGE
                if($db_prop["VERSION"]==2 && $db_prop["MULTIPLE"]=="N")
                    $fn = "FPS".$iPropCnt.".PROPERTY_".$db_prop["ORIG_ID"];
                else
                    $fn = "FPV".$iPropCnt.".VALUE";
                /** @var CIBlockElement $propVAL */
                $propVAL->prepareSql(array($propVAL->strField), $propVAL->arFilter, false, false);
                $r = $fn.(substr($cOperationType, 0, 1) == "N"? ' NOT': '').' IN  (
						SELECT '.$propVAL->sSelect.'
						FROM '.$propVAL->sFrom.'
						WHERE 1=1
							'.$propVAL->sWhere.'
						)'
                ;

            } else {
                if(!is_array($propVAL))
                    $propVAL = array($propVAL);

                if($db_prop["PROPERTY_TYPE"]=="L")
                {
                    if($bValueEnum)
                        $r = CIBlock::FilterCreateEx("FPEN".$iFpenCnt.".VALUE", $propVAL, "string", $bFullJoin, $cOperationType);
                    elseif($db_prop["VERSION"]==2 && $db_prop["MULTIPLE"]=="N")
                        $r = CIBlock::FilterCreateEx("FPS".$iPropCnt.".PROPERTY_".$db_prop["ORIG_ID"], $propVAL, "number", $bFullJoin, $cOperationType);
                    else
                        $r = CIBlock::FilterCreateEx("FPV".$iPropCnt.".VALUE_ENUM", $propVAL, "number", $bFullJoin, $cOperationType);
                }
                elseif($db_prop["PROPERTY_TYPE"]=="N" || $db_prop["PROPERTY_TYPE"]=="G" || $db_prop["PROPERTY_TYPE"]=="E")
                {
                    if($db_prop["VERSION"]==2 && $db_prop["MULTIPLE"]=="N")
                        $r = CIBlock::FilterCreateEx("FPS".$iPropCnt.".PROPERTY_".$db_prop["ORIG_ID"], $propVAL, "number", $bFullJoin, $cOperationType);
                    else
                        $r = CIBlock::FilterCreateEx("FPV".$iPropCnt.".VALUE_NUM", $propVAL, "number", $bFullJoin, $cOperationType);
                }
                else
                {
                    if($db_prop["VERSION"]==2 && $db_prop["MULTIPLE"]=="N")
                        $r = CIBlock::FilterCreateEx("FPS".$iPropCnt.".PROPERTY_".$db_prop["ORIG_ID"], $propVAL, "string", $bFullJoin, $cOperationType);
                    else
                        $r = CIBlock::FilterCreateEx("FPV".$iPropCnt.".VALUE", $propVAL, "string", $bFullJoin, $cOperationType);
                }
            }
        }

        if(strlen($r) > 0)
        {
            if($db_prop["VERSION"] == 2 && $db_prop["MULTIPLE"]=="N")
            {
                if(!array_key_exists($db_prop["IBLOCK_ID"], $arJoinProps["FPS"]))
                    $arJoinProps["FPS"][$db_prop["IBLOCK_ID"]] = $iPropCnt;
            }
            else
            {
                if(!array_key_exists($db_prop["ID"], $arJoinProps["FP"]))
                    $arJoinProps["FP"][$db_prop["ID"]] = array(
                      "CNT" => count($arJoinProps["FP"]),
                      "bFullJoin" => false,
                    );

                if($res["LEFT_JOIN"])
                    $arJoinProps["FP"][$db_prop["ID"]]["bFullJoin"] &= $bFullJoin;
                else
                    $arJoinProps["FP"][$db_prop["ID"]]["bFullJoin"] |= $bFullJoin;

                if(!array_key_exists($db_prop["ID"], $arJoinProps["FPV"]))
                    $arJoinProps["FPV"][$db_prop["ID"]] = array(
                      "CNT" => $iPropCnt,
                      "IBLOCK_ID" => $db_prop["IBLOCK_ID"],
                      "MULTIPLE" => $db_prop["MULTIPLE"],
                      "VERSION" => $db_prop["VERSION"],
                      "JOIN" => $arJoinProps["FP"][$db_prop["ID"]]["CNT"],
                      "bFullJoin" => false,
                    );

                if($res["LEFT_JOIN"])
                    $arJoinProps["FPV"][$db_prop["ID"]]["bFullJoin"] &= $bFullJoin;
                else
                    $arJoinProps["FPV"][$db_prop["ID"]]["bFullJoin"] |= $bFullJoin;
            }

            if($db_prop["PROPERTY_TYPE"]=="L" && $bValueEnum)
            {
                if(!array_key_exists($db_prop["ID"], $arJoinProps["FPEN"]))
                    $arJoinProps["FPEN"][$db_prop["ID"]] = array(
                      "CNT" => $iFpenCnt,
                      "MULTIPLE" => $db_prop["MULTIPLE"],
                      "VERSION" => $db_prop["VERSION"],
                      "ORIG_ID" => $db_prop["ORIG_ID"],
                      "JOIN" => $iPropCnt,
                      "bFullJoin" => false,
                    );

                if($res["LEFT_JOIN"])
                    $arJoinProps["FPEN"][$db_prop["ID"]]["bFullJoin"] &= $bFullJoin;
                else
                    $arJoinProps["FPEN"][$db_prop["ID"]]["bFullJoin"] |= $bFullJoin;
            }

            if(is_array($res["FIELD"]))
            {
                if(!array_key_exists($db_prop["ID"], $arJoinProps["BE"]))
                    $arJoinProps["BE"][$db_prop["ID"]] = array(
                      "CNT" => $iElCnt,
                      "MULTIPLE" => $db_prop["MULTIPLE"],
                      "VERSION" => $db_prop["VERSION"],
                      "ORIG_ID" => $db_prop["ORIG_ID"],
                      "JOIN" => $iPropCnt,
                      "bJoinIBlock" => false,
                      "bJoinSection" => false,
                    );
            }

            $arSqlSearch[] = $r;
        }
    }

    function prepareSql($arSelectFields=array(), $arFilter=array(), $arGroupBy=false, $arOrder=array("SORT"=>"ASC"))
    {
        global $DB, $USER;
        $MAX_LOCK = intval(COption::GetOptionString("workflow","MAX_LOCK_TIME","60"));
        $uid = is_object($USER)? intval($USER->GetID()): 0;

        $formatActiveDates = CPageOption::GetOptionString("iblock", "FORMAT_ACTIVE_DATES", "-") != "-";
        $shortFormatActiveDates = CPageOption::GetOptionString("iblock", "FORMAT_ACTIVE_DATES", "SHORT");

        $arIblockElementFields = array(
          "ID"=>"BE.ID",
          "TIMESTAMP_X"=>$DB->DateToCharFunction("BE.TIMESTAMP_X"),
          "TIMESTAMP_X_UNIX"=>'UNIX_TIMESTAMP(BE.TIMESTAMP_X)',
          "MODIFIED_BY"=>"BE.MODIFIED_BY",
          "DATE_CREATE"=>$DB->DateToCharFunction("BE.DATE_CREATE"),
          "DATE_CREATE_UNIX"=>'UNIX_TIMESTAMP(BE.DATE_CREATE)',
          "CREATED_BY"=>"BE.CREATED_BY",
          "IBLOCK_ID"=>"BE.IBLOCK_ID",
          "IBLOCK_SECTION_ID"=>"BE.IBLOCK_SECTION_ID",
          "ACTIVE"=>"BE.ACTIVE",
          "ACTIVE_FROM"=>(
          $formatActiveDates
            ?
            $DB->DateToCharFunction("BE.ACTIVE_FROM", $shortFormatActiveDates)
            :
            "IF(EXTRACT(HOUR_SECOND FROM BE.ACTIVE_FROM)>0, ".$DB->DateToCharFunction("BE.ACTIVE_FROM", "FULL").", ".$DB->DateToCharFunction("BE.ACTIVE_FROM", "SHORT").")"
          ),
          "ACTIVE_TO"=>(
          $formatActiveDates
            ?
            $DB->DateToCharFunction("BE.ACTIVE_TO", $shortFormatActiveDates)
            :
            "IF(EXTRACT(HOUR_SECOND FROM BE.ACTIVE_TO)>0, ".$DB->DateToCharFunction("BE.ACTIVE_TO", "FULL").", ".$DB->DateToCharFunction("BE.ACTIVE_TO", "SHORT").")"
          ),
          "DATE_ACTIVE_FROM"=>(
          $formatActiveDates
            ?
            $DB->DateToCharFunction("BE.ACTIVE_FROM", $shortFormatActiveDates)
            :
            "IF(EXTRACT(HOUR_SECOND FROM BE.ACTIVE_FROM)>0, ".$DB->DateToCharFunction("BE.ACTIVE_FROM", "FULL").", ".$DB->DateToCharFunction("BE.ACTIVE_FROM", "SHORT").")"
          ),
          "DATE_ACTIVE_TO"=>(
          $formatActiveDates
            ?
            $DB->DateToCharFunction("BE.ACTIVE_TO", $shortFormatActiveDates)
            :
            "IF(EXTRACT(HOUR_SECOND FROM BE.ACTIVE_TO)>0, ".$DB->DateToCharFunction("BE.ACTIVE_TO", "FULL").", ".$DB->DateToCharFunction("BE.ACTIVE_TO", "SHORT").")"
          ),
          "SORT"=>"BE.SORT",
          "NAME"=>"BE.NAME",
          "PREVIEW_PICTURE"=>"BE.PREVIEW_PICTURE",
          "PREVIEW_TEXT"=>"BE.PREVIEW_TEXT",
          "PREVIEW_TEXT_TYPE"=>"BE.PREVIEW_TEXT_TYPE",
          "DETAIL_PICTURE"=>"BE.DETAIL_PICTURE",
          "DETAIL_TEXT"=>"BE.DETAIL_TEXT",
          "DETAIL_TEXT_TYPE"=>"BE.DETAIL_TEXT_TYPE",
          "SEARCHABLE_CONTENT"=>"BE.SEARCHABLE_CONTENT",
          "WF_STATUS_ID"=>"BE.WF_STATUS_ID",
          "WF_PARENT_ELEMENT_ID"=>"BE.WF_PARENT_ELEMENT_ID",
          "WF_LAST_HISTORY_ID"=>"BE.WF_LAST_HISTORY_ID",
          "WF_NEW"=>"BE.WF_NEW",
          "LOCK_STATUS"=>"if (BE.WF_DATE_LOCK is null, 'green', if(DATE_ADD(BE.WF_DATE_LOCK, interval ".$MAX_LOCK." MINUTE)<now(), 'green', if(BE.WF_LOCKED_BY=".$uid.", 'yellow', 'red')))",
          "WF_LOCKED_BY"=>"BE.WF_LOCKED_BY",
          "WF_DATE_LOCK"=>$DB->DateToCharFunction("BE.WF_DATE_LOCK"),
          "WF_COMMENTS"=>"BE.WF_COMMENTS",
          "IN_SECTIONS"=>"BE.IN_SECTIONS",
          "SHOW_COUNTER"=>"BE.SHOW_COUNTER",
          "SHOW_COUNTER_START"=>$DB->DateToCharFunction("BE.SHOW_COUNTER_START"),
          "SHOW_COUNTER_START_X"=>"BE.SHOW_COUNTER_START",
          "CODE"=>"BE.CODE",
          "TAGS"=>"BE.TAGS",
          "XML_ID"=>"BE.XML_ID",
          "EXTERNAL_ID"=>"BE.XML_ID",
          "TMP_ID"=>"BE.TMP_ID",
          "USER_NAME"=>"concat('(',U.LOGIN,') ',ifnull(U.NAME,''),' ',ifnull(U.LAST_NAME,''))",
          "LOCKED_USER_NAME"=>"concat('(',UL.LOGIN,') ',ifnull(UL.NAME,''),' ',ifnull(UL.LAST_NAME,''))",
          "CREATED_USER_NAME"=>"concat('(',UC.LOGIN,') ',ifnull(UC.NAME,''),' ',ifnull(UC.LAST_NAME,''))",
          "LANG_DIR"=>"L.DIR",
          "LID"=>"B.LID",
          "IBLOCK_TYPE_ID"=>"B.IBLOCK_TYPE_ID",
          "IBLOCK_CODE"=>"B.CODE",
          "IBLOCK_NAME"=>"B.NAME",
          "IBLOCK_EXTERNAL_ID"=>"B.XML_ID",
          "DETAIL_PAGE_URL"=>"B.DETAIL_PAGE_URL",
          "LIST_PAGE_URL"=>"B.LIST_PAGE_URL",
          "CANONICAL_PAGE_URL"=>"B.CANONICAL_PAGE_URL",
          "CREATED_DATE"=>$DB->DateFormatToDB("YYYY.MM.DD", "BE.DATE_CREATE"),
          "BP_PUBLISHED"=>"if(BE.WF_STATUS_ID = 1, 'Y', 'N')",
        );
        unset($shortFormatActiveDates);
        unset($formatActiveDates);

        $this->bDistinct = false;

        $this->PrepareGetList(
          $arIblockElementFields,
          $arJoinProps,

          $arSelectFields,
          $sSelect,
          $arAddSelectFields,

          $arFilter,
          $sWhere,
          $sSectionWhere,
          $arAddWhereFields,

          $arGroupBy,
          $sGroupBy,

          $arOrder,
          $arSqlOrder,
          $arAddOrderByFields
        );

        $this->arFilterIBlocks = isset($arFilter["IBLOCK_ID"])? array($arFilter["IBLOCK_ID"]): array();
        //******************FROM PART********************************************
        $sFrom = "";
        foreach($arJoinProps["FPS"] as $iblock_id => $iPropCnt)
        {
            $sFrom .= "\t\t\tINNER JOIN b_iblock_element_prop_s".$iblock_id." FPS".$iPropCnt." ON FPS".$iPropCnt.".IBLOCK_ELEMENT_ID = BE.ID\n";
            $this->arFilterIBlocks[$iblock_id] = $iblock_id;
        }

        foreach($arJoinProps["FP"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];

            if($db_prop["bFullJoin"])
                $sFrom .= "\t\t\tINNER JOIN b_iblock_property FP".$i." ON FP".$i.".IBLOCK_ID = B.ID AND ".
                  (
                  intval($propID)>0?
                    " FP".$i.".ID=".intval($propID)."\n":
                    " FP".$i.".CODE='".$DB->ForSQL($propID, 200)."'\n"
                  );
            else
                $sFrom .= "\t\t\tLEFT JOIN b_iblock_property FP".$i." ON FP".$i.".IBLOCK_ID = B.ID AND ".
                  (
                  intval($propID)>0?
                    " FP".$i.".ID=".intval($propID)."\n":
                    " FP".$i.".CODE='".$DB->ForSQL($propID, 200)."'\n"
                  );

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["FPV"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];

            if($db_prop["MULTIPLE"]=="Y")
                $this->bDistinct = true;

            if($db_prop["VERSION"]==2)
                $strTable = "b_iblock_element_prop_m".$db_prop["IBLOCK_ID"];
            else
                $strTable = "b_iblock_element_property";

            if($db_prop["bFullJoin"])
                $sFrom .= "\t\t\tINNER JOIN ".$strTable." FPV".$i." ON FPV".$i.".IBLOCK_PROPERTY_ID = FP".$db_prop["JOIN"].".ID AND FPV".$i.".IBLOCK_ELEMENT_ID = BE.ID\n";
            else
                $sFrom .= "\t\t\tLEFT JOIN ".$strTable." FPV".$i." ON FPV".$i.".IBLOCK_PROPERTY_ID = FP".$db_prop["JOIN"].".ID AND FPV".$i.".IBLOCK_ELEMENT_ID = BE.ID\n";

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["FPEN"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];

            if($db_prop["VERSION"] == 2 && $db_prop["MULTIPLE"] == "N")
            {
                if($db_prop["bFullJoin"])
                    $sFrom .= "\t\t\tINNER JOIN b_iblock_property_enum FPEN".$i." ON FPEN".$i.".PROPERTY_ID = ".$db_prop["ORIG_ID"]." AND FPS".$db_prop["JOIN"].".PROPERTY_".$db_prop["ORIG_ID"]." = FPEN".$i.".ID\n";
                else
                    $sFrom .= "\t\t\tLEFT JOIN b_iblock_property_enum FPEN".$i." ON FPEN".$i.".PROPERTY_ID = ".$db_prop["ORIG_ID"]." AND FPS".$db_prop["JOIN"].".PROPERTY_".$db_prop["ORIG_ID"]." = FPEN".$i.".ID\n";
            }
            else
            {
                if($db_prop["bFullJoin"])
                    $sFrom .= "\t\t\tINNER JOIN b_iblock_property_enum FPEN".$i." ON FPEN".$i.".PROPERTY_ID = FPV".$db_prop["JOIN"].".IBLOCK_PROPERTY_ID AND FPV".$db_prop["JOIN"].".VALUE_ENUM = FPEN".$i.".ID\n";
                else
                    $sFrom .= "\t\t\tLEFT JOIN b_iblock_property_enum FPEN".$i." ON FPEN".$i.".PROPERTY_ID = FPV".$db_prop["JOIN"].".IBLOCK_PROPERTY_ID AND FPV".$db_prop["JOIN"].".VALUE_ENUM = FPEN".$i.".ID\n";
            }

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["BE"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];

            $sFrom .= "\t\t\tLEFT JOIN b_iblock_element BE".$i." ON BE".$i.".ID = ".
              (
              $db_prop["VERSION"]==2 && $db_prop["MULTIPLE"]=="N"?
                "FPS".$db_prop["JOIN"].".PROPERTY_".$db_prop["ORIG_ID"]
                :"FPV".$db_prop["JOIN"].".VALUE_NUM"
              ).
              (
              $arFilter["SHOW_HISTORY"] != "Y"?
                " AND ((BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL)".($arFilter["SHOW_NEW"]=="Y"? " OR BE.WF_NEW='Y'": "").")":
                ""
              )."\n";

            if($db_prop["bJoinIBlock"])
                $sFrom .= "\t\t\tLEFT JOIN b_iblock B".$i." ON B".$i.".ID = BE".$i.".IBLOCK_ID\n";

            if($db_prop["bJoinSection"])
                $sFrom .= "\t\t\tLEFT JOIN b_iblock_section BS".$i." ON BS".$i.".ID = BE".$i.".IBLOCK_SECTION_ID\n";

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["BE_FPS"] as $iblock_id => $db_prop)
        {
            list($iblock_id, $link) = explode("~", $iblock_id, 2);
            $sFrom .= "\t\t\tLEFT JOIN b_iblock_element_prop_s".$iblock_id." JFPS".$db_prop["CNT"]." ON JFPS".$db_prop["CNT"].".IBLOCK_ELEMENT_ID = BE".$db_prop["JOIN"].".ID\n";

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["BE_FP"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];
            list($propID, $link) = explode("~", $propID, 2);

            if($db_prop["bFullJoin"])
                $sFrom .= "\t\t\tINNER JOIN b_iblock_property JFP".$i." ON JFP".$i.".IBLOCK_ID = BE".$db_prop["JOIN"].".IBLOCK_ID AND ".
                  (
                  intval($propID)>0?
                    " JFP".$i.".ID=".intval($propID)."\n":
                    " JFP".$i.".CODE='".$DB->ForSQL($propID, 200)."'\n"
                  );
            else
                $sFrom .= "\t\t\tLEFT JOIN b_iblock_property JFP".$i." ON JFP".$i.".IBLOCK_ID = BE".$db_prop["JOIN"].".IBLOCK_ID AND ".
                  (
                  intval($propID)>0?
                    " JFP".$i.".ID=".intval($propID)."\n":
                    " JFP".$i.".CODE='".$DB->ForSQL($propID, 200)."'\n"
                  );

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["BE_FPV"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];
            list($propID, $link) = explode("~", $propID, 2);

            if($db_prop["MULTIPLE"]=="Y")
                $this->bDistinct = true;

            if($db_prop["VERSION"]==2)
                $strTable = "b_iblock_element_prop_m".$db_prop["IBLOCK_ID"];
            else
                $strTable = "b_iblock_element_property";

            if($db_prop["bFullJoin"])
                $sFrom .= "\t\t\tINNER JOIN ".$strTable." JFPV".$i." ON JFPV".$i.".IBLOCK_PROPERTY_ID = JFP".$db_prop["JOIN"].".ID AND JFPV".$i.".IBLOCK_ELEMENT_ID = BE".$db_prop["BE_JOIN"].".ID\n";
            else
                $sFrom .= "\t\t\tLEFT JOIN ".$strTable." JFPV".$i." ON JFPV".$i.".IBLOCK_PROPERTY_ID = JFP".$db_prop["JOIN"].".ID AND JFPV".$i.".IBLOCK_ELEMENT_ID = BE".$db_prop["BE_JOIN"].".ID\n";

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        foreach($arJoinProps["BE_FPEN"] as $propID => $db_prop)
        {
            $i = $db_prop["CNT"];
            list($propID, $link) = explode("~", $propID, 2);

            if($db_prop["VERSION"] == 2 && $db_prop["MULTIPLE"] == "N")
            {
                if($db_prop["bFullJoin"])
                    $sFrom .= "\t\t\tINNER JOIN b_iblock_property_enum JFPEN".$i." ON JFPEN".$i.".PROPERTY_ID = ".$db_prop["ORIG_ID"]." AND JFPS".$db_prop["JOIN"].".PROPERTY_".$db_prop["ORIG_ID"]." = JFPEN".$i.".ID\n";
                else
                    $sFrom .= "\t\t\tLEFT JOIN b_iblock_property_enum JFPEN".$i." ON JFPEN".$i.".PROPERTY_ID = ".$db_prop["ORIG_ID"]." AND JFPS".$db_prop["JOIN"].".PROPERTY_".$db_prop["ORIG_ID"]." = JFPEN".$i.".ID\n";
            }
            else
            {
                if($db_prop["bFullJoin"])
                    $sFrom .= "\t\t\tINNER JOIN b_iblock_property_enum JFPEN".$i." ON JFPEN".$i.".PROPERTY_ID = JFPV".$db_prop["JOIN"].".IBLOCK_PROPERTY_ID AND JFPV".$db_prop["JOIN"].".VALUE_ENUM = JFPEN".$i.".ID\n";
                else
                    $sFrom .= "\t\t\tLEFT JOIN b_iblock_property_enum JFPEN".$i." ON JFPEN".$i.".PROPERTY_ID = JFPV".$db_prop["JOIN"].".IBLOCK_PROPERTY_ID AND JFPV".$db_prop["JOIN"].".VALUE_ENUM = JFPEN".$i.".ID\n";
            }

            if($db_prop["IBLOCK_ID"])
                $this->arFilterIBlocks[$db_prop["IBLOCK_ID"]] = $db_prop["IBLOCK_ID"];
        }

        if(strlen($arJoinProps["BES"]))
        {
            $sFrom .= "\t\t\t".$arJoinProps["BES"]."\n";
        }

        if(strlen($arJoinProps["FC"]))
        {
            $sFrom .= "\t\t\t".$arJoinProps["FC"]."\n";
            $this->bDistinct = $this->bDistinct || (isset($arJoinProps["FC_DISTINCT"]) && $arJoinProps["FC_DISTINCT"] == "Y");
        }

        if($arJoinProps["RV"])
            $sFrom .= "\t\t\tLEFT JOIN b_rating_voting RV ON RV.ENTITY_TYPE_ID = 'IBLOCK_ELEMENT' AND RV.ENTITY_ID = BE.ID\n";
        if($arJoinProps["RVU"])
            $sFrom .= "\t\t\tLEFT JOIN b_rating_vote RVU ON RVU.ENTITY_TYPE_ID = 'IBLOCK_ELEMENT' AND RVU.ENTITY_ID = BE.ID AND RVU.USER_ID = ".$uid."\n";
        if($arJoinProps["RVV"])
            $sFrom .= "\t\t\t".($arJoinProps["RVV"]["bFullJoin"]? "INNER": "LEFT")." JOIN b_rating_vote RVV ON RVV.ENTITY_TYPE_ID = 'IBLOCK_ELEMENT' AND RVV.ENTITY_ID = BE.ID\n";

        //******************END OF FROM PART********************************************

        $this->bCatalogSort = false;
        if(!empty($arAddSelectFields) || !empty($arAddWhereFields) || !empty($arAddOrderByFields))
        {
            if (Loader::includeModule("catalog"))
            {
                $catalogQueryResult = \CProductQueryBuilder::makeQuery(array(
                  'select' => $arAddSelectFields,
                  'filter' => $arAddWhereFields,
                  'order' => $arAddOrderByFields
                ));

                if (!empty($catalogQueryResult))
                {
                    if (
                      !empty($catalogQueryResult['select'])
                      && !$this->bOnlyCount
                      && !isset($this->strField)
                    )
                    {
                        $sSelect .= ', '.implode(', ', $catalogQueryResult['select']).' ';
                    }
                    // filter set in CIBlockElement::MkFilter
                    if (!empty($catalogQueryResult['order']))
                    {
                        $this->bCatalogSort = true;
                        foreach ($catalogQueryResult['order'] as $index => $field)
                            $arSqlOrder[$index] = $field;
                        unset($field);
                        unset($index);
                    }
                    if (!empty($catalogQueryResult['join']))
                    {
                        $sFrom .= "\n\t\t\t".implode("\n\t\t\t", $catalogQueryResult['join'])."\n";
                    }
                }
                unset($catalogQueryResult);
            }
        }

        $i = array_search("CREATED_BY_FORMATTED", $arSelectFields);
        if ($i !== false)
        {
            if (
              $sSelect
              && $sGroupBy==""
              && !$this->bOnlyCount
              && !isset($this->strField)
            )
            {
                $sSelect .= ",UC.NAME UC_NAME, UC.LAST_NAME UC_LAST_NAME, UC.SECOND_NAME UC_SECOND_NAME, UC.EMAIL UC_EMAIL, UC.ID UC_ID, UC.LOGIN UC_LOGIN";
            }
            else
            {
                unset($arSelectFields[$i]);
            }
        }

        $sOrderBy = "";
        foreach($arSqlOrder as $i=>$val)
        {
            if(strlen($val))
            {
                if($sOrderBy=="")
                    $sOrderBy = " ORDER BY ";
                else
                    $sOrderBy .= ",";

                $sOrderBy .= $val." ";
            }
        }

        $sSelect = trim($sSelect, ", \t\n\r");
        if(strlen($sSelect) <= 0)
            $sSelect = "0 as NOP ";

        $this->bDistinct = $this->bDistinct || (isset($arFilter["INCLUDE_SUBSECTIONS"]) && $arFilter["INCLUDE_SUBSECTIONS"] == "Y");

        if($this->bDistinct)
            $sSelect = str_replace("%%_DISTINCT_%%", "DISTINCT", $sSelect);
        else
            $sSelect = str_replace("%%_DISTINCT_%%", "", $sSelect);

        $sFrom = "
			b_iblock B
			INNER JOIN b_lang L ON B.LID=L.LID
			INNER JOIN b_iblock_element BE ON BE.IBLOCK_ID = B.ID
			".ltrim($sFrom, "\t\n")
          .(in_array("USER_NAME", $arSelectFields)? "\t\t\tLEFT JOIN b_user U ON U.ID=BE.MODIFIED_BY\n": "")
          .(in_array("LOCKED_USER_NAME", $arSelectFields)? "\t\t\tLEFT JOIN b_user UL ON UL.ID=BE.WF_LOCKED_BY\n": "")
          .(in_array("CREATED_USER_NAME", $arSelectFields) || in_array("CREATED_BY_FORMATTED", $arSelectFields)? "\t\t\tLEFT JOIN b_user UC ON UC.ID=BE.CREATED_BY\n": "")."
		";


        $this->sSelect = $sSelect;
        $this->sFrom = $sFrom;
        $this->sWhere = $sWhere;
        $this->sGroupBy = $sGroupBy;
        $this->sOrderBy = $sOrderBy;
    }
}
