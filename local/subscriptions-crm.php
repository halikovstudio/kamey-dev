<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 **/

use Bitrix\Sale\Order;

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = "N";
$APPLICATION->ShowIncludeStat = false;
$placement = $_REQUEST['PLACEMENT'];
$options = json_decode($_REQUEST['PLACEMENT_OPTIONS'], true);
$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
if ($request->get('DOMAIN') != 'corp.ufa.kamey.ru') {
    die('Access denied');
}

?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
    <link rel="stylesheet" href="/local/templates/cameo-v3/css/v4/index.css?v2">
    <link rel="stylesheet" href="/local/templates/cameo-v3/css/v4/personal-area.css?v2">
<?
$userId = null;
switch ($placement) {
    case 'CRM_DEAL_DETAIL_TAB':
        $orderId = B24Sync::getOrderId($options['ID']);
        CModule::IncludeModule('sale');
        try {
            $order = Order::load($orderId);
            $userId = $order->getUserId();
        } catch (Exception $e) {

        }
        break;
    default:
        die('Неизвестное место подключения');
}
if (!$userId) {
    die('Не удалось определить идентификатор пользователя');
}

?>
    <div class="ui container">
        <? $APPLICATION->IncludeFile("/personal-area/new/parts/subscriptions.php",['userId'=>$userId]); ?>
    </div>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
