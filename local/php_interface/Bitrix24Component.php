<?php

use Bitrix\Main;
use Bitrix\Main\Web\HttpClient;

class Bitrix24Component
{
    public $host;
    public $client_id;
    public $client_secret;
    public $application_uri;
    public $access_token;
    public $refresh_token;
    public $config_path;
    protected $request;
    protected $response;
    protected $reactivation = false;

    public function __construct($config = [])
    {
        if (!empty($config)) {
            $this->host = $config['host'] ?: 'https://corp.ufa.kamey.ru/';
            $this->client_id = $config['client_id'] ?: 'local.5db136a2aa02c5.12296202';
            $this->client_secret = $config['client_secret'] ?: 'kv5im1vJ8mhN1su5agh6EAz10IJN0H71cwzteu9SMCeN0LqPAO';
            $this->application_uri = $config['application_uri'];
            $this->access_token = $config['access_token'];
            $this->refresh_token = $config['refresh_token'];
            $this->config_path = $config['config_path'] ?: $_SERVER['DOCUMENT_ROOT'] . '/edit-order/bitrix24.json';
        }
    }

    public function Send($action, $data = [])
    {
        $this->SendRequest($action, $data);

        return $this->response;
    }

    protected function SendRequest($action, $data = [])
    {
        $data['auth'] = $this->access_token;
        $result = new Main\Result();
        $httpClient = $this->getHttpClient();
        $this->request = $httpClient->post($this->host . 'rest/' . $action . '/', $data);
        $errors = $httpClient->getError();
        $status = $httpClient->getStatus();
        $this->response = json_decode($this->request, true);

        if (!$this->response || !empty($errors) || $status != 200) {
            if (!$this->reactivation) {
                $this->reactivation = true;
                if ($this->UpdateAuthToken()) {
                    $this->SendRequest($action, $data);
                }
            }
        }
    }

    protected static function getHttpClient()
    {
        return new HttpClient(array(
          "version" => "1.1",
          "socketTimeout" => 5,
          "streamTimeout" => 5,
          "redirect" => true,
          "redirectMax" => 5,
        ));
    }

    protected function UpdateAuthToken()
    {
        $url = 'https://oauth.bitrix.info/oauth/token/'
          . '?grant_type=refresh_token'
          . '&client_id=' . $this->client_id
          . '&client_secret=' . $this->client_secret
          . '&redirect_uri=' . $this->application_uri
          . '&refresh_token=' . $this->refresh_token;

        $httpClient = $this->getHttpClient();
        $request = $httpClient->get($url);
        $response = json_decode($request, true);

        if (!$request && !is_array($response)) {
            throw new HttpException('');
        }

        return $this->WriteToConfig($response);
    }
//
//
//
//    protected function AddAuth()
//    {
//        $this->request->addData(['auth' => $this->access_token]);
//    }

//    protected function ThrowBitrixException($response = false)
//    {
//        if ( ! $response) {
//            $response = $this->response;
//        }
//        throw new HttpException(
//            $response->getHeaders()->get('http-code'),
//            YII_DEBUG ? $response->content['error_description'] : ''
//        );
//    }

    protected function WriteToConfig($data)
    {
        $this->refresh_token = $data['refresh_token'];
        $this->access_token = $data['access_token'];
        $config = file_get_contents($this->config_path);
        $config = json_decode($config, true);
        $config['refresh_token'] = $data['refresh_token'];
        $config['access_token'] = $data['access_token'];
        $config_string = json_encode($config);
        if ($config['refresh_token'] && $config['access_token']) {
            return file_put_contents($this->config_path, $config_string);
        } else {
            return true;
        }
    }
}
