<?
$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('sale', 'onSaleDeliveryTrackingClassNamesBuildList', 'addCustomDeliveryTracking');

function addCustomDeliveryTracking(\Bitrix\Main\Event $event)
{
    $result = new \Bitrix\Main\EventResult(
      \Bitrix\Main\EventResult::SUCCESS,
      array(
        '\Bitrix\Sale\Delivery\Tracking\Cdek' => '/local/php_interface/delivery/tracking/cdek.php'
      )
    );

    return $result;
}
