<?

namespace Bitrix\Sale\Delivery\Tracking;

use Bitrix\Main\Error;
use mrkody\cdek\CdekSdk;

class Cdek extends Base
{
    /**
     * Returns class name for administration interface
     * @return string
     */
    public function getClassTitle()
    {
        return 'СДЭК';
    }

    /**
     * Returns class description for administration interface
     * @return string
     */
    public function getClassDescription()
    {
        return 'Система трэкинга на основе АПИ службы доставки СДЭК';
    }

    public function getStatus($trackingNumber)
    {
        $result = new StatusResult();
        /** @var \safin\delivery\cdek $service */
        $service = $this->deliveryService;
        if ($service instanceof \safin\delivery\cdek) {
            require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/vendor/autoload.php";
            $sdk = new CdekSdk($service->getDataValue('account'), $service->getDataValue('secure_password'));
            $response = $sdk->statusReport(0, null, null, [
              [
                'DispatchNumber' => $trackingNumber
              ]
            ]);
            $status = $response->result['StatusReport']['Order']['Status']['@attributes'];
            if (!$status) {
                $result->addError(new Error($response->result['StatusReport']['Order']['@attributes']['Msg']));
            } else {
                $result->description = $status['Description'];
                $result->lastChangeTimestamp = strtotime($status['Date']);
                switch ($status['Code']) {
                    case 4:
                        $result->status = Statuses::HANDED;
                        break;
                    case 5:
                        $result->status = Statuses::RETURNED;
                        break;
                    case 2:
                        $result->status = Statuses::PROBLEM;
                        break;
                    case 1:
                    case 3:
                    case 6:
                    case 16:
                        $result->status = Statuses::WAITING_SHIPMENT;
                        break;
                    case 10:
                    case 11:
                    case 12:
                    case 9:
                    case 18:
                        $result->status = Statuses::ARRIVED;
                        break;
                    case 7:
                    case 21:
                    case 22:
                    case 13:
                    case 17:
                    case 19:
                    case 20:
                    case 8:
                        $result->status = Statuses::ON_THE_WAY;
                        break;
                }
            }
        }

        return $result;
    }

    /**
     * Returns params structure
     * @return array
     */
    public function getParamsStructure()
    {
        return array();
    }
}
