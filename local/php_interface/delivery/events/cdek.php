<?php

use Bitrix\Main\ArgumentOutOfRangeException;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\EventResult;
use Bitrix\Main\NotSupportedException;
use Bitrix\Sale\Helpers\Admin\OrderEdit;
use Bitrix\Sale\PropertyValue;
use Bitrix\Sale\PropertyValueCollection;
use Bitrix\Sale\ResultError;
use Bitrix\Sale\Shipment;
use Bitrix\Sale\ShipmentCollection;
use mrkody\cdek\CdekSdk;

$eventManager = EventManager::getInstance();
$eventManager->addEventHandler(
  'sale',
  'OnBeforeSaleShipmentSetField',
  'sendOrderToCdek'
);

function sendOrderToCdek(Event $event, $test = false)
{
    $value = $event->getParameter("VALUE");
    /** @var Shipment $shipment */
    $shipment = $event->getParameter("ENTITY");
    /** @var ShipmentCollection $shipmentCollection */
    $shipmentCollection = $shipment->getCollection();
    $order = $shipmentCollection->getOrder();
    if ($event->getParameter("NAME") == "ALLOW_DELIVERY") {
        try {
            $shipment->setFieldNoDemand("DEDUCTED", $value);
        } catch (ArgumentOutOfRangeException $e) {
        }
    }
    if (($event->getParameter("NAME") == "ALLOW_DELIVERY" && $value == "Y" && !$shipment->getField("TRACKING_NUMBER")) || $test) {
        /** @var \safin\delivery\cdek $delivery */
        $delivery = $shipment->getDelivery();
        if ($delivery instanceof \safin\delivery\cdek) {
            require $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/vendor/autoload.php";
            require $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/CdekSdk.php";

            $basket = $order->getBasket();
            $locationCode = $order->getPropertyCollection()->getDeliveryLocation()->getValue();
            $rsData = \CSaleLocation::GetList(array(), array('CODE' => $locationCode), false, false,
              array("ID"))->Fetch();
            $locationId = $rsData['ID'];
            $zip = \CSaleLocation::GetLocationZIP($locationId)->Fetch()["ZIP"];

            $cdek = new CdekSdk($delivery->getDataValue("account"), $delivery->getDataValue("secure_password"));

            $address = array();
            $cdekServices = array();
            $packages = array();
            $discountsList = OrderEdit::getDiscountsApplyResult($order, false);
            if (isset($discountsList["PRICES"]["DELIVERY"]["DISCOUNT"])) {
                $deliveryDiscount = $discountsList["PRICES"]["DELIVERY"]["DISCOUNT"];
            } else {
                $deliveryDiscount = 0;
            }
            $price = $order->getDeliveryPrice() + $deliveryDiscount;
            $payed = $order->getField('PAYED') == 'Y';

            $extraServicesMap = array();

            $deliveryExtraServices = new \Bitrix\Sale\Delivery\ExtraServices\Manager($delivery->getId(),
              $delivery->getCurrency());

            foreach ($deliveryExtraServices->getItems() as $key => $extraService) {
                /** @var Bitrix\Sale\Delivery\ExtraServices\Checkbox $extraService */
                $extraServicesMap[$key] = $extraService->getCode();
            }

            $fitting = false;
            $extraServices = $shipment->getExtraServices();
            foreach ($extraServicesMap as $id => $mapValue) {
                $value = $extraServices[$id];
                if (!$value || $value === 'N') {
                    continue;
                }
                switch ($mapValue) {
                    case "pvz":
                        $address["PvzCode"] = $value;
                        break;
                    case "insurance":

                        break;
                    case "30":
                        $fitting = true;
                        $cdekServices[] = array(
                          "ServiceCode" => 36
                        );
                        $cdekServices[] = array(
                          "ServiceCode" => 37
                        );
                        $cdekServices[] = array(
                          "ServiceCode" => $extraServicesMap[$id]
                        );
                        break;
                    default:

                        $cdekServices[] = array(
                          "ServiceCode" => $extraServicesMap[$id]
                        );
                        break;
                }
            }

            $useDeliveryLine = $fitting;
            if ($useDeliveryLine && !$payed) {
                $comment = "Ув. клиент, в случае факт.покупки товаров менее, чем на 3000(три тысячи) руб. или ОТКАЗА от выкупа, стоимость доставки, примерки и страховки ТК СДЭК - ОПЛАЧИВАЕТ КЛИЕНТ ($price рублей)!";
            } else {
                $comment = "";
            }

            if ($fitting) {
                $comment = "Примерка разрешена. " . $comment;
            } else {
                $comment = "Примерка и осмотр товара запрещен. " . $comment;
                logAdd("without_fitting", $extraServicesMap);
            }

            if ($order->getField("COMMENTS")) {
                $comment .= " " . $order->getField("COMMENTS");
            }

            $setZip = $order->getPropertyCollection()->getDeliveryLocationZip() ? $order->getPropertyCollection()->getDeliveryLocationZip()->getValue() : $zip;
            $setCityId = null;
            if (!$setZip) {
                $setZip = null;
                $item = CSaleLocation::GetByID($locationId, "RU");
                $setCityId = \safin\delivery\cdek::findCityId($item['CITY_NAME']);
            }

            $setDeliveryPrice = $shipment->getPrice();

            if ($useDeliveryLine && $order->getBasket()->getPrice() < 3000) {
                $setDeliveryPrice = 0;
            }

            if ($payed) {
                $setDeliveryPrice = 0;
            }

            $name = $order->getPropertyCollection()->getPayerName()->getValue();
            $email = $order->getPropertyCollection()->getUserEmail()->getValue();
            $phone = $order->getPropertyCollection()->getPhone() ? $order->getPropertyCollection()->getPhone()->getValue() : "";

            $notMeName = getPropertyByCode($order->getPropertyCollection(), "not_me_fio");
            $notMePhone = getPropertyByCode($order->getPropertyCollection(), "not_me_phone");
            $notMeEmail = getPropertyByCode($order->getPropertyCollection(), "not_me_email");

            if ($notMeName && $notMeName->getValue()) {
                if ($notMeName) {
                    $name = $notMeName->getValue();
                }
                if ($notMePhone) {
                    $phone = $notMePhone->getValue();
                }
                if ($notMeEmail) {
                    $email = $notMeEmail->getValue();
                }
            }

            $cdekOrder = array(
              "Number" => $order->getField("ACCOUNT_NUMBER"),
              "SendCityCode" => 256,
              "RecCityPostCode" => $setZip,
              "RecCityCode" => $setCityId,
              "RecipientName" => $name,
              "RecipientEmail" => $email,
              "Phone" => $phone,
              "TariffTypeCode" => $delivery->getDataValue("tariff"),
              "DeliveryRecipientCost" => $setDeliveryPrice,
              "DeliveryRecipientVATRate" => "VAT18",
              "DeliveryRecipientVATSum" => round($setDeliveryPrice / 1.18 * 0.18, 2),
              "RecipientCurrency" => "RUB",
              "ItemsCurrency" => "RUB",
              "SellerName" => "Камея",
              "Comment" => $comment,
              "Address" => &$address,
              "Packages" => &$packages,
              "AddServices" => &$cdekServices
            );

            if (!$address['PvzCode']) {
                $street = getPropertyByCode($order->getPropertyCollection(), "street");
                $house = getPropertyByCode($order->getPropertyCollection(), "house");
                $flat = getPropertyByCode($order->getPropertyCollection(), "flat");
                $corps = getPropertyByCode($order->getPropertyCollection(), "corps");
                if ($street) {
                    $address['Street'] = $street->getValue();
                }
                if ($house) {
                    $address['House'] = $house->getValue();
                }
                if ($corps && $corps->getValue()) {
                    $address['House'] .= ", к. " . $corps->getValue();
                }
                if ($flat) {
                    $address['Flat'] = $flat->getValue();
                }
            }
            $temp = array();
            $bonusPayment = BonusCard::getPayment($order->getPaymentCollection(), false);
            $bonus_withdraw = $bonusPayment ? $bonusPayment->getSum() : 0;
            $bonusPerPosition = 0;
            if ($bonus_withdraw > 0) {
                $quantity = 0;
                foreach ($basket->getBasketItems() as $basketItem) {
                    /**
                     * @var \Bitrix\Sale\BasketItem $basketItem
                     */
                    $quantity += $basketItem->getQuantity();
                }
                $bonusPerPosition = $bonus_withdraw / $quantity;
            }
            foreach ($basket->getBasketItems() as $basketItem) {
                /**
                 * @var \Bitrix\Sale\BasketItem $basketItem
                 */
                $count = $basketItem->getQuantity();
                while ($count > 0) {
                    $temp[] = $basketItem;
                    $count--;
                }
            }

            $temp = array_chunk($temp, $delivery->getPackageValue("count"));
            $combineList = array();
            CModule::IncludeModule('iblock');
            $totalChunk = count($temp) - 1;
            foreach ($temp as $k => $chunk) {
                $normalized = array();
                $weight = 0;
                foreach ($chunk as $item) {
                    /**
                     * @var \Bitrix\Sale\BasketItem $item
                     */
                    //$article = $item->getPropertyCollection()->getPropertyValues()['article']['VALUE'];
                    $itemWeight = (float)$item->getWeight();
                    if (!$itemWeight) {
                        $itemWeight = (float)$delivery->getSizeValue("weight");
                    }
                    if (!$normalized[$item->getId()]) {
                        $props = $item->getPropertyCollection()->getPropertyValues();
                        $productId = $item->getField("PRODUCT_ID");
                        $arItem = CIBlockElement::GetList(array(), array("ID" => $productId), false, false,
                          array("NAME"))->Fetch();
                        $itemPrice = round($item->getPrice() - $bonusPerPosition, 2);
                        $normalized[$item->getId()] = array(
                          "WareKey" => $item->getId(),
                          "Amount" => 0,
                          "Cost" => $itemPrice,
                          "Payment" => $payed ? 0 : $itemPrice,
                          "PaymentVATRate" => "VAT10",
                          "PaymentVATSum" => $payed ? 0 : round($itemPrice / 1.1 * 0.1, 2),
                          "Weight" => $itemWeight,
                          "Comment" => $arItem['NAME']
                        );
                    }
                    $weight += $itemWeight;
                    $normalized[$item->getId()]['Amount']++;
                }
                if ($k == $totalChunk && $useDeliveryLine && !$payed) {
                    $normalized['delivery'] = array(
                      "WareKey" => 999999,
                      "Amount" => 1,
                      "Cost" => 0,
                      "Payment" => $price,
                      "PaymentVATRate" => "VAT20",
                      "PaymentVATSum" => round($price / 1.20 * 0.20, 2),
                      "Weight" => 0,
                      "Comment" => "Обязательная доставка в случае фактической покупки товаров менее, чем на 3000 рублей"
                    );
                }
                $combineList[] = array(
                  'weight' => $weight,
                  "items" => $normalized
                );
            }
            foreach ($combineList as $key => $package) {
                $packages[] = array(
                  "Number" => $key + 1,
                  "BarCode" => $key + 1,
                  "Weight" => $package['weight'],
                  "SizeA" => $delivery->getPackageValue("length") / 10,
                  "SizeB" => $delivery->getPackageValue("width") / 10,
                  "SizeC" => $delivery->getPackageValue("height") / 10,
                  "Items" => $package['items']
                );
            }
            if ($test) {
                echo "<pre>";
                print_r($cdekOrder);
                echo "</pre>";
                die();
            }

            $today = new \Bitrix\Main\Type\DateTime();
            $date = $order->getDateInsert()->format('Y-m-d h:i:s');
            if ($today->format('Y-m-d') != $order->getDateInsert()->format('Y-m-d')) {
                $date = $today->format('Y-m-d h:i:s');
            }

            $result = $cdek->deliveryRequest($shipment->getField("ACCOUNT_NUMBER"), $date, array($cdekOrder));
            $errors = array();
            foreach ($result->result['response']['Order'] as $item) {
                if ($item['@attributes']['ErrorCode']) {
                    $errors[] = $item['@attributes']['Msg'];
                }
            }
            if (!empty($errors)) {
                logAdd("CDEK_SEND_ERROR", $errors, $order->getId());

                //logAdd("CDEK_ORDER_INFO", $cdekOrder, $order->getId());
                return new EventResult(
                  EventResult::ERROR,
                  new ResultError(implode(", ", $errors), 'SALE_EVENT_WRONG_ORDER'),
                  'sale'
                );
            } else {
                $dispatchNumber = $result->result['response']['Order'][0]['@attributes']['DispatchNumber'];
                try {
                    $shipment->setField("TRACKING_NUMBER", $dispatchNumber);
                    $shipment->setField("COMMENTS",
                      "https://www.kamey.ru/include/cdek_doc.php?order=" . $order->getId());
                    logAdd("successSend", "Накладная успешно создана: " . $dispatchNumber, $order->getId());
                } catch (NotSupportedException $e) {
                    logAdd("CDEK_SEND_ERROR", $e->getMessage(), $order->getId());
                }
            }
        }
    } elseif ($event->getParameter("NAME") == "ALLOW_DELIVERY" && $value == "N" && $shipment->getField("TRACKING_NUMBER")) {
        /** @var Shipment $shipment */
        $shipment = $event->getParameter("ENTITY");
        /** @var \safin\delivery\cdek $delivery */
        $delivery = $shipment->getDelivery();
        if ($delivery instanceof \safin\delivery\cdek) {
            require $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/vendor/autoload.php";
            require $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/CdekSdk.php";
            /** @var ShipmentCollection $shipmentCollection */
            $shipmentCollection = $shipment->getCollection();
            $order = $shipmentCollection->getOrder();
            if ($order->getField("ACCOUNT_NUMBER")) {
                $cdek = new CdekSdk($delivery->getDataValue("account"), $delivery->getDataValue("secure_password"));
                try {
                    $result = $cdek->deleteRequest($order->getField("ACCOUNT_NUMBER"), array(
                      array(
                        "Number" => $order->getField("ACCOUNT_NUMBER")
                      )
                    ));
                    $shipment->setField("TRACKING_NUMBER", "");
                    $shipment->setField("COMMENTS", "");
                } catch (Exception $e) {
                }
            }
        }
    }
}

/**
 * @param PropertyValueCollection $propertyCollection
 * @param $code
 *
 * @return PropertyValue|null
 */
function getPropertyByCode($propertyCollection, $code)
{
    foreach ($propertyCollection as $property) {
        if ($property->getField('CODE') == $code) {
            return $property;
        }
    }

    return null;
}
