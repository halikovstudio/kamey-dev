<?php

class CdekPvz extends Bitrix\Sale\Delivery\ExtraServices\Base
{
    var $pvz = array();
    var $selected = array();

    public function __construct($id, array $structure, $currency, $value = null, array $additionalParams = array())
    {
        $structure["PARAMS"]["ONCHANGE"] = $this->createJSOnchange($id);
        parent::__construct($id, $structure, $currency, $value, $additionalParams);
        $this->params["TYPE"] = "ENUM";
        $this->params["OPTIONS"] = array();
        $this->params["pvz"] = array();
    }

    public function getId(){
        return $this->id;
    }

    protected function createJSOnchange($id)
    {
        return "BX.onCustomEvent('onDeliveryExtraServiceValueChange', [{'id' : '" . $id . "', 'value': this.value, 'price': false}]);";
    }

    static function getAdminParamsControl($name, array $params, $currency = "")
    {
        return "";
    }

    public static function isEmbeddedOnly()
    {
        return true;
    }

    public function getClassTitle()
    {
        return "СДЭК ПВЗ";
    }

    public function getCost()
    {
        return false;
    }

    public function getAdminDefaultControl($name, $value = false)
    {
        return "";
    }

    public function getEditControl($prefix = "", $value = false)
    {
        $this->createOptions();
        global $USER;

        if (!defined("ADMIN_SECTION") && !defined("EDIT_ORDER")) {
            ob_start();
            $selected = $this->params["pvz"][$this->getValue()]['Address'];
            if (!$selected) {
                $selected = "-";
            }
            ?>
            <div class="btn btn-yellow"
                 onclick="selectPvzList(<?= $this->id ?>);return false;"><?= ($selected == "-" ? "Выбрать пункт выдачи" : $selected) ?></div>
            <?
            $html = ob_get_contents();
            ob_end_clean();

            return $html;
        }

        return parent::getEditControl($prefix, $value);
    }

    function createOptions($fullRequired = false, $debug = false)
    {
        $admin = false;
        global $USER;
        if ((defined("ADMIN_SECTION") && ADMIN_SECTION) || $fullRequired) {
            $admin = true;
            $zip = null;
        } elseif ($GLOBALS['DELIVERY_LOCATION_ZIP']) {
            $zip = $GLOBALS['DELIVERY_LOCATION_ZIP'];
            $locationTo = $zip;
        } elseif ($GLOBALS['DELIVERY_LOCATION_CODE']) {
            $zip = \Bitrix\Sale\Location\LocationTable::getByCode($GLOBALS['DELIVERY_LOCATION_CODE'])->fetch();
            $zip = \CSaleLocation::GetLocationZIP($zip['CITY_ID'])->Fetch()["ZIP"];
            $locationTo = $zip['CITY_ID'];
        } else {
            if (trim($_GET['locationId'])) {
                $locationTo = $_GET['locationId'];
            } elseif ($_SESSION['AUTO_CITY']['ID']) {
                $locationTo = $_SESSION['AUTO_CITY']['ID'];
            } elseif ($GLOBALS['DELIVERY_LOCATION']) {
                $locationTo = $GLOBALS['DELIVERY_LOCATION'];
            } elseif ($GLOBALS['DELIVERY_LOCATION_ID']) {
                $locationTo = $GLOBALS['DELIVERY_LOCATION_ID'];
            }
            if (!$locationTo) {
                return false;
            }
            $rsZip = \CSaleLocation::GetLocationZIP($locationTo);
            $zip = $rsZip->Fetch()["ZIP"];
        }
        $toId = null;
        if ($fullRequired) {
            $admin = true;
            $zip = null;
            $toId = null;
        }
        if (!$admin && !$zip) {
            $item = CSaleLocation::GetByID($locationTo, "RU");
            $toId = \safin\delivery\cdek::findCityId($item['CITY_NAME']);
        }
        global $USER;
        $obCache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_time = "86400";

        $cacheId = $zip == null && $toId == null ? "full-list" : "city." . $zip . '-' . $toId . '--5';
        if ($fullRequired) {
            $cache_time = 86400;
            $cacheId = "fullRequired";
        }
        $pvzData = array(
          "options" => array(),
          "pvz" => array()
        );
        if ($zip == "450000") {
            $zip = "450073";
        }

        if ($obCache->initCache($cache_time, $cacheId, "/cdek/pvz")) {
            $pvzData = $obCache->GetVars();
        } elseif ($obCache->startDataCache()) {
            require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/vendor/autoload.php";
            require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/delivery/cdek/sdk/CdekSdk.php";
            try {
                $cdek = new \mrkody\cdek\CdekSdk("1", "1");
                $result = $cdek->pvzList($toId, $zip);
                if (!$result->result['PvzList']['Pvz']) {
                    $obCache->abortDataCache();
                } else {
                    if ($result->result['PvzList']['Pvz']['WorkTimeY']) {
                        $result->result['PvzList']['Pvz'] = array(
                          $result->result['PvzList']['Pvz']
                        );
                    }
                    foreach ($result->result['PvzList']['Pvz'] as $item) {
                        $data = $item['@attributes'];
                        if ($data['IsDressingRoom'] != 'есть' && $data['IsDressingRoom'] != 'true') {
                            continue;
                        }
                        $pvzData['options'][$data['Code']] = "[" . $data['Code'] . '] ' . ($zip ? $data['Address'] : $data['FullAddress']);
                        $pvzData["pvz"][$data['Code']] = $data;
                    }

                    if (!empty($pvzData['pvz'])) {
                        $obCache->endDataCache($pvzData);
                    } else {
                        $obCache->abortDataCache();
                    }
                }
            } catch (Exception $e) {
                $obCache->abortDataCache();
            }
        }

        $this->params['OPTIONS'] = $pvzData['options'];
        $this->params['pvz'] = $pvzData['pvz'];
        if ($admin) {
            if (!$this->getValue()) {
                $this->params['OPTIONS']["-"] = "-";
                $this->setValue("-");
            }

            if (!$this->params['OPTIONS'][$this->getValue()]) {
                $this->params['OPTIONS']["-"] = "-";
                $this->setValue("-");
            }
        } else {
            global $version;
            if (!$this->getValue() || !$this->params['OPTIONS'][$this->getValue()]) {
                reset($this->params['OPTIONS']);
                if ($version == 3) {
                    $this->setValue(key($this->params['OPTIONS']));
                } else {
                    $this->setValue("");
                }
            }
        }

        return true;
    }

    function getFormattedValue($full = false)
    {
        return $this->params['pvz'][$this->getValue()][$full ? 'FullAddress' : 'Address'];
    }

    function getPvzList()
    {
        return $this->params['pvz'];
    }

    function getObjectValue()
    {
        return $this->params['pvz'][$this->getValue()];
    }

    public function getViewControl()
    {
        $this->createOptions();

        return parent::getViewControl();
    }
}
