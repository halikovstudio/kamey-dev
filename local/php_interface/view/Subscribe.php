<?php

namespace View;

use CModule;
use CUser;
use Bitrix\Sender\Recipient;

class Subscribe
{
  static $loaded = false;
  static $list = array();
  static $idList = array();
  static $checked = array();

  public static function show()
  {
    self::init();
    $result = array_chunk(self::$list, 2);
    ?>
      <form action="user.subscribe" class="ui form js-subscribes-validate">
          <input type="hidden" name="type" value="email">
          <div class="ui normal uppercase header">Рубрика подписок</div>
          <div class="nested-fields">
            <?
            foreach ($result as $group) {
              ?>
                <div class="four fields">
                  <?
                  foreach ($group as $item) {
                    ?>
                      <div class="field">
                          <div class="ui checkbox">
                              <input type="hidden" name="subscription[_<?= $item['ID'] ?>]" value="N">
                              <input type="checkbox" name="subscription[_<?= $item['ID'] ?>]"
                                     value="Y" <?= in_array($item['ID'], self::$checked) ? "checked" : "" ?>
                                     id="form_subscription_email_<?= $item['ID'] ?>">
                              <label for="form_subscription_email_<?= $item['ID'] ?>"
                                     class="gray-color"><?= $item['NAME'] ?></label>
                          </div>
                      </div>
                    <?
                  }
                  ?>
                </div>
              <?
            }
            ?>
          </div>
          <div class="ui success message eight wide field">Изменения сохранены</div>
          <div class="ui error message eight wide field"></div>
          <button class="ui primary button" type="submit">Сохранить</button>
      </form>
    <?
  }

  public static function init()
  {
    if (!self::$loaded) {
      self::$loaded = true;
      CModule::IncludeModule("sender");
      $email = CUser::GetParam("EMAIL");
        $subscriptionDb = \Bitrix\Sender\MailingSubscriptionTable::getSubscriptionList(array(
          'select' => array('EXISTED_MAILING_ID' => 'MAILING.ID'),
          'filter' => array(
            '=CONTACT.TYPE_ID' => Recipient\Type::EMAIL,
            '=CONTACT.CODE' => strtolower($email),
            '!MAILING.ID' => null
          ),
        ));

      $arSubscriptionRubrics = array();
      while (($subscription = $subscriptionDb->fetch())) {
        //get user's newsletter categories
        if (intval($subscription['EXISTED_MAILING_ID']) > 0)
          $arSubscriptionRubrics[] = $subscription['EXISTED_MAILING_ID'];
      }
      $arFilter = array("SITE_ID" => SITE_ID, "IS_PUBLIC" => "Y");
      $mailingList = \Bitrix\Sender\Subscription::getMailingList($arFilter);
      foreach ($mailingList as $item) {
        self::$idList[] = $item['ID'];
      }
      self::$list = $mailingList;
      self::$checked = $arSubscriptionRubrics;
    }
  }

  /**
   * @param int|array $mailingIdList
   * @param string $email
   */
  public static function unsubscribe($mailingIdList, $email = null)
  {
    if (is_null($email)) {
      $email = CUser::GetEmail();
    }
    if (!is_array($mailingIdList)) {
      $mailingIdList = array($mailingIdList);
    }
    \Bitrix\Sender\Subscription::unsubscribe(array(
      "EMAIL" => $email,
      "UNSUBSCRIBE_LIST" => $mailingIdList
    ));
  }

  public static function convert()
  {
    $rs = CUser::GetList($by, $order, array("ACTIVE" => "Y", "@UF_EMAIL_SUBSCRIBES" => array(1, 2, 3, 4)), array(
      "SELECT" => array("UF_EMAIL_SUBSCRIBES"),
      "FIELDS" => array(
        "ID", "LOGIN", "EMAIL"
      )
    ));
    $map = array(
      4 => 1,
      3 => 4,
      2 => 3,
      1 => 2
    );
    self::init();
    while ($ob = $rs->Fetch()) {
      $list = array();
      foreach ($ob['UF_EMAIL_SUBSCRIBES'] as $key) {
        $list[] = $map[$key];
      }
      self::subscribe($list, $ob['EMAIL']);
    }
  }

  /**
   * @param int|array $mailingIdList
   * @param string $email
   */
  public static function subscribe($mailingIdList, $email = null)
  {
    if (is_null($email)) {
      $email = CUser::GetEmail();
    }
    if (!is_array($mailingIdList)) {
      $mailingIdList = array($mailingIdList);
    }
    global $APPLICATION;
    \Bitrix\Sender\Subscription::add($email, $mailingIdList, SITE_ID);
    $APPLICATION->set_cookie("SENDER_SUBSCR_EMAIL", $email, time() + 60 * 60 * 24 * 30 * 12 * 10);
  }
}