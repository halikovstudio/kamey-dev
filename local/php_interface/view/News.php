<?php

class News
{
  static $loaded = false;
  static private $iblockId = 92;
  static private $list = array();
  static private $adminOnly = false;

  static function show()
  {
    Dynamic::begin("main-news");
    if (!$_SESSION['close-news'] && self::access()) {
      self::load();
      if (!empty(self::$list)) {
        ?>
          <div class="primary-news">
              <div class="ui fluid container">
                  <div class="close-news"><? icon("close") ?></div>
                  <div class="ui grid">
                    <?
                    foreach (self::$list as $item) {
                      ?>
                        <div class="column five wide widescreen eight wide computer sixteen wide tablet sixteen wide mobile">
                            <div class="inner">
                              <? if ($item['PREVIEW_PICTURE']) { ?>
                                  <div class="image fit-container">
                                      <img src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>" alt="">
                                  </div>
                              <? } ?>
                                <div class="content">
                                    <div class="title"><?= $item['NAME'] ?></div>
                                    <div class="description"><?= $item['PREVIEW_TEXT'] ?></div>
                                </div>
                            </div>
                        </div>
                      <?
                    }
                    ?>
                  </div>
              </div>
          </div>
        <?
      }
    }
    Dynamic::end(null,null,false,true);
  }

  static function access()
  {
    global $USER;
    if (self::$adminOnly && $USER->IsAdmin()) {
      return true;
    } elseif (!self::$adminOnly) {
      return true;
    }

    return false;
  }

  static function load()
  {
    if (self::$loaded) {
      return;
    }
    global $location;
    self::$list = array();
    $rs = CIBlockElement::GetList(array('SORT' => "ASC", "ACTIVE_FROM" => "DESC"), array(
      'IBLOCK_ID' => self::$iblockId,
      "ACTIVE" => "Y",
      "ACTIVE_DATE" => "Y",
      array(
        "LOGIC" => "OR",
        array(
          "PROPERTY_in_city" => $location['id'],
        ),
        array(
          "PROPERTY_in_city" => false,
        )
      )
    ), false, false, array("ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PROPERTY_in_city", "PROPERTY_not_in_city"));
    while ($result = $rs->Fetch()) {
      if (in_array($location['id'], $result['PROPERTY_NOT_IN_CITY_VALUE'])) {
        continue;
      }
      $result['PREVIEW_PICTURE'] = CFile::GetFileArray($result['PREVIEW_PICTURE']);
      self::$list[] = $result;
    }
    self::$loaded = true;
  }
}