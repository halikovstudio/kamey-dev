<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 24.10.2017
 * Time: 10:27
 */

namespace Product;

use Bitrix\Main\ArgumentNullException;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Order;
use CIBlockElement;

class Basket extends Base
{
    public static $basket = array();
    public static $item = array();
    public static $editable = true;
    public static $infoText = 'Возможность примерки доступна<br>только при заказе меньше 7 товаров';
    public static $infoTextSmall = 'Внимание!<br>
Добавлено более 6-ти товаров, примерка будет недоступна';

    /**
     * @param \Bitrix\Sale\Basket|\Bitrix\Sale\BasketItem[] $basket
     *
     * @param bool $small
     *
     * @return string
     * @throws ArgumentNullException
     */
    public static function view($basket, $small = true)
    {
        global $location;
        global $version;
        $showInfo = self::getCount($basket) > 6 && in_array($location['id'], getVipCities());
        $count = 0;
        $views = "";
        foreach ($basket->getOrderableItems() as $basketItem) {
            /** @var BasketItem $basketItem */
            $data = self::getData($basketItem->getProductId());
            $data['name'] = $basketItem->getField("NAME");
            $data['price'] = $basketItem->getPrice();
            $data['price_formatted'] = CurrencyFormat($basketItem->getPrice(), "RUB");
            $data['total'] = $basketItem->getFinalPrice();
            $data['total_formatted'] = CurrencyFormat($data['total'], 'RUB');
            $data['count'] = $basketItem->getQuantity();
            $data['basketId'] = $basketItem->getId();
            $data['productId'] = $basketItem->getProductId();
            $data['canBuy'] = $basketItem->canBuy();
            self::$basket[$basketItem->getId()] = $data;
            if ($small) {
                $views .= self::item($data);
            } else {
                $views .= self::itemMain($data);
                $count += $basketItem->getQuantity();
                if ($count > 6 && $showInfo) {
                    $showInfo = false;
                    $views .= self::info();
                }
            }
        }
        if ($showInfo) {
            $views .= self::info();
        }

        return $views;
    }

    /**
     * @param \Bitrix\Sale\Basket $basket
     * @param bool $all
     *
     * @return int
     * @throws ArgumentNullException
     */
    public static function getCount($basket, $all = false)
    {
        if ($all) {
            return array_sum($basket->getQuantityList());
        } else {
            $total = 0;
            foreach ($basket->getOrderableItems() as $orderableItem) {
                $total += $orderableItem->getQuantity();
            }

            return $total;
        }
    }

    public static function getData($productId)
    {
        \CModule::IncludeModule("iblock");
        $obCache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_time = "1800";
        $cache_id = "basketData:" . $productId;
        $data = [];
        if ($obCache->initCache($cache_time, $cache_id, "/basket/")) {
            $data = $obCache->GetVars();
        } elseif ($obCache->startDataCache()) {
            $rs = CIBlockElement::GetList(array(), array("ID" => $productId, "IBLOCK_ID" => 70), false, false, array(
              "DETAIL_PAGE_URL",
              "PROPERTY_PHOTO",
              "PROPERTY_ROST",
              "PROPERTY_RAZMER_ODEZHDA",
              "PROPERTY_COLOR.NAME",
              "PROPERTY_COLOR",
              "PROPERTY_CML2_LINK.PROPERTY_CML2_ARTICLE",
              "PROPERTY_COLOR_MAIN",
              "PROPERTY_CML2_LINK",
              "PROPERTY_CML2_LINK.PROPERTY_SALENAME"
            ))->GetNext();
            $colors = $rs['PROPERTY_COLOR_MAIN_VALUE'];
            \CModule::IncludeModule('highloadblock');
            $highLoadData = getHighloadEntity(25);
            $entityDataClass = $highLoadData['class'];
            $sTableID = $highLoadData['tableId'];
            $rsData = $entityDataClass::getList(array(
              "select" => array('UF_NAME', "UF_XML_ID", "UF_FILE"),
              "filter" => array(
                'UF_XML_ID' => $colors
              ),
                // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
              "order" => array("UF_SORT" => "ASC")
            ));
            $colorResult = array();
            while ($ob = $rsData->Fetch()) {
                if ($ob['UF_FILE']) {
                    $temp = \CFile::ResizeImageGet($ob['UF_FILE'], array("width" => 16, "height" => 16));
                    $ob['UF_FILE'] = $temp['src'];
                }
                $colorResult[] = $ob;
            }
            $data = [
              "image_id" => $rs["PROPERTY_PHOTO_VALUE"],
              'colors' => $rs['PROPERTY_COLOR_MAIN_VALUE'],
              "image" => self::resize($rs["PROPERTY_PHOTO_VALUE"][0], 260, 390),
              "link" => $rs["DETAIL_PAGE_URL"],
              "parentId" => $rs['PROPERTY_CML2_LINK_VALUE'],
              "id" => $productId,
              'name' => $rs['NAME'],
              "saleName" => $rs['PROPERTY_CML2_LINK_PROPERTY_SALENAME_VALUE'],
              "props" => array(
                'article' => array(
                  "name" => "Артикул",
                  "value" => $rs['PROPERTY_CML2_LINK_PROPERTY_CML2_ARTICLE_VALUE']
                ),
                'color' => array(
                  "name" => "Цвет",
                  "value" => self::prepareColorName($rs['PROPERTY_COLOR_NAME'])
                ),
                'colorId' => array(
                  "skip" => true,
                  "value" => $rs['PROPERTY_COLOR_VALUE']
                ),
                'color_main' => array(
                  "skip" => true,
                  'value' => $colorResult
                ),
                'size' => array(
                  "name" => "Размер",
                  "value" => $rs['PROPERTY_RAZMER_ODEZHDA_VALUE'] / 2
                ),
                'height' => array(
                  "name" => "Рост",
                  "value" => $rs['PROPERTY_ROST_VALUE']
                ),
              )
            ];
            $obCache->endDataCache($data);
        }

        return $data;
    }

    public static function prepareColorName($string)
    {
        $string = explode(
          "/",
          strtolower(
            trim($string)
          )
        );
        $string = array_map("trim", $string);

        return implode(
          "/",
          array_unique(
            $string
          )
        );
    }

    public static function item($arFields)
    {
        if (SITE_TEMPLATE_ID === 'cameo-v3') {
            return self::v3($arFields);
        }

        return '
    <div class="ui divider"></div>
    <div class="card product" data-basket="' . $arFields['basketId'] . '">
            <div class="content">
                <a href="' . $arFields["link"] . '" class="ui heading">' . getModelNameByProductName($arFields["name"]) . '</a>
                <div class="description price uppercase">' . self::getPrice($arFields) . '</div>
                <div class="flat">' . self::params($arFields['props']) . '</div>
                <div class="link remove-from-basket" data-basket-id="' . $arFields['basketId'] . '" data-url="cart.remove">Удалить</div>
            </div>
            <div class="image small rect fitted">
                <img src="' . $arFields["image"] . '">
            </div>
        </div>
    ';
    }

    public static function v3($arFields)
    {
        self::$item = $arFields;
        $name = $arFields['saleName'] ? $arFields['saleName'] : getModelNameByProductName(self::$item["name"]);
        $bonus_string = \BonusCard::isset() && \BonusCard::allow() ? '<br/><span class="bonus-accrued primary-color">+' . (pluralize(\BonusCard::accruedAmount($arFields['total']),
            'бонус')) . '</span>' : '';

        return "<div class='item' data-basket='$arFields[basketId]'>
    <div class='image'>
        <a href='$arFields[link]' class='fit-container'>
            <img src='$arFields[image]' alt='$name'>
        </a>
    </div>
    <div class='content'>
        <a href='$arFields[link]' class='title'>$name</a>
        <div class='article'>Артикул: " . self::getParamValue("article") . "</div>
        <div class='total popup-trigger' data-content='Цена за 1 шт. $arFields[price_formatted]'>$arFields[total_formatted]$bonus_string</div>
        <div class='ui accordion'>
            <div class='title'>Подробнее" . icon("dropdown", '', array(), true) . "</div>
            <div class='content'>
                <div class='params'>" . self::params() . "</div>
                " . self::stepper() . "
            </div>
        </div>
    </div>
    <div class='actions'>
        " . icon('trash', 'delete', array('data-id' => $arFields['basketId']), true) . "
        " . icon('pencil', 'edit', array('data-link' => $arFields['link']), true) . "
        <div class='favorite-trigger' data-target='$arFields[parentId]'>" . icon(array('like', 'like-inner'), '',
            array(), true) . "</div>
    </div>
</div>";
    }

    public static function getParamValue($field)
    {
        return self::$item['props'][$field]['value'];
    }

    public static function params($props = array())
    {
        $result = "";
        if (SITE_TEMPLATE_ID === 'cameo-v3') {
            return "<div class='color-param'>
                <div class='title'>Цвет" . self::getColorCircles() . "</div>
                <div class='value'>" . self::getColorString() . "</div>
            </div>
            <div class='params-group'>
                <div class='param'>
                  <div class='title'>Размер</div>
                  <div class='value'>" . self::getParamValue('size') . "</div>
                </div>
                <div class='param'>
                    <div class='title'>Рост</div>
                    <div class='value'>" . self::getParamValue('height') . "</div>
                </div>
            </div>";
        }
        foreach ($props as $prop) {
            if ($prop['skip']) {
                continue;
            }
            $result .= "<div class=\"ability\">
                            <span class=\"secondary description\">$prop[name]</span>
                            <span class=\"description\">
                                $prop[value]
                            </span>
                        </div>";
        }

        return $result;
    }

    public static function getColorCircles()
    {
        $colors = self::getParamValue("color_main");
        if (empty($colors)) {
            global $version;
            $colorId = self::getParamValue("colorId");
            $cache = \Bitrix\Main\Data\Cache::createInstance();
            $result = null;
            if ($cache->initCache(86400, $colorId, "/colors/" . ($version == 3 ? '' : 'v' . $version))) {
                $result = $cache->getVars();
            } elseif ($cache->startDataCache()) {
                $rs = \CIBlockElement::GetList(array(), array("ID" => $colorId), false, false,
                  array("PROPERTY_ICON" . ($version == 3 ? '' : '_SQUARE')))->Fetch();
                if (!$rs['PROPERTY_ICON' . ($version == 3 ? '' : '_SQUARE') . '_VALUE']) {
                    $cache->abortDataCache();
                } else {
                    $result = \CFile::GetPath($rs['PROPERTY_ICON' . ($version == 3 ? '' : '_SQUARE') . '_VALUE']);
                    $cache->endDataCache($result);
                }
            }
            if ($result) {
                return "<div class='color-row'><div class='color color-property' style='background-image: url(" . $result . ")'></div></div>";
            }

            return "";
        } else {
            $result = "<div class='color-row'>";
            foreach ($colors as $color) {
                $background = $color['UF_FILE'] ? 'background-image: url(' . $color['UF_FILE'] . ')' : 'background-color: ' . $color['UF_XML_ID'];
                $result .= '<div class="color color-property" style="' . $background . '"></div>';
            }
            $result .= '</div>';

            return $result;
        }
    }

    public static function getColorString()
    {
        $colors = self::getParamValue("color_main");
        if (empty($colors)) {
            return self::getParamValue("color");
        } else {
            $result = array();
            foreach ($colors as $k => $color) {
                if ($k !== 0) {
                    $color['UF_NAME'] = strtolower($color['UF_NAME']);
                }
                $result[] = $color['UF_NAME'];
            }

            return implode(", ", $result);
        }
    }

    public static function stepper()
    {
        if (!self::$editable) {
            return self::$item['count'];
        }

        return "<div class='stepper'>
            <div class='ui action input'>
                <div class='minus ui basic icon button'>
                    " . icon('chevron-left-button', '', array(), true) . "
                </div>
                <input type='text' value='" . self::$item['count'] . "'>
                <div class='plus ui basic icon button'>
                    " . icon('chevron-right-button', '', array(), true) . "
                </div>
            </div>
        </div>";
    }

    public static function getPrice($arFields)
    {
        $string = CurrencyFormat($arFields['total'], 'RUB');
        if ($arFields['count'] > 1) {
            $string .= " <span>за " . $arFields['count'] . ' шт.</span>';
        }

        return $string;
    }

    public static function itemMain($arFields)
    {
        global $version;
        if ($version == 4) {
            return self::itemMainV4($arFields);
        }
        self::$item = $arFields;
        $name = $arFields['saleName'] ? $arFields['saleName'] : getModelNameByProductName(self::$item["name"]);
        $bonus_string = \BonusCard::isset() && \BonusCard::allow() ? '<br/><span class="bonus-accrued primary-color">+' . (pluralize(\BonusCard::accruedAmount($arFields['total']),
            'бонус')) . '</span>' : '';

        return "<tr class='item' data-basket='$arFields[basketId]'>
    <td class='image'>
        <a href='$arFields[link]' class='fit-container'>
            <img src='$arFields[image]' alt='$name'>
        </a>
    </td>
    <td>
        <a href='$arFields[link]' class='title'>$name</a>
        <div class='article'>Артикул:<br>" . self::getParamValue("article") . "</div>
    </td>
    <td class='computer only tablet only grouped-params'>" . self::params() . "</td>
    <td class='color-param param computer hidden tablet hidden'>
        " . self::getColorCircles() . self::getColorString() . "
    </td>
    <td class='size-param param computer hidden tablet hidden'>" . self::getParamValue('size') . "</td>
    <td class='height-param param computer hidden tablet hidden'>" . self::getParamValue('height') . "</td>
    <td>" . self::stepper() . "</td>
    <td><span class='total popup-trigger' data-position='bottom left' data-content='Цена за 1 шт. $arFields[price_formatted]'>$arFields[total_formatted]$bonus_string</span></td>
    <td class='action computer hidden action-edit'>" . icon('pencil', 'edit', array('data-link' => $arFields['link']),
            true) . "</td>
    <td class='action computer hidden action-delete'>" . icon('trash', 'delete',
            array('data-id' => $arFields['basketId']), true) . "</td>
    <td class='actions computer only action-list'>" . icon('trash', 'delete', array('data-id' => $arFields['basketId']),
            true) . icon('pencil', 'edit', array('data-link' => $arFields['link']), true) . "</td>
</tr>";
    }

    public static function itemMainV4($arFields)
    {
        self::$item = $arFields;
        $name = $arFields['saleName'] ? $arFields['saleName'] : getModelNameByProductName(self::$item["name"]);
        $bonus_string = \BonusCard::isset() && \BonusCard::allow() ? '<br/><span class="bonus-accrued primary-color">+' . (pluralize(\BonusCard::accruedAmount($arFields['total']),
            'бонус')) . '</span>' : '';

        return "<tr class='item' data-basket='$arFields[basketId]'>
    <td class='column-image'>
        <a href='$arFields[link]' class='fit-container'>
            <img src='$arFields[image]' alt='$name'>
        </a>
    </td>
    <td class='column-name'>
        <a href='$arFields[link]' class='title'>$name</a>
        " . icon('dropdown', '', [], true) . "
    </td>
    <td class='color-param param column-color'>
        <div class='color-param-wrapper'>
        " . self::getColorCircles() . self::getColorString() . "
        </div>
    </td>
    <td class='size-param param column-size'>" . self::getParamValue('size') . "<span class='mobile only height-value'> - " . self::getParamValue("height") . "</span></td>
    <td class='height-param param column-height'>" . self::getParamValue('height') . "</td>
    <td class='column-stepper'>" . self::stepper() . "</td>
    <td class='column-price'><span class='total popup-trigger' data-position='bottom left' data-content='Цена за 1 шт. $arFields[price_formatted]'>$arFields[total_formatted]$bonus_string</span></td>
   " . (self::$editable ? "<td class='action action-delete column-action'>" . icon('trash-v4', 'delete',
              array('data-id' => $arFields['basketId']), true) . "</td>" : '') . "
</tr>";
    }

    public static function info()
    {
        return "<tr class='info'><td colspan='7'>" . icon('info', '', [],
            true) . "<div>" . self::$infoText . "</div></td></tr>";
    }

    public static function getItemData($basket)
    {
        $list = [];
        /**
         *
         * @var $basket \Bitrix\Sale\Basket
         */
        foreach ($basket->getBasketItems() as $basketItem) {
            /** @var BasketItem $basketItem */
            $data = self::getData($basketItem->getProductId());
//            if ($parent = CIBlockElement::GetByID($data['parentId'])->Fetch()) {
//                $data['name'] = $parent['NAME'];
//            } else {
//
//            }
            $data['name'] = $basketItem->getField("NAME");
            $data['price'] = $basketItem->getPrice();
            $data['price_formatted'] = CurrencyFormat($basketItem->getPrice(), "RUB");
            $data['price_base'] = $basketItem->getBasePrice();
            $data['price_base_formatted'] = CurrencyFormat($basketItem->getBasePrice(), "RUB");
            $data['total'] = $basketItem->getFinalPrice();
            $data['total_formatted'] = CurrencyFormat($data['total'], 'RUB');
            $data['total_base'] = $data['price_base'] * $basketItem->getQuantity();
            $data['total_base_formatted'] = CurrencyFormat($data['total_base'], 'RUB');
            $data['count'] = $basketItem->getQuantity();
            $data['basketId'] = $basketItem->getId();
            $data['productId'] = $basketItem->getProductId();
            $data['canBuy'] = $basketItem->canBuy();
            $data['delay'] = $basketItem->isDelay();
            self::$item = $data;
            $data['color'] = self::getColorCircles() . self::getColorString();
            $data['size'] = self::getParamValue('size');
            $data['height'] = self::getParamValue('height');
            $data['bonus'] = \BonusCard::isset() && \BonusCard::allow() ? '<br/><span class="bonus-accrued primary-color">+' . (pluralize(\BonusCard::accruedAmount($data['total']),
                'бонус')) . '</span>' : '';
            $data['custom'] = $basketItem->getField('CUSTOM_PRICE');
            $list[] = $data;
        }

        return $list;
    }

    public static function getBonusMail(Order $order)
    {
        if ($order->getPersonTypeId() == 2) {
            return '';
        }
        $result = '';
        $oldGuid = \BonusCard::$guid;
        $guid = \BonusCard::findGuidByUser($order->getUserId());
        \BonusCard::$guid = $guid;
        if (\BonusCard::isset()) {
            $total = \BonusCard::accruedAmount($order->getPrice());
            $result = '<div style="height: 60px"></div><table>
<tr><td colspan="2">Будет начислено <span style="color: #24A56B;">+' . $total . '</span> ' . pluralize($total, 'бонус',
                false, false) . '</td></tr>
<tr><td colspan="2" height="8"></td></tr>
<tr><td width="26"><img src="https://www.kamey.ru/local/templates/cameo-v3/images/mail-info.png"/></td><td style="color: #929292;font-size: 14px;">Бонусы будут начислены только <br>на выкупленный товар по факту</td></tr>
</table>';
        } else {
        }
        \BonusCard::$guid = $oldGuid;

        return $result;
    }

    public static function getDiscounts(Order $order, $returnArray = false, $bonus = true)
    {
        if ($returnArray) {
            $result = [];
        } else {
            $result = '';
        }
        $payment = \BonusCard::getPayment($order->getPaymentCollection(), false);
        if ($payment && $payment->getSum() && $bonus) {
            if ($returnArray) {
                $result[] = [
                  'name' => 'Бонусов спишется',
                  'value' => $payment->getSum()
                ];
            } else {
                $result .= '<tr><td>Бонусов спишется</td><td>&nbsp;</td><td>-' . $payment->getSum() . '</td></tr><tr><td colspan="3" height="10"></td></tr>';
            }
        }
        $discount = $order->getDiscount()->getApplyResult();
        if (!empty($discount['DISCOUNT_LIST'])) {
            foreach ($discount['DISCOUNT_LIST'] as $id => $item) {
                if ($item['USE_COUPONS'] == 'Y') {
                    $item['NAME'] = 'Скидка по промокоду';
                }
                $value = 0;
                foreach ($discount['BASKET'] as $basketItem) {
                    foreach ($basketItem as $resultItem) {
                        if ($resultItem['DISCOUNT_ID'] == $id) {
                            foreach ($resultItem['RESULT']['DESCR_DATA'] as $datum) {
                                $value += $datum['RESULT_VALUE'];
                            }
                        }
                    }
                }
                foreach ($discount['ORDER'] as $orderItem) {
                    if ($orderItem['DISCOUNT_ID'] == $id) {
                        foreach ($orderItem['RESULT']['BASKET'] as $resultItem) {
                            foreach ($resultItem['DESCR_DATA'] as $datum) {
                                $value += $datum['RESULT_VALUE'];
                            }
                        }
                        foreach ($orderItem['RESULT']['DELIVERY']['DESCR_DATA'] as $datum) {
                            $value += $datum['RESULT_VALUE'];
                        }
                    }
                }
                if ($returnArray) {
                    $result[] = [
                      'name' => $item['NAME'],
                      'value' => $value
                    ];
                } else {
                    $result .= '<tr><td>' . $item['NAME'] . '</td><td>&nbsp;</td><td>-' . $value . '</td></tr><tr><td colspan="3" height="10"></td></tr>';
                }
            }
        }

        return $result;
    }

    /**
     * @param \Bitrix\Sale\Basket $basket
     *
     * @param bool $replaceImagePath
     *
     * @return string
     * @throws ArgumentNullException
     */
    public static function viewEmail(\Bitrix\Sale\Basket $basket, $replaceImagePath = false)
    {
        $views = "";
        global $version;
        foreach ($basket->getOrderableItems() as $basketItem) {
            /** @var BasketItem $basketItem */
            $data = self::getData($basketItem->getProductId());
            $data['name'] = $basketItem->getField("NAME");
            $data['price'] = $basketItem->getPrice();
            $data['price_formatted'] = CurrencyFormat($basketItem->getPrice(), "RUB");
            $data['total'] = $basketItem->getFinalPrice();
            $data['total_formatted'] = CurrencyFormat($data['total'], 'RUB');
            $data['count'] = $basketItem->getQuantity();
            $data['basketId'] = $basketItem->getId();
            $data['productId'] = $basketItem->getProductId();
            $data['canBuy'] = $basketItem->canBuy();

            if ($replaceImagePath) {
                $data['image'] = "/include/v3/image.php?id=" . $basketItem->getProductId();
            }

            self::$basket[$basketItem->getId()] = $data;
            if ($version == 4) {
                $views .= self::itemEmailV4($data);
            } else {
                $views .= self::itemEmail($data);
            }
        }

        return $views;
    }

    public static function itemEmailV4($arFields)
    {
        self::$item = $arFields;
        $name = $arFields['saleName'] ? $arFields['saleName'] : getModelNameByProductName(self::$item["name"]);

        return '<tr class="product">
    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 0; padding-top: 24px;"
        valign="top">
        <div style="width:80px;height:107px;background-image:url(\'' . $arFields['image'] . '\');background-size:cover;"></div>
    </td>
    <td style="border-collapse: collapse;vertical-align: top;padding-top: 24px;border-bottom: 1px dashed #c8c8c8;">
        <div style="text-transform:uppercase;color:black;">' . $name . '</div>
    </td>
    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-bottom: 1px dashed #c8c8c8; vertical-align: top; padding-top: 24px; font-size: 12px; line-height: 14px;"
        valign="top">
        <table>
        <tr>
        <td>' . str_replace(
            array(
              "background-image",
            ),
            array(
              "width:24px;height:24px;background-position:center;display:inline-block;margin-right:8px;vertical-align:top;background-size:cover;background-image",
            ), self::getColorCircles()) . '</td><td>' . self::getColorString() . '</td>
</tr>
</table>
    </td>
    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-bottom: 1px dashed #c8c8c8; vertical-align: top; padding-top: 24px;"
        valign="top"><span class="show" style="display: none">Размер: </span>' . self::getParamValue('size') . '
    </td>
    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-bottom: 1px dashed #c8c8c8; vertical-align: top;  padding-top: 24px;"
        valign="top"><span class="show" style="display: none">Рост: </span>' . self::getParamValue('height') . '
    </td>
    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-bottom: 1px dashed #c8c8c8; vertical-align: top;  padding-top: 24px;"
        valign="top"><span class="show" style="display: none">Количество: </span>' . $arFields['count'] . '
    </td>
    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;border-bottom: 1px dashed #c8c8c8; vertical-align: top;  padding-top: 24px;"
        valign="top"><span class="show" style="display: none">Цена: </span><b>' . $arFields['total'] . ' руб.</b>
    </td>
</tr>';
    }

    public static function itemEmail($arFields)
    {
        self::$item = $arFields;
        $name = $arFields['saleName'] ? $arFields['saleName'] : getModelNameByProductName(self::$item["name"]);

        return '<tr class="product" style="border-bottom: 1px solid #c9c9c9;">
                                                        <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 25px; padding-top: 25px;" valign="top">
                                                            <table style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #616161;">
                                                                <tr>
                                                                    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" valign="top">
                                                                        <div style="width:64px;height:64px;background-image:url(\'' . $arFields['image'] . '\');background-size:cover;"></div>
                                                                    </td>
                                                                    <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-left: 18px;" valign="top">
                                                                        <div style="text-transform:uppercase;color:black;margin-bottom:12px">' . $name . '</div>Артикул:<br>' . self::getParamValue("article") . '</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 25px; padding-top: 25px; font-size: 12px; line-height: 14px;" valign="top">
                                                        ' . str_replace(
            array(
              "background-image",
              "color-row'"
            ),
            array(
              "width:12px;height:12px;border-radius:50%;background-position:center;display:inline-block;margin-right:8px;vertical-align:top;background-image",
              "color-row' style='margin-bottom:15px;'"
            ), self::getColorCircles()) . self::getColorString() . '
                                                        </td>
                                                        <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 25px; padding-top: 25px;" valign="top"><span class="show" style="display: none">Размер: </span>' . self::getParamValue('size') . '</td>
                                                        <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 25px; padding-top: 25px;" valign="top"><span class="show" style="display: none">Рост: </span>' . self::getParamValue('height') . '</td>
                                                        <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 25px; padding-top: 25px;" valign="top"><span class="show" style="display: none">Количество: </span>' . $arFields['count'] . '</td>
                                                        <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top; padding-bottom: 25px; padding-top: 25px;" valign="top"><span class="show" style="display: none">Цена: </span>' . $arFields['total'] . ' руб.</td>
                                                    </tr>';
    }

    public static function convertToBase64($path)
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . $path;
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);

        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    /**
     * @param \Bitrix\Sale\Basket|\Bitrix\Sale\BasketItem[] $basket
     *
     * @param bool $format
     *
     * @return string|float
     * @throws ArgumentNullException
     */
    public static function getTotalPrice($basket, $format = true)
    {
        $price = 0;
        foreach ($basket->getOrderableItems() as $basketItem) {
            if (!$basketItem->isBundleChild()) {
                $price += $basketItem->getFinalPrice();
            }
        }

        return $format ? CurrencyFormat($price, "RUB") : $price;
    }

    /**
     * @param \Bitrix\Sale\Basket|\Bitrix\Sale\BasketItem[] $basket
     *
     * @param bool $small
     *
     * @return string
     * @throws ArgumentNullException
     */
    public static function viewNotAvailable($basket, $small = true)
    {
        $views = "";
        foreach ($basket->getBasketItems() as $basketItem) {
            /** @var BasketItem $basketItem */
            if ($basketItem->canBuy()) {
                continue;
            }
            $data = self::getData($basketItem->getProductId());
            $data['name'] = $basketItem->getField("NAME");
            $data['price'] = $basketItem->getPrice();
            $data['price_formatted'] = CurrencyFormat($basketItem->getPrice(), "RUB");
            $data['total'] = $basketItem->getFinalPrice();
            $data['total_formatted'] = CurrencyFormat($data['total'], 'RUB');
            $data['count'] = $basketItem->getQuantity();
            $data['basketId'] = $basketItem->getId();
            $data['productId'] = $basketItem->getProductId();
            $data['canBuy'] = $basketItem->canBuy();
            self::$basket[$basketItem->getId()] = $data;
            if ($small) {
                $views .= self::item($data);
            } else {
                $views .= self::itemMain($data);
            }
        }

        return $views;
    }

    public static function getPriceOnly()
    {
        return self::$item['total_formatted'];
    }
}
