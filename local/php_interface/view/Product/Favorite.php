<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 24.10.2017
 * Time: 10:27
 */

namespace Product;

use CUser;

class Favorite extends Base
{
    public static function view($list)
    {
        if (SITE_TEMPLATE_ID === 'cameo-v3') {
            if (empty($list)) {
                return "<div class='ui header normal big'>Вы еще не добавили товары в избранное</div>";
            }
            ob_start();
            global $APPLICATION;
            $APPLICATION->IncludeFile("/include/v3/favorite-component.php", array(
              "FAVORITE_CLASS" => "column favorite-column",
              "UNWRAP" => false,
              "SET_LIST" => $list
            ), array("MODE" => "php", 'SHOW_BORDER' => false));
            $result = ob_get_contents();
            ob_end_clean();

            return $result;
        }
        if (empty($list)) {
            return "";
        }
        $result = "";
        $obCache = \Bitrix\Main\Data\Cache::createInstance();
        $cache_time = "86400";
        sort($list);
        $cache_id = "list." . implode(",", $list);
        \CModule::IncludeModule("iblock");
        if ($obCache->initCache($cache_time, $cache_id, "/favorites/")) {
            $result = $obCache->GetVars();
        } elseif ($obCache->startDataCache()) {
            $rs = \CIBlockElement::GetList(array(
              "SORT" => "ASC",
              "ID" => "DESC"
            ), array("ID" => $list), false, false, array(
              "DETAIL_PAGE_URL",
              "PROPERTY_MODEL",
              "PROPERTY_MINIMUM_PRICE_2",
              "DETAIL_PICTURE",
              "ID",
              "DETAIL_PICTURE"
            ));
            $i = 0;
            while ($ob = $rs->GetNext()) {
                if ($i > 0) {
                    $result .= '<div class="divider"></div>';
                }
                $result .= "<div class=\"card product\">
            <div class=\"content\">
                <a href=\"$ob[DETAIL_PAGE_URL]\" class=\"ui heading\">$ob[PROPERTY_MODEL_VALUE]</a>
                <div class=\"description price uppercase\">" . CurrencyFormat($ob['PROPERTY_MINIMUM_PRICE_2_VALUE'],
                    "RUB") . "</div>
                <div class='flat'></div>
                <div class=\"link heart-trigger\" style=\"color:grey!important\" data-target=\"$ob[ID]\">Удалить</div>
            </div>
            <div class=\"image small rect fitted\">
                <img src=\"" . self::resize($ob['DETAIL_PICTURE'], 70, 100) . "\" alt=\"$ob[NAME]\">
            </div>
        </div>";
                $i++;
            }
            $obCache->endDataCache($result);
        }

        return $result;
    }

    public static function getListTransform()
    {
        $list = self::getList();
        $result = [
          'id' => [],
          'transform' => []
        ];
        foreach ($list as $item) {
            $parts = explode(":", $item);
            if (count($parts) == 3) {
                $result['id'][] = $parts[0];
                $result['transform'][$parts[0]][$parts[1]][] = $parts[2];
            } else {
                $result['id'][] = $parts[0];
                $result['transform'][$parts[0]]['self'] = true;
            }
        }
        $result['id'] = array_unique($result['id']);

        return $result;
    }

    public static function getList()
    {
        global $request;
        if (CUser::IsAuthorized()) {
            $arUser = CUser::GetList($by, $order, array("ID" => CUser::GetID()), array(
              'SELECT' => array("ID", "UF_FAVORITE"),
              "FIELDS" => array("UF_FAVORITE")
            ))->Fetch();;
            $list = $arUser['UF_FAVORITE'] ?: array();
        } else {
            $list = unserialize($request->getCookie("favorite")) ?: array();
        }

        return array_unique($list);
    }
}
