<?php
/**
 * Created by PhpStorm.
 * User: Developer
 * Date: 11.09.2017
 * Time: 9:56
 */

namespace Product;

use CFile;
use Element\Item;
use Element\Price;

class Card extends Base
{
    public static $debug = false;

    static public function make($arItem, $component, $class = "column")
    {
        Item::cast($arItem);
        /** @var Item $arItem */
        $images = self::getImagesOne($arItem);
        $price = self::getPrice($arItem);
        $selectedImageId = $arItem->getSelectedOffer()->PROPERTIES['COLOR']['VALUE'];
        ?>
        <div class="<?= $class ?>">
            <a class="ui clothes card" id="<?= $arItem->area($component) ?>" href="<?= $arItem->link ?>">
                <div class="image">
                    <div class="heart heart-trigger" data-target="<?= $arItem->id ?>">
                        <div class="fi fi-valentines-heart"></div>
                    </div>
                    <div class="overlay flat centered">
                        <div class="border-wrapper">
                            <div class="ui white button fast-view" data-modal="product" data-url="<?= $arItem->link ?>"
                                 data-id="<?= $arItem->id ?>">
                                Быстрый просмотр
                            </div>
                        </div>
                    </div>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/no-image.png" alt="Нет фото" class="placeholder">
                    <img class="target" src="<?= current($images) ?>" data-source="images-<?= $arItem->id ?>"
                         alt="<?= $arItem->prop("MODEL")->value() ?>">
                    <div class="radio flat">
                        <?
                        foreach ($images as $k => $image) {
                            ?>
                            <div class="thumb square fitted<?= ($k == 0 ? " active" : "") ?>"><img
                                        src="<?= $image ?>"
                                        data-target="images-<?= $arItem->id ?>"
                                        data-source="<?= $image ?>">
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <div class="labels">
                        <? if ($arItem->prop("NOVINKA")->value() == "Да"): ?>
                            <div class="new">Новинка</div>
                        <? elseif ($arItem->prop("LIDER_PRODAZH")->value() == "Да"): ?>
                            <div class="leader">Лидер продаж</div>
                        <? elseif ($price && $price->hasDiscount): ?>
                            <div class="discount-price">Скидка <?= $price->discountPercent ?>%</div>
                        <? elseif ($arItem->prop("SPETSPREDLOZHENIE")->value() == "Да"): ?>
                            <div class="discount">Скидка</div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="content">
                    <?
                    $density = null;
                    switch ($arItem->prop("PLOTNOST")->value()) {
                        case "Низкая":
                            $density = 1;
                            break;
                        case "Средняя":
                            $density = 2;
                            break;
                        case "Высокая":
                            $density = 3;
                            break;
                    }
                    if ($density) {
                        ?>
                        <div class="description">Плотность ткани: <?= $arItem->prop("PLOTNOST")->value() ?>
                            <?
                            while ($density > 0) {
                                ?>
                                <div class="fi fi-quill"></div>
                                <?
                                $density--;
                            }
                            ?>
                        </div>
                        <?
                    }
                    ?>
                    <div class="divider"></div>
                    <div class="description product-name"><?= $arItem->prop("MODEL")->value() ?></div>
                    <?
                    if ($price) {
                        ?>
                        <div class="ui heading price"><?= $price->printValue() ?><?
                            if ($price->hasDiscount) { ?><span
                                    class="old-price"><?= $price->printRealValue() ?></span><?
                            } ?></div>
                        <?
                    } ?>
                </div>
            </a>
        </div>
        <?
    }

    /**
     * @param Item $arItem
     *
     * @return array
     */
    private static function getImagesOne(Item $arItem)
    {
        $images = array();
        $sorts = array();
        foreach ($arItem->offers as $offer) {
            if (!$images[$offer->PROPERTIES['COLOR']['VALUE']]) {
                if ($offer->previewPicture && $offer->previewPicture->src()) {
                    $images[$offer->PROPERTIES['COLOR']['VALUE']] = $offer->previewPicture->resize(260, 390);
                    $sorts[$offer->PROPERTIES['COLOR']['VALUE']] = $offer->sort;
                } else {
                    $imageId = current($offer->PROPERTIES['PHOTO']['VALUE']);
                    $sorts[$offer->PROPERTIES['COLOR']['VALUE']] = $offer->sort;
                    $images[$offer->PROPERTIES['COLOR']['VALUE']] = self::resize($imageId, 260, 390);
                }
            }
        }
        array_multisort($sorts, SORT_ASC, $images);

        return $images;
    }

    /**
     * @param Item $arItem
     *
     * @return Price
     */
    private static function getPrice(Item $arItem)
    {
        $price = $arItem->price;
        if ($arItem->hasOffers && (!isset($arItem['MIN_PRICE']) || $arItem['MIN_PRICE'] == false)) {
            $price = $arItem->getSelectedOffer()->price;
        }

        return $price;
    }

    static public function v4($arItem, $component, $clear = false, $isOffer = false)
    {
        if (self::$arParams['IS_FAVORITE'] && !$isOffer) {
            $transform = self::$arParams['TRANSFORM'][$arItem['ID']];
            foreach ($transform['color'] as $colorId) {
                foreach ($arItem['OFFERS'] as $arOffer) {
                    if ($arOffer['PROPERTIES']['COLOR']['VALUE'] == $colorId) {
                        $arOffer['PROPERTY_CML2_LINK_ID'] = $arItem['ID'];
                        $arOffer['PROPERTY_CML2_LINK_PROPERTY_SALENAME_VALUE'] = $arItem['PROPERTIES']['SALENAME']['VALUE'];
                        $arOffer['PROPERTY_CML2_LINK_PROPERTY_MODEL_VALUE'] = $arItem['PROPERTIES']['MODEL']['VALUE'];
                        $arOffer['PROPERTY_CML2_LINK_PROPERTY_LIDER_PRODAZH_VALUE'] = $arItem['PROPERTIES']['LIDER_PRODAZH']['VALUE'];
                        $arOffer['PROPERTY_CML2_LINK_PROPERTY_NOVINKA_VALUE'] = $arItem['PROPERTIES']['NOVINKA']['VALUE'];
                        $arOffer['DETAIL_PAGE_URL'] = $arItem['DETAIL_PAGE_URL'] . '?offer=' . $arOffer['ID'];
                        self::v4($arOffer, $component, $clear, true);
                        break;
                    }
                }
            }
            if (!$transform['self']) {
                return;
            }
        }
        Item::cast($arItem);
        if ($isOffer) {
            self::setParentProperties($arItem, ['SALENAME', 'MODEL', 'LIDER_PRODAZH', 'NOVINKA']);
        }
        /** @var Item $arItem */
        $class = isset(self::$arParams['CLASS']) ? self::$arParams['CLASS'] : 'column eight wide';
        $price = self::getPrice($arItem);
        if (!$price) {
            $price = new Price(array());
        }
        if (!$isOffer) {
            $images = self::getImagesWithNames($arItem, 2);
            $colorData = [];
        } else {
            $images = [];
            foreach (array_slice($arItem->prop("PHOTO")->value(), 0, 2) as $id) {
                $images[] = self::resize($id, 534, 710);
            }
            $colorData = self::getColorData($arItem);
        }

        if (self::$arParams['UNWRAP'] !== true) {
            ?>
            <div class="<?= $class ?>">
            <?
        }
        ?>
        <div class="product-item" id="<?= $arItem->area($component) ?>">
            <div class="image-container">
                <a href="<?= $arItem->link ?>" class="image fit-container">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/no-image.png" alt="Нет фото" class="placeholder">
                    <?
                    if (!$isOffer) {
                        foreach ($images[0]['images'] as $k => $image) {
                            ?>
                            <img src="<?= $image ?>"
                                 alt="<?= $arItem->prop("MODEL")->value() ?>"
                              <?= ($k === 1 ? ' class="overlay"' : ' class="main"') ?>>
                            <?
                        }
                        ?>
                        <div class="color-row">
                            <div class="color-name"><?= $images[0]['name'] ?></div>
                            <div class="color-items">
                                <?
                                foreach ($images as $k => $group) {
                                    ?>
                                    <div class="color<?= ($group['icon_property'] ? ' color-property' : '') ?><?= ($k == 0 ? ' active' : '') ?>"
                                         style="background-image: url(<?= $group['icon'] ?>)"
                                         data-images="<?= htmlspecialchars(json_encode($group['images'])) ?>"
                                         data-name="<?= $group['name'] ?>">
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <?
                    } else {
                        foreach ($images as $k => $image) {
                            ?>
                            <img src="<?= $image ?>"
                                 alt="<?= $arItem->prop("MODEL")->value() ?>"
                              <?= ($k === 1 ? ' class="overlay"' : ' class="main"') ?>>
                            <?
                        }
                    }
                    if (self::$arParams['IS_FAVORITE']) {
                        ?>
                        <div class="favorite-delete"
                             data-target="<?= $isOffer ? $arItem['PROPERTY_CML2_LINK_ID'] . ':color:' . $arItem->prop('COLOR')->value() : $arItem->id ?>">
                            <?
                            icon("trash");
                            ?>
                        </div>
                        <?
                    } else {
                        ?>
                        <div class="favorite-trigger"
                             data-target="<?= $isOffer ? $arItem['PROPERTY_CML2_LINK_ID'] . ':color:' . $arItem->prop('COLOR')->value() : $arItem->id ?>">
                            <?
                            icon("like-bold");
                            ?>
                        </div>
                        <?
                    }
                    ?>
                </a>
            </div>
            <div class="info-container">
                <div class="label-list">
                    <?
                    if (count($images) > 1 && !$isOffer) {
                        ?>
                        <div class="label-count">
                            <div class="label">
                                <div class="content">+</div>
                            </div>
                            <?= pluralize(count($images) - 1, 'цвет') ?>
                        </div>
                        <?
                    }
                    if ($arItem->prop("NOVINKA")->value() == "Да") { ?>
                        <div class="label" title="Новинка">
                            <div class="content">new</div>
                        </div>
                    <? }
                    if ($arItem->prop("LIDER_PRODAZH")->value() == "Да") { ?>
                        <div class="label" title="Лидер продаж">
                            <div class="content">top</div>
                        </div>
                    <? }
                    if ($price && $price->hasDiscount) { ?>
                        <div class="label" title="Скидка <?= $price->discountPercent ?>%">
                            <div class="content percent">
                                <?= $price->discountPercent ?>%
                            </div>
                        </div>
                    <? } elseif ($arItem->prop("SPETSPREDLOZHENIE")->value() == "Да") { ?>
                        <div class="label" title="Распродажа">
                            <div class="content percent">
                                %
                            </div>
                        </div>
                    <? }; ?>
                </div>
                <a href="<?= $arItem->link ?>">
                    <div class="name"><?= ($arItem->prop("SALENAME")->hasValue() ? $arItem->prop("SALENAME")->value() : ($arItem->prop("MODEL")->hasValue() ? $arItem->prop("MODEL")->value() : $arItem->name)) ?></div>
                    <?
                    if (!$clear) {
                        ?>
                        <div class="prices">
                            <?
                            if ($price->hasDiscount) {
                                ?>
                                <div class="old-price"><?= $price->printRealValue() ?></div>
                                <?
                            }
                            ?>
                            <div class="price"><?= ($price->printValue() ? $price->printValue() : "Нет в наличии") ?></div>
                        </div>
                    <? } ?>
                </a>
            </div>
        </div>
        <?
        if (self::$arParams['UNWRAP'] !== true) {
            ?>
            </div>
            <?
        }
    }

    private static function setParentProperties(Item &$arItem, $properties)
    {
        foreach ($properties as $property) {
            $arItem->props[$property] = $arItem->prop('cml2_link_property_' . strtolower($property));
        }
    }

    private static function getImagesWithNames(Item $arItem, $length = 2)
    {
        $images = array();
        $sorts = array();
        global $version;
        foreach ($arItem->offers as $offer) {
            if (!$images[$offer->PROPERTIES['COLOR']['VALUE']]) {
                $sorts[$offer->PROPERTIES['COLOR']['VALUE']] = $offer->sort;

                $result = self::getColorData($offer);
                $photos = array();
                foreach (array_slice($offer->prop("PHOTO")->value(), 0, $length) as $id) {
                    $photos[] = self::resize($id, 534, 710);
                }
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['icon'] = $result['icon'] ? $result['icon'] : $photos[0];
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['icon_property'] = $result['icon'] ? true : false;
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['name'] = mb_ucfirst($result['name']);
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['images'] = $photos;
            }
        }
        array_multisort($sorts, SORT_ASC, $images);

        return $images;
    }

    private static function getColorData(Item $arItem)
    {
        global $version;
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $result = null;
        if ($cache->initCache(86400, $arItem->PROPERTIES['COLOR']['VALUE'] . '/name',
          "/colors/" . ($version == 3 ? '' : 'v' . $version))) {
            $result = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $rs = \CIBlockElement::GetList(array(), array("ID" => $arItem->PROPERTIES['COLOR']['VALUE']), false,
              false, array("PROPERTY_ICON" . ($version == 3 ? '' : '_SQUARE'), 'NAME'))->Fetch();
            if (!$rs['PROPERTY_ICON' . ($version == 3 ? '' : '_SQUARE') . '_VALUE'] && !$rs['NAME']) {
                $cache->abortDataCache();
            } else {
                $result = [
                  'icon' => CFile::GetPath($rs['PROPERTY_ICON' . ($version == 3 ? '' : '_SQUARE') . '_VALUE']),
                  'name' => Basket::prepareColorName($rs['NAME'])
                ];
                $cache->endDataCache($result);
            }
        }

        return $result;
    }

    static public function v3($arItem, $component, $clear = false)
    {
        if ($clear) {
            self::clear($arItem, $component);

            return;
        }
        $isCollection = false;
        Item::cast($arItem);
        /** @var Item $arItem */
        if ($isCollection) {
            $class = "column sixteen wide";
        } else {
            $class = isset(self::$arParams['CLASS']) ? self::$arParams['CLASS'] : 'column eight wide';
        }
        $price = self::getPrice($arItem);
        if (!$price) {
            $price = new Price(array());
        }
        $images = self::getImages($arItem, $isCollection ? 1 : 2);
        ?>
        <?
        if (self::$arParams['UNWRAP'] !== true) {
            ?>
            <div class="<?= $class ?>">
            <?
        }
        ?>
        <div class="item<?= ($isCollection ? ' with-collection' : '') ?>" id="<?= $arItem->area($component) ?>">
            <div class="image-container">
                <a href="<?= $arItem->link ?>" class="image fit-container">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/no-image.png" alt="Нет фото" class="placeholder">
                    <?
                    foreach ($images[0]['images'] as $k => $image) {
                        ?>
                        <img src="<?= $image ?>"
                             alt="<?= $arItem->prop("MODEL")->value() ?>"
                          <?= ($k === 1 ? ' class="overlay"' : ' class="main"') ?>>
                        <?
                    }
                    ?>
                    <div class="label-list">
                        <? if ($arItem->prop("NOVINKA")->value() == "Да") { ?>
                            <div class="label" title="Новинка">
                                <div class="content">NEW</div>
                            </div>
                        <? }
                        if ($arItem->prop("LIDER_PRODAZH")->value() == "Да") { ?>
                            <div class="label" title="Лидер продаж">
                                <div class="content">TOP</div>
                            </div>
                        <? }
                        if ($price && $price->hasDiscount) { ?>
                            <div class="label" title="Скидка <?= $price->discountPercent ?>%">
                                <div class="content percent">
                                    <?= $price->discountPercent ?>%
                                </div>
                            </div>
                        <? } elseif ($arItem->prop("SPETSPREDLOZHENIE")->value() == "Да") { ?>
                            <div class="label" title="Распродажа">
                                <div class="content percent">
                                    %
                                </div>
                            </div>
                        <? }; ?>
                    </div>
                    <div class="color-row">
                        <?
                        foreach ($images as $group) {
                            ?>
                            <div class="color" style="background-image: url(<?= $group['icon'] ?>)"
                                 data-images="<?= htmlspecialchars(json_encode($group['images'])) ?>">
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </a>
            </div>
            <a href="<?= $arItem->link ?>" class="content">
                <div class="title"><?= ($arItem->prop("SALENAME")->hasValue() ? $arItem->prop("SALENAME")->value() : ($arItem->prop("MODEL")->hasValue() ? $arItem->prop("MODEL")->value() : $arItem->name)) ?></div>
                <div class="actions-block">
                    <div class="content">
                        <?
                        if ($price->hasDiscount) {
                            ?>
                            <div class="old-price"><?= $price->printRealValue() ?></div>
                            <?
                        }
                        ?>
                        <div class="price"><?= ($price->printValue() ? $price->printValue() : "Нет в наличии") ?></div>
                        <?
                        if (self::$arParams['IS_FAVORITE']) {
                            ?>
                            <div class="favorite-delete" data-target="<?= $arItem->id ?>">
                                <?
                                icon("trash");
                                ?>
                            </div>
                            <?
                        } else {
                            ?>
                            <div class="favorite-trigger" data-target="<?= $arItem->id ?>">
                                <?
                                icon(array("like", "like-inner"));
                                ?>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </a>
        </div>
        <?
        if (self::$arParams['UNWRAP'] !== true) {
            ?>
            </div>
            <?
        }
    }

    public static function clear($arItem, $component)
    {
        Item::cast($arItem);
        /** @var Item $arItem */
        $class = isset(self::$arParams['CLASS']) ? self::$arParams['CLASS'] : 'column eight wide';
        $images = self::getImages($arItem, 2);
        if (self::$arParams['UNWRAP'] !== true) {
            ?>
            <div class="<?= $class ?>">
            <?
        }
        ?>
        <div class="item" id="<?= $arItem->area($component) ?>">
            <div class="image-container">
                <a href="<?= $arItem->link ?>" class="image fit-container">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/no-image.png" alt="Нет фото" class="placeholder">

                    <?
                    foreach ($images[0]['images'] as $k => $image) {
                        ?>
                        <img src="<?= $image ?>"
                             alt="<?= $arItem->prop("MODEL")->value() ?>"
                          <?= ($k === 1 ? ' class="overlay"' : ' class="main"') ?>>
                        <?
                    }
                    ?>
                    <div class="color-row">
                        <?
                        foreach ($images as $group) {
                            ?>
                            <div class="color" style="background-image: url(<?= $group['icon'] ?>)"
                                 data-images="<?= htmlspecialchars(json_encode($group['images'])) ?>">
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </a>
            </div>
            <a href="<?= $arItem->link ?>" class="content">
                <div class="title"><?= ($arItem->prop("SALENAME")->hasValue() ? $arItem->prop("SALENAME")->value() : ($arItem->prop("MODEL")->hasValue() ? $arItem->prop("MODEL")->value() : $arItem->name)) ?></div>
            </a>
        </div>
        <?
        if (self::$arParams['UNWRAP'] !== true) {
            ?>
            </div>
            <?
        }
    }

    private static function getImages(Item $arItem, $length = 2)
    {
        $images = array();
        $sorts = array();
        global $version;
        foreach ($arItem->offers as $offer) {
            if (!$images[$offer->PROPERTIES['COLOR']['VALUE']]) {
                $sorts[$offer->PROPERTIES['COLOR']['VALUE']] = $offer->sort;
                $cache = \Bitrix\Main\Data\Cache::createInstance();
                $result = null;
                if ($cache->initCache(86400, $offer->PROPERTIES['COLOR']['VALUE'],
                  "/colors/" . ($version == 3 ? '' : 'v' . $version))) {
                    $result = $cache->getVars();
                } elseif ($cache->startDataCache()) {
                    $rs = \CIBlockElement::GetList(array(), array("ID" => $offer->PROPERTIES['COLOR']['VALUE']), false,
                      false,
                      array("PROPERTY_ICON" . ($version == 3 ? '' : '_SQUARE')))->Fetch();
                    if (!$rs['PROPERTY_ICON' . ($version == 3 ? '' : '_SQUARE') . '_VALUE']) {
                        $cache->abortDataCache();
                    } else {
                        $result = \CFile::GetPath($rs['PROPERTY_ICON' . ($version == 3 ? '' : '_SQUARE') . '_VALUE']);
                        $cache->endDataCache($result);
                    }
                }
                $photos = array();
                foreach (array_slice($offer->prop("PHOTO")->value(), 0, $length) as $id) {
                    $photos[] = self::resize($id, 534, 710);
                }
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['icon'] = $result ? $result : $photos[0];
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['icon_property'] = $result ? true : false;
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['color'] = $offer->PROPERTIES['COLOR']['VALUE'];
                $images[$offer->PROPERTIES['COLOR']['VALUE']]['images'] = $photos;
            }
        }
        array_multisort($sorts, SORT_ASC, $images);

        return $images;
    }
}
