<?php

namespace Product;

use CFile;

abstract class Base
{
  public static $arParams;

  static public function resize($id, $width, $height)
  {
    $resize = CFile::ResizeImageGet($id, array(
      'width' => $width,
      'height' => $height
    ));

    return $resize['src']?:SITE_TEMPLATE_PATH."/images/no-image.png";
  }

  static public function setParams($arParams)
  {
    self::$arParams = $arParams;
  }

  static public function clearParams()
  {
    self::$arParams = array();
  }
}