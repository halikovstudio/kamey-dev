<?php


if (!function_exists("testTemplate")) {
    function testTemplate()
    {
        return false;
    }
}

use Bitrix\Main\Application;
use Bitrix\Main\UserConsent\Consent;
use Bitrix\Main\UserConsent\Internals\ConsentTable;
use Bitrix\Main\Web\Cookie;
use Bitrix\Sale\Order;
use morphos\Russian\NounPluralization;
use Sale\Handlers\Delivery\AdditionalHandler;

class SpeedDebug
{
    public static $begin = array();
    public static $beginFile = array();
    public static $debug = true;
    public static $execute = false;
    public static $data = array();
    public static $subData = array();
    public static $uid = array();
    public static $temp = array();

    public static function begin($name = null)
    {
        if (self::$debug) {
            $uid = $name ? $name : randString(4);
            if (in_array($uid, self::$uid)) {
                self::$temp[$uid] += 1;
                $uid .= ':' . self::$temp[$uid];
            }
            self::$uid[] = $uid;
            self::$begin[$uid] = microtime(true);
            $bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
            $caller = array_shift($bt);
            self::$beginFile[$uid] = str_replace($_SERVER['DOCUMENT_ROOT'], "",
              $caller['file'] . ':' . $caller['line']);
        }
    }

    public static function end($printImmediate = false, $return = false)
    {
        if (self::$debug) {
            $last = count(self::$uid) - 1;
            $uid = self::$uid[$last];
            if ($uid) {
                $time = microtime(true) - self::$begin[$uid];
                $result = sprintf('%.4F', $time);
                $bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
                $caller = array_shift($bt);
                $endFile = str_replace($_SERVER['DOCUMENT_ROOT'], "", $caller['file'] . ':' . $caller['line']);
                $table = array(
                  "begin" => self::$beginFile[$uid],
                  "end" => $endFile,
                  'time' => (float)$result,
                  "exclude" => $last != 0
                );
                self::$data[$uid] = $table;
                unset(self::$uid[$last]);
                self::$uid = array_values(self::$uid);
                if ($printImmediate === true && $return === false) {
                    echo "<pre>";
                    print_r($table);
                    echo "</pre>";
                } elseif ($printImmediate === true && $return === true) {
                    return print_r($table, true);
                } elseif ($printImmediate === false && $return === true) {
                    return $table;
                }
                if (!self::$execute && !$printImmediate) {
                    AddEventHandler("main", "OnEndBufferContent", array("SpeedDebug", "execute"));
                    self::$execute = true;
                }

                return true;
            }
        }

        return false;
    }

    public static function add()
    {
        self::$subData[] = func_get_args();
    }

    public static function execute()
    {
        if (self::$debug) {
            $total = 0;
            foreach (self::$data as $table) {
                if (!$table['exclude']) {
                    $total += $table['time'];
                }
            }
            self::$data['total'] = array(
              'time' => $total
            );
            ?>
            <script id="speed-table">
              console.table(<?=json_encode(self::$data)?>, ['begin', 'end', 'time']);
              <?
              if (self::$subData) {
              ?>
              console.log(<?=json_encode(self::$subData)?>);
              <?
              }
              ?>
            </script>
            <?
        }
    }
}

if ($_REQUEST['bxrand']) {
    SpeedDebug::$debug = false;
}
global $version;
$version = 4;

require('include/Dynamic.php');
require_once('include/Kint.class.bundle.php');
require('include/SocialMeta.php');
require('include/helpers.php');
require('include/events_handlers.php');
require('include/CallableHighload.php');
require('include/userAddressesData.php');
require('include/orgAddressesData.php');
require('Bitrix24Component.php');
require('include/B24Sync.php');
require('include/SizeTable.php');

AddEventHandler("main", "OnBeforeEventAdd", array("EventChange", "OnBeforeEventAddHandler"));
AddEventHandler("main", "OnBeforeEventAdd", array("EventChange", "DeliverySet"));

AddEventHandler("main", "OnBeforeEventAdd", array("EventChange", "OnBeforeStatusEventAdd"));

class EventChange
{
    function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        if (in_array($event, array(
          'SALE_ORDER_CANCEL',
          'TEST'
        ))) {
            CModule::IncludeModule('sale');
            $order = CSaleOrder::GetByID($arFields['ORDER_REAL_ID']);
            $arFields['ACCOUNT_NUMBER'] = $order['ACCOUNT_NUMBER'];
        }
    }

    function OnBeforeStatusEventAdd(&$event, &$lid, &$arFields)
    {
        if (strpos($event, "SALE_STATUS_CHANGED") !== false) {
            CModule::IncludeModule('sale');
            $rsOrder = CSaleOrder::GetList(array(), array(
              'ACCOUNT_NUMBER' => $arFields['ORDER_ID']
            ), false, false, array('CANCELED'))->Fetch();
            if ($rsOrder['CANCELED'] == 'Y' || $GLOBALS['BREAK_SEND_SALE_STATUS_CHANGE'] === true) {
                $arFields['EMAIL'] = "";
                $arFields['BCC'] = "";
            }
        }
    }

    function DeliverySet(&$event, &$lid, &$arFields)
    {
        if (strpos($event, "SALE") === 0 || $event == 'ORDER_STATUS_CHANGED') {
            $orderID = null;
            if (!$orderID = $arFields['ORDER_REAL_ID']) {
                $orderID = $arFields['ORDER_ID'];
            }
            if (intval($orderID)) {
                $rsOrder = CSaleOrder::GetList(array(), array(
                  'ID' => $orderID
                ), false, false, array("PRICE_DELIVERY"))->Fetch();
                $address = CSaleOrderPropsValue::GetList(array(),
                  array('CODE' => 'ADDRESS', 'ORDER_ID' => $orderID))->Fetch();
                $location = CSaleOrderPropsValue::GetList(array(),
                  array('CODE' => 'LOCATION', 'ORDER_ID' => $orderID))->Fetch();
                $arLocs = CSaleLocation::GetByID($location['VALUE']);
                $list = array();
                $list[] = $arLocs['COUNTRY_NAME'];
                $list[] = $arLocs['REGION_NAME'];
                $list[] = $arLocs['CITY_NAME'];
                $list = implode(", ", array_filter($list));
                $arFields['DELIVERY_PRICE'] = $rsOrder['PRICE_DELIVERY'] == 0 ? 'бесплатно' : $rsOrder['PRICE_DELIVERY'] . ' руб.';
                $arFields['DELIVERY_ADDRESS'] = $address['VALUE'];
                $arFields['DELIVERY_LOCATION'] = $list;
            }
        }
    }

    function test(&$arOrder, $fields)
    {
        global $USER;
        if ($USER->IsAdmin()) {
            die(json_encode($fields));
        }
    }
}

AddEventHandler("main", "OnAfterEpilog", "ChangeHeaders");
function ChangeHeaders()
{
    header_remove("X-FRAME-OPTIONS");
    header_remove("x-frame-options");
}

require "delivery/events/include.php";
require "include/Element.php";
require "delivery/services/include.php";
require "delivery/cdek/include.php";
require "delivery/tracking/include.php";
require "delivery/restrictions/include.php";
require "payment/restrictions/include.php";
require "geolocation/include.php";
require "view/include.php";
require "include/Logger.php";
require "include/BonusCardTable.php";
require "include/BonusCard.php";
require "events/include.php";

require "include/location.php";
require "include/CatalogMirror.php";

if (!defined("ADMIN_SECTION")) {
    require "include/PageOptions.php";
    require "include/Options.php";
}

function pluralize($count, $word, $case = false, $number = true)
{
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/morphos/vendor/autoload.php';
    if ($case) {
        $word = morphos\Russian\NounDeclension::getCase($word, morphos\Russian\NounDeclension::IMENIT);
    }

    return $number ? morphos\Russian\pluralize($count, $word) : NounPluralization::pluralize($word, $count);
}

AddEventHandler("main", "OnAfterUserAuthorize", "favoriteCheck");
function favoriteCheck($arUser)
{
    $arUser = $arUser['user_fields'];
    global $APPLICATION, $request;
    $list = unserialize($request->getCookie("favorite")) ?: array();
    $rsUser = CUser::GetByID($arUser['ID'])->Fetch();
    if (!$rsUser['UF_TIMEZONE']) {
        $APPLICATION->set_cookie("detect_timezone", 'Y', 0);
    }
    if (!empty($list)) {
        $realList = $rsUser['UF_FAVORITE'] ?: array();
        $list = array_merge($realList, $list);
        global $USER;
        $USER->Update($arUser['ID'], array(
          "UF_FAVORITE" => $list
        ));
        $cookie = new Cookie("favorite", "");
        Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
    }
    if ($_SESSION['agreement_checked']) {
        Consent::addByContext(
          1
        );
        $_SESSION['agreement_checked'] = false;
    }
}

//if ($_REQUEST['closed'] == 'Y') {
//  COption::SetOptionString("main", "site_stopped", "Y");
//}

function testRequest()
{
    CModule::IncludeModule("sale");
    global $DB;
    $arFilter = Array(
      ">=DATE_INSERT" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), strtotime("2019-06-14")),
      "<=DATE_INSERT" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), strtotime("2019-06-15 23:59:59")),
    );

    $db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
    while ($ob = $db_sales->Fetch()) {
        $user_id = $ob['USER_ID'];
        if ($ob['CANCELED'] == 'N') {
            $order = Order::load($ob['ID']);
            $user = CUser::GetByID($user_id)->Fetch();
            if (!$user['UF_CARD_GUID']) {
                BonusCard::explode_name($user);
                $data = [
                  'PhoneNumber' => BonusCard::simplify_phone($user['PERSONAL_PHONE']),
                  'Name' => $user['NAME'],
                  'Surname' => $user['LAST_NAME'],
                  'Patronymic' => $user['SECOND_NAME'],
                  'Email' => $user['EMAIL'],
                  'StartBonuses' => 300,
                    //'Gender' => $this->data['gender'] == 'MALE' ? 'Male' : 'Female'
                ];
                $birthday = BonusCard::reverse_birthday($user['PERSONAL_BIRTHDAY']);
                if ($birthday) {
                    $data['Birthday'] = $birthday;
                }
                $result = BonusCard::request('CreateDiscountCard', $data);
                if ($result && $result['CardGUID']) {
                    if ($result['NewCard']) {
                        BonusCardTable::add([
                          'PhoneNumber' => BonusCard::simplify_phone($user['PERSONAL_PHONE']),
                          'CardGUID' => $result['CardGUID'],
                          'CardNumber' => $result['CardNumber']
                        ]);
                    }
                    $u = new CUser();
                    $u->Update($user['ID'], ['UF_CARD_GUID' => $result['CardGUID']]);
                } else {
                    var_dump(['error' => BonusCard::$error, 'order_id' => $ob['ID'],'data'=>$data]);
                }
            }
        }
    }
}

function agreementCheck($agreement_id = 1, $user_id = null)
{
    if (!CUser::IsAuthorized()) {
        return $_SESSION['agreement_checked'];
    }
    if (is_null($user_id)) {
        $user_id = CUser::GetID();
    }
    if (!$user_id) {
        return false;
    }

    return !!ConsentTable::getList([
      'filter' => [
        'USER_ID' => $user_id,
        'AGREEMENT_ID' => $agreement_id
      ]
    ])->fetch();
}

function send_sms_ext($phone, $message){
    $ch = curl_init("https://smsc.ru/sys/send.php?login=cameo&psw=NyKNXQO2e9ieDAD&phones=$phone&mes=$message&sender=CAMEO");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function send_bonus_card_registration_email($guid,$email){
    BonusCard::$guid = $guid;
    BonusCard::find();
    $jwt = BonusCard::jwt(true);
    CEvent::SendImmediate("BONUS_CARD_NEW", "s2", [
      "EMAIL" => $email,
      "GUID" => $guid,
      'NUMBER' => BonusCard::$number,
      "JWT" => $jwt
    ]);
}

AddEventHandler("sale", "OnCheckCollateDocuments", "OnCheckCollateDocuments");

function OnCheckCollateDocuments($entities)
{
    foreach ($entities as $entity)
    {
        if ($entity instanceof \Bitrix\Sale\Payment)
        {
            $order = $entity->getCollection()->getOrder();
            if ($order->isPaid())
            {
                $related = [];

//                foreach ($entity->getCollection() as $payment)
//                {
//                    if ($payment->getId() != $entity->getId())
//                    {
//                        $related[\Bitrix\Sale\Cashbox\Check::PAYMENT_TYPE_ADVANCE][] = $payment;
//                    }
//                }

                foreach ($order->getShipmentCollection() as $shipment)
                {
                    if (!$shipment->isSystem())
                    {
                        $related[\Bitrix\Sale\Cashbox\Check::SHIPMENT_TYPE_NONE][] = $shipment;
                    }
                }

                return new \Bitrix\Main\EventResult(
                  \Bitrix\Main\EventResult::SUCCESS,
                  [['TYPE' => 'fullprepayment', 'ENTITIES' => [$entity], 'RELATED_ENTITIES' => $related]]
                );
            }
            else
            {
                if ($entity->isPaid() && !$order->isPaid())
                {
                    return new \Bitrix\Main\EventResult(
                      \Bitrix\Main\EventResult::SUCCESS,
                      [['TYPE' => 'advancepayment', 'ENTITIES' => [$entity], 'RELATED_ENTITIES' => []]]
                    );
                }
            }
        }
    }
}

function __test(){
    CModule::IncludeModule('sale');
    CModule::IncludeModule('catalog');
    require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sale/handlers/delivery/additional/handler.php';
    $order = Order::load(3757);
    $shipmentCollection = $order->getShipmentCollection();
    $shipment = $shipmentCollection[0];
    $shipmentParams = AdditionalHandler::getShipmentParams($shipment,'RUSPOST');
    print_r($shipmentParams);
}
function generate_password($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0','.',',',
        '(',')','[',']','!','?',
        '&','^','%','@','*','$',
        '<','>','/','|','+','-',
        '{','}','`','~');
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}

function getAccessCode($phone){
    $iblockCode = 100;
    $arSelect = ["ID", "NAME", "CODE", "XML_ID", "TIMESTAMP_X"];
    $arFilter = [
        "IBLOCK_ID" => $iblockCode,
        "NAME" => $phone
    ];
    $exist      = CIBlockElement::GetList(['ID'=>'DESC'], $arFilter, false, ["nTopCount" => 1], $arSelect)->Fetch();
    return $exist;
}

