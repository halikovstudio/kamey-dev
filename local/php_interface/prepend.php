<?php
if (function_exists("getallheaders")) {
  $headers = getallheaders();
} else {
  $headers = array();
}


//mail('it@halikov-studio.ru','data', print_r($_SERVER, true));
if ($_SERVER['SCRIPT_URL'] == '/bitrix/admin/1c_exchange.php' && $_GET['type'] == 'crm') {
    require $_SERVER['DOCUMENT_ROOT'].'/local/admin/1c_exchange.php';
    die();
}

//if ($_SERVER['REMOTE_ADDR'] == '188.234.148.65' || $_SERVER['REMOTE_ADDR'] == '92.50.158.6') {
//    $_REQUEST['version'] = 4;
//    $_GET['ncc'] = 1;
//}

include_once "cityReplace.php";
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  if ($_GET['sef_path']) {
    $query = array();
    foreach ($_GET as $key => $value) {
      if ($key == "sef_path") continue;
      $query[] = $key . "=" . $value;
    }
    $_SERVER['REQUEST_URI'] = $_GET['sef_path'] . (!empty($query) ? "?" . implode("&", $query) : "");
  }
  $locationId = $_COOKIE['BITRIX_CAMEO_locationId'];
  if (!$locationId && $_COOKIE['BITRIX_CAMEO_location']) {
    $data = json_decode($_COOKIE['BITRIX_CAMEO_location'], true);
    $locationId = $data['id'];
    setcookie("BITRIX_CAMEO_locationId", $locationId, time() + 60 * 60 * 24 * 30 * 12, "/");
    $_COOKIE['BITRIX_CAMEO_locationId'] = $locationId;
  }
  $url = $_SERVER['REQUEST_URI'];
  $extendComposite = strpos($url, "/catalog") === 0 || $_SERVER['SCRIPT_URL'] === '/shops/';
  if (!isset($_GET['bxrand']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    $parsedUrl = parse_url($url);
    $query = "";
    if ($extendComposite) {
      $_GET['locationId'] = $_REQUEST['locationId'] = $locationId;
      $query = "locationId=$locationId";
    }
    if ($_COOKIE['BITRIX_TEMPLATE']) {
      $query .= "&templateId=" . $_COOKIE['BITRIX_TEMPLATE'];
    }
    if ($parsedUrl['path'] == null) {
      $url .= '/';
    }
    $separator = ($parsedUrl['query'] == null) ? '?' : '&';
    if ($query) {
      $url .= $separator . $query;
    }
    $GLOBALS['TEMP_URL'] = $url;
    $_SERVER['REQUEST_URI'] = $url;
  }
  if (!isset($_COOKIE['BITRIX_CAMEO_locationId']) && $extendComposite) {
    $_GET['ncc'] = 1;
  }
  if ($_GET['test']) {
    die();
  }
}

define("DevMode", ($headers['DevMode'] || $_SERVER['DevMode']) ? true : $_GET['v3template'] == 'true');
function testTemplate()
{
  return $_COOKIE['BITRIX_TEMPLATE'] == 'v3' || DevMode || $_GET['v3template'] == 'true' && ($_SERVER['SERVER_NAME'] == 'www.kamey.ru' || $_SERVER['SERVER_NAME'] == 'kamey.ru');
}

if (DevMode) {
  $_GET['ncc'] = 1;
  $_REQUEST['ncc'] = 1;
  $_GET['v3template'] = "true";
  $_REQUEST['v3template'] = "true";
  if ($_GET['ip']) {
    $_SERVER['GEOIP_ADDR'] = $_GET['ip'];
    $_SERVER['HTTP_X_FORWARDED_FOR'] = $_GET['ip'];
    $_SERVER['HTTP_X_REAL_IP'] = $_GET['ip'];
    $_SERVER['HTTP_GEOIP_ADDR'] = $_GET['ip'];
    $_SERVER['REMOTE_ADDR'] = $_GET['ip'];
  }
}
