<?
ini_set('display_errors', 'On');
error_reporting(E_ERROR | E_PARSE);

CModule::IncludeModule("highloadblock");

use Bitrix\Highloadblock as HL;

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementUpdateHandler");
//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "LogProductChanges");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "CheckMorePhoto");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementUpdateHandler");
function LogProductChanges($arFields)
{
    $catalogIBlockID = getIBlockID("catalog");
    if ($arFields['IBLOCK_ID'] == $catalogIBlockID) {
        //Logger::debug($arFields);
    }
}

function getProductForPhotoApply($id, $catalogIBlockID)
{
    if (!isset($GLOBALS['photoApplyCache'][$catalogIBlockID][$id])) {
        $GLOBALS['photoApplyCache'][$catalogIBlockID][$id] = CIBlockElement::GetList(
          array(),
          array(
            'ID' => $id,
            'IBLOCK_ID' => $catalogIBlockID,
            'ACTIVE' => 'Y'
          ), false, false,
          array(
            'ID',
            'NAME',
            'DETAIL_PICTURE',
            'PROPERTY_MORE_PHOTO',
            'PROPERTY_MINIMUM_PRICE_2',
            'PROPERTY_MAXIMUM_PRICE_2',
            'PROPERTY_CARE'
          )
        )->GetNext();
    }

    return $GLOBALS['photoApplyCache'][$catalogIBlockID][$id];

    return CIBlockElement::GetList(
      array(),
      array(
        'ID' => $id,
        'IBLOCK_ID' => $catalogIBlockID,
        'ACTIVE' => 'Y'
      ), false, false,
      array(
        'ID',
        'NAME',
        'DETAIL_PICTURE',
        'PROPERTY_MORE_PHOTO',
        'PROPERTY_MINIMUM_PRICE_2',
        'PROPERTY_MAXIMUM_PRICE_2'
      )
    )->GetNext();
}

function getHighloadEntity($id)
{
    if (!isset($GLOBALS['highloadCache'][$id])) {
        $hlBlock = HL\HighloadBlockTable::getById($id)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

        $entityDataClass = $entity->getDataClass();
        $entityTableName = $hlBlock['TABLE_NAME'];

        $sTableID = 'tbl_' . $entityTableName;

        $GLOBALS['highloadCache'][$id] = array(
          "class" => $entityDataClass,
          'tableId' => $sTableID,
          'tableName' => $entityTableName
        );
    }

    return $GLOBALS['highloadCache'][$id];
}

function OnBeforeIBlockElementUpdateHandler(&$arFields)
{
    $deactivate = true;

    $catalogIBlockID = getIBlockID('catalog');
    $offersIBlockID = getIBlockID('offers');

    $clearCatalogIBlockID = getIBlockID("catalog-clear");
    $clearOffsersIBlockID = getIBlockID("offers-clear");

    if (in_array($arFields['IBLOCK_ID'], array($clearCatalogIBlockID, $clearOffsersIBlockID))) {
        $catalogIBlockID = $clearCatalogIBlockID;
        $offersIBlockID = $clearOffsersIBlockID;
        $deactivate = false;
    }

    $colorAssocIBlockID = getIBlockID('colors_matching');
    $clothesIBlockID = getIBlockID('CLOTHES');

    CModule::IncludeModule('iblock');

    global $USER;

    switch ($arFields['IBLOCK_ID']) {
        case $catalogIBlockID:
            //Logger::debug("BeforeUpdate - " . $arFields['NAME'] . ' [' . $arFields['ID'] . ']');
            $modelPropId = getIBlockPropertyID($catalogIBlockID, 'MODEL');
            $attributesPropId = getIBlockPropertyID($catalogIBlockID, 'CML2_ATTRIBUTES');
            $morePhotoPropId = getIBlockPropertyID($catalogIBlockID, 'MORE_PHOTO');

            if ($arFields['NAME']) {
                $arFields['PROPERTY_VALUES'][$modelPropId]['VALUE'] = getModelNameByProductName($arFields['NAME']);
            }

            if ($arFields['PROPERTY_VALUES'][$attributesPropId]) {
                unset($arFields['PROPERTY_VALUES'][$attributesPropId]);
            }

            if ($arFields['PROPERTY_VALUES'][$morePhotoPropId] && 0) {
                if (defined("LOG_FILENAME")) {
                    $fh = fopen(LOG_FILENAME, 'w');
                    fclose($fh);
                }

                //TODO replace default description to ID
                if (!$GLOBALS['MORE_PHOTO_CACHE']) {
                    $GLOBALS['MORE_PHOTO_CACHE'] = array();
                }
                foreach ($arFields['PROPERTY_VALUES'][$morePhotoPropId] as $k => $item) {
                    if (!$item['DESCRIPTION']) {
                        continue;
                    }
                    preg_match_all("/\[(.*?)\]/", $item['DESCRIPTION'], $matches);
                    $matches = $matches[1];
                    $description = array();
                    //region format description
                    $break = false;
                    foreach ($matches as $key => $match) {
                        switch ($key) {
                            case 0:
                                if ($GLOBALS['MORE_PHOTO_CACHE'][$match]) {
                                    $description = $GLOBALS['MORE_PHOTO_CACHE'][$match];
                                } else {
                                    $hlBlock = HL\HighloadBlockTable::getById('14')->fetch();
                                    $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

                                    $entityDataClass = $entity->getDataClass();
                                    $entityTableName = $hlBlock['TABLE_NAME'];

                                    $sTableID = 'tbl_' . $entityTableName;

                                    $rsData = $entityDataClass::getList(array(
                                      "select" => array('ID'),
                                      "filter" => array(
                                        'UF_NAME' => "%[" . $match . "]%"
                                      ),
                                        // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                                      "order" => array("UF_SORT" => "ASC")
                                    ));

                                    $rsData = new CDBResult($rsData, $sTableID);
                                    while ($hlRow = $rsData->Fetch()) {
                                        $description[] = "[$hlRow[ID]]";
                                    }
                                    $GLOBALS['MORE_PHOTO_CACHE'][$match] = $description;
                                }
                                if (empty($description)) {
                                    $break = true;
                                }
                                break;
                            case 1:
                                if ($match == '-') {
                                    foreach ($description as $i => $value) {
                                        $description[$i] .= "[-]";
                                    }
                                } else {
                                    if ($GLOBALS['MORE_PHOTO_CACHE'][$match]) {
                                        $hlRow = $GLOBALS['MORE_PHOTO_CACHE'][$match];
                                    } else {
                                        $hlBlock = HL\HighloadBlockTable::getById('15')->fetch();
                                        $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

                                        $entityDataClass = $entity->getDataClass();
                                        $entityTableName = $hlBlock['TABLE_NAME'];

                                        $sTableID = 'tbl_' . $entityTableName;

                                        $rsData = $entityDataClass::getList(array(
                                          "select" => array('ID'),
                                          "filter" => array(
                                            'UF_NAME' => "%[" . $match . "]%"
                                          ),
                                            // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                                          "order" => array("UF_SORT" => "ASC")
                                        ));

                                        $rsData = new CDBResult($rsData, $sTableID);
                                        $hlRow = $rsData->Fetch();
                                        $GLOBALS['MORE_PHOTO_CACHE'][$match] = $hlRow;
                                    }
                                    foreach ($description as $i => $value) {
                                        $description[$i] .= "[$hlRow[ID]]";
                                    }
                                }
                                break;
                            case 2:
                                if ($match == '-') {
                                    foreach ($description as $i => $value) {
                                        $description[$i] .= "[-]";
                                    }
                                } else {
                                    if ($GLOBALS['MORE_PHOTO_CACHE'][$match]) {
                                        $hlRow = $GLOBALS['MORE_PHOTO_CACHE'][$match];
                                    } else {
                                        $hlBlock = HL\HighloadBlockTable::getById('13')->fetch();
                                        $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

                                        $entityDataClass = $entity->getDataClass();
                                        $entityTableName = $hlBlock['TABLE_NAME'];

                                        $sTableID = 'tbl_' . $entityTableName;

                                        $rsData = $entityDataClass::getList(array(
                                            //выбираем все поля
                                          "select" => array('ID'),
                                          "filter" => array(
                                            'UF_NAME' => "%[" . $match . "]%"
                                          ),
                                            // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                                          "order" => array("UF_SORT" => "ASC")
                                        ));

                                        $rsData = new CDBResult($rsData, $sTableID);
                                        $hlRow = $rsData->Fetch();
                                        $GLOBALS['MORE_PHOTO_CACHE'][$match] = $hlRow;
                                    }
                                    foreach ($description as $i => $value) {
                                        $description[$i] .= "[$hlRow[ID]]";
                                    }
                                }
                                break;
                            case 3:
                                foreach ($description as $i => $value) {
                                    $description[$i] .= " [$match]";
                                }
                                break;
                        }
                        if ($break) {
                            break;
                        }
                    }
                    if (!empty($description)) {
                        $description = implode("", $description);
                    }
                    //endregion
                }
            }

            break;

        case $offersIBlockID:
            $offerMainClothPropertyId = getIBlockPropertyID($offersIBlockID, 'OSNOVNAYA_TKAN');
            $offerFinishClothPropertyId = getIBlockPropertyID($offersIBlockID, 'TKAN_OTDELKI');
            $offerPantsClothPropertyId = getIBlockPropertyID($offersIBlockID, 'TKAN_BRYUK');
            $offerToProductLinkPropertyId = getIBlockPropertyID($offersIBlockID, 'CML2_LINK');
            $offerPicturesPropertyId = getIBlockPropertyID($offersIBlockID, 'PHOTO');
            $offerMainColorPropertyId = getIBlockPropertyID($offersIBlockID, 'COLOR_MAIN');

            $offerUserColorPropertyId = getIBlockPropertyID($offersIBlockID, 'COLOR');
            $offerClothCompositionPropertyId = getIBlockPropertyID($offersIBlockID, 'SOSTAV_TKANI');

            $propertyValuesExist = !empty($arFields['PROPERTY_VALUES']);

            $productParams = CCatalogProduct::GetByID($arFields['ID']);

            //все изображения продукта id => описание
            $productPictures = array();
            //code картинки для торгового предложения вида [clothCode][clothCode][clothCode]
            $offerPictureCode = '';
            $offerPictureCodeId = '';
            //новые картинки предложения
            $newOfferPictures = array();
            $hasQuantity = $productParams['QUANTITY'] > 0;
            if (isset($productParams['QUANTITY']) && $deactivate) {
                if ($productParams['QUANTITY'] <= 0) {
                    $arFields['ACTIVE'] = 'N';
                } elseif ($productParams['QUANTITY'] > 0) {
                    $arFields['ACTIVE'] = 'Y';
                }
                //Logger::debug("quantityIsset", $arFields);
            }

            if ($propertyValuesExist) {
                $offerMainClothXmlId = reset($arFields['PROPERTY_VALUES'][$offerMainClothPropertyId]);
                $offerMainClothXmlId = $offerMainClothXmlId['VALUE'];
                $offerFinishClothXmlId = reset($arFields['PROPERTY_VALUES'][$offerFinishClothPropertyId]);
                $offerFinishClothXmlId = $offerFinishClothXmlId['VALUE'];
                $offerPantsClothXmlId = reset($arFields['PROPERTY_VALUES'][$offerPantsClothPropertyId]);
                $offerPantsClothXmlId = $offerPantsClothXmlId['VALUE'];
                $offerColorAssocValue = reset($arFields['PROPERTY_VALUES'][$offerUserColorPropertyId]);
                $offerColorAssocValue = $offerColorAssocValue['VALUE'];

                $offerMainColorValue = reset($arFields['PROPERTY_VALUES'][$offerMainColorPropertyId]);
                $offerMainColorValue = $offerMainColorValue['VALUE'];

                $productId = reset($arFields['PROPERTY_VALUES'][$offerToProductLinkPropertyId]);
                $productId = $productId['VALUE'];

                //region Соответствия цветов
                //фильтр для запроса соответстия цветов
                $colorAssocFilter = array(
                  'ACTIVE' => 'Y',
                  'IBLOCK_ID' => $colorAssocIBlockID,
                );

                if ($offerMainClothXmlId) {
                    $colorAssocFilter['PROPERTY_MAIN_CLOTH'] = $offerMainClothXmlId;
                } else {
                    $colorAssocFilter['PROPERTY_MAIN_CLOTH'] = false;
                }

                if ($offerFinishClothXmlId) {
                    $colorAssocFilter['PROPERTY_DECALS_CLOTH'] = $offerFinishClothXmlId;
                } else {
                    $colorAssocFilter['PROPERTY_DECALS_CLOTH'] = false;
                }

                if ($offerPantsClothXmlId) {
                    $colorAssocFilter['PROPERTY_PANTS_CLOTH'] = $offerPantsClothXmlId;
                } else {
                    $colorAssocFilter['PROPERTY_PANTS_CLOTH'] = false;
                }

                $colorAssoc = CIBlockElement::GetList(
                  array(),
                  $colorAssocFilter,
                  false,
                  false,
                  array('ID', 'PROPERTY_MAIN_CLOTH', 'PROPERTY_PANTS_CLOTH', 'PROPERTY_DECALS_CLOTH')
                )->GetNext();

                if ($colorAssoc) {
                    if ($colorAssoc['ID'] != $offerColorAssocValue) {
                        $arFields['PROPERTY_VALUES'][$offerUserColorPropertyId]['VALUE'] = $colorAssoc['ID'];
                    }
                } elseif ($offerMainClothXmlId) {
                    //если не найдено, то добавляем новое соответствие цвета
                    if ($GLOBALS['iblockElementInstance']) {
                        $iblockElement = $GLOBALS['iblockElementInstance'];
                    } else {
                        $iblockElement = new CIBlockElement();
                        $GLOBALS['iblockElementInstance'] = $iblockElement;
                    }

                    $assocMainClothPropertyId = getIBlockPropertyID($colorAssocIBlockID, 'MAIN_CLOTH');
                    $assocFinishClothPropertyId = getIBlockPropertyID($colorAssocIBlockID, 'DECALS_CLOTH');
                    $assocPantsClothPropertyId = getIBlockPropertyID($colorAssocIBlockID, 'PANTS_CLOTH');

                    $newAssocColorElId = $iblockElement->Add(array(
                      "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
                      "IBLOCK_ID" => $colorAssocIBlockID,
                      "PROPERTY_VALUES" => array(
                        $assocMainClothPropertyId => $offerMainClothXmlId,
                        $assocFinishClothPropertyId => $offerFinishClothXmlId,
                        $assocPantsClothPropertyId => $offerPantsClothXmlId
                      ),
                      "NAME" => " ",
                      "ACTIVE" => "Y",
                    ));

                    if ($newAssocColorElId) {
                        $arFields['PROPERTY_VALUES'][$offerUserColorPropertyId]['VALUE'] = $newAssocColorElId;
                    }
                }
                if (!$offerMainColorValue) {
                    $colorList = array();
                    $cache = Bitrix\Main\Data\Cache::createInstance();

                    if ($cache->initCache(86400, $offerColorAssocValue, "/color/")) {
                        $colorList = $cache->getVars();
                    } elseif ($cache->startDataCache()) {
                        $colorList = array();
                        $rs = CIBlockElement::GetList(array(), array("ID" => $offerColorAssocValue), false, false,
                          array("PROPERTY_COLOR", "ID"))->Fetch();
                        if ($rs['PROPERTY_COLOR_VALUE']) {
                            $colorList = $rs['PROPERTY_COLOR_VALUE'];
                            $cache->endDataCache($colorList);
                        } else {
                            $cache->abortDataCache();
                        }
                    }
                    if ($colorList) {
                        foreach ($colorList as $k => $colorValue) {
                            $arFields['PROPERTY_VALUES'][$offerMainColorPropertyId]["n" . ($k + 1)] = array(
                              "VALUE" => $colorValue
                            );
                        }
                    }
                }

                if (!$GLOBALS['colorCode'][$offerColorAssocValue]) {
                    $_rs = CIBlockElement::GetList([], ['ID' => $offerColorAssocValue], false, false,
                      ['CODE'])->Fetch();
                    $GLOBALS['colorCode'][$offerColorAssocValue] = $_rs['CODE'] ? $_rs['CODE'] : $offerColorAssocValue;
                }

                $arFields['CODE'] = $GLOBALS['colorCode'][$offerColorAssocValue];

                //endregion

                //region Формирование картинок
                if ($productId) {
                    $product = getProductForPhotoApply($productId, $catalogIBlockID);

                    if ($product) {
                        if (!empty($product['PROPERTY_MORE_PHOTO_VALUE'])) {
                            foreach ($product['PROPERTY_MORE_PHOTO_VALUE'] as $key => $pictureId) {
                                $productPictures[$pictureId] = $product['PROPERTY_MORE_PHOTO_DESCRIPTION'][$key];
                            }
                        }

                        if ($product['DETAIL_PICTURE']) {
                            $detailPictureInfo = CFile::GetFileArray($product['DETAIL_PICTURE']);

                            if (!in_array($detailPictureInfo['DESCRIPTION'], $productPictures) && $detailPictureInfo) {
                                $productPictures[$detailPictureInfo['ID']] = $detailPictureInfo['DESCRIPTION'];
                            }
                        }

                        if ($offerMainClothXmlId) {
                            //получение основной ткани из HL блока
                            $highLoadData = getHighloadEntity(14);
                            $entityDataClass = $highLoadData['class'];
                            $sTableID = $highLoadData['tableId'];

                            $rsData = $entityDataClass::getList(array(
                              "select" => array('*'),
                              "filter" => array(
                                'UF_XML_ID' => $offerMainClothXmlId
                              ),
                                // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                              "order" => array("UF_SORT" => "ASC")
                            ));

                            $rsData = new CDBResult($rsData, $sTableID);
                            $hlRow = $rsData->Fetch();
                            $offerPictureCode .= getClothCode($hlRow['UF_NAME']);
                            $offerPictureCodeId .= "[" . $hlRow['ID'] . "]";
                            $offerClothName = getClothName($hlRow['UF_NAME']);

                            $cloth = CIBlockElement::GetList(array(),
                              array('IBLOCK_ID' => $clothesIBlockID, 'NAME' => $offerClothName), false, false,
                              array('ID', 'PROPERTY_FABRIC_COMPOSITION'))->GetNext();

                            if ($cloth['PROPERTY_FABRIC_COMPOSITION_VALUE']) {
                                $arFields['PROPERTY_VALUES'][$offerClothCompositionPropertyId] = $cloth['PROPERTY_FABRIC_COMPOSITION_VALUE'];
                            }
                            if ($cloth['ID'] && !$product['PROPERTY_CARE']) {
                                CIBlockElement::SetPropertyValuesEx($productId, $catalogIBlockID, array(
                                  "CARE" => $cloth['ID']
                                ));
                                $GLOBALS['photoApplyCache'][$catalogIBlockID][$productId]['PROPERTY_CARE'] = $cloth['ID'];
                            }
                        } else {
                            $offerPictureCode .= '[-]';
                            $offerPictureCodeId .= '[-]';
                        }

                        if ($offerFinishClothXmlId) {
                            //получение ткани отделки из HL блока
                            $highLoadData = getHighloadEntity(15);
                            $entityDataClass = $highLoadData['class'];
                            $sTableID = $highLoadData['tableId'];

                            $rsData = $entityDataClass::getList(array(
                              "select" => array('*'),
                              "filter" => array(
                                'UF_XML_ID' => $offerFinishClothXmlId
                              ),
                                // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                              "order" => array("UF_SORT" => "ASC")
                            ));

                            $rsData = new CDBResult($rsData, $sTableID);
                            $hlRow = $rsData->Fetch();

                            $offerPictureCode .= getClothCode($hlRow['UF_NAME']);
                            $offerPictureCodeId .= "[" . $hlRow['ID'] . ']';
                        } else {
                            $offerPictureCode .= '[-]';
                            $offerPictureCodeId .= "[-]";
                        }

                        if ($offerPantsClothXmlId) {
                            //получение ткани брюк из HL блока
                            $highLoadData = getHighloadEntity(13);
                            $entityDataClass = $highLoadData['class'];
                            $sTableID = $highLoadData['tableId'];

                            $rsData = $entityDataClass::getList(array(
                                //выбираем все поля
                              "select" => array('*'),
                              "filter" => array(
                                'UF_XML_ID' => $offerPantsClothXmlId
                              ),
                                // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                              "order" => array("UF_SORT" => "ASC")
                            ));

                            $rsData = new CDBResult($rsData, $sTableID);
                            $hlRow = $rsData->Fetch();

                            $offerPictureCode .= getClothCode($hlRow['UF_NAME']);
                            $offerPictureCodeId .= "[" . $hlRow['ID'] . "]";
                        } else {
                            $offerPictureCode .= '[-]';
                            $offerPictureCodeId .= '[-]';
                        }
                        //формирование картинок

                        if (!empty($productPictures)) {
                            //картинки торгового предложения из продукта (id значения => описание)
                            $offerPicturesFromProduct = array();
                            foreach ($productPictures as $pictureId => $description) {
                                if (strpos($description, $offerPictureCode) === false) {
                                    continue;
                                }

                                $offerPicturesFromProduct[$pictureId] = $description;

                                $newOfferPictures[] = array(
                                  'VALUE' => $pictureId,
                                  'DESCRIPTION' => $description
                                );
                            }

                            if (!empty($newOfferPictures)) {
                                $sortData = array();
                                $sortValue = null;
                                foreach ($newOfferPictures as $key => $picture) {
                                    preg_match_all("/\[([0-9]+)\]/", $picture['DESCRIPTION'], $matches);
                                    if (!$sortValue) {
                                        $sortValue = $matches[1][1];
                                    }

                                    $find = +$matches[1][0];
                                    $sortData[$key] = $find;
                                }
                                if ($sortValue) {
                                    $arFields['SORT'] = $sortValue;
                                }

                                array_multisort($sortData, SORT_ASC, $newOfferPictures);
                                $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId] = $newOfferPictures;
                            } else {
                                $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId] = false;
                                $arFields['ACTIVE'] = 'N';
                            }
                        } else {
                            $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId] = false;
                            $arFields['ACTIVE'] = 'N';
                        }
                    }
                    // Logger::debug($arFields['ID'],$offerPictureCode,$productPictures,$newOfferPictures);
                }
                //endregion
            }

            break;
    }
}

//AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementUpdateHandler");
//AddEventHandler("main", "OnFileDelete", array("Logger", "debug"));
//AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementUpdateHandler");

function updateOfferPictures($offerId)
{
    if ((int)$offerId === 0) {
        throw new Exception(__FUNCTION__ . ': Не передан ID торгового предложения.');

        return false;
    }

    CModule::IncludeModule('iblock');

    $catalogIBlockID = getIBlockID('catalog');
    $offersIBlockID = getIBlockID('offers');
    $offerPicturesPropertyId = getIBlockPropertyID($offersIBlockID, 'PHOTO');
    $arFields = array();

    $offer = CIBlockElement::GetList(
      array(),
      array('IBLOCK_ID' => $offersIBlockID, 'ID' => $offerId),
      false,
      false,
      array(
        'ID',
        'ACTIVE',
        'NAME',
        'DETAIL_PICTURE',
        'PROPERTY_PHOTO',
        'PROPERTY_OSNOVNAYA_TKAN',
        'PROPERTY_TKAN_OTDELKI',
        'PROPERTY_TKAN_BRYUK',
        'PROPERTY_CML2_LINK',
        'PROPERTY_COLOR'
      )
    )->GetNext();

    $offerMainClothXmlId = $offer['PROPERTY_OSNOVNAYA_TKAN_VALUE'];
    $offerFinishClothXmlId = $offer['PROPERTY_TKAN_OTDELKI_VALUE'];
    $offerPantsClothXmlId = $offer['PROPERTY_TKAN_BRYUK_VALUE'];
    $productId = $offer['PROPERTY_CML2_LINK_VALUE'];

    $product = CIBlockElement::GetList(
      array(),
      array(
        'ID' => $productId,
        'IBLOCK_ID' => $catalogIBlockID
      ),
      false, false,
      array(
        'ID',
        'NAME',
        'DETAIL_PICTURE',
        'PROPERTY_MORE_PHOTO'
      )
    )->GetNext();

    //все изображения продукта id => описание
    $productPictures = array();
    //code картинки для торгового предложения вида [clothCode][clothCode][clothCode]
    $offerPictureCode = '';
    //новые картинки предложения
    $newOfferPictures = array();

    if ($productId) {
        if ($offer) {
            if (!empty($product['PROPERTY_MORE_PHOTO_VALUE'])) {
                foreach ($product['PROPERTY_MORE_PHOTO_VALUE'] as $key => $pictureId) {
                    $productPictures[$pictureId] = $product['PROPERTY_MORE_PHOTO_DESCRIPTION'][$key];
                }
            }

            if ($product['DETAIL_PICTURE']) {
                $detailPictureInfo = CFile::GetFileArray($product['DETAIL_PICTURE']);

                if (!in_array($detailPictureInfo['DESCRIPTION'], $productPictures) && $detailPictureInfo) {
                    $productPictures[$detailPictureInfo['ID']] = $detailPictureInfo['DESCRIPTION'];
                }
            }

            if ($offerMainClothXmlId) {
                //получение основной ткани из HL блока
                $hlBlock = HL\HighloadBlockTable::getById('14')->fetch();
                $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

                $entityDataClass = $entity->getDataClass();
                $entityTableName = $hlBlock['TABLE_NAME'];

                $sTableID = 'tbl_' . $entityTableName;

                $rsData = $entityDataClass::getList(array(
                  "select" => array('*'),
                  "filter" => array(
                    'UF_XML_ID' => $offerMainClothXmlId
                  ),
                    // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                  "order" => array("UF_SORT" => "ASC")
                ));

                $rsData = new CDBResult($rsData, $sTableID);
                $hlRow = $rsData->Fetch();

                $offerPictureCode .= getClothCode($hlRow['UF_NAME']);
            } else {
                $offerPictureCode .= '[-]';
            }

            if ($offerFinishClothXmlId) {
                //получение ткани отделки из HL блока
                $hlBlock = HL\HighloadBlockTable::getById('15')->fetch();
                $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

                $entityDataClass = $entity->getDataClass();
                $entityTableName = $hlBlock['TABLE_NAME'];

                $sTableID = 'tbl_' . $entityTableName;

                $rsData = $entityDataClass::getList(array(
                  "select" => array('*'),
                  "filter" => array(
                    'UF_XML_ID' => $offerFinishClothXmlId
                  ),
                    // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                  "order" => array("UF_SORT" => "ASC")
                ));

                $rsData = new CDBResult($rsData, $sTableID);
                $hlRow = $rsData->Fetch();

                $offerPictureCode .= getClothCode($hlRow['UF_NAME']);
            } else {
                $offerPictureCode .= '[-]';
            }

            if ($offerPantsClothXmlId) {
                //получение ткани брюк из HL блока
                $hlBlock = HL\HighloadBlockTable::getById('13')->fetch();
                $entity = HL\HighloadBlockTable::compileEntity($hlBlock);

                $entityDataClass = $entity->getDataClass();
                $entityTableName = $hlBlock['TABLE_NAME'];

                $sTableID = 'tbl_' . $entityTableName;

                $rsData = $entityDataClass::getList(array(
                    //выбираем все поля
                  "select" => array('*'),
                  "filter" => array(
                    'UF_XML_ID' => $offerPantsClothXmlId
                  ),
                    // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                  "order" => array("UF_SORT" => "ASC")
                ));

                $rsData = new CDBResult($rsData, $sTableID);
                $hlRow = $rsData->Fetch();

                $offerPictureCode .= getClothCode($hlRow['UF_NAME']);
            } else {
                $offerPictureCode .= '[-]';
            }

            //формирование картинок
            if (!empty($productPictures)) {
                //картинки торгового предложения из продукта (id значения => описание)
                $offerPicturesFromProduct = array();

                foreach ($productPictures as $pictureId => $description) {
                    if (strpos($description, $offerPictureCode) === false) {
                        continue;
                    }

                    $offerPicturesFromProduct[$pictureId] = $description;
                }

                foreach ($offerPicturesFromProduct as $pictureId => $description) {
                    $newOfferPictures[] = array(
                      'VALUE' => $pictureId,
                      'DESCRIPTION' => $description
                    );
                }

                if (!empty($newOfferPictures)) {
                    $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId] = $newOfferPictures;
                } else {
                    $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId] = false;
                }
            } else {
                $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId] = false;
            }
            //Logger::debug("update photo to", $arFields['PROPERTY_VALUES'][$offerPicturesPropertyId], $offerId, $product);
            CIBlockElement::SetPropertyValuesEx($offerId, $offersIBlockID, $arFields['PROPERTY_VALUES']);
            $el = new CIBlockElement;

            if (empty($arFields['PROPERTY_VALUES'][$offerPicturesPropertyId])) {
                $el->Update($offerId, array('ACTIVE' => 'N'));
                //если появились фотки, но элемент не активен.
            } elseif ($offer['ACTIVE'] == 'N') {
                $el->Update($offerId, array('ACTIVE' => 'Y'));
            }
        }
    }
}

function OnAfterIBlockElementUpdateHandler(&$arFields)
{
    $catalogIBlockID = getIBlockID('catalog');
    $offersIBlockID = getIBlockID('offers');

    CModule::IncludeModule('iblock');

    switch ($arFields['IBLOCK_ID']) {
        case $catalogIBlockID:
            $modelPropId = getIBlockPropertyID($catalogIBlockID, 'MODEL');
            $attributesPropId = getIBlockPropertyID($catalogIBlockID, 'CML2_ATTRIBUTES');

//      $dbOffers = CIBlockElement::GetList(
//        array(),
//        array('IBLOCK_ID' => $offersIBlockID, 'PROPERTY_CML2_LINK' => $arFields['ID']),
//        false,
//        false,
//        array('ID', 'NAME', 'OSNOVNAYA_TKAN', 'TKAN_OTDELKI', 'TKAN_BRYUK')
//      );
//
//      Logger::debug("Attempt to update photo", $arFields['NAME'], $arFields['ID']);
//
//      while ($offer = $dbOffers->GetNext()) {
//        updateOfferPictures($offer['ID']);
//      }

            if ($arFields['NAME']) {
                $arFields['PROPERTY_VALUES'][$modelPropId]['VALUE'] = getModelNameByProductName($arFields['NAME']);
            }

            if ($arFields['PROPERTY_VALUES'][$attributesPropId]) {
                unset($arFields['PROPERTY_VALUES'][$attributesPropId]);
            }

            break;
    }
}

function CheckMorePhoto(&$arFields)
{
    $catalogIBlockID = getIBlockID("catalog");
    if ($arFields['IBLOCK_ID'] == $catalogIBlockID) {
        $morePhotoPropId = getIBlockPropertyID($catalogIBlockID, 'MORE_PHOTO');
        $morePhoto = $arFields['PROPERTY_VALUES'][$morePhotoPropId];
        $count = 0;
        $delete = 0;
        \Util\Logger::info("before", $morePhoto);
        foreach ($morePhoto as $item) {
            if ($item['del'] == 'Y' || $item['VALUE']['del'] == 'Y') {
                $delete++;
            } else {
                $count++;
            }
        }
        if ($count == $delete) {
            $rs = CIBlockElement::GetList(array(),
              array("ID" => $arFields['ID'], 'IBLOCK_ID' => $arFields['IBLOCK_ID']), false, false,
              array("PROPERTY_MORE_PHOTO"))->Fetch();
            $data = unserialize($rs['PROPERTY_MORE_PHOTO_VALUE']);
            $morePhoto = array();
            foreach ($data['ID'] as $key => $id) {
                $morePhoto[$id]['VALUE'] = array(
                  "name" => "",
                  "type" => "",
                  "tmp_name" => "",
                  "error" => 4,
                  "size" => 0,
                  "description" => $data['DESCRIPTION'][$key]
                );
                $morePhoto[$id]['DESCRIPTION'] = $data['DESCRIPTION'][$key];
                unset($morePhoto[$id]['VALUE']['del'], $morePhoto[$id]['del']);
            }
            $arFields['PROPERTY_VALUES'][$morePhotoPropId] = $morePhoto;
        }
        \Util\Logger::info("after", $morePhoto, $count, $delete);
    }
    if ($arFields['IBLOCK_ID'] == 88) {
        $morePhotoPropId = getIBlockPropertyID(88, 'MORE_PHOTO');
        $morePhoto = $arFields['PROPERTY_VALUES'][$morePhotoPropId];
        $rs = CIBlockElement::GetList(array(), array("ID" => $arFields['ID'], 'IBLOCK_ID' => $arFields['IBLOCK_ID']),
          false, false, array("PROPERTY_MORE_PHOTO"))->Fetch();
        if (CUser::GetLogin() == 'seo') {
            echo "<pre>";
            print_r($rs);
            echo "</pre>";
            echo "<pre>";
            print_r($morePhoto);
            echo "</pre>";
            die();
        }
    }
}

/*
AddEventHandler("catalog", "OnProductAdd", "OnProductUpdateHandler");
AddEventHandler("catalog", "OnProductUpdate", "OnProductUpdateHandler");

function OnProductUpdateHandler($id, $arFields) {

    //Если количество =0 тогда деактивируем товар
    if(isset($arFields["QUANTITY"]) && $arFields["QUANTITY"] == 0) {
        $el = new CIBlockElement;
        $arLoadProductArray = Array(
            $arFields["ACTIVE"] = "N"
        );
        $res = $el->Update($id, $arLoadProductArray);
    }
}
*/

//OnBeforeStoreProductUpdate - событие, вызываемое перед изменением записи в таблице остатков товара.

//AddEventHandler("catalog", "OnBeforeStoreProductUpdate", "OnBeforeStoreProductUpdateHandler");
//AddEventHandler("catalog", "OnBeforeStoreProductAdd", "OnBeforeStoreProductUpdateHandler");

function OnBeforeStoreProductUpdateHandler($id, $arFields)
{
    //Если количество =0 тогда деактивируем товар
    if (isset($arFields["AMOUNT"]) && $arFields["AMOUNT"] == 0) {
        $el = new CIBlockElement;
        $arLoadProductArray = array(
          $arFields["ACTIVE"] = "N"
        );
        $res = $el->Update($id, $arLoadProductArray);
    }
}

/*
 * Обработка обновления ИБ
 */
//AddEventHandler("iblock", "OnBeforeIBlockAdd", "OnBeforeIBlockUpdateHandler" );
//AddEventHandler("iblock", "OnBeforeIBlockUpdate", "OnBeforeIBlockUpdateHandler" );
//function OnBeforeIBlockUpdateHandler(&$arFields) {}

/*
 * Обработка изменения свойств ИБ
 */
AddEventHandler("iblock", "OnBeforeIBlockPropertyAdd", "OnBeforeIBlockPropertyUpdateHandler");
AddEventHandler("iblock", "OnBeforeIBlockPropertyUpdate", "OnBeforeIBlockPropertyUpdateHandler");
AddEventHandler("iblock", "OnBeforeIBlockPropertyUpdate", "OnBeforeIBlockPropertyUpdateHandlerSecond");

function OnBeforeIBlockPropertyUpdateHandler(&$arFields)
{
    $catalogIBlockID = getIBlockID('catalog');
    $offersIBlockID = getIBlockID("offers");
    $arFields['NAME'] = preg_replace("/^[0-9]+\s/", '', $arFields['NAME']);
    $arFields['CODE'] = preg_replace("/^_[0-9]_/", '', $arFields['CODE']);
    if (!$arFields['CODE']) {
        $arParams = array("replace_space" => "_", "replace_other" => "_");
        $arFields['CODE'] = (Cutil::translit($arFields['NAME'], "ru", $arParams));
        $arFields['CODE'] = strtoupper($arFields['CODE']);
    }
    if ($arFields['CODE'] == 'RAZMER_ODEZHDA') {
        $arFields['NAME'] = 'Размер';
    }

    if ($offersIBlockID == $arFields['IBLOCK_ID'] && $arFields['CODE'] == 'MORE_PHOTO' && 0) {
        $arFields['NAME'] = "Дополнительные фотографии";
        $arFields['PROPERTY_TYPE'] = "S";
    }

    switch ($arFields['IBLOCK_ID']) {
        case $catalogIBlockID:
        case $offersIBlockID:
            if ($arFields['CODE'] == 'RAZMER_ODEZHDA') {
                $arFields['NAME'] = 'Размер';
            }

            if ($arFields['CODE'] == 'ROST') {
                $arFields['NAME'] = 'Рост';
            }

            if ($arFields['CODE'] == 'OSNOVNAYA_TKAN') {
                $arFields['NAME'] = 'Основная ткань';
            }

            if ($arFields['CODE'] == 'TKAN_OTDELKI') {
                $arFields['NAME'] = 'Ткань отделки';
            }

            if ($arFields['CODE'] == 'TKAN_BRYUK') {
                $arFields['NAME'] = 'Ткань брюк';
            }

            $arFields['NAME'] = preg_replace("/^[0-9]+\s/", '', $arFields['NAME']);
        //AddMessage2Log($arFields);
        //$bannedProps = array('TKAN', 'RAZMER_GOLOVNOY_UBOR', 'RAZMER_OBUV', 'TSVET_OBUV', 'RAZMER');
//
        //if (in_array($arFields['CODE'], $bannedProps)) {
        //    global $APPLICATION;
        //    $APPLICATION->throwException("Отмена добавления неиспользуемых свойств импорта данных.");
        //    return false;
        //}

    }
}

//При добавлении цены заполняем свойства Минимальная и максимальная цена (нужно для сортировки по ним)
AddEventHandler("catalog", "OnPriceAdd", "DoIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "DoIBlockAfterSave");

function DoIBlockAfterSave($arg1, $arg2 = false)
{
    $ELEMENT_ID = false;
    $IBLOCK_ID = false;
    $OFFERS_IBLOCK_ID = false;
    $OFFERS_PROPERTY_ID = false;
    if (CModule::IncludeModule('currency')) {
        $strDefaultCurrency = CCurrency::GetBaseCurrency();
    }

    //Check for catalog event
    if (is_array($arg2) && $arg2["PRODUCT_ID"] > 0) {
        //Get iblock element
        $rsPriceElement = CIBlockElement::GetList(
          array(),
          array(
            "ID" => $arg2["PRODUCT_ID"],
          ),
          false,
          false,
          array("ID", "IBLOCK_ID")
        );
        if ($arPriceElement = $rsPriceElement->Fetch()) {
            $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
            if (is_array($arCatalog)) {
                //Check if it is offers iblock
                if ($arCatalog["OFFERS"] == "Y") {
                    //Find product element
                    $rsElement = CIBlockElement::GetProperty(
                      $arPriceElement["IBLOCK_ID"],
                      $arPriceElement["ID"],
                      "sort",
                      "asc",
                      array("ID" => $arCatalog["SKU_PROPERTY_ID"])
                    );
                    $arElement = $rsElement->Fetch();
                    if ($arElement && $arElement["VALUE"] > 0) {
                        $ELEMENT_ID = $arElement["VALUE"];
                        $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
                        $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
                        $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
                    }
                } elseif ($arCatalog["OFFERS_IBLOCK_ID"] > 0) {//or iblock which has offers
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
                    $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
                } else { //or it's regular catalog
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = false;
                    $OFFERS_PROPERTY_ID = false;
                }
            }
        }
    } elseif (is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0) {//Check for iblock event
        //Check if iblock has offers
        $arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
        if (is_array($arOffers)) {
            $ELEMENT_ID = $arg1["ID"];
            $IBLOCK_ID = $arg1["IBLOCK_ID"];
            $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
            $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
        }
    }
    if ($ELEMENT_ID) {
        //Compose elements filter
        if ($OFFERS_IBLOCK_ID) {
            $rsOffers = CIBlockElement::GetList(
              array(),
              array(
                "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                "PROPERTY_" . $OFFERS_PROPERTY_ID => $ELEMENT_ID,
              ),
              false,
              false,
              array("ID")
            );
            while ($arOffer = $rsOffers->Fetch()) {
                $arProductID[] = $arOffer["ID"];
            }

            if (!is_array($arProductID)) {
                $arProductID = array($ELEMENT_ID);
            }
        } else {
            $arProductID = array($ELEMENT_ID);
        }

        $minPrice = array();
        $maxPrice = array();
        //Get prices
        $rsPrices = CPrice::GetList(
          array(),
          array(
            "PRODUCT_ID" => $arProductID,
          )
        );
        while ($arPrice = $rsPrices->Fetch()) {
            if (CModule::IncludeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY']) {
                $arPrice["PRICE"] = CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"],
                  $strDefaultCurrency);
            }
            $PRICE = $arPrice["PRICE"];
            if (!$minPrice[$arPrice['CATALOG_GROUP_ID']] || $minPrice[$arPrice['CATALOG_GROUP_ID']] > $PRICE) {
                $minPrice[$arPrice['CATALOG_GROUP_ID']] = $PRICE;
            }
            if (!$maxPrice[$arPrice['CATALOG_GROUP_ID']] || $maxPrice[$arPrice['CATALOG_GROUP_ID']] < $PRICE) {
                $maxPrice[$arPrice['CATALOG_GROUP_ID']] = $PRICE;
            }
        }

        foreach ($minPrice as $priceId => $minPriceItem) {
            $fields['MINIMUM_PRICE_' . $priceId] = $minPriceItem;
            $fields['MAXIMUM_PRICE_' . $priceId] = $maxPrice[$priceId];
        }
        //PR($fields);
        //Save found minimal price into property
        CIBlockElement::SetPropertyValuesEx(
          $ELEMENT_ID,
          $IBLOCK_ID,
          $fields
        );
    }
}

//AddEventHandler("catalog", "OnSuccessCatalogImport1C", "OnSuccessCatalogImport1CHandlerTime");
//AddEventHandler("catalog", "OnSuccessCatalogImport1C", "OnSuccessCatalogImport1CHandler");
//AddEventHandler("catalog", "OnBeforeCatalogImport1C", "OnBeforeCatalogImport1CHandler");
//function OnSuccessCatalogImport1CHandler()
//{
//  CModule::IncludeModule('iblock');
//  $offersIBlockID = getIBlockIdByXmlId('catalog-aaf08ae3-17ee-44b2-ba47-3a77cd818268#');
//  $rs = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $offersIBlockID, '<=CATALOG_QUANTITY' => 0, 'ACTIVE' => 'Y'), false, false, array('ID'));
//  $el = new CIBlockElement();
//  while ($ob = $rs->Fetch()) {
//    $el->Update($ob['ID'], array(
//      'ACTIVE' => 'N'
//    ));
//  }
//}

//function OnBeforeCatalogImport1CHandler()
//{
//  $i = COption::GetOptionInt('catalog', 'import_counter', 0);
//  if ($i == 0 || $i >= 6) {
//    COption::SetOptionInt('catalog', 'import_counter', 1);
//    COption::SetOptionInt('catalog', 'import_min_time', time());
//  } else {
//    COption::SetOptionInt('catalog', 'import_counter', $i + 1);
//  }
//}
//
//function OnSuccessCatalogImport1CHandlerTime()
//{
//  $i = COption::GetOptionInt('catalog', 'import_counter', 0);
//  if ($i == 6) {
//    CModule::IncludeModule('iblock');
//    $time = COption::GetOptionInt('catalog', 'import_min_time');
//    $el = new CIBlockElement();
//    $rs = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 70, 'ACTIVE' => "Y", '<TIMESTAMP_X' => array(false, ConvertTimeStamp($time, 'FULL'))), false, false, array('ID'));
//    while ($ob = $rs->Fetch()) {
//      $el->Update($ob['ID'], array(
//        'ACTIVE' => 'N'
//      ));
//    }
//    OnSuccessCatalogImport1CHandler();
//  }
//}
