<?php

class orgAddressesData
{
  static $instance;
  static $prevent = false;
  public $userID;
  public $onError = null;
  /** @var CallableHighLoad */
  private $highload;
  private $list = array(
    "ID",
    "UF_USER",
    "UF_NAME",
    "UF_TIN",
    "UF_CHECKPOINT",
    "UF_MSRN",
    "UF_PERSON",
    "UF_PHONE",
    "UF_EMAIL",
    "UF_ADDRESS",
    "UF_ACTUAL_ADDRESS",
    "UF_DEFAULT",
      "UF_BANK_NAME",
      "UF_BIK",
      "UF_KS",
      "UF_RS"
  );

  function __construct()
  {
    global $USER;
    $this->highload = getHighload(17);
    $this->userID = CUser::IsAuthorized() ? CUser::GetID() : null;
    global $rootApi;
    /** @var Api $rootApi */
    $this->api = $rootApi;
    if (!$this->userID && $this->api) {
      $this->api->error("Необходима авторизация");
    }
//        if (!$this->api) {
//            throw new Error("Класс используется не в том контексте");
//        }
    $eventManager = \Bitrix\Main\EventManager::getInstance();

    $eventManager->addEventHandler('', 'OrganizationAddresesOnBeforeUpdate', array("orgAddressesData", "beforeCheck"));
    $eventManager->addEventHandler('', 'OrganizationAddresesOnBeforeAdd', array("orgAddressesData", "beforeCheck"));

    $eventManager->addEventHandler('', 'OrganizationAddresesOnAfterUpdate', array("orgAddressesData", "afterCheck"));
    $eventManager->addEventHandler('', 'OrganizationAddresesOnAfterAdd', array("orgAddressesData", "afterCheck"));
    self::$instance = $this;
  }

  public static function beforeCheck(\Bitrix\Main\Entity\Event $event)
  {
    /** @var Bitrix\Main\Entity\Base $entity */
    $entity = $event->getEntity();
    $result = new \Bitrix\Main\Entity\EventResult();
    $arFields = $event->getParameter("fields");
//        if (!$arFields['UF_MARK']) {
//            $result->addError(new \Bitrix\Main\Entity\FieldError($entity->getField("UF_MARK"), "Укажите марку"));
//        }
//        if (!$arFields['UF_MODEL']) {
//            $result->addError(new \Bitrix\Main\Entity\FieldError($entity->getField("UF_MODEL"), "Укажите модель"));
//        }
//        if (!$arFields['UF_USER']) {
//            $result->addError(new \Bitrix\Main\Entity\FieldError($entity->getField("UF_USER"), "Укажите пользователя"));
//        }
    return $result;
  }

  public static function afterCheck(\Bitrix\Main\Entity\Event $event)
  {
    if (!self::$prevent) {
      $arFields = $event->getParameter("fields");
      if (!$arFields['ID'] && $event->getParameter('id')) {
        $arFields['ID'] = $event->getParameter('id');
      }
      if ($arFields['UF_DEFAULT']) {
        $rs = self::$instance->highload->getList(array(
          "filter" => array(
            "!ID" => $arFields['ID'],
            "UF_USER" => self::$instance->userID,
            "UF_DEFAULT" => 1
          ),
          "select" => array(
            "ID"
          )
        ));
        self::$prevent = true;
        while ($ob = $rs->Fetch()) {
          self::$instance->update($ob['ID'], array(
            'default' => 0
          ));
        }
        self::$prevent = false;
      }
    }
  }

  function update($id, $data)
  {
    $this->checkItem($id);
    $data = array_merge(array(), $this->highload->getById($id)->Fetch(), $this->prepare($data));
    $result = $this->highload->update($id, $data);
    $this->error($result);
  }

  function checkItem($id)
  {
    $item = $this->highload->getList(array(
      "filter" => $this->prepare(array("id" => $id)),
      'select' => array('ID')
    ))->Fetch();
    if (!$item && $this->api) {
      $this->api->error("Недостаточно прав для модификации элемента");
    }
  }

  private function prepare($array = array(), $sanitize = true, $addUser = true)
  {
    if ($addUser) {
      $array['user'] = $this->userID;
    }
    foreach ($array as $key => $value) {
      if (strpos($key, "UF_") === false && !in_array($key, array('ID', 'id'))) {
        $array["UF_" . strtoupper($key)] = $value;
        unset($array[$key]);
      } elseif ($key == 'id') {
        $array['ID'] = $value;
        unset($array['id']);
      }
    }
    if ($sanitize) {
      foreach ($array as $key => $value) {
        if (!in_array($key, $this->list)) {
          unset($array[$key]);
        }
      }
    }

    return $array;
  }

  /**
   * @param \Bitrix\Main\Entity\AddResult $result
   */
  private function error($result)
  {
    if ($result->isSuccess() == false) {
      if (is_callable($this->onError)) {
        $func = $this->onError;
        $func();
      }
      if ($this->api) {
        $this->api->error(implode("\r\n", $result->getErrorMessages()));
      }
    }
  }

  public static function instance()
  {
    if (self::$instance) {
      return self::$instance;
    } else {
      return new orgAddressesData();
    }
  }

  function delete($id)
  {
    $this->checkItem($id);
    $result = $this->highload->delete($id);
    $this->error($result);
  }

  function add($data)
  {
    $data = $this->prepare($data);
    $result = $this->highload->add($data);
    $this->error($result);

    return $result;
  }

  function get()
  {
    $result = $this->highload->getList(array(
      "filter" => $this->prepare()
    ));
    $cars = array();
    while ($ob = $result->Fetch()) {
      $this->resultPrepare($ob);
      $cars[] = $ob;
    }

    return $cars;
  }

  private function resultPrepare(&$result)
  {
    $return = array();
    foreach ($result as $key => $item) {
      $key = strtolower(str_replace("UF_", "", $key));
      if ($key == 'default') {
        $item = (bool)$item;
      }
      if ($key == 'mark') {
        $return['langMark'] = $item;
      }
      $return[$key] = $item;
    }
    $result = $return;
  }

  function getById($id)
  {
    if (!is_numeric($id)) return [];
    $rs = $this->highload->getList(array(
      "filter" => $this->prepare(array(
        "id" => $id
      ))
    ));
    $result = $rs->Fetch();
    if ($result) {
      $this->resultPrepare($result);
    }

    return $result;
  }
}
