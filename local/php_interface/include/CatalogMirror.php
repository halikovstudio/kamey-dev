<?php

class CatalogMirror
{
    const IBLOCK_ID = 38;
    const OFFERS_IBLOCK_ID = 10;
    const DESTINATION_IBLOCK_ID = 100;
    const DESTINATION_OFFERS_IBLOCK_ID = 101;

    public static function copySections()
    {
        CModule::IncludeModule('iblock');
        $rsSections = CIBlockSection::GetList(['depth_level' => 'asc'], ['IBLOCK_ID' => self::IBLOCK_ID], false,
          ['ID', 'ACTIVE','NAME','DESCRIPTION','IBLOCK_SECTION_ID']);
        while ($arSection = $rsSections->Fetch()) {
            echo "<pre>";
            print_r($arSection);
            echo "</pre>";
        }
    }
}
