<?php

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\ObjectException;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Cashbox;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Bitrix\Sale\PaySystem\Manager;
use Bitrix\Sale\PaySystem\Service;
use Bitrix\Sale\Shipment;
use bonus\google\Loyalty;
use PKPass\PKPass;
use Rarus\Sms4b\Sms4bException;

class BonusCard
{
    const NOT_FOUND = 0;
    const NOT_AUTHORIZED = -1;
    const FIRST_TIME = 1;
    const ANOTHER_TIME = 2;
    const PAYMENT_ID = 6;
    public static $number = null;
    public static $guid = null;
    public static $phone = null;
    public static $error = null;
    public static $url = 'http://1c.kamey.ru/discount/hs/api/v1/';
    public static $name;
    public static $surname;
    public static $patronymic;
    public static $break_dates = [
      [
        'from' => '2019-06-14 00:00:00',
        'to' => '2019-06-18 23:59:59'
      ],
      [
        'from' => '2019-08-07 06:00:00',
        'to' => '2019-08-22 23:59:59'
      ]
    ];
    public static $amountDate;
    private static $cache = [
      'accumulated' => [],
      'next_accumulated' => []
    ];

    public static function findGuid($number)
    {
        $item = BonusCardTable::getList([
          'filter' => [
            'CardNumber' => $number
          ]
        ])->fetch();

        return $item['CardGUID'];
    }

    public static function loyalty_id()
    {
        return ISSUER_ID . '.' . LOYALTY_OBJECT_ID . '_' . self::$guid;
    }

    public static function pkpass()
    {
        $pass = new PKPass($_SERVER['DOCUMENT_ROOT'] . '/api/sdk/bonus/apple/cert.p12', '8jwPBdb3doq9');

        $data = self::pkpass_data();
        $pass->setData($data);

        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/logo_letter.jpg', 'icon.png');

        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/logo_for_wallet.png',
          'logo.png');
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/logo_for_wallet@2x.png',
          'logo@2x.png');
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/logo_for_wallet@3x.png',
          'logo@3x.png');
        $percent = self::percent();
        switch ($percent) {
            case 5:
            case 7:
            case 10:
            case 12:
                $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/strip_' . $percent . '.png',
                  'strip.png');
                $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/strip_' . $percent . '@2x.png',
                  'strip@2x.png');
                $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/strip_' . $percent . '@3x.png',
                  'strip@3x.png');
                break;
            default:
                $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/strip.png',
                  'strip.png');
                $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/strip@2x.png',
                  'strip@2x.png');
                $pass->addFile($_SERVER['DOCUMENT_ROOT'] . '/local/templates/cameo-v3/images/wallet/strip@3x.png',
                  'strip@3x.png');
                break;
        }

        return $pass;
    }

    public static function pkpass_data()
    {
        self::findName();

        return [
          'passTypeIdentifier' => 'pass.ru.kamey',
          'formatVersion' => 1,
          'organizationName' => "Cameo",
          'teamIdentifier' => 'M2DB6MU4GT',
          'serialNumber' => BonusCard::$guid . '',
          'backgroundColor' => 'rgb(255, 255, 255)',
          'foregroundColor' => 'rgb(0,0,0)',
          'labelColor' => 'rgb(36,165,108)',
          'stripColor' => 'rgb(255,255,255)',
          'logoText' => '',
          'description' => 'Бонусная карта',
          'storeCard' => [
            'headerFields' => [
              [
                'key' => 'header',
                'label' => 'Бонусов',
                'value' => BonusCard::amount() . ''
              ]
            ],
            'primaryFields' => [
              [
                'key' => 'primary',
                'label' => 'Процент начисления',
                'value' => BonusCard::percent() . '%'
              ]
            ],
            'secondaryFields' => [
              [
                'key' => 'actual',
                'label' => 'Владелец бонусной карты',
                'value' => self::formatName()
              ]
            ],
            'backFields' => [
              [
                'key' => 'id',
                'label' => 'Номер карты',
                'value' => BonusCard::$number . ''
              ],
              [
                'key' => 'next_level',
                'label' => 'До следующего уровня',
                'value' => BonusCard::next_accumulated() ? BonusCard::next_accumulated() . ' р.' : '-'
              ],
              [
                'key' => 'name',
                'label' => 'Ваш номер телефона',
                'value' => BonusCard::$phone . ''
              ],
              [
                'key' => 'about_bonus_system',
                'label' => 'Бонусная система',
                'value' => "Карта выдается при покупке от 3000 р. с уже начисленными 300 приветственными несгораемыми бонусами.

Один бонус равен одному рублю. Бонусами Вы можете оплатить до 100% Вашей следующей покупки.

При покупке товара со скидкой в 100%, с Вас удержат всего один рубль.

Бонусами гарантированно можно воспользоваться на следующий день после совершенной покупки.

<a href='https://www.kamey.ru/bonus-system/'>Полные условия на сайте kamey.ru</a>"
              ],
              [
                'key' => 'shops',
                'label' => 'Наши магазины',
                'value' => 'https://www.kamey.ru/shops/?find'
              ],
              [
                'key' => 'support_phone',
                'label' => 'Служба поддержки',
                'value' => '8(800)77-55-323'
              ],
            ]
          ],
          'barcode' => [
            'format' => 'PKBarcodeFormatCode128',
            'message' => BonusCard::$number . '',
            'messageEncoding' => 'iso-8859-1',
            'altText' => BonusCard::$number . ''
          ],
          "webServiceURL" => "https://www.kamey.ru/api/bonus.apple.service/",
          "authenticationToken" => "rXkm3Gg8umDm2QHz",
          'locations' => [
            [
              'latitude' => 54.728464,
              'longitude' => 55.9705593,
              'relevantText' => 'Ваш Cameo находится по адресу ул. Кирова, д. 91'
            ]
          ]
        ];
    }

    public static function findName()
    {
        if (self::$name == null && self::$guid) {
            $item = BonusCardTable::getById(self::$guid)->fetch();
            if ($item) {
                self::$name = $item['Name'];
                self::$surname = $item['Surname'];
                self::$patronymic = $item['Patronymic'];
            }
        }
    }

    /**
     * @return int|mixed
     */
    public static function amount()
    {
        self::find();
        if (!self::isset()) {
            return 0;
        }
        $fields = [
          'CardGUID' => self::$guid
        ];
        if (self::$amountDate) {
            $fields['QueryDate'] = self::$amountDate;
        }
        $result = self::request("GetBonuses", $fields);

        return $result['Bonuses'];
    }

    public static function find()
    {
        if (self::$number == null) {
            if (CUser::IsAuthorized() || self::$guid) {
                if (is_null(self::$guid)) {
                    $result = self::findGuidByUser(CUser::GetID());
                    self::$guid = $result ? $result : self::NOT_FOUND;
                }
                if (self::$guid) {
                    $item = BonusCardTable::getList([
                      'filter' => [
                        'CardGUID' => self::$guid
                      ]
                    ])->fetch();

                    if (!$item) {
                        self::$guid = self::NOT_FOUND;
                    } else {
                        self::$number = $item['CardNumber'];
                        self::$phone = $item['PhoneNumber'];
                        self::$name = $item['Name'];
                        self::$surname = $item['Surname'];
                        self::$patronymic = $item['Patronymic'];
                    }
                }
            } else {
                self::$guid = self::NOT_AUTHORIZED;
            }
        }
    }

    public static function findGuidByUser($id)
    {
        $arUser = CUser::GetList($by, $order, ['ID' => $id], [
          'SELECT' => ['UF_CARD_GUID'],
          'FIELDS' => ['ID']
        ])->Fetch();

        return $arUser['UF_CARD_GUID'];
    }

    /**
     * @return bool
     */
    public static function isset()
    {
        self::find();

        return self::$guid !== self::NOT_FOUND && self::$guid !== self::NOT_AUTHORIZED && self::$guid != '';
    }

    static function request($function = null, $params = null, $headers = array(), $timeout = null)
    {
        $params = array(
          "Function" => $function,
          "Params" => $params
        );
        $data_string = json_encode($params);
        $ch = curl_init(self::$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_USERPWD, "webstore" . ":" . "T0QeHSZjcv84Ztb");
        if ($timeout) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $timeout);
        }

        $headers = array_merge(array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($data_string),
        ), $headers);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code !== 200) {
            self::$error = $result;

            return false;
        }
        $decoded = json_decode($result, true);
        if ($decoded === null) {
            self::$error = $result;

            return false;
        } else {
            self::$error = false;
        }

        return $decoded;
    }

    public static function percent()
    {
        self::find();
        if (!self::isset()) {
            return 0;
        } else {
            $accumulated = self::accumulated();
            foreach (array_reverse(self::matrix(), true) as $amount => $percent) {
                if ($accumulated >= $amount) {
                    return $percent;
                }
            }

            return array_shift(self::matrix());
        }
    }

    public static function accumulated()
    {
        if (self::$cache['accumulated'][self::$guid] === null) {
            self::$cache['accumulated'][self::$guid] = self::request("GetAamountOfSavings",
              ['CardGUID' => self::$guid]);
        }

        return self::$cache['accumulated'][self::$guid]['AmountOfSavings'];
    }

    public static function matrix()
    {
        return [
          0 => 5,
          10000 => 7,
          40000 => 10,
          70000 => 12
        ];
    }

    public static function formatName()
    {
        if (!self::$name) {
            return '-';
        }

        return implode(" ", array_filter([
          self::$name,
          self::$patronymic,
          mb_substr(self::$surname, 0, 1) . '.'
        ]));
    }

    public static function next_accumulated()
    {
        if (self::$cache['next_accumulated'][self::$guid] === null) {
            $percent = self::percent();
            $matrix = self::matrix();
            $next = false;
            $amount = false;
            foreach ($matrix as $a => $p) {
                if ($next) {
                    $amount = $a;
                    break;
                }
                if ($p == $percent) {
                    $next = true;
                }
            }
            if ($amount !== false) {
                $amount = $amount - self::accumulated();
            } else {
                $amount = '';
            }
            self::$cache['next_accumulated'][self::$guid] = $amount;
        }

        return self::$cache['next_accumulated'][self::$guid];
    }

    public static function findGuidByPhone($phone)
    {
        $phone = self::simplify_phone($phone);
        if (substr($phone, 0, 1) == '8') {
            $phone = '7' . substr($phone, 1);
        }
        $result = BonusCardTable::getList([
          'filter' => ['PhoneNumber' => $phone]
        ])->fetch();

        return $result['CardGUID'];
    }

    public static function simplify_phone($phone)
    {
        return str_replace(['(', ' ', ')', '-', '+'], '', $phone);
    }

    public static function update($is_agent = false)
    {
        @set_time_limit(0);
        $result = self::request("GetDiscountCards");
        foreach ($result as $item) {
            if (BonusCardTable::getById($item['CardGUID'])->fetch()) {
                BonusCardTable::update($item['CardGUID'], $item);
            } else {
                BonusCardTable::add($item);
            }
            self::bind($item);
            //self::update_google($item);
        }
        if ($is_agent) {
            return "BonusCard::update(true);";
        }

        return null;
    }

    public static function bind($item)
    {
        $result = self::find_user_by_phone($item['PhoneNumber']);
        if ($result) {
            $u = new CUser();
            $u->Update($result['ID'], ['UF_CARD_GUID' => $item['CardGUID']]);
        }
    }

    public static function get_phone_formats($phone){
        $phone = self::simplify_phone($phone);
        $code = substr($phone, 1, 3);
        $number = substr($phone, 4);
        $parts = [
          substr($number, 0, 3),
          substr($number, 3, 2),
          substr($number, 5, 2),
            //another template
          substr($number, 0, 2),
          substr($number, 2, 2),
          substr($number, 4, 3),
        ];
        $formats = [
          '\'' . "+7" . $code . $number . '\'',
          '\'' . "7" . $code . $number . '\'',
          '\'' . "8" . $code . $number . '\'',
          '\'' . "+7 (" . $code . ") " . $parts[0] . '-' . $parts[1] . '-' . $parts[2] . '\'',
          "'+7(" . $code . ")" . $parts[0] . '-' . $parts[1] . '-' . $parts[2] . '\'',
          "'+7 " . $code . " " . $parts[0] . ' ' . $parts[1] . ' ' . $parts[2] . '\'',
          '\'' . "+7 (" . $code . ") " . $parts[3] . '-' . $parts[4] . '-' . $parts[5] . '\'',
          "'+7(" . $code . ")" . $parts[3] . '-' . $parts[4] . '-' . $parts[5] . '\'',
          "'+7 " . $code . " " . $parts[3] . ' ' . $parts[4] . ' ' . $parts[5] . '\'',
        ];
        return $formats;
    }

    public static function find_user_by_phone($phone, $filterGuid = true)
    {
        $fields = ['PERSONAL_PHONE' => implode(" | ", self::get_phone_formats($phone)),];
        if ($filterGuid) {
            $fields['UF_CARD_GUID'] = false;
        }

        $result = CUser::GetList($by, $order, $fields)->Fetch();

        return $result;
    }

    public static function normalizePhone($phone)
    {
        return "+7" . substr(self::simplify_phone($phone), 1);
    }

    public static function update_google($item)
    {
        self::autoload_google();
        global $service;
        self::$number = $item['CardNumber'];
        self::$guid = $item['CardGUID'];
        self::$phone = $item['PhoneNumber'];
        try {
            $wobObject = $service->loyaltyobject->get(ISSUER_ID . '.' . LOYALTY_OBJECT_ID . '_' . self::$guid);
        } catch (Google_Service_Exception $exception) {
            return;
        }
        $loyaltyObject = Loyalty::generateLoyaltyObject(ISSUER_ID, LOYALTY_CLASS_ID,
          LOYALTY_OBJECT_ID . '_' . self::$guid, [
            'number' => self::$number,
            'guid' => self::$guid,
            'phone' => self::$phone
          ], $wobObject);
        $service->loyaltyobject->update(ISSUER_ID . '.' . LOYALTY_OBJECT_ID . '_' . self::$guid, $loyaltyObject);
    }

    public static function autoload_google()
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/api/sdk/bonus/google/autoload.php';
    }

    /**
     * @return int
     */
    public static function firstTime()
    {
        self::find();
        switch (self::$guid) {
            case self::NOT_FOUND:
                return self::NOT_FOUND;
            case self::NOT_AUTHORIZED:
                return self::NOT_AUTHORIZED;
            default:
                if (self::accumulated() > 0) {
                    return self::ANOTHER_TIME;
                }

                return self::FIRST_TIME;
        }
    }

    /**
     * @param int|array $id
     *
     * @return array
     */
    public static function history()
    {
        self::find();

        return self::request("GetBonusesExtended", ['CardGUID' => self::$guid]);
    }

    public static function withdrawal_request($user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = CUser::GetID();
        }
        self::find();
        if (!self::isset()) {
            return [
              'error' => true,
              'message' => self::$guid == self::NOT_AUTHORIZED ? 'Авторизуйтесь для списания бонусов' : 'Укажите карту в личном кабинете для списания бонусов'
            ];
        }
        $phone = self::findPhone();
        if (!$phone) {
            return [
              'error' => true,
              'message' => 'В вашей анкете не заполнен номер телефона. Обратитесь в ближайший магазин для активации бонусной карты для интернет-магазина'
            ];
        }
        $rsUser = CUser::GetList(
          $by,
          $order,
          array('ID' => $user_id),
          array("SELECT" => ["UF_TIME_CARD"], "FIELDS" => array("ID", "UF_TIME_CARD"))
        );

        $u = $rsUser->Fetch();
        $diff = $u['UF_TIME_CARD'] + 180 - time();
        if ($u['UF_TIME_CARD'] && $diff > 0) {
            return [
              'error' => true,
              'resend' => true,
              'message' => "Повторная отправка возможна через " . pluralize($diff, "секунда")
            ];
        }
        $code = rand(1000, 9999);
        CModule::IncludeModule("rarus.sms4b");
        $SMS4B = new Csms4b();
        try {
            send_sms_ext(str_replace(array("(", ")", "-", " "), "", $phone), $code);
//            $list = array(
//              str_replace(array("(", ")", "-", " "), "", $phone) => ($code)
//            );
//            $result = $SMS4B->SendSmsSaveGroup($list)[0];
//            if ($result['Result'] != 1) {
//                return [
//                  'error' => true,
//                  'message' => "Произошла ошибка при отправке смс. Попробуйте позже"
//                ];
//            }
            global $USER;
            $USER->Update($u['ID'], array(
              'UF_CODE_CARD' => $code,
              'UF_TIME_CARD' => time()
            ));

            return [
              'success' => true
            ];
        } catch (ArgumentException $e) {
            return [
              'error' => true,
              'message' => "Произошла ошибка: " . $e->getMessage()
            ];
        } catch (ObjectException $e) {
            return [
              'error' => true,
              'message' => "Произошла ошибка: " . $e->getMessage()
            ];
        } catch (Sms4bException $e) {
            return [
              'error' => true,
              'message' => "Произошла ошибка: " . $e->getMessage()
            ];
        }
    }

    public static function findPhone()
    {
        self::find();

        return self::$phone;
    }

    public static function confirmation_withdrawal($code, $user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = CUser::GetID();
        }
        $rsUser = CUser::GetList(
          $by,
          $order,
          array('ID' => $user_id),
          array("SELECT" => ["UF_CODE_CARD"], "FIELDS" => array("ID", "UF_CODE_CARD"))
        );
        $u = $rsUser->Fetch();
        if ($code != $u['UF_CODE_CARD']) {
            return [
              'error' => true,
              'message' => 'Неверный код подтверждения'
            ];
        }
        global $USER;
        $result = $USER->Update($u['ID'], array(
          'UF_CODE_CARD' => false,
          "UF_TIME_CARD" => false
        ));
        if ($result == false) {
            return [
              'error' => true,
              'message' => $USER->LAST_ERROR
            ];
        } else {
            return [
              'success' => true
            ];
        }
    }

    public static function order_withdraw(Order $order, $amount)
    {
        $collection = $order->getPaymentCollection();
        $payment = self::getPayment($collection);
        if ($payment) {
            $another = self::getAnotherPayment($collection);
            $payment->setField("SUM", $amount);
            $payment->setPaid("Y");
            $another->setField('SUM', $another->getSum() - $amount);//todo ordergetfull price - another
            $collection->save();
            $order->save();
        }
    }

    /**
     * @param $collection
     *
     * @param bool $create
     *
     * @return Payment|bool|mixed
     */
    public static function getPayment($collection, $create = true)
    {
        /** @var Payment $payment */
        foreach ($collection as $payment) {
            if ($payment->getPaymentSystemId() == self::PAYMENT_ID) {
                return $payment;
            }
        }

        if ($create) {
            /** @var Service $paySystem */
            if ($paySystem = Manager::getObjectById(self::PAYMENT_ID)) {
                return $collection->createItem($paySystem);
            }
        }

        return false;
    }

    public static function getAnotherPayment($collection)
    {
        foreach ($collection as $item) {
            /** @var Payment $item */
            if ($item->getPaymentSystemId() != self::PAYMENT_ID) {
                return $item;
            }
        }

        return false;
    }

    public static function init_event_handlers()
    {
        EventManager::getInstance()->addEventHandler(
          'sale',
          'OnSaleOrderSaved',
          ['BonusCard', 'event_withdraw']
        );

        EventManager::getInstance()->addEventHandler(
          'sale',
          'OnSaleOrderCanceled',
          ['BonusCard', 'order_cancel_handler']
        );
        EventManager::getInstance()->addEventHandler(
          'sale',
          'OnSaleBeforeOrderDelete',
          ['BonusCard', 'order_cancel_handler']
        );
        EventManager::getInstance()->addEventHandler(
          'sale',
          'OnSaleStatusOrderChange',
          ['BonusCard', 'order_finish_handler']
        );
    }

    public static function event_withdraw(Event $event)
    {
        /** @var Order $order */
        $order = $event->getParameter("ENTITY");
        $isNew = $event->getParameter('IS_NEW');
        if ($isNew) {
            $anotherPayment = self::getAnotherPayment($order->getPaymentCollection());
            $total = $anotherPayment->getSum();
            self::$guid = self::findGuidByUser($order->getUserId());
            if (self::$guid === null) {
                self::$guid = '';
            }
            self::$number = null;
            self::find();
            if (!self::isset() && ($total >= 3000 || !self::allow($order->getDateInsert()))) {
                self::create_check($order->getUserId(), $order);
            }
        }

        /** @var Payment $payment */
        $payment = self::getPayment($order->getPaymentCollection());
        if ($payment) {
            $data = [
              'payment_id' => $payment->getPaymentSystemId()
            ];
            $data['break'] = !$GLOBALS['break_SetBonuses'] ? 'false' : 'true';
            if (!$GLOBALS['break_SetBonuses']) {
                /** @var Order $order */
                $order = $payment->getCollection()->getOrder();
                $order_number = $order->getField('ACCOUNT_NUMBER');
                $data['order_number'] = $order_number;
                $result = self::request('SetBonuses', [
                  'OrderID' => $order_number,
                  'CardGUID' => self::findGuidByUser($order->getUserId()),
                  'WrittenOffDateTime' => date('c'),
                  'WrittenOffBonuses' => $payment->isPaid() ? $payment->getSum() : 0,
                  'OrderDateTime' => date('c', strtotime($order->getDateInsert()))
                ]);
                $data['cardGuid'] = self::findGuidByUser($order->getUserId());
                $data['writeOff'] = $payment->isPaid() ? $payment->getSum() : 0;
                $data['result'] = $result;
                $data['error'] = self::$error;
            }
            $GLOBALS['break_SetBonuses'] = false;
        }
    }

    public static function allow($date = null)
    {
        $time = is_null($date) ? time() : strtotime($date);
        $allow = true;
        foreach (self::$break_dates as $date) {
            if ($time >= strtotime($date['from']) && $time <= strtotime($date['to'])) {
                $allow = false;
            }
        }

        return $allow;
    }

    public static function create_check($user_id, Order $order = null)
    {
        $arUser = CUser::GetByID($user_id)->Fetch();
        $phone = self::simplify_phone($arUser['PERSONAL_PHONE']);
        if (!$phone && $order) {
            $u = new CUser();
            try {
                $u->Update($user_id, ['PERSONAL_PHONE' => $order->getPropertyCollection()->getPhone()->getValue()]);
                $phone = self::simplify_phone($order->getPropertyCollection()->getPhone()->getValue());
            } catch (Exception $exception) {
            }
        }
        $birthday = self::reverse_birthday($arUser['PERSONAL_BIRTHDAY']);
        self::explode_name($arUser);
        if (!$arUser['LAST_NAME'] && !$arUser['SECOND_NAME'] && $order) {
            try {
                $arUser['NAME'] = $order->getPropertyCollection()->getPayerName()->getValue();
                self::explode_name($arUser);
            } catch (Exception $exception) {
            }
        }
        $data = [
          'PhoneNumber' => $phone,
          'Name' => $arUser['NAME'],
          'Surname' => $arUser['LAST_NAME'],
          'Patronymic' => $arUser['SECOND_NAME'],
          'Email' => $arUser['EMAIL'],
          'StartBonuses' => 300
        ];
        if ($birthday) {
            $data['Birthday'] = $birthday;
        }
        if ($arUser['PERSONAL_GENDER']) {
            $data['Sex'] = ($arUser['PERSONAL_GENDER'] == 'M' ? 'Male' : 'Female');
        }
        $result = self::request('CreateDiscountCard', $data);
        if ($result && $result['CardGUID']) {
            if ($result['NewCard']) {
                BonusCardTable::add([
                  'PhoneNumber' => $phone,
                  'CardGUID' => $result['CardGUID'],
                  'CardNumber' => $result['CardNumber']
                ]);
            }
            $u = new CUser();
            $u->Update($user_id, ['UF_CARD_GUID' => $result['CardGUID']]);
            self::$guid = $result['CardGUID'];
            $jwt = BonusCard::jwt(true);
            CEvent::SendImmediate("BONUS_CARD_NEW", "s2", [
              "EMAIL" => $arUser['EMAIL'],
              "GUID" => $result['CardGUID'],
              'NUMBER' => $result['CardNumber'],
              "JWT" => $jwt
            ]);
            B24Sync::sendOrderMessage($order->getId(), 'Создана бонусная карта ' . $result['CardNumber']);
        } else {
            B24Sync::sendOrderMessage($order->getId(),
              'Ошибка создания бонусной карты: ' . self::$error . print_r($data, true));
            mail('office@kamey.ru', 'Ошибка при создании карты после оформления заказа',
              self::$error . print_r($data, true));
            //todo send mail to office@kamey.ru
        }
    }

    public static function reverse_birthday($date, $format = 'c')
    {
        if (!$date) {
            return '';
        }
        $birthday = explode(".", $date);
        $birthday = date($format, strtotime(implode('-', array_reverse($birthday))));

        return $birthday;
    }

    public static function explode_name(&$arUser)
    {
        if ($arUser['NAME'] && (!$arUser['LAST_NAME'] && !$arUser['SECOND_NAME'])) {
            $data = explode(" ", $arUser['NAME']);

            $arUser['LAST_NAME'] = $data[1];
            $arUser['SECOND_NAME'] = $data[2];
            $arUser['NAME'] = $data[0];
        }
    }

    public static function jwt($simple = false)
    {
        self::find();
        if (!self::isset()) {
            return false;
        }
        self::autoload_google();
        global $client;
        require_once $_SERVER['DOCUMENT_ROOT'] . '/api/sdk/bonus/google/utils/wob_utils.php';
        $loyaltyObject = Loyalty::generateLoyaltyObject(ISSUER_ID, LOYALTY_CLASS_ID,
          LOYALTY_OBJECT_ID . '_' . self::$guid, [
            'number' => self::$number,
            'guid' => self::$guid,
            'phone' => self::$phone,
            "simple" => $simple
          ]);
        $wobPayload = new WobPayload(ORIGINS);
        $wobPayload->addWalletObjects($loyaltyObject, LOYALTY_OBJECT_ID);
        $requestBody = $wobPayload->getSaveToWalletRequest();

        $utils = new WobUtils();

        return $utils->makeSignedJwt($requestBody, $client);
    }

    public static function order_cancel_handler(Event $event)
    {
        /** @var Order $order */
        $order = $event->getParameter("ENTITY");
        $guid = self::findGuidByUser($order->getUserId());
        if ($guid) {
            self::$guid = $guid;
            self::request('SetBonuses', [
              'OrderID' => $order->getField('ACCOUNT_NUMBER'),
              'CardGUID' => $guid,
              'DeleteOrder' => true
            ]);
            $GLOBALS['break_SetBonuses'] = true;
            $payment = self::getPayment($order->getPaymentCollection(), false);
            if ($payment) {
                $payment->setPaid('N');
            }
        }
    }

    public static function order_finish_handler(Event $event)
    {
        /** @var Order $order */
        $order = $event->getParameter("ENTITY");
        if ($order->getField('STATUS_ID') == 'F' && $order->getField('PAYED') == 'Y') {
            $payment = self::getAnotherPayment($order->getPaymentCollection());
            $total = $payment->getSum();
            self::$guid = self::findGuidByUser($order->getUserId());
            if (self::$guid === null) {
                self::$guid = '';
            }
            self::$number = null;
            self::find();
            if (in_array($payment->getPaymentSystemId(), [12, 8, 9, 13, 14])) {
                $shipment = null;
                foreach ($order->getShipmentCollection() as $item) {
                    /** @var Shipment $item */
                    if ($item->isSystem()) {
                        continue;
                    }
                    $shipment = $item;
                    break;
                }
                if ($shipment) {
                    $canPrint = true;
                    foreach ($order->getPrintedChecks() as $check) {
                        if ($check->getField('SHIPMENT_ID') == $shipment->getId()) {
                            $canPrint = false;
                            break;
                        }
                    }
                    if ($canPrint) {
                        $entities = [$shipment];

                        $addResult = Cashbox\CheckManager::addByType($entities, 'sell', []);
                        if (!$addResult->isSuccess()) {
                            B24Sync::sendOrderMessage($order->getId(), implode("\n", $addResult->getErrorMessages()));
                        } else {
                            B24Sync::sendOrderMessage($order->getId(), 'Добавлен чек для отгрузки');
                        }
                    }
                } else {
                    B24Sync::sendOrderMessage($order->getId(), "Не удалось определить отгрузку для печати чеков");
                }
            }
            if (self::isset()) {
                if (self::allow($order->getDateInsert())) {
                    $total = $payment->getSum() - $order->getDeliveryPrice();
                    $accrued = self::accruedAmount($total);
                    self::request('SetBonuses', [
                      'OrderID' => $order->getField('ACCOUNT_NUMBER'),
                      'CardGUID' => self::$guid,
                      'AccruedBonuses' => $accrued,
                      'AccruedDateTime' => date('c'),
                      'OrderDateTime' => date('c', strtotime($order->getDateInsert()))
                    ]);
                    B24Sync::sendOrderMessage($order->getId(), 'Клиенту начислено бонусов: ' . $accrued);
                    $products = [];
                    CModule::IncludeModule('iblock');
                    foreach ($order->getBasket()->getBasketItems() as $basketItem) {
                        /** @var BasketItem $basketItem */
                        $item = CIBlockElement::GetList([], ['ID' => $basketItem->getProductId()], false, false,
                          ['XML_ID'])->Fetch();
                        $codes = explode("#", $item['XML_ID']);
                        $products[] = [
                          'ProductGUID' => $codes[0],
                          'OfferGUID' => $codes[1],
                          'Quantity' => $basketItem->getQuantity(),
                          'Price' => $basketItem->getPrice()
                        ];
                    }
                    self::request('SetSavings', [
                      'OrderID' => $order->getField('ACCOUNT_NUMBER'),
                      'CardGUID' => self::$guid,
                      'Products' => $products
                    ]);
                }
            }
        } elseif ($order->getField('STATUS_ID') == 'F') {
            B24Sync::sendOrderMessage($order->getId(),
              "Заказ завершен без полной оплаты.\nОплачено: " . $order->getPaymentCollection()->getPaidSum() . "\nФлаг полной оплаты: " . $order->getField('PAYED'));
        }
    }

    public static function accruedAmount($total, $_percent = null)
    {
        $percent = is_null($_percent) ? self::percent() : $_percent;
        if ($percent <= 0) {
            return 0;
        }

        return floor($total * ($percent / 100));
    }

    public static function history_list()
    {
        self::find();

        return self::request("GetBonusesHistory", ["CardGUID" => self::$guid]);
    }
}

function paymentTestFunction(Event $event)
{
    mail('it@halikov-studio.ru', 'hello from text funcyion', 'there');
}
