<?

require('import_events.php');

AddEventHandler("main", "OnAfterUserRegister", "OnAfterUserRegisterHandler");
function OnAfterUserRegisterHandler(&$arFields)
{
    //Отправим письмо пользователю после регистрации со всеми данными
    $arUserFields = array(
      "USER_ID" => $arFields["USER_ID"],
      "STATUS" => ($arFields["ACTIVE"] == "Y" ? 'Активен' : 'Не активен'),
      "MESSAGE" => '',
      "LOGIN" => $arFields["LOGIN"],
      "PASSWORD" => $arFields["PASSWORD"],
      "URL_LOGIN" => urlencode($arFields["LOGIN"]),
      "CHECKWORD" => $arFields["CHECKWORD"],
      "NAME" => $arFields["NAME"],
      "LAST_NAME" => $arFields["LAST_NAME"],
      "EMAIL" => $arFields["EMAIL"],
    );

    //CEvent::Send('USER_INFO', SITE_ID, $arFields, 'N');
    CEvent::SendImmediate('USER_INFO', SITE_ID, $arUserFields, 'N');

    //Отправим стандартную информацию пользователю, но без пароля,
    //тогда отправку выше CEvent::SendImmediate(0 надо закомментить
    //CUser::SendUserInfo($USER->GetID(), SITE_ID, 'Вы успешно зарегистрированы.', true);
}

//Устанавливаем свой формат цены
AddEventHandler("currency", "CurrencyFormat", "myFormat");

function myFormat($fSum, $strCurrency)
{
    return number_format($fSum, 0, '.', ' ') . ' ₽';
}

/*
 *  События заказа
*/
AddEventHandler("sale", "OnBeforeOrderUpdate", "OnBeforeOrderUpdate");

function OnBeforeOrderUpdate($orderId, $newOrderFields)
{
    if ($newOrderFields['STATUS_ID']) {
        $oldOrder = CSaleOrder::GetByID($orderId);

        if ($newOrderFields['STATUS_ID'] != $oldOrder['STATUS_ID'] && $newOrderFields['CANCELED'] != 'Y' && $oldOrder['CANCELED'] != 'Y' && 0) {
            $status = CSaleStatus::GetByID($newOrderFields['STATUS_ID']);

            $orderEmailProp = CSaleOrderPropsValue::GetList(
              array(),
              array(
                'ORDER_ID' => $orderId,
                'CODE' => 'EMAIL'
              ),
              false,
              false,
              array('NAME', 'VALUE')
            )->GetNext();

            if ($orderEmailProp && $orderEmailProp['VALUE']) {
                CEvent::Send(
                  'ORDER_STATUS_CHANGED',
                  's2',
                  array(
                    'ORDER_ID' => $orderId,
                    'EMAIL' => $orderEmailProp['VALUE'],
                    'ORDER_STATUS' => $status['NAME']
                  )
                );
            }
        }
    }
}

AddEventHandler("sale", "OnSaleComponentOrderProperties", "SetOrderDefaultParams");
AddEventHandler("sale", "OnSaleComponentOrderOneStepOrderProps", "CheckUserCityValue");
AddEventHandler("main", "onEndBufferContent", "checkYandexKassa");
AddEventHandler("main", "onEndBufferContent", "replaceCityLinks");

function CheckUserCityValue($arResult, &$arUserResult, $arParams)
{
    $GLOBALS['DELIVERY_LOCATION'] = $arUserResult['DELIVERY_LOCATION'];
}

function SetOrderDefaultParams(&$arUserResult, $request, &$arParams, &$arResult)
{
    $GLOBALS['tempData'] = $arUserResult;
    global $userAddresses, $organizations, $location, $version;
    if ($version > 3) {
        return;
    }
    $defaultAddress = array();
    $defaultOrganization = array();
    foreach ($userAddresses as $address) {
        if ($address['default']) {
            $defaultAddress = $address;
            break;
        }
    }
    foreach ($organizations as $organization) {
        if ($organization['default']) {
            $defaultOrganization = $organization;
        }
    }

    $addressMap = array(
      1 => 26,
      2 => 28
    );
    $addressFieldsMap = array(
      "index" => array(1 => 4, 2 => 15),
      "street" => array(1 => 23, 2 => 29),
      "house" => array(1 => 24, 2 => 30),
      "corpse" => array(1 => 27, 2 => 31),
      "flat" => array(1 => 25, 2 => 32)
    );
    $cityMap = array(
      1 => 6,
      2 => 17
    );

    $zipMap = array(
      1 => 4,
      2 => 15
    );

    $organizationMap = 33;

    $organizationFieldsMap = array(
      "name" => 8,
      "tin" => 10,
      "msrn" => 11,
      "person" => 12,
      "address" => 9,
      "email" => 13,
      "phone" => 14
    );
    $personId = $arUserResult['PERSON_TYPE_ID'];
    foreach ($cityMap as $person => $id) {
        if (empty($arUserResult['ORDER_PROP'][$id])) {
            $arUserResult['ORDER_PROP'][$id] = $location['code'];
            if ($personId == $person && empty($arUserResult['ORDER_PROP'][$zipMap[$person]])) {
                $arUserResult['ORDER_PROP'][$zipMap[$person]] = \CSaleLocation::GetLocationZIP($location['id'])->Fetch()["ZIP"];
            }
        }
    }
    if (!empty($defaultAddress)) {
        $defaultAddressCityCode = CSaleLocation::GetList(array(), array("ID" => $defaultAddress['city_id']), false,
          false, array("CODE"))->Fetch();
        $defaultAddressCityCode = $defaultAddressCityCode['CODE'];
        if ($defaultAddressCityCode == $arUserResult['ORDER_PROP'][$cityMap[$arUserResult['PERSON_TYPE_ID']]]) {
            $id = $addressMap[$personId];
            if (strlen($arUserResult['ORDER_PROP'][$id]) === 0) {
                $arUserResult['ORDER_PROP'][$id] = $defaultAddress['id'];
                foreach ($addressFieldsMap as $field => $ids) {
                    $arUserResult['ORDER_PROP'][$ids[$personId]] = $defaultAddress[$field];
                }
            }
        }
    }
    if (!empty($defaultOrganization) && empty($arUserResult['ORDER_PROP'][$organizationMap]) && $arUserResult['ORDER_PROP'][$organizationMap] !== "0") {
        $arUserResult['ORDER_PROP'][$organizationMap] = $defaultOrganization['id'];
        foreach ($organizationFieldsMap as $field => $id) {
            $arUserResult['ORDER_PROP'][$id] = $defaultOrganization[$field];
        }
    }
}

use Bitrix\Main;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\DeliveryService;
use Bitrix\Sale\Order;
use Bitrix\Sale\ResultError;
use Bitrix\Sale\Shipment;
use Product\Basket;

Main\EventManager::getInstance()->addEventHandler(
  'sale',
  'OnSaleOrderSaved',
  'sendCustomOrderNotification'
);

function resendNewOrderEmail($order_id)
{
    CModule::IncludeModule('sale');
    $order = Order::load($order_id);
    sendNewOrderEmailNotification($order);
}

function sendNewOrderEmailNotification(Order $order, $true = true, $email = null, $fieldsOnly = false)
{
    $shipment_is_loaded = false;
    foreach ($order->getShipmentCollection() as $shipment) {
        if (!$shipment->isSystem() && $shipment->getDeliveryId() != 21) {
            $shipment_is_loaded = true;
        }
    }

    if (!$shipment_is_loaded) {
        CAgent::Add(array(
          "NAME" => "resendNewOrderEmail(" . $order->getID() . ");",
          "MODULE_ID" => "sale",
          "IS_PERIOD" => "N",
          "AGENT_INTERVAL" => 1,
          "ACTIVE" => "Y",
          "NEXT_EXEC" => date("d.m.Y H:i:00"),
          "USER_ID" => 4372
        ));

        return;
    }
    $mailFields = [
      'EMAIL' => $email ? $email : $order->getPropertyCollection()->getUserEmail()->getValue(),
      'ORDER_ID' => $order->getField("ACCOUNT_NUMBER"),
      'ORDER_REAL_ID' => $order->getId(),
      'ORDER_DATE' => date('d.m.Y H:i'),
      'PRICE' => isParityNumber($order->getPrice()) ? formatPrice($order->getPrice()) : $order->getPrice(),
      "DELIVERY__PRICE" => $order->getDeliveryPrice(),
      'DELIVERY_REAL_PRICE' => $order->getShipmentCollection()->getBasePriceDelivery(),
      'ORDER_LIST' => '',
      'BASKET_PRICE' => $order->getBasket()->getBasePrice(),
      'DISCOUNTS' => Basket::getDiscounts($order),
      'BONUS' => Basket::getBonusMail($order)
    ];
    if ($true) {
        if ($order->getPropertyCollection()->getPayerName() && $order->getPropertyCollection()->getPayerName()->getValue()) {
            $mailFields['ORDER_USER'] = ', ' . $order->getPropertyCollection()->getPayerName()->getValue();
        } else {
            $mailFields['ORDER_USER'] = '';
        }
        $mailFields['ORDER_LIST'] = Basket::viewEmail($order->getBasket(), false);
        $mailFields['ORDER_LIST'] = str_replace("/upload", "https://www.kamey.ru/upload", $mailFields['ORDER_LIST']);
        $mailFields['ORDER_LIST'] = str_replace("/include", "https://www.kamey.ru/include", $mailFields['ORDER_LIST']);

        $shipment = false;
        foreach ($order->getShipmentCollection() as $collectionShipment) {
            if ($collectionShipment->isSystem() == true) {
                continue;
            }
            if (!$shipment) {
                $shipment = $collectionShipment;
            }
        }
        if (!$shipment) {
            $collection = $order->getShipmentCollection();
            $shipment = $collection[0];
        }

        if (!$shipment) {
            $mailFields['DELIVERY'] = "Доставка не найдена";
        } else {
            $mailFields['DELIVERY'] = $shipment->getDelivery()->getName();
        }

        $collection = $order->getPaymentCollection();
        /** @var Bitrix\Sale\Payment $payment */
        $payment = $collection[0];
        if (!$payment) {
            $mailFields['PAYMENT'] = "Оплата не найдена";
        } else {
            $mailFields['PAYMENT'] = $payment->getPaymentSystemName();
        }
        $mailFields['DELIVERY_INFO'] = "";
        $props = getOrderProps($order->getId());
        $deliveryInfo = getDeliveryInfo($order->getId(), $props);
        if (!empty($deliveryInfo['city'])) {
            $mailFields['DELIVERY_INFO'] .= 'Город: ' . $deliveryInfo['city'] . '<br/>';
        }
        $mailFields['DELIVERY_INFO'] .= $deliveryInfo['pvz'] ? 'Пункт выдачи: ' : 'Адрес: ';
        $mailFields['DELIVERY_INFO'] .= $deliveryInfo['address'] . '<br/>';
        $mailFields['DELIVERY_INFO'] .= "Получатель: " . ($props["FIO"]["VALUE"] ? $props["FIO"]["VALUE"] : $props["COMPANY"]["VALUE"]);
        logAdd("sendNewOrderEmailNotification", [
          "basketCount" => $order->getBasket()->count(),
          "deliveryPrice" => $order->getDeliveryPrice(),
          "payment" => $mailFields['PAYMENT']
        ]);
        if ($fieldsOnly) {
            return $mailFields;
        }
        global $version;
        CEvent::SendImmediate(
          "SALE_NEW_ORDER_CUSTOM_V" . $version,
          "s2",
          $mailFields
        );
    } else {
        $orderListHTML =
          '<table width="100%" cellspacing="0">
            <thead align="left" style="background: #f6f6f6; color: #333; height: 40px;">
                <tr class="basket-products-table__head">
                    <th class="basket-products-table__head-column" style="padding: 12px 5px; color: #333;" align="center">Фото</th>
                    <th class="basket-products-table__head-column" style="padding: 12px 5px; color: #333;">Описание</th>
                    <th class="basket-products-table__head-column" style="padding: 12px 5px; color: #333;">Параметры товара</th>
                    <th class="basket-products-table__head-column" style="padding: 12px 5px; color: #333;">Цена за шт.</th>
                    <th class="basket-products-table__head-column" style="padding: 12px 5px; color: #333;">Количество</th>
                    <th class="basket-products-table__head-column" style="padding: 12px 5px; color: #333;">Стоимость</th>
                </tr>
            </thead>
                <tbody class="basket-products-table__products">';
        $catalogIBlockID = getIBlockID('catalog');
        $offersIBlockID = getIBlockID('offers');
        $colorRef = getColorRef();
        foreach ($order->getBasket()->getBasketItems() as $basketItem) {
            /** @var BasketItem $basketItem */
            $productInfo = CCatalogSku::GetProductInfo($basketItem->getProductId());
            $offer = CIBlockElement::GetList(
              [],
              [
                'ID' => $basketItem->getProductId(),
                'IBLOCK_ID' => $offersIBlockID
              ],
              false,
              false,
              ['ID', 'NAME', 'PROPERTY_PHOTO', 'PROPERTY_COLOR', 'PROPERTY_RAZMER_ODEZHDA', 'PROPERTY_ROST']
            )->GetNext();

            $product = CIBlockElement::GetList(
              [],
              [
                'ACTIVE' => 'Y',
                'ID' => $productInfo['ID'],
                'IBLOCK_ID' => $catalogIBlockID
              ],
              false,
              false,
              ['ID', 'IBLOCK_ID', 'PROPERTY_CML2_ARTICLE', 'PROPERTY_PLOTNOST']
            )->GetNext();

            /*AddMessage2Log($offer);
            AddMessage2Log($product);*/

            $colorPictures = [];
            foreach ($offer["PROPERTY_PHOTO_VALUE"] as $photoKey => $photoId) {
                $colorPictures[$photoId] = $offer["PROPERTY_PHOTO_DESCRIPTION"][$photoKey];
            }

            /*
             * СОРТИРОВКА КАРТИНОК ЦВЕТА
            * */
            uasort($colorPictures, function ($a, $b) {
                $aMatches = [];
                $bMatches = [];

                $pattern = '/.*?\[(\d+)\].*?/';

                preg_match($pattern, $a, $aMatches);
                preg_match($pattern, $b, $bMatches);

                return +$aMatches[1] < +$bMatches[1] ? -1 : 1;
            });

            reset($colorPictures);

            $orderListHTML .=
              '<tr class="basket-products-table-item" style="border-bottom: 1px solid #f6f6f6; height: 118px;">
                        <td class="basket-products-table-item__column" style="padding: 12px 5px;">
                            <img src=" ' . imageResize(key($colorPictures), [78, 115]) . ' " class="basket-products-table-item__photo" style="border: 1px solid #e7e7e7" />
                        </td>
                        <td class="basket-products-table-item__column" style="padding: 12px 5px;">
                            <div class="property-line"><span class="property-line__property">Артикул: </span><span style="color: #a6a6a6;">' . $product['PROPERTY_CML2_ARTICLE_VALUE'] . '</span></div>
                            <div class="property-line"><span class="property-line__property">Плотность ткани: </span><span style="color: #a6a6a6;">' . $product['PROPERTY_PLOTNOST_VALUE'] . '</span></div>
                        </td>
                        <td class="basket-products-table-item__column" style="padding: 12px 5px;">
                            <div class="property-line">
                                <span class="property-line__property">Цвет: </span>
                                <span style="color: #a6a6a6;"> ' . (trim($colorRef[$offer['PROPERTY_COLOR_VALUE']]) ? $colorRef[$offer['PROPERTY_COLOR_VALUE']] : 'Без названия') . '</span>
                            </div>
                            <div class="property-line">
                                <span class="property-line__property">Размер: </span>
                                <span style="color: #a6a6a6;">' . $offer['PROPERTY_RAZMER_ODEZHDA_VALUE'] / 2 . '</span>
                            </div>
                            <div class="property-line">
                                <span class="property-line__property">Рост: </span>
                                <span style="color: #a6a6a6;">' . $offer['PROPERTY_ROST_VALUE'] . '</span>
                            </div>
                        </td>
                        <td class="basket-products-table-item__column" style="padding: 12px 5px;">
                            <div style="font-weight: bold;color: #059856;">' . (isParityNumber($basketItem->getPrice()) ? formatPrice($basketItem->getPrice()) : $basketItem->getPrice()) . ' ₽</div>
                        </td>
                        <td style="padding: 12px 5px; color: #a6a6a6;">' . (int)$basketItem->getQuantity() . ' шт. </td>
                        <td style="padding: 12px 5px;">
                            <div style="font-weight: bold;color: #059856;">' . $basketItem->getQuantity() * $basketItem->getPrice() . '  ₽</div>
                        </td>
                   </tr>';
        }
        $orderListHTML .= '</tbody></table>';

        $mailFields['ORDER_LIST'] = $orderListHTML;
        if ($order->getPropertyCollection()->getPayerName() && $order->getPropertyCollection()->getPayerName()->getValue()) {
            $mailFields['ORDER_USER'] = 'Уважаемый(-ая) ' . $order->getPropertyCollection()->getPayerName()->getValue() . ', <br>';
        }
        $result = CEvent::SendImmediate(
          "SALE_NEW_ORDER_CUSTOM",
          "s2",
          $mailFields
        );
    }
}

function sendCustomOrderNotification(Main\Event $event)
{
    /** @var Order $order */
    $order = $event->getParameter("ENTITY");
    $oldValues = $event->getParameter("VALUES");
    $isNew = $event->getParameter("IS_NEW");

    if ($isNew && $order->getId()) {
        sendNewOrderEmailNotification($order);
    }
}

function testSendCustomOrderNotification()
{
    CModule::IncludeModule("sale");
    $event = new Main\Event("sale", "OnSaleOrderSaved", array(
      "ENTITY" => \Bitrix\Sale\Order::load(1142),
      "IS_NEW" => true
    ));
    sendCustomOrderNotification($event);
}

function formatPrice($price)
{
    return preg_replace('/(\d+)\.0+$/', '$1', $price);
}

function isParityNumber($n)
{
    return $n % 2 === 0;
}

AddEventHandler("main", "OnEndBufferContent", "ReplaceCity");

function ReplaceCity(&$content)
{
    if (!defined("ADMIN_SECTION")) {
        global $location;
        $city = $location['city'] ? $location['city'] : 'Уфа';
        require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/morphos/vendor/autoload.php';
        $find = array("<head>", "#city#");
        if (!$GLOBALS['importantCity'] || true) {
            $replace = array(
              "<head><script data-skip-moving='true'>(function(){
	function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    \"(?:^|; )\" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + \"=([^;]*)\"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
	var open = XMLHttpRequest.prototype.open;
	XMLHttpRequest.prototype.open = function(){
	if (arguments[0] !== 'GET') {
		open.apply(this,arguments);
	} else {
		if (arguments[1].match('bxrand')) {
			var location = 'undefined';
			var importantCity = " . json_encode($GLOBALS['importantCity']) . ";
			if (getCookie('BITRIX_CAMEO_locationId')) {
				location = getCookie('BITRIX_CAMEO_locationId');
			}" . ($GLOBALS['importantCityId'] ? "location = " . $GLOBALS['importantCityId'] . ';window.cityChunk="' . $GLOBALS['importantCity'] . '";' : "") . "
			var template = null;
			if (getCookie('BITRIX_TEMPLATE')) {
				template = getCookie('BITRIX_TEMPLATE');
			}
			if ((window.location.pathname === '/shops/' || window.location.pathname.match('/catalog/')) && location !== 'undefined' && location && !importantCity) {
			  arguments[1] += '&locationId=' + location;	
			}
			if (template) {
				arguments[1] += '&templateId=' + template;
			}
		    XMLHttpRequest.prototype.open = open;	
		}
		open.apply(this,arguments);
	}
};
})()</script>",
              $city
            );
        } else {
            $replace = array("<head>", $city);
        }
        foreach (array(
                   "I" => "именительный",
                   "R" => "родительный",
                   "D" => "дательный",
                   "V" => "винительный",
                   "T" => "творительный",
                   "P" => "предложный"
                 ) as $key => $type) {
            $find[] = "#city" . $key . '#';
            $replace[] = morphos\Russian\GeographicalNamesInflection::getCase($city, $type);
        }
        $content = str_replace($find, $replace, $content);

        if (SITE_TEMPLATE_ID == 'cameo-v3') {
            preg_match_all('/<link href.+data-template-style.+\/>/m', $content, $matches);
            $matches[0] = array_filter($matches[0], function ($string) {
                return strpos($string, 'semantic') === false && strpos($string,
                    'cameo-v3/css/style.css') === false && strpos($string, 'cameo-v3/css/v4/index.css') === false;
            });
            $css = implode("\n", $matches[0]);
            foreach ($matches[0] as $match) {
                $content = str_replace($match, '', $content);
            }
            $content = str_replace("</footer>", "</footer>" . $css, $content);
        }
        $content = str_replace('type="text/javascript"', "", $content);
    }
}

AddEventHandler("search", "BeforeIndex", "sanitizeIndexArticle");

function sanitizeIndexArticle($arFields)
{
    if ($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == getIBlockID('catalog')) {
        if (array_key_exists("BODY", $arFields)) {
            preg_match("/([0-9])-0([0-9]+(-[0-9]+)?\[.+\])/s", $arFields["BODY"], $matches);
            if ($matches) {
                $arFields['BODY'] .= " " . $matches[1] . '-' . $matches[2];
            }
        }
    }

    return $arFields;
}

AddEventHandler("main", "OnAfterUserAdd", "addUserClientGroup");
function addUserClientGroup($arFields)
{
    CUser::SetUserGroup($arFields['ID'], array_merge(CUser::GetUserGroup($arFields['ID']), array(9)));
    $_SESSION["SESS_AUTH"]["GROUPS"][] = 9;
}

Main\EventManager::getInstance()->addEventHandler(
  'sale',
  'OnSaleOrderBeforeSaved',
  'setFullAddressIfNeeded'
);

function setFullAddressIfNeeded(Main\Event $event)
{
    /** @var \Bitrix\Sale\Order $order */
    $order = $event->getParameter("ENTITY");
    /** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
    $propertyCollection = $order->getPropertyCollection();
    /** @var \Bitrix\Sale\PropertyValue[] $propsData */
    $propsData = [];

    /**
     * @var \Bitrix\Sale\PropertyValue $propertyItem
     */
    foreach ($propertyCollection as $propertyItem) {
        if (!empty($propertyItem->getField("CODE"))) {
            $propsData[$propertyItem->getField("CODE")] = $propertyItem;
        }
    }
    $sendResult = false;
    if ($propsData['street'] && $propsData['street']->getValue()) {
        $list = [];
        if ($propsData['LOCATION']) {
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
              'filter' => array(
                '=CODE' => $propsData['LOCATION']->getValue(),
                '=PARENTS.NAME.LANGUAGE_ID' => LANGUAGE_ID,
                '=PARENTS.TYPE.NAME.LANGUAGE_ID' => LANGUAGE_ID,
              ),
              'select' => array(
                'VALUE' => 'PARENTS.NAME.NAME'
              ),
              'order' => array(
                'PARENTS.DEPTH_LEVEL' => 'asc'
              )
            ));
            while ($item = $res->fetch()) {
                $list[] = $item['VALUE'];
            }
        }
        if ($value = $propsData['street']->getValue()) {
            if (strpos($value, 'ул') !== 0) {
                $value = "ул " . $value;
            }
            $list[] = $value;
        }
        if ($propsData['house'] && $propsData['house']->getValue()) {
            $list[] = 'д. ' . $propsData['house']->getValue();
        }
        if ($propsData['corps'] && $propsData['corps']->getValue()) {
            $list[] = 'к. ' . $propsData['corps']->getValue();
        }
        if ($propsData['flat'] && $propsData['flat']->getValue()) {
            $list[] = 'кв. ' . $propsData['flat']->getValue();
        }
        if ($propsData['ADDRESS']) {
            $propsData['ADDRESS']->setValue(implode(", ", array_filter($list)));
        }
        $sendResult = true;
    }
    $value = "";
    switch ($order->getPersonTypeId()) {
        case 1:
            $value .= $propsData['FIO'] ? $propsData['FIO']->getValue() : "";
            break;
        case 2:
            $value .= $propsData['COMPANY'] ? $propsData['COMPANY']->getValue() : "";
            break;
    }
    $value .= ($value ? ', ' : '') . ($propsData['ADDRESS'] ? $propsData['ADDRESS']->getValue() : "");
    if (trim($value) && $propsData['MERGE']) {
        $propsData['MERGE']->setValue(trim($value));
        $sendResult = true;
    }
    if ($sendResult) {
        $event->addResult(
          new Main\EventResult(
            Main\EventResult::SUCCESS, $order
          )
        );
    }
}

function logAdd($id, $description = "", $itemId = null, $module = "sale")
{
    $exclude = [
      'extraServices',
      'primerka_y_n',
      'POST_INFO',
      'pvzValue',
      'shipmentParams',
        'cdek_calculate_error'
    ];
    $description = is_array($description) ? json_encode($description,
      JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) : $description;

    if ($itemId && $module == 'sale' && strpos($id, 'etail') === false && !in_array($id, $exclude)) {
        if ($id == 'allowError') {
            B24Sync::changeStatus($itemId, B24Sync::STATUS_ALLOW_ERROR, 'Ошибка создания накладной: ' . $description);
        } else {
            B24Sync::sendOrderMessage($itemId, $id . ': ' . $description);
        }
    }
    CEventLog::Add(array(
      "SEVERITY" => "SECURITY",
      "AUDIT_TYPE_ID" => $id,
      "MODULE_ID" => $module,
      "ITEM_ID" => $itemId,
      "DESCRIPTION" => $description,
    ));
}

function setOrderDeliveryAllow($orderId, $attempt = 0, $error = "")
{
    if ($attempt > 3) {
        logAdd("allowError", "Исчерпано количество повторных попыток отгрузки", $orderId);
        try {
            $order = Order::load($orderId);
            $collection = $order->getShipmentCollection();
            $shipment = $collection[0];
            /** @var DeliveryService $delivery */
            $delivery = $shipment->getDelivery();
            CEvent::SendImmediate("ALLOW_ERROR", 's2', array(
              "ORDER_ID" => $orderId,
              "ACCOUNT_NUMBER" => $order->getField("ACCOUNT_NUMBER"),
              "DELIVERY_NAME" => $delivery->getName(),
              "DELIVERY_PRICE" => $order->getDeliveryPrice(),
              "ERROR" => $error
            ));
        } catch (Main\ArgumentNullException $e) {
        } catch (Main\NotImplementedException $e) {
        }

        return "";
    }
    if (!$orderId) {
        logAdd("allowError", "Не найден id заказа");

        return "";
    }
    $attempt++;
    $GLOBALS['setOrderDeliveryAllow'] = true;

    if (CModule::IncludeModule("sale")) {
        try {
            global $USER;
            if (!$USER instanceof CUser) {
                $USER = new CUser();
            }

            $order = Order::load($orderId);

            $collection = $order->getShipmentCollection();
            $shipment = null;
            foreach ($collection as $item) {
                /** @var Shipment $item */
                if (!$item->isSystem()) {
                    $shipment = $item;
                    break;
                }
            }
//            $shipment = $collection[0];
//            $shipment->setField("CUSTOM_PRICE_DELIVERY", "N");
//            try {
//                $result = $collection->calculateDelivery();
//            } catch (Exception $e) {
//                logAdd("calculateDeliveryError", [
//                  'count' => count($collection),
//                  'exception' => $e->getMessage()
//                ]);
//            }
//            $discount = $order->getDiscount();
//            $discount->setOrderRefresh(true);
//            $order->doFinalAction(true);
//
//            $order->save();
            /** @var Shipment $shipment */

            $delivery = $shipment->getDelivery();

            if ($delivery->getName() !== "Без доставки") {
                $result = $order->getShipmentCollection()->allowDelivery();
                if ($result->isSuccess()) {
                    $order->setField("STATUS_ID", "PK");
                    $saveResult = $order->save();
                    if (!$saveResult->isSuccess()) {
                        logAdd("allowError", $result->getErrorMessages(), $order->getId());
                    } else {
                        B24Sync::changeStatus($orderId, B24Sync::STATUS_ALLOW, 'Накладная успешно создана');
                    }
                } else {
                    logAdd("allowError", $result->getErrorMessages(), $order->getId());

                    return "setOrderDeliveryAllow($orderId,$attempt,'" . implode(", ",
                        $result->getErrorMessages()) . "');";
                }
            }
        } catch (Main\ArgumentNullException $e) {
            logAdd("allowError", $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine(), $orderId);

            return "setOrderDeliveryAllow($orderId,$attempt,'" . $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine() . "');";
        } catch (Main\NotImplementedException $e) {
            logAdd("allowError", $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine(), $orderId);

            return "setOrderDeliveryAllow($orderId,$attempt,'" . $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine() . "');";
        } catch (Main\ArgumentOutOfRangeException $e) {
            logAdd("allowError", $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine(), $orderId);

            return "setOrderDeliveryAllow($orderId,$attempt,'" . $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine() . "');";
        } catch (Main\ArgumentException $e) {
            logAdd("allowError", $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine(), $orderId);

            return "setOrderDeliveryAllow($orderId,$attempt,'" . $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine() . "');";
        } catch (Main\ObjectNotFoundException $e) {
            logAdd("allowError", $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine(), $orderId);

            return "setOrderDeliveryAllow($orderId,$attempt,'" . $e->getMessage() . ' in: ' . $e->getFile() . ':' . $e->getLine() . "');";
        }
    } else {
        logAdd("allowError", "Module not installed", $orderId);
    }

    return "";
}

function checkYandexKassa(&$content)
{
    if ($_GET['ORDER_ID'] && $_GET['PAYMENT_ID']) {
        $order = Order::loadByAccountNumber($_GET['ORDER_ID']);
        if (!$order) {
            $order = Order::load($_GET['ORDER_ID']);
        }
        $phone = $order->getPropertyCollection()->getPhone()->getValue();
        $phone = str_replace(array(" ", "(", ")", "-"), "", $phone);
        $replace = array(
          "customerContact" => $phone,
          "taxSystem" => 1
        );
        foreach ($order->getBasket()->getBasketItems() as $basketItem) {
            /** @var BasketItem $basketItem */
            $replace['items'][] = array(
              "quantity" => $basketItem->getQuantity(),
              "text" => substr($basketItem->getField("NAME"), 0, 128),
              "tax" => 1,
              "price" => array(
                "amount" => number_format($basketItem->getPrice(), 2, '.', ''),
                "currency" => "RUB"
              )
            );
        }
        $deliveryPrice = $order->getDeliveryPrice();
        $replace['items'][] = array(
          "quantity" => 1,
          "text" => ($deliveryPrice == 0 ? 'Бесплатная доставка' : 'Доставка'),
          "tax" => 1,
          "price" => array(
            "amount" => number_format($deliveryPrice, 2, '.', ''),
            "currency" => "RUB"
          )
        );
        $replace = "<input name='ym_merchant_receipt' type='hidden' value='" . json_encode($replace) . "'/>";
        $content = str_replace("</form>", $replace . '</form>', $content);
    }
}

function retailCrmBeforeOrderSend($retailOrder, $order)
{
    $_order = $order;
    if ($GLOBALS['obOrder']) {
        $order = $GLOBALS['obOrder'];
    } else {
        $order = Order::load($order['ID']);
    }
    /** @var \Bitrix\Sale\Shipment $shipment */
    $shipment = $order->getShipmentCollection()[0];
    if ($shipment) {
        /** @var \safin\delivery\cdek $delivery */
        $delivery = $shipment->getDelivery();
        if (!$delivery && $order->getShipmentCollection()[1]) {
            $shipment = $order->getShipmentCollection()[1];
            $delivery = $shipment->getDelivery();
        }
        if (!$retailOrder['delivery']['code']) {
            $retailList = unserialize(COption::GetOptionString("intaro.retailcrm", "deliv_types_arr"));
            if ($delivery) {
                $retailOrder['delivery']['code'] = $retailList[$delivery->getId()];
            }
        }
        if ($delivery instanceof \safin\delivery\cdek) {
            $extraValues = $shipment->getExtraServices();
            try {
                logAdd("extraServices", array(
                  "current" => $extraValues,
                  "set" => \Bitrix\Sale\Delivery\ExtraServices\Manager::getValuesForShipment(
                    $shipment->getId(),
                    $delivery->getId()
                  )
                ), $order->getId());
            } catch (Main\ArgumentException $e) {
            }
//      $retailOrder['delivery']['code'] = $delivery->getId();
//      $retailOrder['delivery']['integrationCode'] = 'sdek';
            $retailOrder['delivery']['data']['tariffType'] = $delivery->getDataValue("tariff");
            foreach ($delivery->getExtraServices()->getItems() as $id => $service) {
                /** @var \Bitrix\Sale\Delivery\ExtraServices\Base $service */
                $value = $extraValues[$id];
                switch ($service->getCode()) {
                    case "pvz":
                        /** @var CdekPvz $service */

                        global $USER;
                        $service->createOptions(true);
                        logAdd("pvzValue", array("current" => $service->getValue(), "set" => $value), $order->getId());
                        $service->setValue($value);
                        $list = $service->getPvzList();
                        $object = $service->getObjectValue();
                        //logAdd("pvzzz",array($value,$object,$list[$value]));
                        $retailOrder['customFields']['punkt_pvz'] = $object['Name'] . '; ' . $service->getValue();
                        $retailOrder['customFields']['pvz'] = $service->getValue();
                        //$retailOrder['delivery']['data']['pickuppointId'] = $service->getValue();

                        break;
                    case "30":
                        logAdd("primerka_y_n", array("current" => $service->getValue(), "set" => $value),
                          $order->getId());
                        $retailOrder['customFields']['primerka_y_n'] = $service->getValue() == 'Y' ? 's_primerkoy' : 'bez-primerki';
                        break;
                }
            }
        }
    }

    if ($retailOrder['createdAt'] instanceof DateTime) {
        $retailOrder['createdAt'] = $retailOrder['createdAt']->format("Y-m-d H:i:s");
    }

    if (isset($retailOrder['delivery']) && $shipment && $shipment->getField('TRACKING_NUMBER')) {
        $retailOrder['delivery']['data']['trackNumber'] = $shipment->getField('TRACKING_NUMBER');
        $retailOrder['customFields']['trek_sdek'] = $shipment->getField('TRACKING_NUMBER');
    }

    if ($shipment) {
        $retailOrder['customFields']['doki_pochta'] = $shipment->getField("COMMENTS");
    }

    if (isset($retailOrder['payments'])) {
        unset($retailOrder['payments']);
    }
    if ($retailOrder['delivery']['cost'] < 0) {
        $retailOrder['delivery']['cost'] = 0;
    }

    logAdd("retailCrmBeforeOrderSend", array("retail" => $retailOrder), $order->getId());

    return $retailOrder;
}

function retailCrmBeforeOrderSave($order)
{
    $GLOBALS['updateAdditional'][$order['externalId']] = true;

    logAdd("retailCrmBeforeOrderSave", $order, $order['externalId'] ? $order['externalId'] : "new");

    return $order;
}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "updateCityRewrite");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "updateCityRewrite");
AddEventHandler("iblock", "OnAfterIBlockElementDelete", "deleteCityRewrite");

function updateCityRewrite(&$arFields)
{
    if ($arFields['ID'] && $arFields['IBLOCK_ID'] == 93) {
        if ($arFields['ACTIVE'] == "N") {
            deleteCityRewrite($arFields);
        } else {
            $list = Main\UrlRewriter::getList(SITE_ID, array(
              "ID" => "city-" . $arFields['ID']
            ));
            $listMain = Main\UrlRewriter::getList(SITE_ID, array(
              "ID" => "city-main-" . $arFields['ID']
            ));
            $importantValue = current(current($arFields['PROPERTY_VALUES']));
            if (!$importantValue || !$arFields['CODE']) {
                $rs = CIBlockElement::GetList(array(),
                  array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']), false, false,
                  array("PROPERTY_location", "CODE"))->Fetch();
                if ($rs) {
                    $arFields['CODE'] = $rs['CODE'];
                    $importantValue = array("VALUE" => $rs['PROPERTY_LOCATION_VALUE']);
                }
            }
            if ($importantValue) {
                if (empty($list)) {
                    Main\UrlRewriter::add(SITE_ID, array(
                      'CONDITION' => '#^/' . $arFields['CODE'] . '(/(catalog|chistoe-proizvodstvo)(.*))#',
                      'RULE' => 'path=$1&importantId=' . $importantValue['VALUE'] . '&importantItemId=' . $arFields['ID'],
                      'ID' => 'city-' . $arFields['ID'],
                      'PATH' => '/replace.php',
                      'SORT' => 0,
                    ));
                } else {
                    Main\UrlRewriter::update(SITE_ID, array(
                      "ID" => "city-" . $arFields['ID']
                    ), array(
                      'CONDITION' => '#^/' . $arFields['CODE'] . '(/(catalog|chistoe-proizvodstvo)(.*))#',
                      'RULE' => 'path=$1&importantId=' . $importantValue['VALUE'] . '&importantItemId=' . $arFields['ID'],
                      'ID' => 'city-' . $arFields['ID'],
                      'PATH' => '/replace.php',
                      'SORT' => 0,
                    ));
                }

                if (empty($listMain)) {
                    Main\UrlRewriter::add(SITE_ID, array(
                      'CONDITION' => '#^/' . $arFields['CODE'] . '/(\?bxrand.*)?$#',
                      'RULE' => 'path=/&importantId=' . $importantValue['VALUE'] . '&importantItemId=' . $arFields['ID'],
                      'ID' => 'city-main-' . $arFields['ID'],
                      'PATH' => '/replace.php',
                      'SORT' => 0,
                    ));
                } else {
                    Main\UrlRewriter::update(SITE_ID, array(
                      "ID" => "city-main-" . $arFields['ID']
                    ), array(
                      'CONDITION' => '#^/' . $arFields['CODE'] . '/(\?bxrand.*)?$#',
                      'RULE' => 'path=/&importantId=' . $importantValue['VALUE'] . '&importantItemId=' . $arFields['ID'],
                      'ID' => 'city-main-' . $arFields['ID'],
                      'PATH' => '/replace.php',
                      'SORT' => 0,
                    ));
                }
            }
        }
    }
}

function deleteCityRewrite($arFields)
{
    if ($arFields['IBLOCK_ID'] == 93) {
        Main\UrlRewriter::delete(SITE_ID, array(
          "ID" => "city-" . $arFields['ID']
        ));
        Main\UrlRewriter::delete(SITE_ID, array(
          "ID" => "city-main-" . $arFields['ID']
        ));
    }
}

function replaceCityLinks(&$content)
{
    if ($GLOBALS['importantCity']) {
        $content = preg_replace("/href=\"(\/(catalog|chistoe-proizvodstvo)(?!bitrix)[^\"]*)/",
          "href=\"/" . $GLOBALS['importantCity'] . "$1", $content);
        //$content = str_replace('href="/"', 'href="/' . $GLOBALS['importantCity'] . '/"', $content);

    }
}

//AddEventHandler("main", "OnBeforeProlog", "setServerVariables");
//AddEventHandler("main", "OnEpilog", "resetServerVariables");
//function setServerVariables()
//{
//  if ($GLOBALS['serverReplace']) {
//    foreach ($GLOBALS['serverReplace']['replace'] as $key => $value) {
//      $_SERVER[$key] = $value;
//    }
//    global $APPLICATION;
//    $GLOBALS["APPLICATION"]->reinitPath();
//
//  }
//}
//
//function resetServerVariables()
//{
//  if ($GLOBALS['serverReplace']) {
//    foreach ($GLOBALS['serverReplace']['original'] as $key => $value) {
//      $_SERVER[$key] = $value;
//    }
//    global $APPLICATION;
//    $GLOBALS["APPLICATION"]->reinitPath();
//    if ($_SERVER['REMOTE_ADDR'] == "81.30.216.149" ) {
//      echo "<pre>";
//      print_r($APPLICATION->GetCurPage());
//      echo "</pre>";
//      echo "<pre>";
//      print_r($_SERVER);
//      echo "</pre>";
//      die();
//    }
//  }
//}

Main\EventManager::getInstance()->addEventHandler(
  'sale',
  'OnSaleOrderBeforeSaved',
  'checkBonusWithdraw'
);
Main\EventManager::getInstance()->addEventHandler(
  'sale',
  'OnSaleOrderSaved',
  'bonusWithdrawReal'
);
function checkBonusWithdraw(Main\Event $event)
{
    /** @var \Bitrix\Sale\Order $order */
    $order = $event->getParameter("ENTITY");
    if ($_POST['confirmorder'] == 'Y' && $_POST['bonus_withdraw_agreement'] == 'Y' && BonusCard::isset() && BonusCard::allow()) {
        $amount = $_POST['bonus_withdraw_amount'];
        $max = BonusCard::amount();
        if ($amount > $max) {
            $amount = $max;
        }
        $code = implode("", $_POST['withdraw_code']);
        $result = BonusCard::confirmation_withdrawal($code);
        if ($result['error']) {
            $event->addResult(new Main\EventResult(Main\EventResult::ERROR,
              new ResultError($result['message'], 'BONUS_CODE_ERROR'), 'sale'));
        } else {
            $GLOBALS['BONUS_SUCCESS'] = $amount;
        }
    }
}

function bonusWithdrawReal(Main\Event $event)
{
    /** @var \Bitrix\Sale\Order $order */
    $order = $event->getParameter("ENTITY");
    if ($_POST['PERSONAL_BIRTHDAY'] || $_POST['PERSONAL_GENDER']) {
        $u = new CUser();
        $u->Update($order->getUserId(), array_filter([
          'PERSONAL_BIRTHDAY' => $_POST['PERSONAL_BIRTHDAY'] ? date('d.m.Y',
            strtotime($_POST['PERSONAL_BIRTHDAY'])) : '',
          'PERSONAL_GENDER' => $_POST['PERSONAL_GENDER']
        ]));
    }
    if ($GLOBALS['BONUS_SUCCESS']) {
        BonusCard::order_withdraw($order, $GLOBALS['BONUS_SUCCESS']);
    }
    B24Sync::upload($order);
    if (!defined('EDIT_ORDER') && $_POST['confirmorder'] == 'Y' && $_POST['track']) {
        global $DB;
        $DB->Query("INSERT INTO k_order_trace (order_id, trace) VALUES (".$order->getId().", '".$_POST['track']."')");
    }
}

function testOrderPayment()
{
    CModule::IncludeModule('sale');
    $order = Order::load(3217);
    BonusCard::order_withdraw($order, 100);
}

AddEventHandler("sale", "OnSaleBeforeOrderCanceled", "OnSaleBeforeOrderCanceledHandlers");

function OnSaleBeforeOrderCanceledHandlers(&$order)
{
    if ($order->isCanceled()) {
        $order->setField("STATUS_ID", 'C');
    }
}
