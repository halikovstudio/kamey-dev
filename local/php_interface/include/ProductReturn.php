<?php

use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Order;
use Dompdf\Dompdf;

class ProductReturn
{
    private $data;
    /** @var Order */
    public $order;
    public $saveResult = 'not saved';

    public function __construct($data)
    {
        CModule::IncludeModule('sale');
        $this->order = Order::load($data['order_id']);
        $this->data = $data;
    }

    public function make()
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/dompdf/autoload.inc.php';
        $dompdf = new Dompdf();
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->loadHtml($this->getHtml());
        $dompdf->render();
        $output = $dompdf->output();

        $this->saveResult = file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/returns/' . $this->order->getId() . '-' . time() . '.pdf',
          $output);
        return '/upload/returns/' . $this->order->getId() . '-' . time() . '.pdf';
    }

    public function getHtml()
    {
        ob_start();
        /** @var Bitrix\Main\Type\DateTime $date */
        $date = $this->order->getField('DATE_INSERT');
        echo $this->getStyles();
        ?>
        <div style="width: 280px;margin-left: auto; text-align: right">
            Директору ООО "Торговый Дом "Камея"<br>
            От<br>
            <div class="field"><?= $this->data['surname'] ?> <?= $this->data['name'] ?> <?= $this->data['middle_name'] ?></div>
            <span style="font-size: 10px">ФИО</span><br>
            <div class="field"><?= $this->data['passport_series'] ?> <?= $this->data['passport_number'] ?></div>
            <span style="font-size: 10px">паспорт (серия, номер)</span><br>
            <div class="field"><?= $this->data['passport_issued_by'] ?>, <?= $this->data['passport_date'] ?></div>
            <span style="font-size: 10px">выдан (кем, когда)</span><br>
            <div class="field"><?= $this->data['inn'] ?></div>
            <span style="font-size: 10px">ИНН</span><br>
        </div>
        <div style="height: 16px"></div>
        <h1 class="text-center">ЗАЯВЛЕНИЕ О ВОЗВРАТЕ ТОВАРА</h1>
        <div>"<?= $date->format('d') ?>" <?= $this->getMonth($date->format('m')) ?> <?= $date->format('Y') ?>г. в
            интернет-магазине "CAMEO" (<a
                    href="https://www.kamey.ru">https://www.kamey.ru</a>), по заказу
            №<?= $this->order->getField('ACCOUNT_NUMBER') ?>, был приобретен товар:
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>№</th>
                <th>Наименование полное</th>
                <th>Размер/<br/>Рост</th>
                <th>Кол-во,<br/>шт</th>
                <th>Цена,<br/>руб</th>
                <th>Сумма,<br/>руб</th>
            </tr>
            </thead>
            <tbody>
            <?
            $i = 1;
            $sum = 0;
            $reasons = [];
            $defects = [];
            foreach ($this->data['product'] as $basketId => $basketData) {
                if (!isset($basketData['checked'])) {
                    continue;
                }
                $reasons[$basketData['type']] = true;
                if ($basketData['type'] == 'defect') {
                    $defects[] = $basketData['defect_description'];
                }
                /** @var BasketItem $item */
                $item = $this->order->getBasket()->getItemById($basketId);
                $productId = $item->getProductId();
                $itemData = \Product\Basket::getData($item->getProductId());
                $sum += $item->getPrice() * $item->getQuantity();
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td class="text-left"><?= $item->getField('NAME') ?></td>
                    <td><?= $itemData['props']['size']['value'] ?>/<?= $itemData['props']['height']['value'] ?></td>
                    <td><?= $item->getQuantity() ?></td>
                    <td><?= $item->getPrice() ?></td>
                    <td><?= $item->getPrice() * $item->getQuantity() ?></td>
                </tr>
                <?
                $i++;
            }
            ?>
            </tbody>
        </table>
        <br>
        <div>Основание для возврата:</div>
        <div class="list">
            <?
            foreach ($reasons as $reason => $bool) {
                ?>
                <div>
                    <?
                    switch ($reason) {
                        case 'other':
                            echo 'Доставлен другой товар';
                            break;
                        case 'damage':
                            echo 'Товар поврежден';
                            break;
                        case 'small':
                            echo 'Не подходит по размеру (мал)';
                            break;
                        case 'big':
                            echo 'Не подходит по размеру (велик)';
                            break;
                        case 'defect':
                            echo 'Брак. А именно: ' . implode(", ", $defects);
                            break;
                    }
                    ?>
                </div>
                <?
            }
            ?>
        </div>
        <br>
        <div>На основании вышеизложенного прошу вернуть мне сумму в размере <?= $sum ?> рублей
            (<?= Number2Word_Rus($sum) ?>)
        </div>
        <br>
        <div>Прошу перечислить
            деньги <?= ($this->data['payment'] == 'bank' ? 'на расчетный счет' : 'почтовым переводом') ?>:
        </div>
        <div class="list">
            <?
            if ($this->data['payment'] == 'bank') {
                ?>
                <div>ФИО владльца счета: <?= $this->data['payment_name'] ?></div>
                <div>Наименование банка: <?= $this->data['bank_name'] ?></div>
                <div>ИНН банка: <?= $this->data['bank_inn'] ?></div>
                <div>БИК: <?= $this->data['bank_bik'] ?></div>
                <div>Кор. счет: <?= $this->data['bank_ks'] ?></div>
                <div>Лицевой счет (если есть): <?= $this->data['bank_personal_account'] ?></div>
                <div>Расчетный счет: <?= $this->data['bank_rs'] ?></div>
                <?
            } else {
                ?>
                <div>Адрес перевода: <?= $this->data['post_address'] ?></div>
                <?
            }
            ?>
        </div>
        <br>
        <div>
            Я, <?= $this->data['surname'] ?> <?= $this->data['name'] ?> <?= $this->data['middle_name'] ?> прилагаю:
        </div>
        <ul>
            <li>Товарный чек "___" ____________ 20___г. №_________</li>
        </ul>
        <br>
        <br>
        <table style="width: 100%">
            <tr>
                <td>
                    "___" ____________ 20___г.
                </td>
                <td class="text-right">
                    _________________________
                    <span style="font-size: 10px">подпись</span>
                </td>
            </tr>
        </table>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    public function getStyles()
    {
        ob_start();
        ?>
        <style>
            body {
                font-family: DejaVu Sans;
                font-size: 12px
            }

            .list {
                padding-left: 40px;
            }

            .text-left {
                text-align: left !important;
            }

            .text-right {
                text-align: right !important;
            }

            .text-center {
                text-align: center;
            }

            h1 {
                font-size: 14px;
            }

            .field {
                border-bottom: 1px solid black;
                margin-top: 2px;
            }

            .input {
                text-align: left;
                display: inline-block;
            }

            .table {
                border-left: 0.01em solid #ccc;
                border-right: 0;
                border-top: 0.01em solid #ccc;
                border-bottom: 0;
                border-collapse: collapse;
            }

            .table td,
            .table th {
                border-left: 0;
                border-right: 0.01em solid #ccc;
                border-top: 0;
                border-bottom: 0.01em solid #ccc;
                text-align: center;
                padding: 5px;
            }

            .table {
                width: 100%;
                margin-top: 16px;
            }
        </style>
        <?
        $css = ob_get_contents();
        ob_end_clean();

        return $css;
    }

    public function getMonth($value)
    {
        switch ($value) {
            case '01':
                return 'января';
            case '02':
                return 'февраля';
            case '03':
                return 'марта';
            case '04':
                return 'апреля';
            case '05':
                return 'мая';
            case '06':
                return 'июня';
            case '07':
                return 'июля';
            case '08':
                return 'августа';
            case '09':
                return 'сентября';
            case '10':
                return 'октября';
            case '11':
                return 'ноября';
            case '12':
                return 'декабря';
        }

        return '';
    }
}
