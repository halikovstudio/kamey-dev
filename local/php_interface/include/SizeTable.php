<?php

class SizeTable
{
    const IBLOCK_ID = 98;
    public $data = [];
    public $type;
    public $gender;
    public $typeExists = false;

    public function __construct($type, $gender)
    {
        $this->type = $type;
        $this->gender = $gender;
        $this->init();
    }

    public function init()
    {
        $result = [];
        $cache = Bitrix\Main\Data\Cache::createInstance();
        if ($cache->initCache(86400, "size-table-gender-" . $this->gender)) {
            $result = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $result = array();
            CModule::IncludeModule('iblock');
            $sections = [];
            $rsSection = CIBlockSection::GetList([],
              ['IBLOCK_ID' => self::IBLOCK_ID, 'UF_GENDER' => $this->gender], false,
              ['ID', 'NAME', 'UF_TYPE', 'UF_GENDER']);
            while ($ob = $rsSection->Fetch()) {
                $sections[$ob['ID']] = [
                  'name' => self::getTypeName($ob['UF_TYPE']),
                  'type' => $ob['UF_TYPE'],
                  'active' => $ob['UF_TYPE'] == $this->type,
                  'gender' => $ob['UF_GENDER'],
                  'items' => []
                ];
            }
            foreach ($sections as $section_id => $data) {
                $rsItems = CIBlockElement::GetList(['SORT' => "ASC"],
                  ['IBLOCK_ID' => self::IBLOCK_ID, 'SECTION_ID' => $section_id], false, false,
                  ['ID', 'IBLOCK_ID', 'PROPERTY_*', 'SECTION_ID']);
                while ($ob = $rsItems->GetNextElement(true, false)) {
                    $props = $ob->GetProperties();
                    foreach ($props as $prop) {
                        $sections[$section_id]['items'][$prop['NAME']][] = $prop['VALUE'];
                    }
                }
                foreach ($sections[$section_id]['items'] as $key => $values) {
                    if (!array_filter($values)) {
                        unset($sections[$section_id]['items'][$key]);
                    }
                }
            }

            $result = $sections;
            if (!empty($result)) {
                $cache->endDataCache($result);
            } else {
                $cache->abortDataCache();
            }
        }
        $this->data = $result;
        foreach ($this->data as $key => $value) {
            if ($value['type'] == $this->type) {
                $this->typeExists = true;
            }
        }
    }

    public static function getTypeName($type)
    {
        switch ($type) {
            case 'hats':
                return 'Головные уборы';
            case 'gown':
                return 'Платья';
            case 'aprons':
                return 'Фартуки';
            case 'pants':
                return 'Брюки';
            case "skirts":
                return 'Юбки';
            case "dress":
                return 'Халаты';
            case "blouse":
                return 'Блузы';
            case 'vest':
                return 'Жилеты';
            case 'costume':
                return 'Костюмы';
            default:
                return '-';
        }
    }

    public static function getUserTable()
    {
        $sizes = self::getSizes();
        if (empty($sizes)) {
            ?>
            <tbody>
            <tr>
                <td>
                    Необходимо заполнить все параметры
                </td>
                <td><a class="ui mini primary button custom" data-modal="find">Заполнить</a></td>
            </tr>
            </tbody>
            <?
            return;
        }
        ?>
        <thead>
        <tr>
            <?
            foreach (array_keys($sizes) as $name) {
                ?>
                <th><?= $name ?></th>
                <?
            }
            ?>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?
        $sizes = array_values($sizes);
        foreach (current($sizes) as $name => $value) {
            ?>
            <tr><?
            foreach ($sizes as $key => $_size) {
                ?>
                <td>
                    <?= $sizes[$key][$name] ?>
                </td>
                <?
            }
            ?>
            <td><?= $name ?></td></tr><?
        }
        ?>
        </tbody>
        <?
    }

    public static function getSizes($type = null)
    {
        $arUser = CUser::IsAuthorized() ? \CUser::GetByID(\CUser::GetID())->Fetch() : $_SESSION['arUser'];
        if ($type != null && !$arUser['UF_AUTO_SIZE']) {
            return [
              'use' => false
            ];
        }
        /** @var int $height Рост */
        $height = $arUser['UF_HEIGHT'];
        /** @var int $height Талия */
        $waist = $arUser['UF_WAIST'];
        /** @var int $height Бедра */
        $hip = $arUser['UF_HIP'];
        /** @var int $height Грудь */
        $chest = $arUser['UF_CHEST'];
        $gender = $arUser['PERSONAL_GENDER'] == 'F' ? 'Женский' : 'Мужской';
        if (!$waist || !$hip || !$chest) {
            return [];
        }
        CModule::IncludeModule('iblock');
        $result = [];
        $sectionFilter = ['IBLOCK_ID' => self::IBLOCK_ID, 'UF_GENDER' => $gender];
        if ($type != null) {
            $sectionFilter['UF_TYPE'] = $type;
        }
        $rsSection = CIBlockSection::GetList([],
          $sectionFilter, false,
          ['ID', 'NAME', 'UF_TYPE', 'UF_GENDER']);
        while ($arSection = $rsSection->Fetch()) {
            $rsItems = CIBlockElement::GetList(['SORT' => "ASC"],
              ['IBLOCK_ID' => self::IBLOCK_ID, 'SECTION_ID' => $arSection['ID']], false, false,
              ['ID', 'IBLOCK_ID', 'PROPERTY_*', 'SECTION_ID']);
            $items = [];
            $first = null;
            $firstChance = 0;
            while ($ob = $rsItems->GetNextElement(true, false)) {
                $props = $ob->GetProperties();
                $sizes = [];
                $checkSizes = [];
                foreach ($props as $prop) {
                    switch ($prop['CODE']) {
                        case 'OBKHVAT_GRUDI_SM':
                            $checkSizes['chest'] = explode("-", str_replace(" ", "", $prop['VALUE']));
                            break;
                        case 'OBKHVAT_TALII_SM':
                            $checkSizes['waist'] = explode("-", str_replace(" ", "", $prop['VALUE']));
                            break;
                        case 'OBKHVAT_BEDER_SM':
                            $checkSizes['hip'] = explode("-", str_replace(" ", "", $prop['VALUE']));
                            break;
                        case 'ROSSIYA':
$sizes[$type != null ? $prop['CODE'] : '<img src="'. SITE_TEMPLATE_PATH .'/images/rus.png" alt=""
                         class="ui image yourparams__image">'] = $prop['VALUE'];
                            break;
                        default:
                            //$sizes[$type != null ? $prop['CODE'] : $prop['NAME']] = $prop['VALUE'];
                            break;
                    }
                }
                $chance = 0;
                if ($waist >= $checkSizes['waist'][0] && $waist < $checkSizes['waist'][1]) {
                    $chance++;
                }
                if ($hip >= $checkSizes['hip'][0] && $hip < $checkSizes['hip'][1]) {
                    $chance++;
                }
                if ($chest >= $checkSizes['chest'][0] && $chest < $checkSizes['chest'][1]) {
                    $chance++;
                }
                $items[$chance] = $sizes;
                if ($first == null) {
                    $first = $sizes;
                    if ($waist <= $checkSizes['waist'][0]) {
                        $firstChance++;
                    }
                    if ($hip <= $checkSizes['hip'][0]) {
                        $firstChance++;
                    }
                    if ($chest <= $checkSizes['chest'][0]) {
                        $firstChance++;
                    }
                }
            }
            $item = $items[max(array_keys($items))];
            if (count($items) == 1 && $firstChance > 0) {
                $item = $first;
            }
            $result[self::getTypeName($arSection['UF_TYPE'])] = $item;
        }
        if ($type != null) {
            $result = current($result);
            $result['height'] = $height;
        }

        return $result;
    }
}
