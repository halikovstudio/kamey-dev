<?php

use Bitrix\Sale\Delivery\ExtraServices\Base;
use Bitrix\Sale\Order;
use Bitrix\Sale\Shipment;
use safin\delivery\cdek;

class B24Sync
{
    const STATUS_ALLOW = '3';
    const STATUS_ALLOW_ERROR = '2';
    const STATUS_NEW = 'NEW';
    static $fields = [
      'site' => [
          //Идентификатор заказа
        'id' => '',
          //Номер заказа
        'account_number' => '',
          //Стоимость доставки
        'delivery_price' => '',
          //Наименование способа доставки
        'delivery_name' => '',
          //Адрес доставки
        'delivery_address' => '',
          //Трек номер
        'tracking_number' => '',
          //Название пункта выдачи
        'point' => '',
          //Код пункта выдачи
        'point_value' => '',
          //Примерка
        'fitting' => '',
          //Ссылка на накладную
        'document' => '',
          //Наименование платежной системы
        'payment_name' => '',
          //Комментарий пользователя
        'comment' => '',
        'campaign' => '',
        'content' => '',
        'medium' => '',
        'source' => '',
        'term' => '',
          //Дата оплаты
        'payment_date' => ''
      ],
      'crm' => [
        'deal' => [
          'UF_CRM_1572243570170' => 'account_number',
          'UF_CRM_1572417857096' => 'fitting',
          'UF_CRM_1572417779338' => 'delivery_name',
          'UF_CRM_1569411875502' => 'delivery_address',
          'UF_CRM_1571913836081' => 'delivery_price',
          'UF_CRM_1571913856878' => 'document',
          'UF_CRM_1571913876733' => 'point',
          'UF_CRM_1571914186481' => 'tracking_number',
          'ORIGIN_ID' => 'id',
          'UF_CRM_1572513474371' => 'payment_name',
          'UF_CRM_1572513497084' => 'payed',
          'UF_CRM_1572513525449' => 'is_payed',
          'COMMENTS' => 'comment',
          'UF_CRM_1575717685138' => 'payment_link',
          'UF_CRM_1582088283197' => 'payment_date'
        ],
        'lead' => []
      ]
    ];
    static private $component;

    static function upload(Order $order)
    {
        $dealId = self::getId($order->getId(), 'deal');
        if ($dealId) {
            self::initOrderFields($order);
            self::uploadDeal($dealId);
        } else {
            $leadId = self::getId($order->getId(), 'lead');
            if ($leadId) {
                self::initOrderFields($order);
                self::uploadLead($leadId);
            }
        }
    }

    static function getId($orderId, $type = 'deal')
    {
        $res = self::component()->Send('crm.' . $type . '.list',
          ['filter' => ['ORIGIN_ID' => $orderId], 'select' => ['ID']]);
        if ($res && $res['result'][0]) {
            return $res['result'][0]['ID'];
        }

        return false;
    }

    static function component()
    {
        if (self::$component == null) {
            $path = $_SERVER['DOCUMENT_ROOT'] . '/edit-order/bitrix24.json';
            $config = file_get_contents($path);
            $config = json_decode($config, true);
            $config['config_path'] = $path;
            self::$component = new Bitrix24Component($config);
        }

        return self::$component;
    }

    static function initOrderFields(Order $order)
    {
        /** @var $shipment Shipment */
        $shipment = $order->getShipmentCollection()[0];

        self::$fields['site']['id'] = $order->getId();
        self::$fields['site']['account_number'] = $order->getField('ACCOUNT_NUMBER');
        self::$fields['site']['delivery_price'] = $order->getDeliveryPrice() ?: 0;
        self::$fields['site']['delivery_name'] = $shipment->getDeliveryName();
        if ($order->getPropertyCollection()->getAddress()) {
            self::$fields['site']['delivery_address'] = $order->getPropertyCollection()->getAddress()->getValue();
        } else {
            self::$fields['site']['delivery_address'] = '';
        }
        self::$fields['site']['tracking_number'] = $order->getField('TRACKING_NUMBER');
        self::$fields['site']['point'] = '';
        self::$fields['site']['point_value'] = '';
        self::$fields['site']['fitting'] = 'Нет';

        self::$fields['site']['document'] = $shipment->getField("COMMENTS");

        self::$fields['site']['comment'] = $order->getField('USER_DESCRIPTION');

        $delivery = $shipment->getDelivery();
        if ($delivery instanceof cdek) {
            $extraValues = $shipment->getExtraServices();
            foreach ($delivery->getExtraServices()->getItems() as $id => $service) {
                /** @var Base $service */
                $value = $extraValues[$id];
                switch ($service->getCode()) {
                    case "pvz":
                        /** @var CdekPvz $service */
                        $service->createOptions(true);
                        $service->setValue($value);
                        self::$fields['site']['point'] = $service->getFormattedValue(true) . '; ' . $service->getValue();
                        self::$fields['site']['point_value'] = $service->getValue();
                        self::$fields['site']['delivery_address'] = '';
                        break;
                    case "30":
                        self::$fields['site']['fitting'] = $service->getValue() == 'Y' ? 'Да' : 'Нет';
                        break;
                }
            }
        }
        $payment = BonusCard::getAnotherPayment($order->getPaymentCollection());
        if ($payment) {
            $paymentObject = \Bitrix\Sale\PaySystem\Manager::getById($payment->getPaymentSystemId());
            if (strlen($paymentObject['ACTION_FILE']) > 0) {
                self::$fields['site']['payment_link'] = 'https://www.kamey.ru/personal-area/order/payment.php' . "?ORDER_ID=" . $order->getField('ACCOUNT_NUMBER') . "&PAYMENT_ID=" . $payment->getField("ACCOUNT_NUMBER") . '&HASH=' . $order->getHash();
            } else {
                self::$fields['site']['payment_link'] = '';
            }
            self::$fields['site']['payment_name'] = $payment->getPaymentSystemName();
            try {
                if ($payment->getField("PAY_VOUCHER_DATE") && $payment->getField("PAY_VOUCHER_DATE")->getTimestamp() &&  $payment->getField("PAY_VOUCHER_DATE")->format('d.m.Y') != '01.01.1970') {
                    self::$fields['site']['payment_date'] = $payment->getField("PAY_VOUCHER_DATE")->format('d.m.Y');
                } elseif ($payment->getField('DATE_PAID') && $payment->getField("DATE_PAID")->getTimestamp()) {
                    self::$fields['site']['payment_date'] = $payment->getField('DATE_PAID')->format('d.m.Y H:i:s');
                } else {
                    self::$fields['site']['payment_date'] = '';
                }
            } catch (\Exception $e) {
                self::$fields['site']['payment_date'] = '';
            }
        } else {
            self::$fields['site']['payment_name'] = '';
            self::$fields['site']['payment_link'] = '';
            self::$fields['site']['payment_date'] = '';
        }
        self::$fields['site']['is_payed'] = $order->getField('PAYED') == 'Y';
        self::$fields['site']['payed'] = $order->getPaymentCollection()->getPaidSum();
    }

    static function uploadDeal($id)
    {
        $fields = self::map('deal');
        if (!empty($fields)) {
            $fields['ORIGINATOR_ID'] = 1;
            self::component()->Send('crm.deal.update', ['id' => $id, 'fields' => $fields]);
            //self::sendMessage("Обновлены поля сделки: ".print_r($fields, true),'deal', $id);
        }
    }

    static function uploadTrace($orderId, $isAccountNumber = false){
        global $DB;
        if ($isAccountNumber) {
            CModule::IncludeModule('sale');
            /** @var Order $order */
            $order = Order::loadByAccountNumber($orderId);
            $orderId = $order->getId();
        }
        $dealId = self::getId($orderId, 'deal');
        $result = $DB->Query("SELECT * from k_order_trace WHERE order_id=" . $orderId)->Fetch();
        if ($result) {
            $b24Result = B24Sync::component()->Send('crm.tracking.trace.add', [
              'ENTITIES' => [
                [
                  'TYPE' => 'DEAL',//COMPANY, CONTACT, DEAL, LEAD, QUOTE
                  'ID' => $dealId
                ]
              ],
              'TRACE' => $result['trace']
            ]);
        }
    }

    static function map($type)
    {
        $result = [];
        foreach (self::$fields['crm'][$type] as $crm => $site) {
            $result[$crm] = self::$fields['site'][$site];
        }

        return $result;
    }

    static function uploadLead($id)
    {
        $fields = self::map('lead');
        if (!empty($fields)) {
            self::component()->Send('crm.lead.update', ['id' => $id, 'fields' => $fields]);
        }
    }

    static function getOrderId($id, $type = 'deal')
    {
        $res = self::component()->Send("crm." . $type . '.get', ['id' => $id]);
        if ($res && $res['result']) {
            return $res['result']['ORIGIN_ID'];
        }

        return false;
    }

    static function changeStatus($orderId, $status, $message = '')
    {
        $dealId = self::getId($orderId);
        if ($dealId) {
            self::component()->Send("crm.deal.update", ['id' => $dealId, 'fields' => ['STAGE_ID' => $status]]);
            if ($message) {
                self::sendMessage($message, 'deal', $dealId);
            }
        }
    }

    static function sendMessage($message, $entityType, $entityId)
    {
        self::component()->Send("crm.livefeedmessage.add", [
          'fields' => [
            'MESSAGE' => $message,
            'ENTITYTYPEID' => $entityType == 'deal' ? 2 : 1,
            'ENTITYID' => $entityId,
            'TITLE' => '123'
          ]
        ]);
    }

    static function sendOrderMessage($orderId, $message)
    {
        $dealId = self::getId($orderId, 'deal');
        if ($dealId) {
            self::sendMessage($message, 'deal', $dealId);
        } else {
            $leadId = self::getId($orderId, 'lead');
            self::sendMessage($message, 'lead', $leadId);
        }
    }

    static function getDealbyOrderID($orderId)
    {
        $dealId = self::getId($orderId, 'deal');
        if ($dealId) {
            return self::component()->Send('crm.deal.get', ['id' => $dealId])['result'];
        }

        return false;
    }

    static function getDealList($filter)
    {
        return self::component()->Send('crm.deal.list', ['filter' => $filter])['result'];
    }
}
