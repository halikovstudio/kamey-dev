<?php

namespace Element {

  use CBitrixComponentTemplate;
  use CFile;
  use CIBlock;
  use CIBlockElement;
  use CIBlockSection;
  use Element\Property\Element;
  use Element\Property\File;
  use Element\Property\Group;
  use Element\Property\SharedList;
  use Element\Property\SharedNumber;
  use Element\Property\SharedString;
  use Element\Property\Undefined;

  /**
   * Class Item
   * @package Element
   *
   * @property Text name
   * @property Text text One of description, detailText, previewText
   * @property Text previewText
   * @property Text detailText
   * @property Text description
   *
   * @property Image previewPicture
   * @property Image detailPicture
   * @property Image picture
   *
   * @property Price price
   * @property Price[] priceList
   *
   * @property int id
   * @property int iblockId
   * @property int sectionId
   * @property int measureId
   * @property int ratio
   *
   * @property string link
   * @property string code
   * @property string measure
   *
   * @property array skuProps
   *
   * @property bool canBuy
   */
  class Item extends \ArrayObject
  {
    public $hasOffers = false;
    public $available = true;
    /**
     * @var self[] $offers
     */
    public $offers = array();
    /**
     * @var SharedString[]|SharedNumber[]|File[]|SharedList[]|Element[]|Group[]|Undefined[] $props
     */
    public $props = array();
    public $isSection = false;
    private $data;
    private $map = array(
      "name" => "NAME",
      "code" => "CODE",
      "previewText" => "PREVIEW_TEXT",
      "previewPicture" => "PREVIEW_PICTURE",
      "detailText" => "DETAIL_TEXT",
      "detailPicture" => "DETAIL_PICTURE",
      "picture" => "PICTURE",
      "description" => "DESCRIPTION",
      "iblockId" => "IBLOCK_ID",
      "link" => array(
        "DETAIL_PAGE_URL",
        "SECTION_PAGE_URL"
      ),
      "price" => "MIN_PRICE",
      "priceList" => array(
        "PRICES",
        "ITEM_PRICES"
      ),
      "sectionId" => "IBLOCK_SECTION_ID",
      "id" => "ID",
      "skuProps" => "SKU_PROPS",
      "measure" => "CATALOG_MEASURE_NAME",
      "measureId" => "CATALOG_MEASURE",
      "ratio" => "CATALOG_MEASURE_RATIO",
      "canBuy" => "CAN_BUY"
    );
    private $types = array(
      "image" => array(
        "PREVIEW_PICTURE",
        "DETAIL_PICTURE",
        "PICTURE"
      ),
      "text" => array(
        "NAME",
        "PREVIEW_TEXT",
        "DETAIL_TEXT",
        "DESCRIPTION",
        "text"
      ),
      "price" => array(
        "MIN_PRICE",
        "PRICES" => true,
        "ITEM_PRICES"=>true
      )
    );

    public function __construct($data)
    {
      if (!is_array($data)) {
        $data = array();
      }
      $this->data = $data;
      $this->initProperties();
      $this->initTexts();
      $this->initImages();
      $this->initOffers();
      $this->initPrices();
      $this->detectSection();
      parent::__construct($this->data);
    }

    private function initProperties()
    {
      $list = array();
      foreach ($this->data['DISPLAY_PROPERTIES'] as $code => $datum) {
        $list[$code] = $datum;
      }
      foreach ($this->data['PROPERTIES'] as $code => $datum) {
        if (!isset($list[$code])) {
          $list[$code] = $datum;
        }
      }
      foreach ($this->data as $key => $item) {
        preg_match("/^PROPERTY_(.+)_VALUE(_ID)?/", $key, $match);
        if ($match[2]) {
          $list[strtolower($match[1])]['ID'] = $item;
          unset($this->data[$key]);
        } elseif ($match[1]) {
          $list[strtolower($match[1])] = array(
            "VALUE" => $item
          );
          unset($this->data[$key]);
        }
        preg_match("/^UF_(.+)/", $key, $match);
        if ($match[1]) {
          $this->isSection = true;
          $list[strtolower($match[1])] = array(
            "VALUE" => $item
          );
        }
      }
      $list = array_filter($list);
      foreach ($list as $code => $item) {
        switch ($item['PROPERTY_TYPE']) {
          case "S":
            $this->props[$code] = new SharedString($item);
            break;
          case "N":
            $this->props[$code] = new SharedNumber($item);
            break;
          case "F":
            $this->props[$code] = new File($item);
            break;
          case "L":
            $this->props[$code] = new SharedList($item);
            break;
          case "E":
            $this->props[$code] = new Element($item);
            break;
          case "G":
            $this->props[$code] = new Group($item);
            break;
          default:
            $this->props[$code] = new Undefined($item);
            break;
        }
      }
      $this->props['__undefined'] = new Undefined(array());
    }

    private function initTexts()
    {
      $this->data['text'] = $this->data['DESCRIPTION'] ?: $this->data['DETAIL_TEXT'] ?: $this->data['PREVIEW_TEXT'];
      foreach ($this->types['text'] as $type) {
        if (!isset($this->data[$type])) {
          continue;
        }
        $this->data[$type] = new Text($this->data[$type]);
      }
    }

    private function initImages()
    {
      foreach ($this->types['image'] as $type) {
        if (!isset($this->data[$type])) {
          continue;
        }
        $this->data[$type] = new Image($this->data[$type]);
      }
    }

    private function initOffers()
    {
      if (!empty($this->data['OFFERS'])) {
        $this->hasOffers = true;
        $keys = null;
        $matrix = null;
        if (!$this->data['MIN_PRICE']) {
          $keys = array();
          $matrix = array();
        }
        foreach ($this->data['OFFERS'] as $key => $offer) {
          if (is_array($keys)) {
            $keys[$key] = $offer['MIN_PRICE']['VALUE'];
            $matrix[$key] = $offer['MIN_PRICE'];
          }
          $this->data['OFFERS'][$key] = new self($offer);
        }
        if (is_array($keys)) {
          $min = min($keys);
          $key = array_search($min, $keys);
          if ($key) {
            $this->data['MIN_PRICE'] = $matrix[$key];
          }
          unset($min, $key, $keys, $matrix);
        }
        $this->offers =& $this->data['OFFERS'];
      }
    }

    private function initPrices()
    {
      if (!$this->data['MIN_PRICE'] && !empty($this->data['PRICES'])) {
        $this->data['MIN_PRICE'] = current($this->data['PRICES']);
      }
      if (!$this->data['MIN_PRICE'] && !empty($this->data['ITEM_PRICES'])) {
        $this->data['MIN_PRICE'] = current($this->data['ITEM_PRICES']);
      }
      foreach ($this->types['price'] as $key => $type) {
        $isArray = false;
        if (is_bool($type)) {
          $isArray = $type;
          $type = $key;
        }
        if (!isset($this->data[$type]) || $this->data[$type] === false) {
          continue;
        }
        if ($isArray) {
          foreach ($this->data[$type] as &$priceItem) {
            $priceItem = new Price($priceItem);
          }
        } else {
          $this->data[$type] = new Price($this->data[$type]);
        }
      }
      $this->available = $this->data['CATALOG_QUANTITY'] > 0;
    }

    private function detectSection()
    {
      $list = array(
        "SECTION_PAGE_URL",
        "DESCRIPTION",
        "PICTURE"
      );
      foreach ($list as $item) {
        if (isset($this->data[$item])) {
          $this->isSection = true;
          break;
        }
      }
    }

    /**
     * @param $data
     *
     * @return void
     */
    public static function cast(&$data)
    {
      $data = new self($data);
    }

    public function __get($name)
    {
      $map = $this->map[$name];
      if ($map) {
        if (is_array($map)) {
          foreach ($map as $value) {
            if (isset($this->data[$value])) {
              return $this->data[$value];
            }
          }
        } else {
          return $this->data[$map];
        }
      }
      if (isset($this->data[$name])) {
        return $this->data[$name];
      }
      $underScoreName = Utility::camelCaseToUnderscore($name);
      if (isset($this->data[$underScoreName])) {
        return $this->data[$underScoreName];
      }

      return null;
    }

    /**
     * @param string $code
     *
     * @return SharedString|SharedNumber|File|SharedList|Element|Group|Undefined
     */
    public function prop($code)
    {
      return isset($this->props[$code]) ? $this->props[$code] : $this->props['__undefined'];
    }

    /**
     * Example:
     * <code>
     * <div id="<?=$item->area($this)?>"></div>
     * </code>
     *
     * @param CBitrixComponentTemplate $component
     * @param bool $edit
     * @param bool $delete
     *
     * @return string
     */
    function area($component, $edit = true, $delete = true)
    {
      $prefix = $this->isSection ? "SECTION" : 'ELEMENT';
      if (!$this->data['IBLOCK_ID']) {
        if ($component->getComponent()->arParams && $component->getComponent()->arParams['IBLOCK_ID']) {
          $this->data['IBLOCK_ID'] = $component->getComponent()->arParams['IBLOCK_ID'];
        }
      }
      if (!$this->data['IBLOCK_ID']) {
        if ($this->isSection) {
          $rs = CIBlockSection::GetList(array(), array("ID" => $this->id), false, array("IBLOCK_ID"))->Fetch();
          $this->data['IBLOCK_ID'] = $rs['IBLOCK_ID'];
          unset($rs);
        } else {
          $this->data['IBLOCK_ID'] = CIBlockElement::GetIBlockByID($this->id);
        }
      }
      if ((!$this->data['EDIT_LINK'] || !$this->data['DELETE_LINK']) && $this->iblockId) {
        $arButtons = CIBlock::GetPanelButtons(
          $this->iblockId,
          ($this->isSection ? 0 : $this->id),
          ($this->isSection ? $this->id : 0),
          array("SESSID" => false)
        );
        $this->data["EDIT_LINK"] = $arButtons["edit"]["edit_" . ($this->isSection ? 'section' : 'element')]["ACTION_URL"];
        $this->data["DELETE_LINK"] = $arButtons["edit"]["delete_" . ($this->isSection ? 'section' : 'element')]["ACTION_URL"];
        unset($arButtons);
      }
      if ($edit && $this->data['EDIT_LINK']) {
        $component->AddEditAction($this->id, $this->EDIT_LINK, CIBlock::GetArrayByID($this->iblockId, $prefix . "_EDIT"));
      }
      if ($delete && $this->data['DELETE_LINK']) {
        $component->AddDeleteAction($this->id, $this->DELETE_LINK, CIBlock::GetArrayByID($this->iblockId, $prefix . "_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_' . $prefix . '_DELETE_CONFIRM')));
      }

      return $component->GetEditAreaId($this->id);
    }

    /**
     * @return self
     */
    function getSelectedOffer()
    {
      if ($this->OFFERS_SELECTED != null) {
        return $this->offers[$this->OFFERS_SELECTED];
      } else {
        return $this->offers[0];
      }
    }

    function setSelectedOffer($id)
    {
      $this->OFFERS_SELECTED = $this->getOfferIndex($id);
    }

    function getOfferIndex($id)
    {
      $index = false;
      foreach ($this->offers as $key => $offer) {
        if ($offer->ID == $id) {
          $index = $key;
        }
      }

      return $index;
    }
  }

  class Collection
  {
  }

  class Text
  {
    public $length = 0;
    /**
     * @var
     */
    private $text;

    /**
     * DataText constructor.
     *
     * @param $data
     */
    function __construct($data)
    {
      $this->text = $data;
      $this->length = strlen(strval($this->text));
    }

    /**
     * @return string
     */
    function __toString()
    {
      return strval($this->text);
    }

    /**
     * @param int $width
     * @param string $marker
     *
     * @return string
     */
    function __invoke($width = 240, $marker = "...")
    {
      return $this->truncate($width, $marker);
    }

    /**
     * @param int $width
     * @param string $marker
     *
     * @return string
     */
    function truncate($width = 240, $marker = "...")
    {
      if ($marker === null) {
        $marker = '...';
      }

      return mb_strimwidth($this->text, 0, $width, $marker);
    }

    /**
     * @return mixed
     */
    public function getText()
    {
      return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
      $this->text = $text;
    }
  }

  /**
   * Class Image
   * @package Element
   *
   * @property string alt
   * @property int id
   * @property int width
   * @property int height
   */
  class Image extends \ArrayObject
  {
    public static $defineList = array(
      "no_photo",
      "no_image",
      "NO_PHOTO",
      "NO_IMAGE",
      "default_photo",
      "default_image",
      "DEFAULT_PHOTO",
      "DEFAULT_IMAGE",
      "image_thumb",
      "thumb",
      "THUMB"
    );
    public static $defaultImage = '';
    /**
     * @var array
     */
    public $data;

    /**
     * DataImage constructor.
     *
     * @param $object
     */
    function __construct($object)
    {
      if (is_null($object)) {
        $object = array();
      }
      $this->data = $object;
      if (is_array($object)) {
        parent::__construct($this->data);
      }
    }

    /**
     * @return string
     */
    function __toString()
    {
      $this->init();

      return strval($this->data['resize'] !== false ? $this->data['SRC'] : '');
    }

    function init()
    {
      if (is_numeric($this->data)) {
        $this->data = CFile::GetFileArray($this->data);
        parent::__construct($this->data);
      } elseif (empty($this->data)) {
        $src = self::$defaultImage;
        foreach (self::$defineList as $item) {
          if (defined($item)) {
            $src = constant($item);
            break;
          }
        }
        $this->data = array(
          "SRC" => $src,
          "resize" => false
        );
      }
    }

    /**
     * @param $width
     * @param $height
     * @param bool|string $placeholder
     *
     * @return string
     */
    function __invoke($width, $height = null, $placeholder = false)
    {
      $this->init();

      return $this->resize($width, $height, $placeholder);
    }

    /**
     * @param $width
     * @param $height
     * @param bool|string $placeholder
     *
     * @return string
     */
    public function resize($width, $height = null, $placeholder = false)
    {
      $this->init();
      if ($this->data['resize'] === false && $placeholder) {
        if ($placeholder === true) {
          return $this->src;
        } else {
          return $placeholder;
        }
      } elseif ($this->data['resize'] === false) {
        return '';
      }
      if ($this->data['resize'] === false || !$this->data['ID']) {
        return $this->src;
      }
      if (!$height) {
        $height = $width;
      }
      $resize = CFile::ResizeImageGet($this->data["ID"], array(
        'width' => $width,
        'height' => $height
      ));

      return $resize['src'];
    }

    function src($placeholder = false)
    {
      $this->init();
      if ($this->data['resize'] === false && $placeholder) {
        if ($placeholder === true) {
          return $this->src;
        } else {
          return $placeholder;
        }
      } elseif ($this->data['resize'] === false) {
        return '';
      } else {
        return $this->src;
      }
    }

    /**
     * @return float|int
     */
    function ratio()
    {
      $this->init();

      return $this->data['WIDTH'] / $this->data['HEIGHT'];
    }

    /**
     * @param $name
     *
     * @return null|mixed
     */
    function __get($name)
    {
      $this->init();
      if (isset($this->data[$name])) {
        return $this->data[$name];
      }
      $underScoreName = Utility::camelCaseToUnderscore($name);
      if (isset($this->data[$underScoreName])) {
        return $this->data[$underScoreName];
      }

      return null;
    }
  }

  class Price extends \ArrayObject
  {
    public $hasDiscount = false;
    public $discountPercent = 0;
    private $data;

    public function __construct($data)
    {
      $this->data = $data;
      parent::__construct($this->data);
      $this->initDiscount();
    }

    private function initDiscount()
    {
      if ($this->data['DISCOUNT_DIFF'] > 0) {
        $this->hasDiscount = true;
        $this->discountPercent = $this->data['DISCOUNT_DIFF_PERCENT'];
      }
    }

    public function printRealValue()
    {
      return $this->data['PRINT_VALUE'];
    }

    public function value()
    {
      return $this->data['DISCOUNT_VALUE'];
    }

    public function realValue()
    {
      return $this->data['VALUE'];
    }

    public function __toString()
    {
      return strval($this->printValue());
    }

    public function printValue()
    {
      return $this->data['PRINT_DISCOUNT_VALUE'];
    }

    public function __get($name)
    {
      if (isset($this->data[$name])) {
        return $this->data[$name];
      }

      $underScoreName = Utility::camelCaseToUnderscore($name);
      if (isset($this->data[$underScoreName])) {
        return $this->data[$underScoreName];
      }

      return null;
    }
  }

  class Utility
  {
    public static function camelCaseToUnderscore($text)
    {
      $text[0] = strtolower($text[0]);
      $func = create_function('$c', 'return "_" . ($c[1]);');

      return strtoupper(preg_replace_callback('/([A-Z])/', $func, $text));
    }
  }
}

namespace Element\Property {

  use CFile;
  use Element\Utility;

  class SharedString extends Item
  {
  }

  class SharedNumber extends Item
  {
  }

  class File extends Item
  {
    public function __construct($data)
    {
      parent::__construct($data);
      if (empty($this->data['FILE_VALUE'])) {
        if ($this->isMultiple()) {
          if (!is_array($this->data['VALUE'])) {
            $this->data['VALUE'] = array($this->data['VALUE']);
          }
          $this->data['FILE_VALUE'] = array();
          foreach ($this->data['VALUE'] as $datum) {
            $this->data['FILE_VALUE'][] = CFile::GetFileArray($datum);
          }
        } else {
          $this->data['FILE_VALUE'] = CFile::GetFileArray($this->data['VALUE']);
        }
      } elseif ($this->isMultiple() && isset($this->data['FILE_VALUE']['SRC'])) {
        $this->data['FILE_VALUE'] = array($this->data['FILE_VALUE']);
      }
    }

    public function __toString()
    {
      $value = $this->value();
      $return = $this->isMultiple() ? $value[0]['SRC'] : $value['SRC'];

      return (string)$return;
    }

    public function value()
    {
      return $this->data['FILE_VALUE'];
    }

    public function id()
    {
      return $this->data['VALUE'];
    }
  }

  class SharedList extends Item
  {
  }

  class Group extends Item
  {
  }

  class Element extends Item
  {
  }

  class Undefined extends Item
  {
  }

  /**
   * Class Item
   * @package Element\Property
   */
  abstract class Item extends \ArrayObject
  {
    /**
     * @var array|object
     */
    public $data;
    /** @var bool $exist */
    public $exist;

    public function __construct($data)
    {
      $this->data = $data;
      $this->exist = !empty($this->value());
      parent::__construct($this->data);
    }

    public function value()
    {
      return $this->data['VALUE'];
    }

    public function isMultiple()
    {
      return $this->data['MULTIPLE'] == 'Y';
    }

    public function __invoke($display = false)
    {
      if ($display) {
        return $this->display();
      } else {
        return $this->value();
      }
    }

    public function display()
    {
      if (isset($this->data['DISPLAY_VALUE'])) {
        return $this->data['DISPLAY_VALUE'];
      } else {
        trigger_error("Display value not found. Return value;");

        return $this->value();
      }
    }

    public function hasValue()
    {
      return $this->exist;
    }

    public function __toString()
    {
      return (string)$this->value();
    }

    public function __get($name)
    {
      if (isset($this->data[$name])) {
        return $this->data[$name];
      }
      $underScoreName = Utility::camelCaseToUnderscore($name);
      if (isset($this->data[$underScoreName])) {
        return $this->data[$underScoreName];
      }

      return null;
    }

    public function __set($name, $value)
    {
      $this->data[$name] = $value;
    }
  }
}


