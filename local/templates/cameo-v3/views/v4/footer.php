<footer>
    <div class="ui container">
        <div class="ui grid three column doubling stackable">
            <div class="column">
                <div class="ui grid two column stackable">
                    <div class="column">
                        <?include "menu/company.php"?>
                    </div>
                    <div class="column">
                        <? include "menu/shops.php"; ?>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="ui grid two column stackable">
                    <div class="column">
                        <? include "menu/legal.php"; ?>
                    </div>
                    <div class="column">
                        <div class="ui fluid vertical menu">
                            <div class="item header">Продукция</div>
                            <a href="/chistoe-proizvodstvo/" class="item">Для чистых помещений</a>
                            <a href="/catalog/hirurgicheskie-kostyumy/" class="item">Костюмы хирургам</a>
                            <a href="/catalog/odezhda-dlya-medsester/" class="item">Одежда для медсестер</a>
                            <a href="/catalog/medicinskaya-odezhda-s-printom/" class="item">Одежда с принтом</a>
                            <a href="/catalog/medicinskaya-odezhda-dlya-pediatorov/" class="item">Одежда для
                                педиатров</a>
                            <a href="/catalog/medicinskie-platya-dlya-zhenshchin/" class="item">Медицинские платья</a>
                            <a href="/about/opt.php" class="item">Медицинская одежда оптом</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="footer-info">
                    <div class="call">
                        <div class="hint">
                            Звонок по России бесплатный
                        </div>
                        <a href="tel:+78007755323" class="black">8 800 77 55 323</a>
                    </div>
                    <div class="spacer"></div>
                    <? include "menu/social.php"?>
                    <div class="copyright">
                        Производитель<br>© ООО «Камея» 2007-<?= date("Y") ?> <br>Все права защищены.
                    </div>
                </div>
            </div>
        </div>
        <div class="payment-info">

            <img class="ui" height="25" src="<?= SITE_TEMPLATE_PATH ?>/images/mir.png"
                 alt="">
            <span>&nbsp;</span>
            <img class="ui" height="25" src="<?= SITE_TEMPLATE_PATH ?>/images/visa.png"
                 alt="">
            <span>&nbsp;</span>
            <img class="ui"
                 height="25"
                 src="<?= SITE_TEMPLATE_PATH ?>/images/mastercard.png"
                 alt="">
            <span>&nbsp;</span>
            <img class="ui"
                 height="25"
                 src="<?= SITE_TEMPLATE_PATH ?>/images/ya_kassa.jpg"
                 alt="">
        </div>
    </div>
</footer>
