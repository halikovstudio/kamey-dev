<div class="ui fluid vertical menu">
    <div class="item header">О компании</div>
    <a href="/about/#about-us" class="item">О нас</a>
    <a href="/about/#cooperation" class="item">Сотрудничество</a>
    <a href="/about/#offices-wholesales" class="item">Офисы оптовых продаж</a>
    <a href="/about/#documents" class="item">Документы</a>
    <a href="/about/catalogs.php" class="item">Каталоги</a>
    <a href="/about/#contacts" class="item">Контакты и реквизиты</a>
</div>