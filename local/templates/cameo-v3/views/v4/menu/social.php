<div class="social">
    <a href="https://vk.com/cameorussia" target="_blank"><img src="/email/assets/vk.png"
                                                              alt="VK"></a>
    <a href="https://www.instagram.com/cameo_russia/" target="_blank"><img
          src="/email/assets/instagram.png" alt="Instagram"></a>
    <a href="https://www.facebook.com/cameorussia/" target="_blank"><img
          src="/email/assets/facebook.png" alt="Facebook"></a>
</div>