<header class="fixed-header">
    <div class="ui fluid container">
        <div class="ui secondary menu header-menu">
            <a href="<?= SITE_DIR ?>" class="item logo">
                <? svgUse("logo-svg") ?>
            </a>
            <div class="header-menu-container">
                <div class="ui secondary menu header-menu-top mobile hidden" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                    <? $APPLICATION->IncludeComponent(
                      "bitrix:menu",
                      "",
                      Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "v4topleft",
                        "USE_EXT" => "N",
                        "CLASS" => "header-menu gadget hidden",
                        "GLOBAL" => "Y",
                        "WRAP" => "N"
                      )
                    ); ?>
                    <? Dynamic::begin("isAuthMainHeader"); ?>
                    <?
                    if (CSite::InGroup(array(6, 1))) {
                        ?>
                        <a href="/corp/" class="black item">Сотрудникам</a>
                        <?
                    }
                    ?>
                    <div class="menu right personal-menu">
                        <? if (CUser::isAuthorized()): ?>
                            <a href="/personal-area/"
                               class="black item"><?= $USER->GetFormattedName() ?><?= (BonusCard::isset() ? '<span class="primary-color ml-10">' . (pluralize(BonusCard::amount(),
                                    'бонус')) . '</span>' : '') ?></a>
                        <? else: ?>
                            <a class="black item"
                               data-modal="auth">Вход</a>
                        <? endif; ?>
                    </div>
                    <? Dynamic::end(false, null, true, true); ?>
                </div>
                <div class="ui secondary menu header-menu-bottom"  itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                    <? $APPLICATION->IncludeComponent(
                      "bitrix:menu",
                      "",
                      Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "topV4",
                        "USE_EXT" => "Y",
                        "CLASS" => "header-menu gadget hidden",
                        "GLOBAL" => "Y",
                        "WRAP" => "N"
                      )
                    ); ?>
                    <div class="menu right">
                        <a href="tel:+78007755323" class="item item-phone">8 800 77 55 323</a>
                        <div class="link item item-icon shape-trigger" data-target="search-side">
                            <? icon("search-bold") ?>
                        </div>
                        <a href="/personal-area/favorites/" class="item item-icon favorite-button loading"
                             data-prevent="Добавьте товар<br> в избранное">
                            <? icon("like-bold") ?>
                            <div class="indicator"></div>
                        </a>
                        <a href="/personal-area/basket/" class="item item-icon loading cart-button"
                             data-prevent="Добавьте товар<br> в корзину">
                            <? icon("cart-bold") ?>
                            <div class="indicator"></div>
                            <div class="indicator-info"></div>
                        </a>
                        <div class="item link item-icon mobile only sidebar-trigger" data-target="mobile-menu">
                            <?
                            icon("hamburger-bold");
                            icon("close");
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
