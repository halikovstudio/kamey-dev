<div class="ui sidebar mobile-menu" id="mobile-menu">
    <? Dynamic::begin("isAuthMainHeaderSidebar"); ?>
    <div class="ui vertical fluid menu">
        <?
        if (CUser::IsAuthorized()) {
            ?>
          <div class="item item-user">
              <a href="/personal-area/" class="">ЛИЧНЫЙ КАБИНЕТ</a>
              /
              <a href="javascript:void(0)" class=" js-logout-trigger">ВЫЙТИ</a>
          </div>
            <?
        } else {
            ?>
            <a href="#login" class="item item-user" data-modal="auth">АВТОРИЗАЦИЯ</a>
            <?
        }
        ?>
    </div>

    <? Dynamic::end(false); ?>
    <div class="sidebar-catalog">
        <? $APPLICATION->IncludeComponent(
          "bitrix:menu",
          "",
          Array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "left",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "DELAY" => "Y",
            "MAX_LEVEL" => "2",
            "MENU_CACHE_GET_VARS" => array(""),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "topV4",
            "USE_EXT" => "Y",
            "CLASS" => "vertical fluid",
            "USE_SALE_IF_ENABLED" => "Y"
          )
        ); ?>
    </div>
    <div class="ui vertical fluid menu">
        <div class="item header">Продукция</div>
        <a href="/chistoe-proizvodstvo/" class="item">Для чистых помещений</a>
    </div>
    <? include "menu/company.php" ?>
    <? include "menu/shops.php"; ?>
    <? include "menu/legal.php"; ?>
    <? Dynamic::begin("sidebarCorp"); ?>
    <?
    if (CSite::InGroup(array(6, 1))) {
        ?>
        <div class="ui vertical fluid menu">
            <div class="item header">Корпоративный раздел</div>
            <a href="/corp/" class="item">Сотрудникам</a>
        </div>
        <?
    }
    ?>
    <? Dynamic::end(false); ?>
    <div class="call">
        <div class="hint">
            Звонок по России бесплатный
        </div>
        <a href="tel:+78007755323" class="black">8 800 77 55 323</a>
    </div>
    <? include "menu/social.php"; ?>
    <div class="copyright">
        Производитель<br>© ООО «Камея» 2007-<?= date("Y") ?> <br>Все права защищены.
    </div>
</div>
