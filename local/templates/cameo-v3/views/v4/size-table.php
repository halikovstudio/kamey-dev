<?php
if ($GLOBALS['dimensions']) {
    $type = getProductType($GLOBALS['dimensions']['article']);
    $gender = $GLOBALS['dimensions']['gender'];
    if ($type && $gender) {
        $table = new SizeTable($type, $gender);
        if (!empty($table->data)) {
            ?>
            <p>
                Этот справочник по размерам носит ориентировочный характер. Если у вас уже есть похожая<br> модель
                нашего
                бренда, размер которой вам подходит, рекомендуем выбрать тот же размер.
            </p>
            <div class="size-tabs">
                <div class="radio-group">
                    <?
                    if (!$table->typeExists) {
                        ?>
                        <div class="item active" data-tab="size-not-found">
                            <?= $table->getTypeName($table->type) ?>
                        </div>
                        <?
                    }
                    foreach ($table->data as $section) {
                        ?>
                        <div class="item<?= ($section['active'] ? ' active' : '') ?>"
                             data-tab="size-<?= $section['type'] ?>"><?= $section['name'] ?></div>
                        <?
                    }
                    ?>
                </div>
            </div>
            <?
            foreach ($table->data as $section) {
                ?>
                <div class="ui tab<?= ($section['active'] ? ' active' : '') ?>" data-tab="size-<?= $section['type'] ?>">
                    <div class="table-container-wrapper">
                        <div class="table-container">
                            <table class="ui table very basic unstackable">
                                <?
                                $divider = false;
                                foreach ($section['items'] as $key => $values) {
                                    if (!$divider && strpos($key, 'бхват') !== false) {
                                        $divider = true;
                                        ?>
                                        <tr class="divider">
                                            <td colspan="<?= count($values) + 1 ?>">Эквивалент в сантиметрах</td>
                                        </tr>
                                        <?
                                    }
                                    ?>
                                    <tr>
                                    <td class="header"><?= $key ?></td>
                                    <?
                                    foreach ($values as $value) {
                                        ?>
                                        <td><?= $value ?></td>
                                        <?
                                    }
                                    ?>
                                    </tr><?
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?
            }
            if (!$table->typeExists) {
                ?>
                <div class="ui tab active" data-tab="size-not-found">
                    <p>Таблица размеров для данного раздела еще не заполнена</p>
                </div>
                <?
            }
        } else {
            ?><p>Таблица размеров еще не заполнена</p><?
        }
    } else {
        ?>
        <p>Таблица размеров еще не заполнена</p>
        <?
    }
}