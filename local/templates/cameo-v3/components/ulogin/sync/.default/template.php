<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<div class="hide">
    <div class="ulogin_synchronisation">
        <?php echo $arResult['ULOGIN_CODE']; ?>
    </div>
    <div id="ulogin_synchronisation">
        <?php echo $arResult['ULOGIN_SYNC']; ?>
    </div>
</div>
<?
$list = array(
  [
    'code' => 'googleplus',
    'image' => 'google.svg',
      'alt'=>'gg'
  ],
  [
    'code' => 'yandex',
    'image' => 'yandex.svg',
    'alt'=>'yandex'
  ],
  array(
    "code" => "facebook",
    "image" => "facebook.svg",
    "alt" => "fb"
  ),
  array(
    "code" => "vkontakte",
    "image" => "vk.svg",
    "alt" => "vk"
  ),
  array(
    "code" => "odnoklassniki",
    "image" => "ok.svg",
    "alt" => "gg"
  ),
  array(
    "code" => "instagram",
    "image" => "instagram.svg",
    "alt" => "ok"
  ),
  array(
    "code" => "mailru",
    "image" => "mailru.svg",
    "alt" => "ok"
  )
);
?>
<p class="personal__socials-text mobile hidden">Вы можете использовать данные из социальных
    сетей, чтобы
    ускорить процесс
    заполнения.
</p>
<p class="personal__socials-text mobile only">Войти через социальные сети</p>
<div class="socials personal__socials" data-ulogin="display=buttons">
    <?
    foreach ($list as $system) {
        if ($arResult['SYNC'][$system['code']]) {
            ?>
            <a class="auth-modal__social active unlink-system" href="javascript:void(0)"
               data-ulogin-identity="<?= $arResult['SYNC'][$system['code']] ?>"
               data-ulogin-network="<?= $system['code'] ?>">
                <?
                require $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/images/social/'.$system['image'];
                ?>
            </a>
            <?
        } else {
            ?>
            <a class="auth-modal__social"  data-uloginbutton="<?= $system['code'] ?>">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/social/<?= $system['image'] ?>" alt="<?= $system['alt'] ?>">
            </a>
            <?
        }
    }
    ?>
    <script type="text/javascript">
      $(document).ready(function() {
        var uloginNetwork = $('.unlink-system');

        uloginNetwork.click(function() {
          if (!confirm('Отвязать социальную сеть?')) {
            return;
          }
          var network = $(this).attr('data-ulogin-network');
          var identity = $(this).attr('data-ulogin-identity');
          uloginDeleteAccount(network, identity);
        });
      });

      function uloginDeleteAccount(network, identity) {
        $.ajax({
          url: '/bitrix/components/ulogin/sync/ulogin-ajax.php',
          type: 'POST',
          dataType: 'json',
          data: {
            identity: identity,
          },
          error: function(data, textStatus, errorThrown) {
            alert('Не удалось выполнить запрос');
          },
          success: function(data) {
            switch (data.answerType) {
              case 'error':
                alert(data.title + '\n' + data.msg);
                break;
              case 'ok':
                alert(data.msg);
                location.reload();
                break;
              default:
                break;
            }
          },
        });
      }
    </script>

