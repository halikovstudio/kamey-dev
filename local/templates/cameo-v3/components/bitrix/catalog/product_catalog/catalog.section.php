<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @global \Bitrix\Main\Request $request */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var array $arCurSection */
global $request;

$link = $APPLICATION->GetCurPageParam("", array("locationId", "ncc", "clear_cache", "order", "sort"));
if (strpos($link, "filter/clear/apply") !== false) {
  $link = str_replace("filter/clear/apply/", "", $link);
}
global $version;
$GLOBALS['canonicalUsed'] = true;
$arSort = array(
  "cheaper" => array(
    "name" => "по цене дешевле",
    "order" => "ASC",
    "field" => "catalog_PRICE_2"
  ),
  "expensive" => array(
    "name" => "по цене дороже",
    "order" => "DESC",
    "field" => "catalog_PRICE_2"
  ),
  "popular" => array(
    "name" => "по популярности",
    "order" => "DESC",
    "field" => "SHOW_COUNTER"
  ),
  "new" => array(
    "name" => "новинки",
    'order' => "ASC",
    'field' => "PROPERTY_NOVINKA"
  )
);
if ($version == 4) {
    unset($arSort['new']);
}
$sort = array(
  "field" => $arParams['ELEMENT_SORT_FIELD'],
  'order' => $arParams['ELEMENT_SORT_ORDER']
);
$defaultSet = false;
foreach ($arSort as $code => $item) {
  if (strtolower($item['field']) == strtolower($sort['field']) && strtolower($item['order']) == strtolower($sort['order'])) {
    $defaultSet = true;
  }
}
if (!$defaultSet) {
  $arSort['default'] = array(
    "name" => "по умолчанию",
    "field" => $sort['field'],
    "order" => $sort['order']
  );
}
$_SESSION['activeSort'] = array();
if ($sortValue = $request->get("sort")) {
  foreach ($arSort as $code => $item) {
    if ($code == $sortValue) {
      $sort['field'] = $item['field'];
      $sort['order'] = $item['order'];
      $_SESSION['activeSort'] = $sort;
      break;
    }
  }
}

if (!$sortValue) {
  foreach ($arSort as $code => $item) {
    if (strtolower($item['field']) == strtolower($sort['field']) && strtolower($item['order']) == strtolower($sort['order'])) {
      $sortValue = $code;
      break;
    }
  }
}
global $catalogMenu;
$current = null;
$sectionCode = $arCurSection['CODE'];
$sectionId = $arCurSection['ID'];

if ($catalogMenu[$sectionId]) {
  $current = $catalogMenu[$sectionId];
} else {
  foreach ($catalogMenu as $menu) {
    foreach ($menu['ITEMS'] as $item) {
      if ($item['PARAMS']["ID"] == $arCurSection['ID']) {
        $current = $menu;
        break;
      }
    }
    if ($current) {
      break;
    }
  }
}
foreach ($current['ITEMS'] as $item) {
  if ($item['SELECTED']) {
    $current['SELECTED'] = false;
    break;
  }
}
if (!$current && $arCurSection) {
  $current = array(
    "TEXT" => $arCurSection['NAME'],
    "LINK" => $arCurSection['SECTION_PAGE_URL'],
    "PARAMS" => array(
      "ID" => $arCurSection['ID']
    )
  );
}

$isTopLevel = $current['PARAMS']['ID'] == $sectionId;
if ($isTopLevel && !$current['SELECTED']) {
  $current['SELECTED'] = $isTopLevel;
}
$useSale = \Page\Options::getNow("sale");
//$useSale = false;
$isSale = $_GET['sale'] === 'true' && $useSale;
$saleLink = $isSale ? $current['LINK'] : $current['LINK'] . '?sale=true';
$saleParam = !$isSale ? '' : '';
$htmlValueAlt = "899856350";
$fieldName = "arrFilter_830";
if ($isSale && $useSale) {
  $GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_830'] = 10278;
} else {
  $GLOBALS[$arParams['FILTER_NAME']]["!PROPERTY_830"] = 10278;
}
$isNew = $_GET['new'] === 'true';
$isTop = $_GET['top'] === 'true';
if ($isNew) {
    $GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_217'] = 288;
}
if ($isTop) {
    $GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_219'] = 292;
}
if ($useSale) {
  $count = CIBlockElement::GetList(array(), array(
    "PROPERTY_830" => 10278,
    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
    "SECTION_ID" => $current['PARAMS']['ID'],
    "INCLUDE_SUBSECTIONS" => "Y",
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
    "CATALOG_AVAILABLE" => "Y"
  ), array(), false, array("ID"));
  if ($count == 0) {
    $useSale = false;
  }
}
$showMenu = $useSale || count($current['ITEMS']) > 0;

include "views/v".$version."/catalog.section.php";
return;
?>
<div class="top-catalog-content">
    <div class="prolog">
        <div class="ui medium header">
          <? $APPLICATION->ShowTitle() ?>
        </div>
        <div class="description secondary">
          <?
          if (strlen($arCurSection['UF_PREVIEW']) > 240) {
            ?>
              <div class="short-text">
                  <div class="before">
                    <?= mb_strimwidth($arCurSection['UF_PREVIEW'], 0, 240, "...") ?> <a class="more">подробнее</a>
                  </div>
                  <div class="after">
                    <?= $arCurSection['UF_PREVIEW'] ?> <a class="less">скрыть</a>
                  </div>
              </div>
            <?
          } else {
            echo $arCurSection['UF_PREVIEW'];
          }

          ?>
        </div>
    </div>
    <div class="title catalog-title">
        <div class="ui uppercase heading" data-sidebar="filter">Фильтр
            <div class="fi fi-angle-arrow-pointing-to-right"></div>
        </div>
        <div class="ui uppercase heading catalog-header"><? $APPLICATION->ShowViewContent("catalog-count") ?></div>
        <div class="ui uppercase heading" data-sidebar="sort">Сортировка
            <div class="fi fi-angle-arrow-pointing-to-right"></div>
        </div>
    </div>
</div>
<?
$arSort = array(
  array(
    "name" => "Стоимость",
    "items" => array(
      array(
        "name" => "Дешевле",
        "code" => "cheaper",
        "order" => "ASC",
        "field" => "PROPERTY_MINIMUM_PRICE_2"
      ),
      array(
        "name" => "Дороже",
        "code" => "expensive",
        "order" => "DESC",
        "field" => "PROPERTY_MINIMUM_PRICE_2"
      )
    )
  ),
  array(
    "name" => "Популярность",
    "items" => array(
      array(
        "name" => "Самые популярные",
        "code" => "popular",
        "order" => "DESC",
        "field" => "SHOW_COUNTER"
      )
    )
  ),
  array(
    "name" => "Наименование",
    "items" => array(
      array(
        "name" => "А-я",
        "code" => "a-z",
        "order" => "ASC",
        "field" => "NAME"
      ),
      array(
        "name" => "Я-а",
        "code" => "z-a",
        "order" => "DESC",
        "field" => "NAME"
      )
    )
  )
);

$filterRequest = $request->get("requestFilter") == "Y" && $request->isAjaxRequest();
$filterValues = "";
$filterData = "";
$sortData = "";

if (!$filterRequest) {
  ob_start();
}
?>
<div class="ui top overlay sidebar" data-sidebar="filter" data-offset=".catalog-title">
    <div class="sidebar-container">
        <div class="ui container filter container">
          <?
          if ($filterRequest) {
            ob_start();
          }
          $GLOBALS[$arParams['FILTER_NAME']] = array(
            'CATALOG_AVAILABLE' => 'Y',
            '!PROPERTY_MORE_PHOTO' => false
          );
          $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "",
            array(
              'filterRequest' => $filterRequest,
              "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
              "IBLOCK_ID" => $arParams["IBLOCK_ID"],
              "SECTION_ID" => $arCurSection['ID'],
              "FILTER_NAME" => $arParams["FILTER_NAME"],
              "PRICE_CODE" => $arParams["PRICE_CODE"],
              "CACHE_TYPE" => $arParams["CACHE_TYPE"],
              "CACHE_TIME" => $arParams["CACHE_TIME"],
              "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
              "SAVE_IN_SESSION" => "N",
              "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
              "XML_EXPORT" => "Y",
              "SECTION_TITLE" => "NAME",
              "SECTION_DESCRIPTION" => "DESCRIPTION",
              'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
              "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
              'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
              'CURRENCY_ID' => $arParams['CURRENCY_ID'],
              "SEF_MODE" => $arParams["SEF_MODE"],
              "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
              "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
              "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
              "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
            ),
            $component,
            array('HIDE_ICONS' => 'Y')
          );
          if ($filterRequest) {
            $filterData = ob_get_contents();
            ob_end_clean();
          }
          ?>
        </div>
    </div>
</div>
<div class="ui top overlay sidebar" data-sidebar="sort" data-offset=".catalog-title">
    <div class="sidebar-container">
        <div class="ui container">
            <div class="ui stackable grid">
                <div class="eight wide right floated column">
                    <div class="ui stackable equal width grid sort-container">
                      <?
                      if ($filterRequest) {
                        ob_start();
                      }
                      foreach ($arSort as $group) {
                        ?>
                          <div class="column">
                              <div class="ui fluid vertical text menu">
                                  <div class="item header">
                                    <?= $group['name'] ?>
                                  </div>
                                <?
                                foreach ($group['items'] as $item) {
                                  ?>
                                    <a href="<?= $APPLICATION->GetCurPageParam("sort=" . $item['code'], array("sort", "requestFilter", 'locationId', 'ncc')) ?>"
                                       class="item<?= ($item['code'] == $sortValue ? " active" : "") ?>"><?= $item['name'] ?></a>
                                  <?
                                }
                                ?>
                              </div>
                          </div>
                        <?
                      }
                      ?>
                        <div class="sixteen wide right aligned column expand top">
                            <a href="<?= $APPLICATION->GetCurPageParam("", array("sort", "requestFilter", 'locationId', 'ncc')) ?>"
                               class="ui primary button">Сбросить</a>
                            <div class="ui primary button close">Закрыть</div>
                        </div>
                      <?
                      if ($filterRequest) {
                        $sortData = ob_get_contents();
                        ob_end_clean();
                      }
                      ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
if (!$filterRequest) {
  $APPLICATION->AddViewContent("filter", ob_get_contents());
  ob_end_clean();
}
?>
<?
?>
<div class="filter result<?= (count($GLOBALS['filterValues']) ? " full" : "") ?>">
  <?
  if ($filterRequest) {
    ob_start();
  }
  foreach ($GLOBALS['filterValues'] as $id => $group) {
    if (count($group['list']) == 1) {
      ?>
        <div class="ui basic label">
          <?= $group['name'] ?>: <?= $group['list'][0] ?>
            <div class="remove" data-id="<?= $id ?>">×</div>
        </div>
      <?
    } else {
      ?>
        <div class="ui basic label">
          <?= pluralize(count($group['list']), $group['name'], false); ?>
            <div class="remove" data-id="<?= $id ?>">×</div>
        </div>
      <?
    }
  }
  if (count($GLOBALS['filterValues'])) {
    ?>
      <div class="ui basic label reset">
          Сбросить фильтр
          <div class="remove" data-id="reset">×</div>
      </div>
    <?
  }
  if ($filterRequest) {
    $filterValues = ob_get_contents();
    ob_end_clean();
  }
  ?>
</div>
<div class="dimmer-container">
    <div class="ui doubling four column grid items-list">
      <?

      if ($request->isAjaxRequest() && !$filterRequest) {
        $APPLICATION->RestartBuffer();
      }
      if ($filterRequest) {
        ob_start();
      }
      $intSectionID = $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        "",
        array(
          "PLURALIZE" => $arCurSection['DEPTH_LEVEL'] == 1 ? "товар" : null,
          "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
          "IBLOCK_ID" => $arParams["IBLOCK_ID"],
          "ELEMENT_SORT_FIELD" => $sort['field'],
          "ELEMENT_SORT_ORDER" => $sort['order'],
          "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
          "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
          "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
          "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
          "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
          "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
          "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
          "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
          "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
          "BASKET_URL" => $arParams["BASKET_URL"],
          "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
          "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
          "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
          "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
          "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
          "FILTER_NAME" => $arParams["FILTER_NAME"],
          "CACHE_TYPE" => $arParams["CACHE_TYPE"],
          "CACHE_TIME" => $arParams["CACHE_TIME"],
          "CACHE_FILTER" => $arParams["CACHE_FILTER"],
          "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
          "SET_TITLE" => $arParams["SET_TITLE"],
          "MESSAGE_404" => $arParams["~MESSAGE_404"],
          "SET_STATUS_404" => $arParams["SET_STATUS_404"],
          "SHOW_404" => $arParams["SHOW_404"],
          "FILE_404" => $arParams["FILE_404"],
          "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
          "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
          "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
          "PRICE_CODE" => $arParams["PRICE_CODE"],
          "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
          "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

          "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
          "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
          "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
          "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
          "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

          "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
          "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
          "PAGER_TITLE" => $arParams["PAGER_TITLE"],
          "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
          "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
          "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
          "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
          "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
          "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
          "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
          "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
          "LAZY_LOAD" => $arParams["LAZY_LOAD"],
          "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
          "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

          "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
          "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
          "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
          "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
          "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
          "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
          "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
          "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

          "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
          "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
          "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
          "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
          "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
          'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
          'CURRENCY_ID' => $arParams['CURRENCY_ID'],
          'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
          'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

          'LABEL_PROP' => $arParams['LABEL_PROP'],
          'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
          'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
          'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
          'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
          'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
          'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
          'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
          'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
          'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
          'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
          'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

          'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
          'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
          'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
          'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
          'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
          'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
          'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
          'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
          'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
          'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
          'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
          'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
          'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
          'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
          'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
          'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
          'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

          'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
          'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
          'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

          'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
          "ADD_SECTIONS_CHAIN" => "Y",
          'ADD_TO_BASKET_ACTION' => $basketAction,
          'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
          'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
          'COMPARE_NAME' => $arParams['COMPARE_NAME'],
          'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
          'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
          'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
        ),
        $component
      );
      if ($filterRequest) {
        $list = ob_get_contents();
        ob_end_clean();
        $title = $APPLICATION->GetViewContent("catalog-count");
        $APPLICATION->RestartBuffer();
        echo json_encode(array(
          "title" => $title,
          "list" => $list,
          "filter" => $filterValues,
          "filterData" => $filterData,
          "sort" => $sortData
        ));
      }
      if ($request->isAjaxRequest()) {
        CMain::FinalActions();
        die();
      }
      ?>
    </div>
  <?
  if ($arCurSection['UF_DETAIL']) {
    ?>
      <div class="catalog-end">
        <?= $arCurSection['UF_DETAIL'] ?>
      </div>
    <?
  }
  ?>
</div>
