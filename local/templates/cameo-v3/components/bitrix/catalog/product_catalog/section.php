<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;

$this->setFrameMode(true);

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
  $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter) {
  $arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
  );
  if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
    $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
  elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
    $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

  $obCache = new CPHPCache();
  if ($obCache->InitCache(36000, serialize($arFilter) . "with-url", "/iblock/catalog/v3")) {
    $arCurSection = $obCache->GetVars();
  } elseif ($obCache->StartDataCache()) {
    $arCurSection = array();
    if (Loader::includeModule("iblock")) {
      $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "UF_PREVIEW", "UF_DETAIL", "DEPTH_LEVEL", 'NAME', 'CODE', "DESCRIPTION", "SECTION_PAGE_URL"));

      if (defined("BX_COMP_MANAGED_CACHE")) {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache("/iblock/catalog");

        if ($arCurSection = $dbRes->GetNext())
          $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

        $CACHE_MANAGER->EndTagCache();
      } else {
        if (!$arCurSection = $dbRes->GetNext())
          $arCurSection = array();
      }
    }
    $obCache->EndDataCache($arCurSection);
  }
  if (!isset($arCurSection))
    $arCurSection = array();
}

$list = array(
  "for-him",
  "for-her"
);
if (in_array($arResult['VARIABLES']['SECTION_ID'], $list)) {
  include $arResult['VARIABLES']['SECTION_ID'] . '.php';
} else {
  include "catalog.section.php";
}
if (preg_match("/filter/i", $_SERVER['REQUEST_URI'])) {
  $APPLICATION->AddHeadString('<link rel="canonical" href="https://www.kamey.ru' . preg_replace("/filter.*/", "", $APPLICATION->GetCurPage()) . '">');
}
if (preg_match("/\?sort/i", $_SERVER['REQUEST_URI'])) {
  $APPLICATION->AddHeadString('<link rel="canonical" href="https://www.kamey.ru' . preg_replace("/\?sort.*/", "", $APPLICATION->GetCurPage()) . '">');
}
