<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @global \Bitrix\Main\Request $request */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var array $arCurSection */
global $request;
$link = $APPLICATION->GetCurPageParam("", array("locationId", "ncc", "clear_cache", "order", "sort"));
if (strpos($link, "filter/clear/apply") !== false) {
  $link = str_replace("filter/clear/apply/", "", $link);
}
$GLOBALS['canonicalUsed'] = true;
$arSort = array(
  "popular" => array(
    "name" => "по популярности",
    "order" => "DESC",
    "field" => "SHOW_COUNTER"
  ),
  "new" => array(
    "name" => "новинки",
    'order' => "ASC",
    'field' => "PROPERTY_NOVINKA"
  )
);
$sort = array(
  "field" => $arParams['ELEMENT_SORT_FIELD'],
  'order' => $arParams['ELEMENT_SORT_ORDER']
);
$defaultSet = false;
foreach ($arSort as $code => $item) {
  if (strtolower($item['field']) == strtolower($sort['field']) && strtolower($item['order']) == strtolower($sort['order'])) {
    $defaultSet = true;
  }
}
if (!$defaultSet) {
  $arSort['default'] = array(
    "name" => "по умолчанию",
    "field" => $sort['field'],
    "order" => $sort['order']
  );
}
$_SESSION['activeSort'] = array();
if ($sortValue = $request->get("sort")) {
  foreach ($arSort as $code => $item) {
    if ($code == $sortValue) {
      $sort['field'] = $item['field'];
      $sort['order'] = $item['order'];
      $_SESSION['activeSort'] = $sort;
      break;
    }
  }
}

if (!$sortValue) {
  foreach ($arSort as $code => $item) {
    if (strtolower($item['field']) == strtolower($sort['field']) && strtolower($item['order']) == strtolower($sort['order'])) {
      $sortValue = $code;
      break;
    }
  }
}
global $catalogMenu;
$current = null;
$sectionCode = $arCurSection['CODE'];
$sectionId = $arCurSection['ID'];
if ($catalogMenu[$sectionId]) {
  $current = $catalogMenu[$sectionId];
} else {
  foreach ($catalogMenu as $menu) {
    foreach ($menu['ITEMS'] as $item) {
      if ($item['PARAMS']["ID"] == $arCurSection['ID']) {
        $current = $menu;
        break;
      }
    }
    if ($current) {
      break;
    }
  }
}
foreach ($current['ITEMS'] as $item) {
  if ($item['SELECTED']) {
    $current['SELECTED'] = false;
    break;
  }
}

$isTopLevel = $current['PARAMS']['ID'] == $sectionId;
$useSale = \Page\Options::getNow("sale");
$isSale = $_GET['sale'] === 'true' && $useSale;
$saleLink = $isSale ? $current['LINK'] : $current['LINK'] . '?sale=true';
$saleParam = !$isSale ? '' : '';
$htmlValueAlt = "899856350";
$fieldName = "arrFilter_830";
if ($isSale && $useSale) {
  $GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_830'] = 10278;
} else {
  $GLOBALS[$arParams['FILTER_NAME']]["!PROPERTY_830"] = 10278;
}
global $version;
include "views/v".$version."/catalog.section.php";
return;
?>