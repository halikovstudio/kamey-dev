<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Sale;
use Product\Basket;

$this->addExternalCss(SITE_TEMPLATE_PATH . '/css/suggestions.min.css');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/suggestions.min.js');

if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
    if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if (strlen($arResult["REDIRECT_URL"]) > 0) {
            $APPLICATION->RestartBuffer();
            ?>
            <script type="text/javascript">
              window.top.location.href = '<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
            </script>
            <?
            die();
        }
    }
}
$this->addExternalJs('https://pay.google.com/gp/p/js/pay.js');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/coupon.js');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/order.js');
$arResult['JS_DATA']['BONUS_CARD_ISSET'] = BonusCard::isset();
$step = $_REQUEST['step'];
if (!$step) {
    $step = 1;
}
$count = 0;
if ($step == 1) {
    /** @var $basket Sale\Basket */
    $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

    $order = Sale\Order::create(SITE_ID, CUser::GetID(), 'RUB');
    $order->setBasket($basket);
    $basket->doFinalAction();
    $discount = $order->getDiscount()->getApplyResult();
    Basket::$editable = false;
    $views = \Product\Basket::view($basket, false);
    $viewsNotAvailable = \Product\Basket::viewNotAvailable($basket, false);
    $count = Basket::getCount($basket);
}
$arResult['JS_DATA']['STEP'] = +$step;
?>

<a name="order_form"></a>

<div id="order_form_div" class="order-checkout basket-ordering">

    <NOSCRIPT>
        <div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
    </NOSCRIPT>

    <?
    if (!function_exists("getColumnName")) {
        function getColumnName($arHeader)
        {
            return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_" . $arHeader["id"]);
        }
    }

    if (!function_exists("cmpBySort")) {
        function cmpBySort($array1, $array2)
        {
            if (!isset($array1["SORT"]) || !isset($array2["SORT"])) {
                return -1;
            }

            if ($array1["SORT"] > $array2["SORT"]) {
                return 1;
            }

            if ($array1["SORT"] < $array2["SORT"]) {
                return -1;
            }

            if ($array1["SORT"] == $array2["SORT"]) {
                return 0;
            }
        }
    }
    ?>

    <div class="bx_order_make ordering">

        <?
        if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N") {
            if (!empty($arResult["ERROR"])) {
                foreach ($arResult["ERROR"] as $v) {
                    echo ShowError($v);
                }
            } elseif (!empty($arResult["OK_MESSAGE"])) {
                foreach ($arResult["OK_MESSAGE"] as $v) {
                    echo ShowNote($v);
                }
            }

            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
        } else {
            if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
                if (strlen($arResult["REDIRECT_URL"]) == 0) {
                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
                }
            } else {
                ?>
                <script type="text/javascript">

                  function submitForm(val) {
                    return order.submit(val);
                  }
                </script>
            <?
            if ($_POST["is_ajax_post"] != "Y")
            {
            ?>
                <div class="ui container">
                    <form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM"
                          id="ORDER_FORM"
                          enctype="multipart/form-data" class="ui form order-form not-semantic"
                    >
                        <?= bitrix_sessid_post() ?>
                        <input type="hidden" name="track" value=''>
                        <div id="order_form_content">
                            <?
                            }
                            else {
                                $APPLICATION->RestartBuffer();
                            }

                            if ($_REQUEST['PERMANENT_MODE_STEPS'] == 1) {
                                ?>
                                <input type="hidden" name="PERMANENT_MODE_STEPS" value="1"/>
                                <?
                            }
                            ?>
                            <?
                            if ($step == 1) {
                                ?>
                                <a href="<?= $arParams['PATH_TO_BASKET'] ?>"
                                   class="icon black underline  back-button">< Корзина<? icon('dropdown',
                                      'mobile only') ?></a>
                                <?
                            } else {
                                ?>
                                <a class="icon black underline back-order-button">< Оформление
                                    заказа<? icon('dropdown', 'mobile only') ?></a>
                                <?
                            }
                            ?>
                            <h1 class="ui header normal  center aligned uppercase"><?= ($step == 1 ? 'Оформление заказа' : 'Способ оплаты') ?></h1>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                              "AREA_FILE_SHOW" => "file",
                              "AREA_FILE_SUFFIX" => "inc",
                              "AREA_FILE_RECURSIVE" => "Y",
                              "EDIT_TEMPLATE" => "standard.php",
                              "PATH" => "/include/v3/delivery-and-payment/attention.php"
                            ),
                              false,
                              array(
                                "ACTIVE_COMPONENT" => "N"
                              )
                            ); ?>
                            <input type="hidden" name="version" value="<?= $_REQUEST['version'] ?>">
                            <input type="hidden" name="step" value="<?= $step ?>">
                            <div class="order-step<?= $step == 1 ? ' active' : '' ?>">
                                <?
                                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php");
                                ?>
                                <div class="cart not-editable">
                                    <div class="header">Заказ</div>
                                    <table class="ui very basic table unstackable">
                                        <thead>
                                        <tr>
                                            <th>Фото</th>
                                            <th class="column-name">Название</th>
                                            <th>Цвет</th>
                                            <th>Размер</th>
                                            <th>Рост</th>
                                            <th>Количество</th>
                                            <th>Цена</th>
                                        </tr>
                                        </thead>
                                        <?
                                        if ($views) {
                                            ?>
                                            <tbody class="available"><?= $views ?></tbody>
                                            <?
                                        }
                                        if ($viewsNotAvailable) {
                                            ?>
                                            <tbody class="not-available">
                                            <?= $viewsNotAvailable ?>
                                            </tbody>
                                            <?
                                        }
                                        ?>
                                    </table>
                                </div>
                                <div class="order-container">
                                    <?
                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php"); ?>
                                    <?

                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/summary.php");
                                    if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0) {
                                        echo $arResult["PREPAY_ADIT_FIELDS"];
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="order-step order-container order-container--small<?= $step == 2 ? ' active' : '' ?>">
                                <?
                                //if (($_SERVER['REMOTE_ADDR'] == '188.234.148.65' || $_SERVER['REMOTE_ADDR'] == '92.50.158.6')) {
                                $total = $arResult['JS_DATA']['TOTAL']['ORDER_TOTAL_PRICE'];
                                if ($step == 2 && $arResult["USER_VALS"]["PERSON_TYPE_ID"] == 1) {
                                    if (BonusCard::isset()) {
                                        $accrued = BonusCard::accruedAmount($total);
                                    } elseif ($total >= 3000) {
                                        $arUser = CUser::GetByID(CUser::GetID())->Fetch();

                                        if (!$arUser || (!$arUser['PERSONAL_BIRTHDAY'] || !$arUser['PERSONAL_GENDER'])) {
                                            ?>
                                            <p class="gray-color">Укажите данные для получения бонусной
                                                карты</p>
                                            <div class="bonus-fields fields">
                                                <?
                                                if (!$arUser['PERSONAL_BIRTHDAY']) {
                                                    ?>
                                                    <div class="field">
                                                        <label for="personal_birthday">День рождения</label>
                                                        <input type="date" placeholder="День рождения"
                                                               name="PERSONAL_BIRTHDAY" id="personal_birthday">
                                                    </div>
                                                    <?
                                                }
                                                if (!$arUser['PERSONAL_GENDER']) {
                                                    ?>
                                                    <div class="field">
                                                        <label for="personal_gender">Пол</label>
                                                        <select name="PERSONAL_GENDER" id="personal_gender"
                                                                class="ui dropdown">
                                                            <option value="M">Мужской</option>
                                                            <option value="F">Женский</option>
                                                        </select>
                                                    </div>
                                                    <?
                                                }
                                                ?>
                                                <div></div>
                                            </div>
                                            <?
                                        }
                                    }
                                }
                                if (BonusCard::isset() && BonusCard::allow() && $arResult["USER_VALS"]["PERSON_TYPE_ID"] == 1) {
                                    $amount = BonusCard::amount();
                                    $max = $amount >= $total ? $total - 1 : $amount;
                                    $max = floor($max);
                                    $checked = $_POST['bonus_withdraw_agreement'] == 'Y';
                                    ?>
                                    <div class="order-bonus-block" data-version="2">
                                        <input type="checkbox"
                                               name="bonus_withdraw_agreement"
                                          <?= ($checked ? 'checked' : '') ?>
                                               value="Y" id="bonus_withdraw_agreement">
                                        <div class="order-bonus-row ui grid two column stackable">
                                            <div class="column">
                                                <div class="d-flex">
                                                    <div class="header">Доступно<br>бонусов</div>
                                                    <div class="primary-color set-bonus-value"
                                                         data-set="<?= $max ?>"><?= $amount ?></div>
                                                </div>
                                            </div>
                                            <div class="column right-side">
                                                <div class="d-flex">
                                                    <div class="field">
                                                        <input type="number" name="bonus_withdraw_amount"
                                                               placeholder="Кол-во"
                                                               max="<?= $max ?>"
                                                          <?= ($checked ? 'readonly' : '') ?>
                                                               value="<?= $_POST['bonus_withdraw_amount'] ?>">
                                                    </div>
                                                    <div class="ui primary button accept_withdraw withdrawal_before<?= ($checked ? ' transition hidden' : '') ?>">
                                                        Списать
                                                    </div>
                                                    <div class="ui basic button cancel_withdraw withdrawal_after<?= (!$checked ? ' transition hidden' : '') ?>">
                                                        Отменить списание
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="withdrawal_after transition <?= ($checked ? 'visible' : 'hidden') ?>">
                                                        <br>
                                                        <div>Код из смс</div>
                                                        <div>
                                                            <div class="code-input">
                                                                <?
                                                                foreach (range(0, 3) as $value) {
                                                                    ?>
                                                                    <input type="text"
                                                                           name="withdraw_code[<?= $value ?>]"
                                                                           value="<?= $_POST['withdraw_code'][$value] ?>">
                                                                    <?
                                                                }
                                                                ?>
                                                            </div>
                                                            <br>
                                                            <a class="resend-code underline pointer">Выслать
                                                                повторно</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>
                                <div class="divider dashed"></div>
                                <?
                                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php"); ?>
                            </div>
                            <div class="order-container error-container">
                                <div class="ui error message"><?
                                    if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
                                        ?>
                                        <ul class="list">
                                            <?
                                            foreach ($arResult["ERROR"] as $v)
                                            ?>
                                            <li><?= $v ?></li><?
                                            ?>
                                        </ul>
                                        <script type="text/javascript">
                                          top.window.order.setErrorState(true);
                                        </script>
                                        <?
                                    }
                                    ?></div>
                            </div>
                            <div class="order-container total-info">
                                <div class="header">Итого</div>
                                <table class="total-table">
                                    <tr>
                                        <td class="header">Стоимость товаров</td>
                                        <td class="value order-real-total"></td>
                                    </tr>
                                    <tr>
                                        <td class="header">Доставка</td>
                                        <td class="value order-delivery"></td>
                                    </tr>
                                    <?
                                    if ($showMessage) {
                                        ?>
                                        <tr>
                                            <td colspan="2">
                                                <div class="no-delivery-big">
                                                    <div class="d-flex">
                                                        <? icon('info') ?>
                                                        <div>
                                                            В данный момент мы не смогли рассчитать стоимость
                                                            доставки, с вами свяжется менеджер
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                    ?>
                                    <tr class="order-discount transition hidden">
                                        <td class="header">Скидка</td>
                                        <td class="value"></td>
                                    </tr>
                                    <tr class="divider">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="header">Сумма к оплате</td>
                                        <td class="value primary-value order-total"></td>
                                    </tr>
                                </table>
                                <?
                                $buttonText = ($step == 1 ? 'Далее' : ($checked ? 'Списать бонусы и оформить заказ' : 'Оформить заказ'));
                                ?>
                                <div class="order-submit-container">
                                    <div class="ui uppercase submit primary button submit-button submit-button-text fluid"><?= $buttonText ?></div>
                                    <div class="agree-content">
                                        Нажимая кнопку «<span
                                                class="submit-button-text"><?= $buttonText ?></span>», вы
                                        принимаете согласие на
                                        обработку <a href="/legal-information/public_offer.php"
                                                     target="_blank">персональных данных</a>
                                    </div>
                                </div>
                            </div>
                            <?
                            if ($_POST["is_ajax_post"] != "Y")
                            {
                            ?>
                        </div>
                        <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                        <input type="hidden" name="profile_change" id="profile_change" value="N">
                        <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                        <input type="hidden" name="json" value="Y">
                        <script>
                          var mapLoaded = false,
                              mapSet = false;

                          function initMap() {
                            mapLoaded = true;
                            if (window.order) {
                              window.order.loadMap();
                            } else {
                              mapSet = true;
                            }
                          }

                          $(function() {
                            window.order = new Order(<?=json_encode($arResult['JS_DATA'])?>, function(_order) {
                              if (mapLoaded && mapSet) {
                                _order.loadMap();
                              }
                            });
                          });
                        </script>
                    </form>
                </div>
            <?
            if ($arParams["DELIVERY_NO_AJAX"] == "N")
            {
            ?>
                <div style="display:none;"><?
                    $APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null,
                      array('HIDE_ICONS' => 'Y')); ?></div>
            <?
            }
            }
            else
            {
            $arResult['JS_DATA']['BASKET_HTML'] = $GLOBALS['basketItems'];
            ?>
                <script type="text/javascript">
                  top.window.order.updateData(<?=json_encode($arResult['JS_DATA'])?>);
                  top.BX('confirmorder').value = 'Y';
                  top.BX('profile_change').value = 'N';
                </script>
                <?
                die();
            }
            }
        }
        ?>
    </div>
</div>

<? if (CSaleLocation::isLocationProEnabled()): ?>
    <div style="display: none">
        <? // we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
        <? $APPLICATION->IncludeComponent(
          "bitrix:sale.location.selector.steps",
          ".default",
          array(),
          false
        ); ?>
        <? $APPLICATION->IncludeComponent(
          "bitrix:sale.location.selector.search",
          ".default",
          array(),
          false
        ); ?>
    </div>

<? endif ?>

<?
if (false && strtotime("2018-01-11") > time()) {
    ?>
    <div class="ui tiny modal" id="delivery-alarm">
        <i class="close"></i>
        <div class="header">
            <div class="ui header">Внимание!</div>
        </div>
        <div class="content">
            При заказе с 25 декабря 2017 года по 10 января 2018 года, товар будет отправлен в службу доставки 11 января
            2018 года
        </div>
    </div>
    <?
}
?>
