<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="section hide">
    <h4><?= GetMessage("SOA_TEMPL_PROP_INFO") ?></h4>
  <?
  $bHideProps = true;

  if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && !empty($arResult["ORDER_PROP"]["USER_PROFILES"])):
    if ($arParams["ALLOW_NEW_PROFILE"] == "Y"):
      ?>
        <div class="bx_block r1x3">
          <?= GetMessage("SOA_TEMPL_PROP_CHOOSE") ?>
        </div>
        <div class="bx_block r3x1">
            <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
                <option value="0"><?= GetMessage("SOA_TEMPL_PROP_NEW_PROFILE") ?></option>
              <?
              foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                ?>
                  <option value="<?= $arUserProfiles["ID"] ?>"<?
                  if ($arUserProfiles["CHECKED"] == "Y") echo " selected"; ?>><?= $arUserProfiles["NAME"] ?></option>
                <?
              }
              ?>
            </select>
            <div style="clear: both;"></div>
        </div>
    <?
    else:
      ?>
        <div class="bx_block r1x3">
          <?= GetMessage("SOA_TEMPL_EXISTING_PROFILE") ?>
        </div>
        <div class="bx_block r3x1">
          <?
          if (count($arResult["ORDER_PROP"]["USER_PROFILES"]) == 1) {
            foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
              echo "<strong>" . $arUserProfiles["NAME"] . "</strong>";
              ?>
                <input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?= $arUserProfiles["ID"] ?>"/>
              <?
            }
          } else {
            ?>
              <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
                <?
                foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                  ?>
                    <option value="<?= $arUserProfiles["ID"] ?>"<?
                    if ($arUserProfiles["CHECKED"] == "Y") echo " selected"; ?>><?= $arUserProfiles["NAME"] ?></option>
                  <?
                }
                ?>
              </select>
            <?
          }
          ?>
            <div style="clear: both;"></div>
        </div>
    <?
    endif;
  else:
    $bHideProps = false;
  endif;
  ?>
</div>
<div class="form-container">
  <?

  PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"]);

  PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);

  ?>
</div>
<? if (!CSaleLocation::isLocationProEnabled()): ?>
    <div style="display:none;">

      <? $APPLICATION->IncludeComponent(
        "bitrix:sale.ajax.locations",
        $arParams["TEMPLATE_LOCATION"],
        array(
          "AJAX_CALL" => "N",
          "COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
          "REGION_INPUT_NAME" => "REGION_tmp",
          "CITY_INPUT_NAME" => "tmp",
          "CITY_OUT_LOCATION" => "Y",
          "LOCATION_VALUE" => "",
          "ONCITYCHANGE" => "submitForm()",
        ),
        null,
        array('HIDE_ICONS' => 'Y')
      ); ?>

    </div>
<? endif ?>
