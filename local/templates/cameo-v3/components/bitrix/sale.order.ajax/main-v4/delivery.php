<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Product\Basket; ?>
<script type="text/javascript">
  function fShowStore(id, showImages, formWidth, siteId) {
    var strUrl = '<?=$templateFolder?>' + '/map.php';
    var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

    var storeForm = new BX.CDialog({
      'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
      head: '',
      'content_url': strUrl,
      'content_post': strUrlPost,
      'width': formWidth,
      'height': 450,
      'resizable': false,
      'draggable': false,
    });

    var button = [
      {
        title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
        id: 'crmOk',
        'action': function() {
          GetBuyerStore();
          BX.WindowManager.Get().Close();
        },
      },
      BX.CDialog.btnCancel,
    ];
    storeForm.ClearButtons();
    storeForm.SetButtons(button);
    storeForm.Show();
  }

  function GetBuyerStore() {
    BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
    //BX('ORDER_DESCRIPTION').value = '<?=GetMessage("SOA_ORDER_GIVE_TITLE")?>: '+BX('POPUP_STORE_NAME').value;
    BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
    BX.show(BX('select_store'));
  }

  function showExtraParamsDialog(deliveryId) {
    var strUrl = '<?=$templateFolder?>' + '/delivery_extra_params.php';
    var formName = 'extra_params_form';
    var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

    if (window.BX.SaleDeliveryExtraParams) {
      for (var i in window.BX.SaleDeliveryExtraParams) {
        strUrlPost += '&' + encodeURI(i) + '=' + encodeURI(window.BX.SaleDeliveryExtraParams[i]);
      }
    }

    var paramsDialog = new BX.CDialog({
      'title': '<?=GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS')?>',
      head: '',
      'content_url': strUrl,
      'content_post': strUrlPost,
      'width': 500,
      'height': 200,
      'resizable': true,
      'draggable': false,
    });

    var button = [
      {
        title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
        id: 'saleDeliveryExtraParamsOk',
        'action': function() {
          insertParamsToForm(deliveryId, formName);
          BX.WindowManager.Get().Close();
        },
      },
      BX.CDialog.btnCancel,
    ];

    paramsDialog.ClearButtons();
    paramsDialog.SetButtons(button);
    //paramsDialog.adjustSizeEx();
    paramsDialog.Show();
  }

  function insertParamsToForm(deliveryId, paramsFormName) {
    var orderForm = BX('ORDER_FORM'),
        paramsForm = BX(paramsFormName);
    wrapDivId = deliveryId + '_extra_params';

    var wrapDiv = BX(wrapDivId);
    window.BX.SaleDeliveryExtraParams = {};

    if (wrapDiv)
      wrapDiv.parentNode.removeChild(wrapDiv);

    wrapDiv = BX.create('div', {props: {id: wrapDivId}});

    for (var i = paramsForm.elements.length - 1; i >= 0; i--) {
      var input = BX.create('input', {
            props: {
              type: 'hidden',
              name: 'DELIVERY_EXTRA[' + deliveryId + '][' + paramsForm.elements[i].name + ']',
              value: paramsForm.elements[i].value,
            },
          },
      );

      window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

      wrapDiv.appendChild(input);
    }

    orderForm.appendChild(wrapDiv);

    BX.onCustomEvent('onSaleDeliveryGetExtraParams', [window.BX.SaleDeliveryExtraParams]);
  }
</script>
<?
$delivery = null;
$prevId = null;
$saveUid = null;

foreach ($arResult['DELIVERY'] as $delivery_id => $arDelivery) {
    if ($arDelivery["CHECKED"] == 'Y') {
        $delivery = $arDelivery;
        $saveUid = $uid;
    }
}
/** @var CdekPvz $pickPoint */
$pickPoint = null;
$insurance = null;
$pickPointId = null;
$insuranceId = null;
foreach ($delivery['EXTRA_SERVICES'] as $k => $service) {
    /** @var CdekPvz $service */
    if ($service->getCode() == 'pvz') {
        $pickPointId = $k;
        $pickPoint = $service;
        if (empty($pickPoint->getParams()['pvz'])) {
            $result = $pickPoint->createOptions(false);
        }
        unset($delivery['EXTRA_SERVICES'][$k]);
    }
    if ($service->getCode() == 'insurance') {
        $insuranceId = $k;
        $insurance = $service;
        unset($delivery['EXTRA_SERVICES'][$k]);
    }
}

?>
<? $key = 0; ?>
<? $curId = null; ?>
<? $currentPickPoint = null; ?>
<?
if ($_SERVER['REMOTE_ADDR'] == '188.234.148.65') {
//    echo "<pre>";
//    var_dump($GLOBALS['test']);
//    echo "</pre>";
}
$price = isset($delivery["DELIVERY_DISCOUNT_PRICE"]) ? $delivery["DELIVERY_DISCOUNT_PRICE"] : $delivery["PRICE"];
$priceFormatted = isset($delivery["DELIVERY_DISCOUNT_PRICE_FORMATED"]) ? $delivery["DELIVERY_DISCOUNT_PRICE_FORMATED"] : $delivery["PRICE_FORMATED"];
if (!$price) {
    $priceFormatted = 'бесплатно';
    if ($delivery['ID'] == 148) {
        $priceFormatted = 'за счет клиента';
    }
}
$period = explode("-", $delivery['PERIOD_TEXT']);
$period = $period[1];
if ($period) {
    $max = (int)filter_var($period, FILTER_SANITIZE_NUMBER_INT);
    $delivery['PERIOD_TEXT'] = 'до ' . ($max + 1) . ' раб. дней';
}

$skip = true;
$showMessage = false;
if (count($arResult['DELIVERY']) == 1 && key($arResult['DELIVERY']) == 149) {
    $skip = false;
    $priceFormatted = '- ₽';
    $delivery['PERIOD_TEXT'] = '-';
    $showMessage = true;
}

?>
<div class="order-delivery-group">
    <div class="location-fields">
        <?
        PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], 'LOCATION', []);
        ?>
    </div>
    <div class="delivery-list">
        <div class="radio-group">
            <?
            foreach ($arResult['DELIVERY'] as $delivery_id => $arDelivery) {
                if ($skip && $delivery_id == 149) {
                    continue;
                }
                if (strpos($arDelivery['NAME'], 'очта') !== false || strpos($arDelivery['NAME'], 'осылка') !== false) {
                    $arDelivery['NAME'] = 'Почтой';
                } elseif (strpos($arDelivery['NAME'], 'выдачи') !== false || strpos($arDelivery['NAME'],
                    'ПВЗ') !== false) {
                    $arDelivery['NAME'] = 'Заберу сам';
                } elseif (strpos($arDelivery['NAME'], 'урьер') !== false) {
                    $arDelivery['NAME'] = 'Курьером';
                }
                ?>
                <div class="item">
                    <input type="radio" name="DELIVERY_ID"
                           id="delivery<?= $delivery_id ?>" <?= $arDelivery["CHECKED"] === "Y" ? "checked" : "" ?>
                           value="<?= $delivery_id ?>" onchange="submitForm()">
                    <label class="<?= $arDelivery["CHECKED"] === "Y" ? "active" : "" ?>"
                           for="delivery<?= $delivery_id ?>"
                    ><?= $arDelivery['NAME'] ?></label>
                </div>
                <?
                if ($delivery['ID'] != $arDelivery['ID']) {
                    foreach ($arDelivery['EXTRA_SERVICES'] as $id => $service) {
                        /** @var \Bitrix\Sale\Delivery\ExtraServices\Enum $service */
                        ?>
                        <input type="hidden"
                               name="DELIVERY_EXTRA_SERVICES[<?= $arDelivery['ID'] ?>][<?= $id ?>]"
                               value="<?= $service->getValue() ?>">
                        <?
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="delivery-info">
        <div class="ui grid">
            <div class="column six wide computer sixteen wide mobile">
                <div class="delivery-header">Служба доставки</div>
                <div class="delivery-value"><?= $delivery['NAME'] ?></div>
            </div>
            <div class="column six wide computer sixteen wide mobile">
                <div class="delivery-header"><?= ($pickPoint !== null ? 'Адрес' : 'Дополнительные услуги') ?></div>
                <div class="delivery-value">
                    <?
                    if ($pickPoint !== null) {
                        ?>
                        <div class="ui fullscreen modal pickPointModal">
                            <? icon('close') ?>
                            <div class="header">
                                <div class="radio-group">
                                    <a class="item active" data-tab="pick_list">Списком</a>
                                    <a class="item" data-tab="pick_map">На карте</a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="ui tab active" data-tab="pick_list">
                                    <table class="ui very basic selectable table">
                                        <thead>
                                        <tr>
                                            <th>Название</th>
                                            <th>Адрес</th>
                                            <th>Время работы</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? foreach ($pickPoint->getParams()["pvz"] as $id => $option): ?>
                                            <?
                                            if ($id == $pickPoint->getValue()) {
                                                $currentPickPoint = $option;
                                                $curId = $id;
                                            } ?>
                                            <tr<?= ($id == $pickPoint->getValue() ? ' class="active"' : '') ?>
                                                    data-point="<?= $id ?>">
                                                <td><?= $option['Name'] ?></td>
                                                <td><?= $option["Address"] ?></td>
                                                <td><?= $option['WorkTime'] ?></td>
                                            </tr>
                                        <? endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="ui tab" data-tab="pick_map">
                                    <div id="pickPointMap">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pickPointTrigger">
                            <div class="field break">
                                <input type="text" name="pick_point" class="value"
                                       readonly
                                       placeholder="Выбрать пункт выдачи"
                                       value="<?= $currentPickPoint['Address'] ?>">
                            </div>
                            <input type="hidden"
                                   class="pvz-value"
                                   name="DELIVERY_EXTRA_SERVICES[<?= $delivery['ID'] ?>][<?= $pickPointId ?>]"
                                   value="<?= $pickPoint->getValue() ?>">
                        </div>
                        <?
                    }
                    ?>
                    <? if ($insurance !== null): ?>
                        <input type="hidden" name="DELIVERY_EXTRA_SERVICES[<?= $delivery['ID'] ?>][<?= $insuranceId ?>]"
                               checked
                               value="Y">
                    <? endif; ?>
                    <?
                    if ($count > 6 && in_array($GLOBALS['ORDER_CITY_ID'],
                        getVipCities()) && $arResult["USER_VALS"]["PERSON_TYPE_ID"] == 1) {
                        ?>
                        <p><?= Basket::$infoText ?></p>
                        <?
                    }
                    if (!empty($delivery['EXTRA_SERVICES'])) {
                        ?>
                        <div class="add-service">
                            <?
                            foreach ($delivery['EXTRA_SERVICES'] as $id => $service) {
                                /** @var \Bitrix\Sale\Delivery\ExtraServices\Enum $service */
                                ?>
                                <div class="field">
                                    <div class="ui checkbox">
                                        <input type="hidden"
                                               name="<?= "DELIVERY_EXTRA_SERVICES[" . $delivery['ID'] . "][$id]" ?>"
                                               value="N">
                                        <input id="add-service<?= $id ?>"
                                               value="Y"
                                               onchange="window.order.submit();"
                                               type="checkbox" <?= $service->getValue() === "Y" ? "checked" : "" ?>
                                               name="<?= "DELIVERY_EXTRA_SERVICES[" . $delivery['ID'] . "][$id]" ?>">
                                        <label for="add-service<?= $id ?>"><span class="flex_block__row">
                                <span class="flex_block__column">
                                    <span class="add-service_name"><?= $service->getName() ?></span>
                                    <span class="add-service_dscr">
                                        <?= $service->getDescription() ?>
                                    </span>
                                </span>
                                <span class="add-service_price">
                                    <?//=CurrencyFormat($service->price, $service->getCurrency())?>
                                </span>
                            </span>
                                        </label>
                                    </div>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                        <?
                    } ?>
                </div>
            </div>
            <div class="column two wide computer five wide mobile">
                <div class="delivery-header">Срок</div>
                <div class="delivery-value"><?= $delivery['PERIOD_TEXT'] ?></div>
            </div>
            <div class="column two wide computer five wide mobile">
                <div class="delivery-header">Стоимость</div>
                <div class="delivery-value"><?= $priceFormatted ?><? if($showMessage){icon('info','no-delivery-info');} ?></div>
            </div>
            <?
            if ($showMessage) {
                ?>
                <div class="column sixteen wide">
                    <div class="no-delivery">В данный момент мы не смогли рассчитать <br>стоимость доставки, с вами свяжется <br/>менеджер</div>
                </div>
                <?
            }
            ?>
        </div>
    </div>
    <div class="fields">
        <?
        PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"]);
        PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
        PrintPropsForm($arResult["ORDER_PROP"]["RELATED"], $arParams["TEMPLATE_LOCATION"]);
        ?>
        <div class="field field-comment">
        <textarea
                id="ORDER_DESCRIPTION"
                name="ORDER_DESCRIPTION"
                placeholder="Комментарий"
                rows="3"
        ><?= $arResult["USER_VALS"]["ORDER_DESCRIPTION"] ?></textarea>
        </div>
    </div>
</div>

<script>
    <?
    if ($_POST["is_ajax_post"] != "Y") {
    ?>
    $(window).one('pvzReady', function() {
      window.order.loadPvz(<?=json_encode($pickPoint ? $pickPoint->getParams()["pvz"] : [])?>);
      window.order.setSelected("<?=$currentPickPoint ? $currentPickPoint["Code"] : null?>");
    });
    <?
    } else {
    ?>
    $(top.window).off('pvzReady').one('pvzReady', function() {
      top.window.order.loadPvz(<?=json_encode($pickPoint ? $pickPoint->getParams()["pvz"] : [])?>);
      top.window.order.setSelected("<?=$currentPickPoint ? $currentPickPoint["Code"] : null?>");
    });
    <?
    }
    ?>

</script>

<?
if (!empty($delivery['STORE'])) {
    ?>
    <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>"/>
    <?
} ?>


