<? use Bitrix\Sale\Location\LocationTable;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?
if (!function_exists("showFilePropertyField")) {
    function showFilePropertyField($name, $property_fields, $values, $max_file_size_show = 50000)
    {
        $res = "";

        if (!is_array($values) || empty($values)) {
            $values = array(
              "n0" => 0,
            );
        }

        if ($property_fields["MULTIPLE"] == "N") {
            $res = "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[0]\" id=\"" . $name . "[0]\"></label>";
        } else {
            $res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

            $res .= "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[0]\" id=\"" . $name . "[0]\"></label>";
            $res .= "<br/><br/>";
            $res .= "<label for=\"\"><input type=\"file\" size=\"" . $max_file_size_show . "\" value=\"" . $property_fields["VALUE"] . "\" name=\"" . $name . "[1]\" id=\"" . $name . "[1]\" onChange=\"javascript:addControl(this);\"></label>";
        }

        return $res;
    }
}

/*
<?if($arProperties["CODE"] === "street" || $arProperties["CODE"] === "house" || $arProperties["CODE"] === "flat" || $arProperties["CODE"] === "ZIP"){
                  continue;
              }?>
              <?if($arProperties["CODE"] === "ADDRESS"):?>
                  <div data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>">
                      <div class="ui tabular menu basket_address">
                          <input type="radio" name="deliveryType"  id="delivery"/>
                          <a class="item active" href="" data-tab="basket_address__first">
                              <div class="flex_block__row flex_start">
                                  <h3 class="ui header">Выберите адрес доставки</h3>
                                  <div class="popup_btn__wrapper-hover">
                                      <button class="ui custom button"></button>
                                  </div>
                                  <div class="ui custom popup">Текст текстовой</div>
                              </div>
                          </a>
                          <a class="item" href="javascript:void(0)" data-tab="basket_address__second">
                              <div class="flex_block__row flex_start">
                                  <h3 class="ui header">Пункт выдачи</h3>
                                  <div class="popup_btn__wrapper-hover">
                                      <button class="ui custom button"></button>
                                  </div>
                                  <div class="ui custom popup">Текст текстовой</div>
                              </div>
                          </a>
                      </div>
                      <div class="ui bottom tab active" data-tab="basket_address__first"></div>
                      <div class="ui bottom tab" data-tab="basket_address__second"></div>
                  </div>
                  <?continue;?>
              <?endif;?>
*/

if (!function_exists("PrintPropsForm")) {
    function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $only = false, $exclude = ['LOCATION'])
    {
        global $currentCityId;
        if (!empty($arSource)) {
            $notMe = 'F';
            foreach ($arSource as $arProperties) {
                if ($arProperties['CODE'] == 'not_me') {
                    $notMe = $arProperties['CHECKED'] == 'Y' ? 'Y' : 'N';
                }
            }
            foreach ($arSource as $arProperties) {
                if ("LOCATION" === $arProperties["CODE"]) {
                    $currentCityId = $arProperties["VALUE"];
                }
                if (in_array($arProperties['CODE'], $exclude)) {
                    continue;
                }
                if ($only && $only != $arProperties['CODE']) {
                    continue;
                }
                if (in_array($arProperties['CODE'], [
                  'street',
                  'house',
                  'corps',
                  'flat'
                ])) {
                    ?>
                    <input
                            type="hidden"
                            data-code="<?= strtolower($arProperties['CODE']) ?>"
                            value="<?= $arProperties["VALUE"] ?>"
                            name="<?= $arProperties["FIELD_NAME"] ?>"
                    />
                    <?
                    continue;
                }
                if ("user-address" === $arProperties["CODE"]) {
                    continue;
                    global $userAddresses;
                    ?>
                    <script>
                      $(function() {
                        window.userAddreses = <?=json_encode($userAddresses)?> ||
                        [];
                      });
                    </script>
                    <div data-property-id-row="<?= intval($arProperties["ID"]) ?>"
                         class="field<?= (CUser::IsAuthorized() ? '' : ' hide') ?>">
                        <div class="floated focus">

                            <select id="<?= $arProperties["FIELD_NAME"] ?>"
                                    class="ui scrolling selection dropdown useraddress fluid"
                                    name="<?= $arProperties["FIELD_NAME"] ?>">
                                <option value="0">Ввести новый адрес</option>
                                <?
                                $selected = false;
                                ?>
                                <?
                                foreach ($userAddresses as $address):?>
                                    <?
                                    if ($currentCityId !== null && $address["city_id"] != $currentCityId) {
                                        continue;
                                    } ?>
                                    <?
                                    if ($arProperties["VALUE"] == $address["id"]) {
                                        $selected = $address["id"];
                                    } ?>
                                    <option value="<?= $address["id"] ?>" <?= !$selected && $address["default"] === "Y" || $selected === $address["id"] ? "selected" : "" ?>><?= $address["name"] ?: $address['formatted'] ?></option>
                                <? endforeach; ?>
                            </select>
                            <?

                            ?>
                        </div>
                    </div>

                    <?
                    continue;
                }
                if ("user-organization" === $arProperties["CODE"]) {
                    continue;
                    global $organizations;
                    ?>
                    <script>
                      $(function() {
                        window.organizations = <?=json_encode($organizations)?> ||
                        [];
                      });
                    </script>
                    <div data-property-id-row="<?= intval($arProperties["ID"]) ?>"
                         class="field<?= (CUser::IsAuthorized() ? '' : ' hide') ?>">
                        <div class="floated focus">
                            <select id="<?= $arProperties["FIELD_NAME"] ?>"
                                    class="ui scrolling selection dropdown useraddress fluid"
                                    name="<?= $arProperties["FIELD_NAME"] ?>">
                                <option value="0">Ввести новую организацию</option>
                                <?
                                $selected = false;
                                ?>
                                <?
                                foreach ($organizations as $address):?>
                                    <?
                                    if ($arProperties["VALUE"] == $address["id"]) {
                                        $selected = $address["id"];
                                    } ?>
                                    <option value="<?= $address["id"] ?>" <?= !$selected && $address["default"] === "Y" || $selected === $address["id"] ? "selected" : "" ?>><?= $address["name"] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <?
                    continue;
                }
                if ($arProperties['CODE'] == 'ZIP') {
                    $value = [];
                    $hasStreet = false;
                    foreach ($arSource as $prop) {
                        if ($prop['VALUE']) {
                            switch ($prop['CODE']) {
                                case "street":
                                    $hasStreet = true;
                                    $value[] = 'ул ' . $prop['VALUE'];
                                    break;
                                case "house":
                                    $value[] = 'д ' . $prop['VALUE'];
                                    break;
                                case "corps":
                                    $value[] = 'к ' . $prop['VALUE'];
                                    break;
                                case "flat":
                                    $value[] = 'кв ' . $prop['VALUE'];
                                    break;
                            }
                        }
                    }
                    if (count($value) == 1 && $hasStreet) {
                        $value[0] = substr($value[0], 3);
                    }
                    $value = implode(", ", $value);
                    ?>
                    <div class="field field-full-address">
                        <input
                                maxlength="250"
                                type="text"
                                data-code="full-address"
                                value="<?= $value ?>"
                                name="full-address"
                                title="Адрес(улица, дом)*"
                                placeholder="Адрес(улица, дом)*"
                        />
                    </div>
                    <?
                }
                ?>
                <div data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>"
                     class="field field-<?= $arProperties['CODE'] ?><?= ($notMe == 'N' && strpos($arProperties['CODE'],
                       'not_me_') !== false ? ' hide' : '') ?>">

                    <?
                    if ($arProperties["TYPE"] == "CHECKBOX") {
                        ?>
                        <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" value="">
                        <div class="ui checkbox">
                            <input id="<?= $arProperties["FIELD_NAME"] ?>" value="Y"
                                   data-code="<?= $arProperties['CODE'] ?>"
                                   data-validate="<?= $arProperties['CODE'] ?>"
                              <? if ($arProperties['CODE'] == 'not_me') {
                                  echo 'onchange="window.order.submit();"';
                              } ?>
                                   type="checkbox" name="<?= $arProperties["FIELD_NAME"] ?>"<?
                            if ($arProperties["CHECKED"] == "Y") {
                                echo " checked";
                            } ?>>
                            <label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?>
                                <?
                                if ($arProperties["REQUIED_FORMATED"] == "Y"):?>
                                    <span class="bx_sof_req">*</span>
                                <? endif; ?></label>
                        </div>
                        <?
                    } elseif ($arProperties["TYPE"] == "TEXT") {
                        $code = strtolower($arProperties['CODE']);
                        if ($code == 'phone' && strlen($arProperties["VALUE"]) == 11) {
                            $firstChar = substr($arProperties['VALUE'],
                              0, 1);
                            if ($firstChar == '7') {
                                $arProperties['VALUE'] = '+' . $arProperties['VALUE'];
                            } elseif ($firstChar == '8') {
                                $arProperties['VALUE'] = "+7" . substr($arProperties['VALUE'], 1);
                            }
                        }
                        $readonly = false;
                        switch ($code) {
                            case 'fio':
                            case 'phone':
                            case 'email':
                                $readonly = $arProperties['VALUE'];
                                break;
                        }
                        ?>
                        <input
                                id="<?= $arProperties["FIELD_NAME"] ?>"
                                maxlength="250"
                                size="<?= $arProperties["SIZE1"] ?>"
                                type="text"
                                data-code="<?= $code ?>"
                          <?= ($readonly ? 'readonly' : '') ?>
                                value="<?= $arProperties["VALUE"] ?>"
                                name="<?= $arProperties["FIELD_NAME"] ?>"
                                placeholder="<?= $arProperties["NAME"] ?><?= $arProperties["REQUIED_FORMATED"] == "Y" ? "*" : "" ?>"
                        />
                        <?
                    } elseif ($arProperties["TYPE"] == "SELECT") {
                        ?>
                        <br/>
                        <div class="bx_block r1x3 pt8">
                            <?= $arProperties["NAME"] ?>
                            <?
                            if ($arProperties["REQUIED_FORMATED"] == "Y"):?>
                                <span class="bx_sof_req">*</span>
                            <? endif; ?>
                        </div>

                        <div class="bx_block r3x1">
                            <select name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>"
                                    size="<?= $arProperties["SIZE1"] ?>">
                                <?
                                foreach ($arProperties["VARIANTS"] as $arVariants):
                                    ?>
                                    <option value="<?= $arVariants["VALUE"] ?>"<?
                                    if ($arVariants["SELECTED"] == "Y") {
                                        echo " selected";
                                    } ?>><?= $arVariants["NAME"] ?></option>
                                <?
                                endforeach;
                                ?>
                            </select>

                            <?
                            if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                                ?>
                                <div class="bx_description">
                                    <?= $arProperties["DESCRIPTION"] ?>
                                </div>
                            <?
                            endif;
                            ?>
                        </div>
                        <div style="clear: both;"></div>
                        <?
                    } elseif ($arProperties["TYPE"] == "MULTISELECT") {
                        ?>
                        <br/>
                        <div class="bx_block r1x3 pt8">
                            <?= $arProperties["NAME"] ?>
                            <?
                            if ($arProperties["REQUIED_FORMATED"] == "Y"):?>
                                <span class="bx_sof_req">*</span>
                            <? endif; ?>
                        </div>

                        <div class="bx_block r3x1">
                            <select multiple name="<?= $arProperties["FIELD_NAME"] ?>"
                                    id="<?= $arProperties["FIELD_NAME"] ?>" size="<?= $arProperties["SIZE1"] ?>">
                                <?
                                foreach ($arProperties["VARIANTS"] as $arVariants):
                                    ?>
                                    <option value="<?= $arVariants["VALUE"] ?>"<?
                                    if ($arVariants["SELECTED"] == "Y") {
                                        echo " selected";
                                    } ?>><?= $arVariants["NAME"] ?></option>
                                <?
                                endforeach;
                                ?>
                            </select>

                            <?
                            if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                                ?>
                                <div class="bx_description">
                                    <?= $arProperties["DESCRIPTION"] ?>
                                </div>
                            <?
                            endif;
                            ?>
                        </div>
                        <div style="clear: both;"></div>
                        <?
                    } elseif ($arProperties["TYPE"] == "TEXTAREA") {
                        $rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
                        ?>

                        <label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?><?= $arProperties["REQUIED_FORMATED"] == "Y" ? "*" : "" ?></label>
                        <textarea
                                id="<?= $arProperties["FIELD_NAME"] ?>"
                                data-code="<?= strtolower($arProperties['CODE']) ?>"
                                name="<?= $arProperties["FIELD_NAME"] ?>"
                                style="padding:2rem 1rem 0;"
                                rows="2"
                        ><?= $arProperties["VALUE"] ?></textarea>

                        <?
                    } elseif ($arProperties["TYPE"] == "LOCATION") {
                        ?>
                        <div class="bx_block r3x1">

                            <?
                            $value = 0;

                            if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0) {
                                foreach ($arProperties["VARIANTS"] as $arVariant) {
                                    if ($arVariant["SELECTED"] == "Y") {
                                        $value = $arVariant["ID"];
                                        $path = array();
                                        $rsPath = LocationTable::getPathToNodeByCode($arVariant['CODE'], array(
                                          "select" => array(
                                            "DISPLAY" => "NAME.NAME",
                                            "TYPE_ID"
                                          ),
                                          "filter" => array(
                                            "=NAME.LANGUAGE_ID" => LANGUAGE_ID,
                                          )
                                        ));
                                        while ($ob = $rsPath->Fetch()) {
                                            $path[] = $ob['DISPLAY'];
                                        }
                                        $path = array_reverse($path);
                                        $path = implode(", ", $path);
                                        $name = $arVariant["NAME"];
                                        $cityName = $arVariant['CITY_NAME'];
                                        break;
                                    }
                                }
                            }
                            //                      if ($_POST["is_ajax_post"] != "Y") {
                            //                          global $location;
                            //                          $name = $location["city"];
                            //                          $value = $location["id"];
                            //                      }
                            $GLOBALS['ORDER_CITY_ID'] = $value;
                            ?>
                            <div class="floated focus">
                                <div class="ui search fluid locations">
                                    <div class="ui icon input fluid">
                                        <input type="text" class="prompt"
                                               value="<?= $path ? $path : $cityName ?>"
                                               id="cityLine"
                                               name="cityLine"
                                               placeholder="<?= $arProperties["NAME"] ?><?= $arProperties["REQUIED_FORMATED"] == "Y" ? "*" : "" ?>">
                                        <? icon("search") ?>
                                        <input id="cityId" type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>"
                                               value="<?= $value ?>">
                                    </div>
                                    <div class="results"></div>
                                </div>
                                <input type="hidden" id="cityName" value="<?= $cityName ?>">
                                <input type="hidden" id="regionName" value="<?= $arVariant['REGION_NAME'] ?>">
                                <input type="hidden" id="countryName" value="<?= $arVariant['COUNTRY_NAME'] ?>">
                            </div>


                            <? /*
                      if (CSaleLocation::isLocationProMigrated()) {
                        $locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
                        $locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
                      }
                      ?>

                      <?
                      if ($locationTemplateP == 'steps'):?>
                          <input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?= intval($arProperties["ID"]) ?>]"
                                 name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?= intval($arProperties["ID"]) ?>]"
                                 value="<?= ($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0') ?>"/>
                      <?endif ?>

                      <?
                      CSaleLocation::proxySaleAjaxLocationsComponent(array(
                        "AJAX_CALL" => "N",
                        "COUNTRY_INPUT_NAME" => "COUNTRY",
                        "REGION_INPUT_NAME" => "REGION",
                        "CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
                        "CITY_OUT_LOCATION" => "Y",
                        "LOCATION_VALUE" => $value,
                        "ORDER_PROPS_ID" => $arProperties["ID"],
                        "ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
                        "SIZE1" => $arProperties["SIZE1"],
                      ),
                        array(
                          "ID" => $value,
                          "CODE" => "",
                          "SHOW_DEFAULT_LOCATIONS" => "Y",

                          // function called on each location change caused by user or by program
                          // it may be replaced with global component dispatch mechanism coming soon
                          "JS_CALLBACK" => "submitFormProxy",

                          // function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
                          // it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
                          "JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

                          // an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
                          // it may be replaced with global component dispatch mechanism coming soon
                          "JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

                          "DISABLE_KEYBOARD_INPUT" => "Y",
                          "PRECACHE_LAST_LEVEL" => "Y",
                          "PRESELECT_TREE_TRUNK" => "Y",
                          "SUPPRESS_ERRORS" => "Y"
                        ),
                        $locationTemplateP,
                        true,
                        ''
                      ) ?>

                      <?
                      if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                        ?>
<!--                          <div class="bx_description">-->
<!--                            --><?//= $arProperties["DESCRIPTION"] ?>
<!--                          </div>-->
                        <?
                      endif;
                      */ ?>

                        </div>
                        <!--                    <div style="clear: both;"></div>-->


                        <?
                    } elseif ($arProperties["TYPE"] == "RADIO") {
                        ?>
                        <div class="bx_block r1x3 pt8">
                            <?= $arProperties["NAME"] ?>
                            <?
                            if ($arProperties["REQUIED_FORMATED"] == "Y"):?>
                                <span class="bx_sof_req">*</span>
                            <? endif; ?>
                        </div>

                        <div class="bx_block r3x1">
                            <?
                            if (is_array($arProperties["VARIANTS"])) {
                                foreach ($arProperties["VARIANTS"] as $arVariants):
                                    ?>
                                    <input
                                            type="radio"
                                            name="<?= $arProperties["FIELD_NAME"] ?>"
                                            id="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"
                                            value="<?= $arVariants["VALUE"] ?>" <?
                                    if ($arVariants["CHECKED"] == "Y") {
                                        echo " checked";
                                    } ?> />

                                    <label for="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"><?= $arVariants["NAME"] ?></label></br>
                                <?
                                endforeach;
                            }
                            ?>

                            <?
                            if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                                ?>
                                <div class="bx_description">
                                    <?= $arProperties["DESCRIPTION"] ?>
                                </div>
                            <?
                            endif;
                            ?>
                        </div>
                        <div style="clear: both;"></div>
                        <?
                    } elseif ($arProperties["TYPE"] == "FILE") {
                        ?>
                        <br/>
                        <div class="bx_block r1x3 pt8">
                            <?= $arProperties["NAME"] ?>
                            <?
                            if ($arProperties["REQUIED_FORMATED"] == "Y"):?>
                                <span class="bx_sof_req">*</span>
                            <? endif; ?>
                        </div>

                        <div class="bx_block r3x1">
                            <?= showFilePropertyField("ORDER_PROP_" . $arProperties["ID"], $arProperties,
                              $arProperties["VALUE"], $arProperties["SIZE1"]) ?>

                            <?
                            if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                                ?>
                                <div class="bx_description">
                                    <?= $arProperties["DESCRIPTION"] ?>
                                </div>
                            <?
                            endif;
                            ?>
                        </div>

                        <div style="clear: both;"></div><br/>
                        <?
                    }
                    ?>
                </div>
                <?
                if (CSaleLocation::isLocationProEnabled()): ?>

                    <?
                    $propertyAttributes = array(
                      'type' => $arProperties["TYPE"],
                      'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form'
                        // value taken from property DEFAULT_VALUE or it`s a user-typed value?
                    );

                    if (intval($arProperties['IS_ALTERNATE_LOCATION_FOR'])) {
                        $propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);
                    }

                    if (intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION'])) {
                        $propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);
                    }

                    if ($arProperties['IS_ZIP'] == 'Y') {
                        $propertyAttributes['isZip'] = true;
                    }
                    ?>
                <?
                endif ?>

                <?
            }
            ?>

            <?
        }
    }
}
?>
