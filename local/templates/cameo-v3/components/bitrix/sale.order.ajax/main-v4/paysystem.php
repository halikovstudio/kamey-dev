<h3 class="ui header"><?= GetMessage("SOA_TEMPL_PAY_SYSTEM") ?></h3>
<div class="ui grid three column doubling stackable payment-list">
    <?
    foreach ($arResult['PAY_SYSTEM'] as $key => $arPaySystem) {
        if ($arPaySystem['ID'] == 11) {
            continue;
        }
        $subClass = '';
        $showName = true;
        if ($arPaySystem['ID'] == 13) {
            $subClass = ' hide check-apple-pay';
            $showName = false;
        } elseif ($arPaySystem['ID'] == 14) {
            $subClass = ' hide check-google-pay';
            $showName = false;
        }
        ?>
        <div class="column<?= $subClass ?>">
            <div class="radio-image">
                <input id="pay<?= $key ?>" type="radio" value="<?= $arPaySystem['ID'] ?>"
                       name="PAY_SYSTEM_ID" <?= ($arPaySystem['CHECKED'] == 'Y' ? ' checked' : '') ?>>
                <label for="pay<?= $key ?>">
                    <div class="image-container">
                        <img src="<?= $arPaySystem['PSA_LOGOTIP']['SRC'] ?>" alt="">
                        <img src="<?= $templateFolder ?>/check.svg" alt="" class="check">
                    </div>
                    <?
                    if ($showName) {
                        echo $arPaySystem['PSA_NAME'];
                    }
                    ?>
                </label>
            </div>
        </div>
        <?
    }
    ?>
</div>
