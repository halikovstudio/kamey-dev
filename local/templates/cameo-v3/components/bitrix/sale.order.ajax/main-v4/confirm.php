<?

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
\Page\Options::set("head-script", '<script data-skip-moving="true">
  fbq(\'track\', \'Purchase\');
</script>');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/google_pay.js');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/apple_pay.js');
?>
<div style="overflow: auto">
    <div class="console-log"></div>
</div>
<div class="order-thanks">
    <h1 class="ui header uppercase normal center aligned">Спасибо за заказ!</h1>
    <div class="ui header center aligned normal uppercase order-number">Номер
        заказа <?= $arResult["ORDER"]["ACCOUNT_NUMBER"] ?></div>
    <?
    $prepay = false;
    if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y') {
    if (!empty($arResult["PAYMENT"])) {
    foreach ($arResult["PAYMENT"] as $payment) {
        if ($payment["PAY_SYSTEM_ID"] == BonusCard::PAYMENT_ID) {
            continue;
        }
    if ($payment["PAID"] != 'Y') {
    if (!empty($arResult['PAY_SYSTEM_LIST'])
      && array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
    ) {
        $arPaySystem = $arResult['PAY_SYSTEM_LIST'][$payment["PAY_SYSTEM_ID"]];
    if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["IS_CASH"] != "Y") {
    } else {
        ?>
        <p class="gray-color">
            В рабочее время с Вами свяжется менеджер для уточнения и подтверждения заказа.
        </p>
    <?
    continue;
    }
    if (empty($arPaySystem["ERROR"])) {
    $prepay = true;
    ?>
    <? if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["IS_CASH"] != "Y"){ ?>
    <?
    $orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
    $paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
    ?>
    <?
    if ($arPaySystem['ID'] != 14 && $arPaySystem['ID'] != 13) {
    ?>
        <p class="gray-color">
        <?
    if ($arPaySystem['IS_AFFORD_PDF']) {
        echo Loc::getMessage("SOA_PAY_PDF",
          array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $orderAccountNumber));
    } else {
    if ($arPaySystem["NEW_WINDOW"] == "Y") {
        echo Loc::getMessage("SOA_PAY_LINK",
          array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $orderAccountNumber . "&PAYMENT_ID=" . $paymentAccountNumber));
    } else {
        ?>
        <div class="payment-form-container"><?
            if (strpos($arPaySystem['BUFFERED_OUTPUT'], 'sberbank__wrapper') !== false) {
                preg_match('/<a\s+(?:[^"\'>]+|"[^"]*"|\'[^\']*\')*href=("[^"]+"|\'[^\']+\'|[^<>\s]+)/i',
                  $arPaySystem['BUFFERED_OUTPUT'], $m);
                $m[1] = str_replace('"', '', $m[1]);
                ?>
                <a href="<?= $m[1] ?>" class="ui primary custom button">Оплатить заказ</a>
                <?
            } else {
                preg_match('/<form\sid=\"paysystem-yandex-form\".*<\/form>/s', $arPaySystem['BUFFERED_OUTPUT'], $m);
                if ($m[0]) {
                    $m[0] = str_replace("btn", "ui primary custom button", $m[0]);
                    $m[0] = str_replace("Оплатить", "Оплатить заказ", $m[0]);
                    $m[0] = str_replace("d-flex", "", $m[0]);
                    $_GET['PAYMENT_ID'] = $paymentAccountNumber;
                    ?>
                    <div class="payment-form-yandex">
                    <?
                    echo $m[0];
                    ?></div><?
                } else {
                    ?>
                    <?
                    echo $arPaySystem['BUFFERED_OUTPUT'];
                }
                ?><?
            }
            ?>
        </div>
        <?
    }
    }
        ?>
        </p>
    <?
    } else {
    ?>
        <p>
        <div class="button-payments">
            <div style="-webkit-appearance: -apple-pay-button;
       -apple-pay-button-type: check-out;
       -apple-pay-button-style: black;"
                 lang=ru class="apple-pay-button hide">

            </div>
        </div>
        </p>
    <?
    if ($arPaySystem['ID'] == 14) {
    ?>
        <script>new GooglePay(<?=$arResult['ORDER']['ID']?>, <?=$payment['SUM']?>);</script>
    <?
    }
    if ($arPaySystem['ID'] == 13) {
    ?>
        <script>new ApplePay(<?=$arResult['ORDER']['ID']?>, <?=$payment['SUM']?>);</script>
    <?
    }
    }
    ?>
    <? } ?>
    <?
    } else {
    ?>
        <p style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR") ?></p>
    <?
    }
    } else {
    ?>
        <p style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR") ?></p>
    <?
    }
    } else {
    ?>
        <p class="gray-color">
            В рабочее время с Вами свяжется менеджер для уточнения и подтверждения заказа.
        </p>
    <?
    }
    }
    }
    } else {
    ?>
        <p class="gray-color">
            В рабочее время с Вами свяжется менеджер для уточнения и подтверждения заказа.
        </p>
        <?
    }
    ?>
    <p class="gray-color">Производитель медицинской одежды с опытом 25 лет<br>Мы знаем всё и даже больше о пошиве
        профессиональной медицинской<br>одежды - работаем в данной сфере с 1992 года.</p>
    <a href="/catalog/" class="ui primary custom button">Перейти в каталог</a>
</div>
<div class="ui tiny modal payment-success">
    <div class="content">
        Платеж успешно проведен
    </div>
</div>
