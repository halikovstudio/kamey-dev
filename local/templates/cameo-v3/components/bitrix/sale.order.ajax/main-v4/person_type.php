<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
if (count($arResult["PERSON_TYPE"]) > 1) {
  ?>
    <div class="form-container user-type">
        <div class="radio-group">
          <?
          foreach ($arResult["PERSON_TYPE"] as $v):?>
              <div class="item">
                  <input type="radio" class="hide" id="PERSON_TYPE_<?= $v["ID"] ?>" name="PERSON_TYPE"
                         onchange="order.submit();" value="<?= $v["ID"] ?>"<?
                  if ($v["CHECKED"] == "Y") echo " checked=\"checked\""; ?>>
                  <label class="item <?
                  if ($v["CHECKED"] == "Y") echo "active"; ?>"
                         for="PERSON_TYPE_<?= $v["ID"] ?>"><?= $v["NAME"] ?></label>
              </div>
          <? endforeach; ?>
        </div>
        <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>"/>
    </div>
  <?
} else {
  if (IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0) {
    //for IE 8, problems with input hidden after ajax
    ?>
      <div class="hide">
            <span style="display:none;">
            <input type="text" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
            <input type="text" name="PERSON_TYPE_OLD" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>"/>
            </span>
      </div>
    <?
  } else {
    foreach ($arResult["PERSON_TYPE"] as $v) {
      ?>
        <input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?= $v["ID"] ?>"/>
        <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $v["ID"] ?>"/>
      <?
    }
  }
}
?>
