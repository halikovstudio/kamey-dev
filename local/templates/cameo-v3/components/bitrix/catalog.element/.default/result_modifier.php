<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

foreach ($arResult['OFFERS'] as $key => &$arOffer) {
  $list = array();
  foreach ($arOffer['PROPERTIES']['PHOTO']['VALUE'] as $fileId) {
    $file = CFile::GetFileArray($fileId);
    if (is_array($file)) {
      $resize = CFile::ResizeImageGet($fileId, array(
        "width"=>580,
        "height"=>773
      ));
      $file['DESCRIPTION'] = $resize['src'];
      $file['RESIZE'] = $resize['src'];
      $list[] = $file;
    }
  }
  $arOffer['MORE_PHOTO'] = $list;
  $arOffer['MORE_PHOTO_COUNT'] = count($list);
  $arResult['JS_OFFERS'][$key]['SLIDER'] = $list;
  $arResult['JS_OFFERS'][$key]['SLIDER_COUNT'] = count($list);
  $arResult['JS_OFFERS'][$key]['DETAIL_PAGE_URL'] = $arOffer['DETAIL_PAGE_URL'];
}

if (!$arResult['CAN_BUY']) {
  $list = array();
  foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $fileId) {
    $file = CFile::GetFileArray($fileId);
    if (is_array($file)) {
      $resize = CFile::ResizeImageGet($fileId, array(
        "width"=>580,
        "height"=>773
      ));
      $file['DESCRIPTION'] = $resize['src'];
      $file['RESIZE'] = $resize['src'];
      $list[] = $file;
    }
  }
  $arResult['MORE_PHOTO'] = $list;
}
