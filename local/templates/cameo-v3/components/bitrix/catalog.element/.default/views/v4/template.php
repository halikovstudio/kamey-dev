<div class="catalog-detail" data-id="<?= $arItem->id ?>" data-uid="<?= $uid ?>" itemscope
     itemtype="http://schema.org/Product">
    <div class="ui grid">
        <div class="column ten wide tablet sixteen wide mobile ten wide hd ten wide computer media">
            <div>
                <div class="media-container<?= ($arItem->prop("RECOMMENDED")->hasValue() ? ' with-see-also' : '') ?>">
                    <div class="hide" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                        <span itemprop="author" itemscope itemtype="http://schema.org/Organization">
                            <meta itemprop="telephone" content="8 (800) 77-55-323">
                            <meta itemprop="name" content='ООО "Торговый Дом "Камея"'>
                            <meta itemprop="email" content='office@kamey.ru'>
                            <a itemprop="url"
                               href='<?= "http" . (!empty($_SERVER['HTTPS']) ? 's' : '') . "://" ?>www.kamey.ru'></a>
                            <img itemprop="logo"
                                 src="<?= "http" . (!empty($_SERVER['HTTPS']) ? 's' : '') . "://" . $_SERVER['SERVER_NAME'] ?>/include/images/logo.png"/>
                            <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <meta itemprop="streetAddress" content='ул. Кирова, д. 9'>
                                <meta itemprop="postalCode" content='450078'>
                                <meta itemprop="addressLocality" content='Уфа'>
                                <meta itemprop="addressCountry" content='Россия'>
                                <meta itemprop="addressRegion" content='Республика Башкортостан'>
                            </span>
                        </span>
                        <meta itemprop="name" content="<?= $strTitle ?>">
                        <img itemprop="contentUrl"
                             src="<?= "http" . (!empty($_SERVER['HTTPS']) ? 's' : '') . "://" . $_SERVER['SERVER_NAME'] ?><?= $morePhoto[0]['SRC'] ?>">
                    </div>

                    <div class="ui grid two column images">
                        <?
                        foreach ($morePhoto as $item) {
                            ?>
                            <div class="column">
                                <a href="<?= $item['SRC'] ?>" class="fit-container">
                                    <img src="<?= $item['RESIZE'] ?>" alt="<?= $strTitle ?>">
                                </a>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="column sixteen wide tablet six wide hd six wide computer sixteen wide mobile info">
            <div class="ui sticky" data-break-mobile="true" data-break-tablet="true"
                 data-observeChanges="true"
                 data-context=".media-container" data-break-height=".media-container">
                <?
                $skuProperty = $arResult['SKU_PROPS']['COLOR'];
                $list = array();
                global $version;
                foreach ($arItem->offers as $offer) {
                    if ($list[$offer->TREE["PROP_" . $skuProperty['ID']]]) {
                        continue;
                    }
                    $cache = Bitrix\Main\Data\Cache::createInstance();
                    if ($cache->initCache(86400, $offer->prop("COLOR")->value(), "/colors-with-data/v".$version)) {
                        $result = $cache->getVars();
                    } elseif ($cache->startDataCache()) {
                        $rs = \CIBlockElement::GetList(array(), array("ID" => $offer->prop("COLOR")->value()), false,
                          false, array("PROPERTY_ICON".($version == 3 ? '' : '_SQUARE')))->Fetch();
                        if (!$rs['PROPERTY_ICON'.($version == 3 ? '' : '_SQUARE').'_VALUE']) {
                            $result = [
                              'icon' => CFile::ResizeImageGet($offer->MORE_PHOTO[0]['ID'], array(
                                "width" => 534,
                                "height" => 710
                              ))['src'],
                              'type' => 'photo'
                            ];
                            $cache->abortDataCache();
                        } else {
                            $result = [
                              'icon' => \CFile::GetPath($rs['PROPERTY_ICON'.($version == 3 ? '' : '_SQUARE').'_VALUE']),
                              'type' => 'property'
                            ];
                            $cache->endDataCache($result);
                        }
                    }
                    $list[$offer->TREE["PROP_" . $skuProperty['ID']]] = $result;
                }
                ?>
                <div class="content">
                    <h1 class="ui header h1" itemprop="name">
                        <?= $strTitle ?>
                        <?
                        if (!$arParams['CLEAR']) {
                            ?><div class="favorite-trigger inverted" data-target="<?= $arItem->id ?>">
                            <?
                        icon(array("like-bold"))
                            ?>
                            </div><?
                        }
                        ?>
                    </h1>
                    <div class="actions-block">
                        <div class="content" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <? if ($arOffer->price->hasDiscount) {
                                ?>
                                <div class="old-price">
                                    <?= $arOffer->price->printRealValue() ?>
                                </div>
                                <?
                            } else {
                                ?>
                                <div class="old-price transition hidden"></div>
                                <?
                            } ?>
                            <div class="price"><?= $arParams['CLEAR'] ? 'Цена по запросу' : $arOffer->price ?></div>
                            <?
                            if (!$arParams['CLEAR']) {
                              ?><div class="send">
                                    Отправим
                                    <span class="value send-value"></span>
                                </div><?
                            }
                            ?>
                            <meta itemprop="price" content="<?= $arOffer->minPrice['VALUE'] ?>">
                            <meta itemprop="priceCurrency" content="<?= $arOffer->minPrice['CURRENCY'] ?>">
                            <meta itemprop="availability"
                                  content="<?= ($canBuy ? 'http://schema.org/InStock' : 'http://schema.org/SoldOut') ?>">
                            <meta itemprop="serialNumber" content="<?= $arItem->PROPERTIES['CML2_ARTICLE']['VALUE'] ?>">
                        </div>
                    </div>
                </div>
                <form action="" class="parameters mobile-content" id="catalog-filter">
                    <input type="hidden" name="id" value="<?= $arOffer->id ?>">
                    <?
                    foreach ($arResult['SKU_PROPS'] as &$skuProperty) {
                        if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']])) {
                            continue;
                        }

                        $propertyId = $skuProperty['ID'];
                        $skuProps[] = array(
                          'ID' => $propertyId,
                          "CODE" => $skuProperty['CODE'],
                          'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                          'VALUES' => $skuProperty['VALUES'],
                          'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                        );
                        ?>
                        <div class="param<?= ($skuProperty['CODE'] === 'COLOR' ? ' color-param' : '') ?>"
                             data-id="<?= $skuProperty['ID'] ?>"
                             data-code="<?= $skuProperty['CODE'] ?>">
                            <div class="title">
                                <?= $skuProperty["NAME"] ?>
                            </div>
                            <input type="hidden" name="prop[<?= $skuProperty['CODE'] ?>][CODE]"
                                   value="<?= $skuProperty['CODE'] ?>">
                            <input type="hidden" name="prop[<?= $skuProperty['CODE'] ?>][NAME]"
                                   value="<?= $skuProperty['NAME'] ?>">
                            <?
                            if ($skuProperty['SHOW_MODE'] == 'TEXT') {
                                $sortData = array();
                                foreach ($skuProperty['VALUES'] as $key => $value) {
                                    $sortData[$key] = $value['NAME'];
                                }

                                array_multisort($sortData, SORT_ASC, $skuProperty['VALUES']);
                                ?>
                                <div class="bold-checkboxes">
                                    <?
                                    foreach ($skuProperty['VALUES'] as &$value) {
                                        if ($skuProperty['CODE'] == 'RAZMER_ODEZHDA' && !$value['NA']) {
                                            $value['NAME'] = $value['NAME'] / 2;
                                        }
                                        ?>
                                        <div class="ui checkbox <?= $value['NA'] ? ' hide' : '' ?>">
                                            <input type="radio" name="prop[<?= $skuProperty['CODE'] ?>][VALUE]"
                                                   value="<?= $value['ID'] ?>"
                                                   id="prop-<?= $skuProperty['ID'] ?>-value-<?= $value['ID'] ?>" <?= ($value['ID'] == $arOffer->TREE["PROP_" . $skuProperty['ID']] ? "checked" : "") ?>>
                                            <label for="prop-<?= $skuProperty['ID'] ?>-value-<?= $value['ID'] ?>"
                                                   class="thumb"><?= $value['NAME'] ?></label>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>
                                <?
                            } else {
                                if ($skuProperty['CODE'] == 'COLOR') {
                                    ?>
                                    <div class="color-checkbox">
                                        <?
                                        foreach ($skuProperty['VALUES'] as $value) {
                                            ?>
                                            <div class="ui checkbox<?= $value['NA'] ? ' hide' : '' ?>">
                                                <input type="radio" name="prop[<?= $skuProperty['CODE'] ?>][VALUE]"
                                                       value="<?= $value['ID'] ?>"
                                                       title="<?= \Product\Basket::prepareColorName($value['NAME']) ?>"
                                                       id="prop-<?= $skuProperty['ID'] ?>-value-<?= $value['ID'] ?>" <?= ($value['ID'] == $arOffer->TREE["PROP_" . $skuProperty['ID']] ? "checked" : "") ?>>
                                                <label class="thumb fitted"
                                                       for="prop-<?= $skuProperty['ID'] ?>-value-<?= $value['ID'] ?>"
                                                       title="<?= \Product\Basket::prepareColorName($value['NAME']) ?>">
                                                    <div class="color<?= ($list[$value['ID']]['type'] == 'property' ? ' color-property' : '') ?>"
                                                         style="background-image: url(<?= $list[$value['ID']]['icon'] ?>)">

                                                    </div>
                                                </label>
                                            </div>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <?
                                }
                            }
                            ?>
                        </div>
                    <? } ?>
                </form>
                <?
                if (!$isAjaxShow) {
                    ?>
                    <div class="size-instructions">
                        <a href="#!" class="shape-trigger" data-target="size-side">
                            <? icon("size-instruction") ?>
                            Руководство <br>по размерам
                        </a>
                        <a href="#!" data-modal="find">
                            <? icon("size-find") ?>
                            Найди <br>свой размер
                        </a>
                    </div>
                    <?
                }
                ?>
                <?
                if ($arParams['CLEAR']) {
                    ?>
                    <div class="ui primary button fluid shape-trigger" data-target="clear-request">Заказать</div>
                    <?
                }elseif ($isBasket) {
                    ?>
                    <div class="ui primary button fluid cart-update mobile hidden"
                         data-basket="<?= $arParams['BASKET_ID'] ?>">
                        Сохранить
                    </div>
                    <div class="ui primary button cart-edit mobile only" data-basket="<?= $arParams['BASKET_ID'] ?>">
                        Изменить
                    </div>
                    <?
                } else {
                    ?>
                    <a href="/personal-area/basket/"
                       class="ui primary fluid button cart-add<?= ($canBuy ? '' : ' disabled') ?>"
                       data-default="Положить в корзину">Положить
                        в корзину</a>
                    <?
                }
                ?>
                <div class="delivery-info transition hidden">
                    <div class="delivery-city">Условия доставки в <span class="value shape-trigger" data-target="city-side"></span></div>
                    <div class="delivery-text"></div>
                </div>
                <div class="mobile-content" id="catalog-info">

                    <div class="ui accordion">
                        <div class="title">Параметры<? icon("dropdown") ?></div>
                        <div class="content properties">
                            <?
                            $usedProperties = array_filter($arResult['DISPLAY_PROPERTIES'], function ($property) {
                                return !in_array($property['CODE'], array("SOSTAV_TKANI", "PLOTNOST", "MODEL"));
                            });
                            ?>
                            <? if (!empty($usedProperties)): ?>
                                <table class="ui very basic unstackable table" width="100%">
                                    <tbody>
                                    <tr itemprop="additionalProperty" itemscope
                                        itemtype="http://schema.org/PropertyValue">
                                        <td width="40%"><span
                                                    class="tabed-content__property-name"
                                                    itemprop="name">Артикул: </span></td>
                                        <td itemprop="value">
                                            <?= $arItem->PROPERTIES['CML2_ARTICLE']['VALUE'] ?>
                                        </td>
                                    </tr>
                                    <? foreach ($usedProperties as $prop): ?>
                                        <tr itemprop="additionalProperty" itemscope
                                            itemtype="http://schema.org/PropertyValue">
                                            <td width="40%"><span
                                                        class="tabed-content__property-name"
                                                        itemprop="name"><?= $prop['NAME']; ?>: </span></td>
                                            <td itemprop="value"><?= is_array($prop['DISPLAY_VALUE']) ? implode(' / ',
                                                  $prop['DISPLAY_VALUE']) : $prop['DISPLAY_VALUE']; ?></td>
                                        </tr>
                                        <? if ($prop['NAME'] == "Пол") {
                                            $strSnoska = ($prop['DISPLAY_VALUE'] == "Мужской") ? " Числовые параметры приведены на размер/рост - 100/182" : " Числовые параметры приведены на размер/рост - 92/170";
                                        } ?>
                                    <? endforeach; ?>
                                    </tbody>
                                </table>
                                <table class="ui very basic unstackable table offer-properties" width="100%">
                                    <tbody></tbody>
                                </table>
                                <small><span class="text-coloured">*</span> <?= $strSnoska ?> </small>
                            <? else: ?>
                                <table class="ui very basic unstackable table offer-properties" width="100%">
                                    <tbody></tbody>
                                </table>
                            <? endif; ?>
                        </div>
                        <div class="title">Состав и уход<? icon("dropdown") ?></div>
                        <div class="content">
                            <p>
                                <b>
                                    <?= $arOffer->prop("SOSTAV_TKANI")->value() ?>
                                </b>
                            </p>

                            <?
                            $density = null;
                            switch ($arItem->prop("PLOTNOST")->value()) {
                                case "Низкая":
                                    $density = 1;
                                    break;
                                case "Средняя":
                                    $density = 2;
                                    break;
                                case "Высокая":
                                    $density = 3;
                                    break;
                            }
                            if ($arItem->prop("PLOTNOST")->hasValue()) {
                                ?>
                                <p>
                                    <?= $arItem->prop("PLOTNOST")->value() ?> плотность ткани &nbsp;<span
                                            class="circles">
                                <?
                                while ($density > 0) {
                                    ?>
                                    <span></span>
                                    <?
                                    $density--;
                                }
                                ?>
                                  </span>
                                </p>
                                <?
                            }
                            $careId = $arItem->prop("CARE")->value();
                            $cache = Bitrix\Main\Data\Cache::createInstance();
                            $care = array();
                            if ($cache->initCache(86400, $careId, "/care/")) {
                                $care = $cache->getVars();
                            } elseif ($cache->startDataCache()) {
                                $care = array();
                                $rsCare = CIBlockElement::GetList(array(), array("ID" => $careId, 'IBLOCK_ID' => 24),
                                  false, false, array("ID", "PROPERTY_CARE_SYMBOLS", "PREVIEW_TEXT"))->Fetch();

                                if ($rsCare) {
                                    $care['description'] = $rsCare['PREVIEW_TEXT'];
                                    $care['symbols'] = array();
                                    $highLoadData = getHighloadEntity(26);
                                    $entityDataClass = $highLoadData['class'];
                                    $sTableID = $highLoadData['tableId'];
                                    $rsData = $entityDataClass::getList(array(
                                      "select" => array('UF_DESCRIPTION', 'UF_FILE'),
                                      "filter" => array(
                                        'UF_XML_ID' => $rsCare['PROPERTY_CARE_SYMBOLS_VALUE']
                                      ),
                                        // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                                      "order" => array("UF_SORT" => "ASC")
                                    ));

                                    $rsData = new CDBResult($rsData, $sTableID);
                                    while ($ob = $rsData->Fetch()) {
                                        if (!$ob['UF_FILE']) {
                                            continue;
                                        }
                                        $care['symbols'][] = array(
                                          "description" => $ob['UF_DESCRIPTION'],
                                          "image" => CFile::GetPath($ob['UF_FILE'])
                                        );
                                    }
                                    $cache->endDataCache($care);
                                } else {
                                    $cache->abortDataCache();
                                }
                            }
                            if ($care['description']) {
                                ?>
                                <p class="gray-color"><?= $care['description'] ?></p>
                                <?
                            }
                            if (!empty($care['symbols'])) {
                                ?>
                                <div class="maintenance">
                                    <?
                                    foreach ($care['symbols'] as $symbol) {
                                        ?>
                                        <div class="value">
                                            <img src="<?= $symbol['image'] ?>" alt="<?= $symbol['description'] ?>">
                                            <span><?= $symbol['description'] ?></span>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                        <div class="title">Информация о товаре<? icon("dropdown") ?></div>
                        <div class="content description">
                            <? if ($arResult['DETAIL_TEXT']): ?>
                                <span itemprop="description"><?= $arResult['DETAIL_TEXT']; ?></span>
                            <? else: ?>
                                Описание находится на стадии заполнения
                                <meta itemprop="description" content="<?= $arResult['PREVIEW_TEXT']; ?>">
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?

    if ($arItem->prop("RECOMMENDED")->value()) {
        ?>
        <div class="see-also mobile-content" id="see-also">
            <div class="ui header normal uppercase">С этим покупают</div>
            <div class="product-slider dark-controls ui grid catalog-list four column doubling stackable" data-count="4">
                <?
                $GLOBALS['recommendedFilter'] = array(
                  "=ID" => $arItem->prop("RECOMMENDED")->value()
                );
                $arParams['HIDE_NOT_AVAILABLE'] = 'Y';
                $APPLICATION->IncludeComponent(
                  'bitrix:catalog.section',
                  '',
                  array(
                    "ADD_CHUNK" => false,
                    "CLASS" => "column",
                    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    'SECTION_ID' => 0,
                    'SECTION_CODE' => '',
                    'ELEMENT_SORT_FIELD' => 'shows',
                    'ELEMENT_SORT_ORDER' => 'desc',
                    'ELEMENT_SORT_FIELD2' => 'sort',
                    'ELEMENT_SORT_ORDER2' => 'asc',
                    'PROPERTY_CODE' => $arParams['LIST_PROPERTY_CODE'],
                    'PROPERTY_CODE_MOBILE' => $arParams['LIST_PROPERTY_CODE_MOBILE'],
                    'INCLUDE_SUBSECTIONS' => $arParams['INCLUDE_SUBSECTIONS'],
                    'BASKET_URL' => $arParams['BASKET_URL'],
                    'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                    'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                    'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
                    'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                    'CACHE_TIME' => $arParams['CACHE_TIME'],
                    'CACHE_FILTER' => $arParams['CACHE_FILTER'],
                    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                    'DISPLAY_COMPARE' => $arParams['USE_COMPARE'],
                    'PRICE_CODE' => $arParams['PRICE_CODE'],
                    'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                    'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
                    'PAGE_ELEMENT_COUNT' => 6,

                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",

                    'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                    'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                    'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
                    'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
                    'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],

                    'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                    'OFFERS_FIELD_CODE' => $arParams['LIST_OFFERS_FIELD_CODE'],
                    'OFFERS_PROPERTY_CODE' => $arParams['LIST_OFFERS_PROPERTY_CODE'],
                    'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
                    'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
                    'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
                    'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],
                    'OFFERS_LIMIT' => $arParams['LIST_OFFERS_LIMIT'],

                    'SECTION_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['section'],
                    'DETAIL_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['element'],
                    'USE_MAIN_ELEMENT_SECTION' => $arParams['USE_MAIN_ELEMENT_SECTION'],
                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                    'HIDE_NOT_AVAILABLE_OFFERS' => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],

                    'LABEL_PROP' => $arParams['LABEL_PROP'],
                    'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                    'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                    'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                    'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                    'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                    'PRODUCT_ROW_VARIANTS' => "[{'VARIANT':'3','BIG_DATA':false}]",
                    'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                    'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                    'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                    'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                    'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                    'DISPLAY_TOP_PAGER' => 'N',
                    'DISPLAY_BOTTOM_PAGER' => 'N',
                    'HIDE_SECTION_DESCRIPTION' => 'Y',

                    'RCM_TYPE' => '',
                    'RCM_PROD_ID' => null,
                    'SHOW_FROM_SECTION' => 'N',

                    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                    'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                    'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                    'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                    'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                    'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                    'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                    'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                    'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                    'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                    'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                    'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

                    'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                    'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                    'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                    'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                    'ADD_TO_BASKET_ACTION' => $basketAction,
                    'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                    'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                    'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                    'BACKGROUND_IMAGE' => '',
                    'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                    "FILTER_NAME" => "recommendedFilter",
                    "SHOW_ALL_WO_SECTION" => "Y"
                  ),
                  $component
                );
                ?>
            </div>
        </div>
        <?
    }
    ?>
</div>
