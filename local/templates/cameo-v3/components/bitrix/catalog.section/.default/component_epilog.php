<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $templateData */

/** @var @global CMain $APPLICATION */

use Bitrix\Main\Loader;

global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME'])) {
  //$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY'])) {
  $loadCurrency = false;
  if (!empty($templateData['CURRENCIES']))
    $loadCurrency = Loader::includeModule('currency');
  CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
  if ($loadCurrency) {
    ?>
      <script type="text/javascript">
				BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
      </script>
    <?
  }
}
if ($arParams['ADD_CHUNK'] !== false) {
  $APPLICATION->AddChainItem("Каталог", "");
}

if ($arParams['NEXT_PAGE'] !== false) {
  $paramName = 'PAGEN_' . $arResult['NAV_CACHED_DATA']->NavNum;
  $paramValue = $arResult['NAV_CACHED_DATA']->NavPageNomer;
  $pageCount = $arResult['NAV_CACHED_DATA']->NavPageCount;

  if ($paramValue < $pageCount) {
    $paramValue = (int)$paramValue + 1;
    $url = $APPLICATION->GetCurPageParam($paramName . '=' . $paramValue, array($paramName, "requestFilter", "ajax_call", "ajax",'locationId','ncc'));
    ?>
      <a href="<?= $url ?>" class="black underline uppercase icon center aligned get-more" data-page="<?= $paramValue ?>"
         data-total="<?= $pageCount ?>">
        <span class="icon-container"><? icon("eye") ?></span>
          Посмотреть еще
      </a>
    <?
  }
  $count = $arResult['NAV_CACHED_DATA']->NavRecordCount;

  $APPLICATION->AddViewContent("catalog-count", pluralize($count, $arParams['PLURALIZE'] ? $arParams['PLURALIZE'] : $arResult['NAME'], true));
}

?>