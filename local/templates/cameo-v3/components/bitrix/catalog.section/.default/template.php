<? use Product\Card;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arParams['NEXT_PAGE'] !== false) {
  $arResult['NAV_CACHED_DATA'] = $arResult['NAV_RESULT'];
}
global $version;
Card::setParams($arParams);
foreach ($arResult['ITEMS'] as $arItem) {
    if (!$version || $version == 3) {
        Card::v3($arItem, $this, $arParams['CLEAR']);
    } elseif ($version == 4) {
        Card::v4($arItem, $this, $arParams['CLEAR']);
    }
}
Card::clearParams();
?>
