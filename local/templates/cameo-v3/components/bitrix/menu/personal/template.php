<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
if (!empty($arResult)) {
    ?>
    <div class="radio-group">
        <?
        foreach ($arResult as $arItem) {
            if ($arItem['DEPTH_LEVEL'] > $arParams['MAX_LEVEL']) {
                continue;
            }
            ?>
            <a href="<?= $arItem['LINK'] ?>"
               class="item tab-item<?= $arItem['SELECTED'] ? ' active' : '' ?>"><?= $arItem['TEXT'] ?></a>
            <?
        }
        ?>
    </div>
<? } ?>
