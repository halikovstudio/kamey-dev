<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

//Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
//Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");
//CJSCore::Init(array('clipboard', 'fx'));
$customArray = $arResult["CUSTOM_ARRAY"];
Loc::loadMessages(__FILE__);

$this->addExternalJs('https://pay.google.com/gp/p/js/pay.js');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/google_pay.js');
$this->addExternalJs(SITE_TEMPLATE_PATH . '/js/apple_pay.js');

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }
} else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    if (!count($arResult['ORDERS'])) {
        ?>
        <div class="ui grid">
            <div class="column sixteen wide center aligned">
                <?
                if ($_REQUEST["filter_history"] == 'Y') {
                    if ($_REQUEST["show_canceled"] == 'Y') {
                        ?>
                        <h3 class="ui normal header"><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
                        <?
                    } else {
                        ?>
                        <h3 class="ui normal header"><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
                        <?
                    }
                } else {
                    ?>
                    <h3 class="ui normal header"><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
                    <?
                }
                ?>
            </div>
        </div>
        <?
    } else {
        $statuses = array();
        $paySystems = array();
        $deliverySystems = array();
        foreach ($arResult["INFO"]["STATUS"] as $status) {
            $statuses[$status["ID"]] = $status["NAME"];
        }
        foreach ($arResult["INFO"]["PAY_SYSTEM"] as $paySystem) {
            $paySystems[$paySystem["ID"]] = $paySystem["NAME"];
        }
        foreach ($arResult["INFO"]["DELIVERY"] as $delivery) {
            $deliverySystems[$delivery["ID"]] = $delivery["DESCRIPTION"] ?: $delivery["NAME"];
        }

        $bonusData = [
          'accrued' => [],
          'withdraw' => []
        ];
        if (BonusCard::isset()) {
            $bonusHistory = BonusCard::history_list();
            foreach ($bonusHistory as $item) {
                if ($item['OrderID']) {
                    if ($item['Charge']) {
                        $bonusData['accrued'][$item['OrderID']] += $item['Charge'];
                    } else {
                        $bonusData['withdraw'][$item['OrderID']] += $item['WriteOff'];
                    }
                }
            }
        }
        ?>
        <form class="ui popup form js-cancel-order-popup">
            <div class="field hide">
                <input type="hidden" name="orderid" value="">
            </div>
            <div class="field">
                <label for="cancelOrderForm_fieldReason" class="grey-color">Вы действительно хотите удалить
                    заказ?</label>
                <input id="cancelOrderForm_fieldReason" type="text" name="reason"
                       placeholder="Напишите нам, почему Вы решили отменить заказ">
            </div>
            <div class="ui success message"></div>
            <div class="ui error message"></div>
            <button class="ui fluid primary custom button" type="submit">Отменить</button>
        </form>

        <div class="personal__orders">
            <div class="ui accordion">
                <div class="personal-accord__heading">
                    <div class="personal-accord__number">
                        №
                    </div>
                    <div class="personal-accord__date">
                        Дата
                    </div>
                    <div class="personal-accord__status mobile hidden">
                        Статус
                    </div>
                    <div class="personal-accord__post-status mobile hidden">
                        Статус почтовой службы
                    </div>
                    <div class="personal-accord__track mobile hidden">
                        Трек номер
                    </div>
                    <div class="personal-accord__sum">
                        Сумма
                    </div>
                    <div class="personal-accord__show">

                    </div>
                </div>
                <div>
                    <?
                    foreach ($arResult["ORDERS"] as $order) {
                        $arOrder = $order["ORDER"];
                        $arBasket = $order["BASKET_ITEMS"];
                        $lastBasketIndex = count($arBasket) - 1;
                        $currentIndex = 0;
                        $copyOrderErrorCondition = $_REQUEST["ID"] == $arOrder["ACCOUNT_NUMBER"] && !empty($customArray) && $_REQUEST["COPY_ORDER"] === "Y";
                        $cache = Bitrix\Main\Data\Cache::createInstance();
                        if ($cache->initCache(86400, "props" . $arOrder['ID'], "/orders/props/")) {
                            $props = $cache->getVars();
                        } elseif ($cache->startDataCache()) {
                            $rsOrder = CSaleOrderPropsValue::GetOrderProps($arOrder["ID"]);
                            $props = array();
                            while ($ob = $rsOrder->Fetch()) {
                                $props[$ob['CODE'] ?: $ob['ID']] = $ob;
                            }
                            $cache->endDataCache($props);
                        }

                        $cache = Bitrix\Main\Data\Cache::createInstance();
                        if ($cache->initCache(86400, "delivery" . $arOrder['ID'], "/orders/delivery/")) {
                            $delivery = $cache->getVars();
                        } elseif ($cache->startDataCache()) {
                            $delivery = array(
                              'pvz' => false,
                              'type' => 'cdek'
                            );
                            /** @var Bitrix\Sale\Shipment[] $shipmentList */
                            $shipmentList = \Bitrix\Sale\Shipment::loadForOrder($arOrder['ID']);
                            /** @var Bitrix\Sale\Shipment $shipment */
                            $shipment = null;
                            foreach ($shipmentList as $item) {
                                if (!$item->isSystem()) {
                                    $shipment = $item;
                                    break;
                                }
                            }
                            if ($shipment->getDelivery() instanceof \safin\delivery\cdek) {
                                $delivery['type'] = 'cdek';
                            } else {
                                $delivery['type'] = 'post';
                            }
                            $delivery['shipmentId'] = $shipment->getId();
                            $extra = $shipment->getDelivery()->getExtraServices();
                            /** @var CdekPvz|null $pvz */
                            $pvz = $extra->getItemByCode("pvz");
                            if ($pvz) {
                                $GLOBALS['DELIVERY_LOCATION_ZIP'] = $props['ZIP']['VALUE'];
                                $GLOBALS['DELIVERY_LOCATION_ID'] = $props['LOCATION']['VALUE'];
                                $extraServices = $shipment->getExtraServices();
                                $pvz->setValue($extraServices[$pvz->getId()]);
                                $pvz->createOptions();
                                $delivery['pvz'] = true;
                                $delivery['address'] = $pvz->getFormattedValue(true);
                                $delivery['data'] = $pvz->getObjectValue();
                            } else {
                                $address = $props['ADDRESS']['VALUE'];
                                if (!$address) {
                                    $address = array("ул. " . $props['street']['VALUE']);
                                    if ($props['house']['VALUE']) {
                                        $address[] = "д. " . $props['house']['VALUE'];
                                    }
                                    if ($props['corps']['VALUE']) {
                                        $address[] = "к. " . $props['corps']['VALUE'];
                                    }
                                    if ($props['flat']['VALUE']) {
                                        $address[] = "кв. " . $props['flat']['VALUE'];
                                    }
                                    $address = implode(", ", $address);
                                }
                                $delivery['address'] = $address;
                            }
                            $cache->endDataCache($delivery);
                        }
                        $sum = 0;
                        foreach ($order['BASKET_ITEMS'] as $item) {
                            $sum += $item['PRICE'] * $item['QUANTITY'];
                        }
                        foreach ($order['PAYMENT'] as $item) {
                            if ($item['PAY_SYSTEM_ID'] == BonusCard::PAYMENT_ID && $item['PAID'] == 'Y') {
                                $sum -= $item['SUM'];
                            }
                        }
                        $link = '#!';
                        switch ($delivery['type']) {
                            case 'post':
                                $link = 'https://www.pochta.ru/tracking#' . $arOrder['TRACKING_NUMBER'];
                                break;
                            case 'cdek':
                                $link = 'https://cdek.ru/tracking';
                                break;
                        }
                        $manager = \Bitrix\Sale\Delivery\Tracking\Manager::getInstance();
                        $deliveryStatus = '';
                        if ($arOrder['TRACKING_NUMBER'] && $delivery['shipmentId']) {
                            $result = $manager->getStatusByShipmentId($delivery['shipmentId'],
                              $arOrder['TRACKING_NUMBER']);
                            $deliveryStatus = \Bitrix\Sale\Delivery\Tracking\Manager::getStatusName($result->status);
                        }
                        ?>
                        <div class="personal-accord__title title<?= $copyOrderErrorCondition ? ' active' : '' ?>">
                            <div class="personal-accord__number">
                                <?= $arOrder["ACCOUNT_NUMBER"] ?>
                            </div>
                            <div class="personal-accord__date">
                                <?= $arOrder["DATE_INSERT_FORMATED"] ?>
                            </div>
                            <div class="personal-accord__status mobile hidden">
                                <span class="personal__mute"><?= $statuses[$arOrder["STATUS_ID"]] ?></span>
                            </div>
                            <div class="personal-accord__post-status mobile hidden">
                                <span class="personal__mute"><?= $deliveryStatus ?></span>
                            </div>
                            <div class="personal-accord__track mobile hidden">
                                <a href="<?= $link ?>" target="_blank"><?= $arOrder['TRACKING_NUMBER'] ?></a>
                            </div>
                            <div class="personal-accord__sum">
                                <span class="personal__price"><?= $arOrder["FORMATED_PRICE"] ?></span>
                            </div>
                            <div class="personal-accord__show">
                                <?= icon('show') ?>
                            </div>
                        </div>
                        <div class="personal-accord__content content<?= $copyOrderErrorCondition ? ' active' : '' ?>">
                            <div class="mobile only">
                                <div class="mb16"></div>
                                <div class="mb16">
                                    <div class="uppercase text--medium mb8">Статус</div>
                                    <div class="personal__mute personal__text"><?= $statuses[$arOrder["STATUS_ID"]] ?></div>
                                </div>
                                <div class="mb16">
                                    <div class="uppercase text--medium mb8">Статус почтовой службы</div>
                                    <div class="personal__mute personal__text"><?= $deliveryStatus ?></div>
                                </div>
                                <div class="mb16">
                                    <div class="uppercase text--medium mb8">Трек номер</div>
                                    <div class="personal__link personal__text">
                                        <a href="<?= $link ?>" target="_blank"><?= $arOrder['TRACKING_NUMBER'] ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="cart personal__cart">
                                <table class="ui very basic table">
                                    <thead>
                                    <tr>
                                        <th>Фото</th>
                                        <th>Название</th>
                                        <th>Цвет</th>
                                        <th>Размер</th>
                                        <th>Рост</th>
                                        <th>Количество</th>
                                        <th>Цена</th>
                                    </tr>
                                    </thead>
                                    <tbody class="available">
                                    <?
                                    foreach ($arBasket as $basketItem) {
                                        $name = getModelNameByProductName($basketItem["NAME"]);
                                        $itemData = \Product\Basket::getData($basketItem["PRODUCT_ID"]);
                                        \Product\Basket::$item = $itemData ?: array();
                                        $colorCircles = \Product\Basket::getColorCircles();
                                        $colorString = \Product\Basket::getColorString();
                                        ?>
                                        <tr class="item">
                                            <td class="column-image">
                                                <a href="<?= $basketItem["DETAIL_PAGE_URL"] ?>"
                                                   class="fit-container">
                                                    <img src="<?= $itemData["image"] ?>"
                                                         alt="<?= $name ?>">
                                                </a>
                                            </td>
                                            <td class="column-name">
                                                <a href="<?= $basketItem["DETAIL_PAGE_URL"] ?>"
                                                   class="uppercase"><?= $name ?>
                                                </a>
                                                <svg class="icon dropdown">
                                                    <use xlink:href="/local/templates/cameo-v3/images/icons.svg#dropdown-icon"
                                                         class="use-dropdown"></use>
                                                </svg>
                                            </td>
                                            <td class="color-param param column-color">
                                                <div class="color-param-wrapper">
                                                    <?= $colorCircles ?>
                                                    <?= $colorString ?>
                                                </div>
                                            </td>
                                            <td class="size-param param column-size"><?= $itemData["props"]["size"]["value"] ?>
                                                <span class="mobile only height-value"> - <?= $itemData["props"]["height"]["value"] ?></span>
                                            </td>
                                            <td class="height-param param column-height"><?= $itemData["props"]["height"]["value"] ?></td>
                                            <td class="column-stepper">
                                                <?= $basketItem["QUANTITY"] ?>
                                            </td>
                                            <td class="column-price">
                          <span class="personal__price popup-trigger installed" data-position="bottom left"
                                data-content="Цена за 1 шт. <?= CurrencyFormat($basketItem["PRICE"],
                                  $basketItem["CURRENCY"]) ?>"><?= CurrencyFormat($basketItem["PRICE"] * $basketItem["QUANTITY"],
                                $basketItem["CURRENCY"]) ?><br></span>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="personal__order-info">
                                <div class="ui grid">
                                    <div class="five wide computer six wide tablet twelve wide mobile column">
                                        <div class="uppercase text--medium mbm8"><?= ($delivery['pvz'] ? 'Пункт выдачи' : 'Адрес доставки') ?></div>
                                        <br class="mobile hidden">
                                        <div class="personal__mute personal__text">
                                            Адрес: <?= $delivery['address'] ?>
                                            <br>
                                            Получатель: <?= $props["FIO"]["VALUE"] ? $props["FIO"]["VALUE"] : $props["COMPANY"]["VALUE"] ?>
                                            <?
                                            if ($props["PHONE"]["VALUE"]): ?>
                                                <br>
                                                Телефон: <?= $props["PHONE"]["VALUE"] ?>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                    <div class="five wide computer five wide tablet twelve wide mobile column">
                                        <div class="uppercase text--medium mbm8">Способ доставки</div>
                                        <br class="mobile hidden">
                                        <div class="personal__mute personal__text">
                                            <?
                                            $list = [];
                                            foreach ($order['SHIPMENT'] as $item) {
                                                $list[] = $item['DELIVERY_NAME'];
                                            }
                                            echo implode("<br>", $list);
                                            ?>
                                        </div>
                                    </div>
                                    <div
                                            class="three wide widescreen four wide large screen five wide tablet twelve wide mobile screen column">
                                        <div class="uppercase text--medium mbm8">Способ оплаты</div>
                                        <br class="mobile hidden">
                                        <div class="personal__mute personal__text">
                                            <?
                                            if (count($order['PAYMENT']) > 1) {
                                                $list = [];
                                                foreach ($order['PAYMENT'] as $item) {
                                                    $list[] = $item['PAY_SYSTEM_NAME'] . ($item['PAID'] == 'Y' ? ' (' . $item['FORMATED_SUM'] . ')' : '');
                                                }
                                                echo implode("<br>", $list);
                                            } else {
                                                echo $order['PAYMENT'][0]['PAY_SYSTEM_NAME'];
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="personal__order-buttons">
                                <div class="personal__order-buttons-left">
                                    <?
                                    if ($arOrder["CAN_CANCEL"] === "Y" && $arOrder["STATUS_ID"] !== "S" && $arOrder["STATUS_ID"] !== "F"): ?>
                                        <button class="ui mini basic button custom js-cancel-order-trigger"
                                                data-order-id="<?= $arOrder["ID"] ?>">Отменить
                                        </button>
                                    <? endif; ?>
                                    <?
                                    /** @var Bitrix\Main\Type\DateTime $dateStatus */
                                    $dateStatus = $arOrder['DATE_STATUS'];
                                    $canReturn = $arOrder['STATUS_ID'] == 'F' && (time() - $dateStatus->getTimestamp()) <= 60 * 60 * 24 * 30;
                                    if ($canReturn) {
                                        ?>
                                        <a class="ui mini basic button custom"
                                           data-modal="getback-<?= $arOrder['ID'] ?>">Возврат</a>
                                    <? } ?>
                                </div>
                                <div class="personal__order-buttons-right">
                                    <?
                                    foreach ($order['PAYMENT'] as $item) {
                                        if ($item['PAID'] == 'Y' || $item['IS_CASH'] == 'Y' || $item['PAY_SYSTEM_ID'] == BonusCard::PAYMENT_ID) {
                                            continue;
                                        }
                                        if ($item['PAY_SYSTEM_ID'] == 13) {
                                            ?>
                                            <div style="-webkit-appearance: -apple-pay-button;
       -apple-pay-button-type: check-out;
       -apple-pay-button-style: black;"
                                                 lang=ru class="apple-pay-button hide" id="apple-pay-button-<?=$arOrder['ID']?>">

                                            </div>
                                            <script>new ApplePay(<?=$arOrder['ID']?>, <?=$item['SUM']?>, false, '#apple-pay-button-<?=$arOrder['ID']?>');</script>
                                        <?
                                        } elseif ($item['PAY_SYSTEM_ID'] == 14) {
                                        ?>
                                            <div class="google-pay-button-container"
                                                 id="google-pay-button-container-<?= $arOrder['ID'] ?>">

                                            </div>
                                            <script>new GooglePay(<?=$arOrder['ID']?>, <?=$item['SUM']?>, false,
                                                  '#google-pay-button-container-<?=$arOrder['ID']?>');</script>
                                        <?
                                        } else {
                                        ?>
                                            <a href="<?= $item['PSA_ACTION_FILE'] ?>"
                                               class="ui mini primary button custom popup-load"
                                               target="_blank" title="<?= $item['PAY_SYSTEM_NAME'] ?>">Оплатить</a>
                                            <?
                                        }
                                    }
                                    ?>
                                    <?
                                    if ($copyOrderErrorCondition): ?>

                                    <? else: ?>
                                        <a href="?COPY_ORDER=Y&ID=<?= $arOrder['ACCOUNT_NUMBER'] ?>"
                                           class="ui mini primary button custom" target="_blank">Повторить</a>
                                    <? endif; ?>
                                </div>
                            </div>
                            <?
                            if ($copyOrderErrorCondition): ?>
                                <?
                                if (count($customArray) === count($arBasket)): ?>
                                    <p class="ui error message js-scroll-to-me">
                                        К сожалению, на данный момент этот заказ повторить невозможно,
                                        так как ни одного товара нет в наличии.
                                    </p>
                                <? else: ?>
                                    <div class="ui error message js-scroll-to-me">
                                        К сожалению, при копировании заказа возникла ошибка, так как
                                        некоторых товаров на данный момент нет в наличии:
                                        <ul>
                                            <?
                                            foreach ($customArray as $productName): ?>
                                                <li><?= $productName ?></li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                    <p class="gray-color">
                                        Вы можете продолжить оформление заказа без них:
                                    </p>
                                    <a href="?COPY_ORDER=Y&ID=<?= $arOrder['ACCOUNT_NUMBER'] ?>&IGNORE_ERRORS=Y"
                                       class="ui mini primary button custom">В корзину</a>
                                <? endif; ?>
                            <? endif; ?>
                        </div>
                        <?
                        if ($canReturn) {
                            ?>
                            <div class="ui fullscreen modal getback__modal" id="getback-<?= $arOrder['ID'] ?>">
                                <div class="close">
                                    <?= icon('close') ?>
                                </div>
                                <div class="modal-content getback">
                                    <div class="ui container">
                                        <div class="getback__header">Возврат заказа <?= $arOrder['ACCOUNT_NUMBER'] ?>
                                            от <?= $arOrder["DATE_INSERT_FORMATED"] ?></div>
                                        <form class="ui form getback__form" action="/api/order.return" method="post">
                                            <input type="hidden" name="order_id" value="<?= $arOrder['ID'] ?>">
                                            <div class="cart personal__cart">
                                                <table class="ui very basic table">
                                                    <thead>
                                                    <tr>
                                                        <th>Фото</th>
                                                        <th>Название</th>
                                                        <th>Цвет</th>
                                                        <th>Размер</th>
                                                        <th>Рост</th>
                                                        <th>Количество</th>
                                                        <th>Цена</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="available">
                                                    <?
                                                    foreach ($arBasket as $basketItem) {
                                                        $name = getModelNameByProductName($basketItem["NAME"]);
                                                        $itemData = \Product\Basket::getData($basketItem["PRODUCT_ID"]);
                                                        \Product\Basket::$item = $itemData ?: array();
                                                        $colorCircles = \Product\Basket::getColorCircles();
                                                        $colorString = \Product\Basket::getColorString();
                                                        ?>
                                                        <tr class="item"
                                                            data-checkbox="product-return-<?= $basketItem['ID'] ?>"
                                                            data-id="<?= $basketItem['ID'] ?>">
                                                            <td class="column-image">

                                                                <a href="<?= $basketItem["DETAIL_PAGE_URL"] ?>"
                                                                   class="fit-container">
                                                                    <img src="<?= $itemData["image"] ?>"
                                                                         alt="<?= $name ?>">
                                                                    <?= icon('check') ?>
                                                                </a>
                                                            </td>
                                                            <td class="column-name">
                                                                <div class="field">
                                                                    <input type="checkbox"
                                                                           class="hide"
                                                                           name="product[<?= $basketItem['ID'] ?>][checked]"
                                                                           data-validate="product_check"
                                                                           value=""
                                                                           id="product-return-<?= $basketItem['ID'] ?>">
                                                                    <a href="<?= $basketItem["DETAIL_PAGE_URL"] ?>"
                                                                       class="uppercase"><?= $name ?>
                                                                    </a>
                                                                    <svg class="icon dropdown">
                                                                        <use xlink:href="/local/templates/cameo-v3/images/icons.svg#dropdown-icon"
                                                                             class="use-dropdown"></use>
                                                                    </svg>
                                                                </div>
                                                            </td>
                                                            <td class="color-param param column-color">
                                                                <div class="color-param-wrapper">
                                                                    <?= $colorCircles ?>
                                                                    <?= $colorString ?>
                                                                </div>
                                                            </td>
                                                            <td class="size-param param column-size"><?= $itemData["props"]["size"]["value"] ?>
                                                                <span class="mobile only height-value"> - <?= $itemData["props"]["height"]["value"] ?></span>
                                                            </td>
                                                            <td class="height-param param column-height"><?= $itemData["props"]["height"]["value"] ?></td>
                                                            <td class="column-stepper">
                                                                <?= $basketItem['QUANTITY'] ?>
                                                            </td>
                                                            <td class="column-price">
                            <span class="personal__price popup-trigger installed" data-position="bottom left"
                                  data-content="Цена за 1 шт. <?= CurrencyFormat($basketItem["PRICE"],
                                    $basketItem["CURRENCY"]) ?>"><?= CurrencyFormat($basketItem["PRICE"] * $basketItem["QUANTITY"],
                                  $basketItem["CURRENCY"]) ?><br></span>
                                                            </td>
                                                        </tr>
                                                        <tr class="getback__reason">
                                                            <td></td>
                                                            <td colspan="5">
                                                                <div class="uppercase text--medium text--big mb24">
                                                                    Основание
                                                                    возврата
                                                                </div>
                                                                <div class="ui two fields">
                                                                    <div class="field">
                                                                        <div class="grouped fields reason-types">
                                                                            <div class="field">
                                                                                <div class="ui checkbox custom">
                                                                                    <input type="checkbox"
                                                                                           name="product[<?= $basketItem['ID'] ?>][type]"
                                                                                           value="other" tabindex="0"
                                                                                           data-validate="product-<?= $basketItem['ID'] ?>-type-other"
                                                                                           class="hidden">
                                                                                    <label>Доставлен другой
                                                                                        товар</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="field">
                                                                                <div class="ui checkbox custom">
                                                                                    <input type="checkbox"
                                                                                           name="product[<?= $basketItem['ID'] ?>][type]"
                                                                                           data-validate="product-<?= $basketItem['ID'] ?>-type-damage"
                                                                                           value="damage" tabindex="0"
                                                                                           class="hidden">
                                                                                    <label>Товар поврежден</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="field">
                                                                                <div class="ui checkbox custom">
                                                                                    <input type="checkbox"
                                                                                           name="product[<?= $basketItem['ID'] ?>][type]"
                                                                                           data-validate="product-<?= $basketItem['ID'] ?>-type-small"
                                                                                           value="small" tabindex="0"
                                                                                           class="hidden">
                                                                                    <label>Не подходит по размеру
                                                                                        (мал)</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="field">
                                                                                <div class="ui checkbox custom">
                                                                                    <input type="checkbox"
                                                                                           name="product[<?= $basketItem['ID'] ?>][type]"
                                                                                           data-validate="product-<?= $basketItem['ID'] ?>-type-big"
                                                                                           value="big" tabindex="0"
                                                                                           class="hidden">
                                                                                    <label>Не подходит по размеру
                                                                                        (велик)</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="field">
                                                                                <div class="ui checkbox custom">
                                                                                    <input type="checkbox"
                                                                                           name="product[<?= $basketItem['ID'] ?>][type]"
                                                                                           data-validate="product-<?= $basketItem['ID'] ?>-type-defect"
                                                                                           value="defect" tabindex="0"
                                                                                           class="hidden getback-textarea-toggle">
                                                                                    <label>Брак</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                    </div>
                                                                    <div class="field custom getback-textarea transition hidden">
                                                                    <textarea
                                                                            name="product[<?= $basketItem['ID'] ?>][defect_description]"
                                                                            placeholder="Характер брака"></textarea>
                                                                    </div>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <?
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="getback__fields">
                                                <div class="ui grid centered">
                                                    <div class="five wide computer seven wide tablet sixteen wide mobile column">
                                                        <div class="uppercase text--medium text--big mb24">Личные данные
                                                        </div>
                                                        <div class="field custom">
                                                            <input type="text" value="" placeholder="Фамилия"
                                                                   name="surname">
                                                        </div>
                                                        <div class="field custom">
                                                            <input type="text" value="" placeholder="Имя" name="name">
                                                        </div>
                                                        <div class="field custom">
                                                            <input type="text" value="" placeholder="Отчество"
                                                                   name="middle_name">
                                                        </div>
                                                        <div class="mb24"></div>
                                                        <div class="uppercase text--medium mb8">Паспорт</div>
                                                        <div class="two fields custom">
                                                            <div class="field custom">
                                                                <input type="text" value="" placeholder="Серия"
                                                                       name="passport_series">
                                                            </div>
                                                            <div class="field custom">
                                                                <input type="text" value="" placeholder="Номер"
                                                                       name="passport_number">
                                                            </div>
                                                        </div>
                                                        <div class="field custom">
                                                        <textarea name="passport_issued_by" id="" rows="2"
                                                                  placeholder="Кем выдан"></textarea>
                                                        </div>
                                                        <div class="field custom">
                                                            <input type="date" value="" placeholder="Когда выдан"
                                                                   name="passport_date">
                                                        </div>
                                                        <div class="mb24"></div>
                                                        <div class="uppercase text--medium mb8">инн</div>
                                                        <div class="field custom">
                                                            <input type="text" value=""
                                                                   name="inn"
                                                                   placeholder="Номер ИНН физического лица">
                                                        </div>
                                                    </div>
                                                    <div class="one wide column"></div>
                                                    <div class="five wide computer seven wide tablet sixteen wide mobile column">
                                                        <div class="uppercase text--medium text--big mb24">Прошу
                                                            перечислить
                                                            деньги
                                                        </div>
                                                        <div class="mb24">
                                                            <div class="grouped fields custom">
                                                                <div class="field">
                                                                    <div class="ui checkbox radio custom">
                                                                        <input type="checkbox" name="payment"
                                                                               tabindex="0"
                                                                               value="bank"
                                                                               data-validate="payment-bank"
                                                                               checked
                                                                               class="hidden">
                                                                        <label>На расчетный счет</label>
                                                                    </div>
                                                                </div>
                                                                <div class="field">
                                                                    <div class="ui checkbox radio custom">
                                                                        <input type="checkbox" name="payment"
                                                                               tabindex="0"
                                                                               value="post"
                                                                               data-validate="payment-post"
                                                                               class="hidden">
                                                                        <label>Почтовым переводом</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value=""
                                                                   name="payment_name"
                                                                   placeholder="ФИО владельца счета">
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value="" placeholder="Наименование банка"
                                                                   name="bank_name">
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value="" placeholder="ИНН банка"
                                                                   name="bank_inn">
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value="" placeholder="БИК"
                                                                   name="bank_bik">
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value="" placeholder="Кор. счет"
                                                                   name="bank_ks">
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value=""
                                                                   placeholder="Лицевой счет (если есть)"
                                                                   name="bank_personal_account">
                                                        </div>
                                                        <div class="field custom bank-field">
                                                            <input type="text" value="" placeholder="Расчетный счет"
                                                                   name="bank_rs">
                                                        </div>
                                                        <div class="field custom post-field transition hidden">
                                                            <input type="text" value="" placeholder="Адрес перевода"
                                                                   name="post_address">
                                                        </div>
                                                        <div class="mb56 mobile hidden"></div>
                                                        <div class="field custom">
                                                            <div class="ui checkbox  custom">
                                                                <input type="checkbox" name="agree" tabindex="0"
                                                                       value="1"
                                                                       class="hidden">
                                                                <label>Я даю согласие на обработку персональных данных и
                                                                    подтверждаю корректность
                                                                    реквизитов
                                                                    для перечисления денежных средств
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="ui error message"></div>
                                                        <div class="field">
                                                            <button type="submit"
                                                                    class="ui button primary mini fluid custom">
                                                                Создать
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

        <script type="text/javascript">$(function() {
            var errors = $('.js-scroll-to-me');
            if (errors.length !== 0) {
              window.scrollToElement('.js-scroll-to-me', $(window).height() / 3);
            }
            $('.getback__form').each(function() {
              var $form = $(this);
              $form.find('[name=passport_series]').inputmask('9999');
              $form.find('[name=passport_number]').inputmask('999999');
              $form.find('[name=inn]').inputmask('9{10,12}');
              $form.find('[name=bank_inn]').inputmask('9{10}');
              $form.find('[name=bank_bik]').inputmask('9{9}');
              $form.find('[name=bank_rs]').inputmask('9{20}');
              $form.find('[name=bank_personal_account]').inputmask('9{20}');
              var fields = {
                agree: {rules: [{type: 'checked', prompt: 'Необходимо ваше согласие'}]},
                surname: {rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                name: {rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                middle_name: {rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                passport_series: {rules: [{type: 'exactLength[4]', prompt: 'Укажите серию'}]},
                passport_number: {rules: [{type: 'exactLength[6]', prompt: 'Укажите номер'}]},
                passport_issued_by: {rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                passport_date: {rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                inn: {
                  rules: [
                    {type: 'empty', prompt: 'Поле не может быть пустым'}, {
                      type: 'minLength[10]',
                      prompt: 'Это поле должно содержать не менее {ruleValue} символов',
                    },
                    {
                      type: 'maxLength[12]',
                      prompt: 'Это поле должно содержать не более {ruleValue} символов',
                    },
                    {
                      type: 'number',
                      prompt: 'Значение должно состоять из цифр',
                    }],
                },
                payment_name: {depends: 'payment-bank', rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                bank_name: {depends: 'payment-bank', rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                bank_inn: {
                  depends: 'payment-bank',
                  rules: [
                    {type: 'empty', prompt: 'Поле не может быть пустым'},
                    {type: 'exactLength[10]', prompt: 'Введите 10 символов'},
                    {type: 'number', prompt: 'Значение должно состоять из цифр'}],
                },
                bank_bik: {
                  depends: 'payment-bank',
                  rules: [
                    {type: 'empty', prompt: 'Поле не может быть пустым'},
                    {type: 'exactLength[9]', prompt: 'Введите 9 символов'},
                    {type: 'number', prompt: 'Значение должно состоять из цифр'}],
                },
                bank_ks: {
                  depends: 'payment-bank',
                  rules: [
                    {type: 'empty', prompt: 'Поле не может быть пустым'},
                    {type: 'number', prompt: 'Значение должно состоять из цифр'}],
                },
                bank_rs: {
                  depends: 'payment-bank',
                  rules: [
                    {type: 'empty', prompt: 'Поле не может быть пустым'},
                    {type: 'number', prompt: 'Значение должно состоять из цифр'},
                    {type: 'exactLength[20]', prompt: 'Введите 20 символов'}],
                },
                bank_personal_account: {
                  depends: 'payment-bank',
                  optional: true
                  rules: [
                    {type: 'exactLength[20]', prompt: 'Введите 20 символов'},
                    {type: 'number', prompt: 'Значение должно состоять из цифр'},
                  ],
                },
                post_address: {depends: 'payment-post', rules: [{type: 'empty', prompt: 'Поле не может быть пустым'}]},
                product_check: {rules: [{type: 'checked', prompt: 'Необходимо выбрать хотя бы один товар'}]},
              };
              $form.find('[name=payment]').on('change', function(e) {
                if (!this.checked) {
                  this.checked = true;
                }
                if (this.checked) {
                  if (this.value === 'post') {
                    $form.find('[value=bank][name=payment]').prop('checked', false);
                    $form.find('.bank-field').transition('fade out', function() {
                      $form.find('.post-field').transition('fade in');
                    });
                  } else {
                    $form.find('[value=post][name=payment]').prop('checked', false);
                    $form.find('.post-field').transition('fade out', function() {
                      $form.find('.bank-field').transition('fade in');
                    });
                  }
                }
              });
              $form.find('tr[data-id]').each(function() {
                var id = $(this).data('id'),
                    $list = $(this).next().find('[type=checkbox]').not('[data-validate="product_check"]');
                $list.on('change', function() {
                  if (this.checked) {
                    $list.not($(this)).filter(':checked').prop('checked', false).trigger('change');
                  }
                });
                fields['product[' + id + '][type]'] = {
                  rules: [{type: 'checked', prompt: 'Выберите один из вариантов'}],
                  depends: 'product[' + id + '][checked]',
                };
                fields['product[' + id + '][defect_description]'] = {
                  rules: [{type: 'empty', prompt: 'Укажите характер брака'}],
                  depends: 'product-' + id + '-type-defect',
                };
              });
              $form.form({
                inline: true,
                on: 'submit change',
                fields,
              }).on('submit', function(e) {
                e.preventDefault();
                if ($form.form('is valid')) {
                  $.post('/api/order.return/', $form.serialize(), function(response) {
                    if (response.link) {
                      window.location.href = response.link;
                    }
                  }, 'json');
                }
              });
            });
          });</script>
        <?
    }
}
?>
<div class="ui tiny modal" id="success-return">
    <div class="content">
        Заявка на возврат успешно отправлена. Ожидайте письмо с заявлением.
    </div>
</div>
