<div class="ui mini modal auth-modal" id="auth">
  <div class="close">
      <? icon("close") ?>
  </div>
  <div class="modal-content auth-modal__content">
    <div class="ui active tab" data-tab="phone">
      <div class="auth-modal_start transition">
        <div class="auth-modal__header">Войти или создать профиль</div>
        <form class="ui form small auth-modal__form js-user-validate js-not-reload-after-submit" action="javascript:void(null)" data-action="user.auth-phone">
          <?=bitrix_sessid_post()?>
          <div class="field custom">
            <input  class="js-auto-disabled" type="tel" name="phone" data-validate="phone-required" placeholder="Номер телефона" autofocus>
          </div>
          <div class="auth-modal__replay"></div>
          <div class="ui error message"></div>
          <div class="field custom">
            <div class="ui submit button mini fluid gray custom disabled">Далее</div>
          </div>
        </form>
        <div class="auth-modal__meta">Нажимая кнопку «Далее»,
          вы принимаете согласие на обработку персональных данных
        </div>
      </div>
      <div class="auth-modal_auth transition hidden">
        <div class="auth-modal__header">Войти в профиль</div>
        <form class="ui form small auth-modal__form js-user-validate" action="javascript:void(null)" data-action="user.auth-code">
          <?=bitrix_sessid_post()?>
          <input type="hidden" name="phone">
          <div class="field custom">
            <input class="js-auto-submit" type="text" name="code" data-validate="text-required" placeholder="Введите код из СМС" autofocus>
          </div>
          <div class="auth-modal__replay"></div>
          <div class="ui error message"></div>
          <div class="field custom">
            <div class="ui button mini fluid gray js-get-code-again custom">Получить смс повторно</div>
          </div>
          <div class="fiel custom">
            <div class="ui submit button mini fluid primary custom">Войти</div>
          </div>
        </form>
      </div>
      <div class="auth-modal_reg transition hidden">
        <div class="auth-modal__header">зарегистрируйтесь</div>
        <form class="ui form small auth-modal__form js-user-validate" action="javascript:void(null)" data-action="user.reg-code">
          <?=bitrix_sessid_post()?>
          <input type="hidden" name="phone">
          <div class="field custom">
            <input type="text" name="full_name" data-validate="text-required" placeholder="ФИО" class="register-full-name">
              <input type="hidden" name="name">
              <input type="hidden" name="surname">
              <input type="hidden" name="patronymic">
              <input type="hidden" name="gender">
          </div>
          <div class="field custom">
            <input type="text" name="email" data-validate="email" placeholder="Электронная почта">
          </div>
          <div class="field custom">
            <input class="js-auto-submit" type="text" name="code" data-validate="text-required" placeholder="Введите код из СМС">
          </div>
          <div class="auth-modal__replay"></div>
          <div class="ui error message"></div>
          <div class="field custom">
            <div class="ui button mini fluid gray js-get-code-again custom">Получить смс повторно</div>
          </div>
          <div class="field custom">
            <div class="ui submit button mini fluid primary custom">Войти</div>
          </div>
        </form>
      </div>
    </div>
    <div class="ui tab" data-tab="default">
      <div class="auth-modal_default transition">
        <div class="auth-modal__header">Войти<br>в профиль</div>
        <form class="ui form small auth-modal__form js-user-validate" action="javascript:void(null)" data-action="user.authorize">
          <div class="field custom">
            <input type="text" name="login" placeholder="Телефон / Почта" value="" data-validate="username"
                   data-autofocus>
          </div>
          <div class="field relative custom">
            <button type="button" class="js-password-toggler eye-button"><? icon("eye") ?></button>
            <input type="password" name="password" placeholder="Пароль" value="" data-validate="password">
          </div>
          <div class="ui error message"></div>
          <a href="javascript:void(null)" class="black underline js-forgot-paswrd">Забыли пароль?</a>
          <br>
          <br>
          <br>
          <div class="field custom">
            <div class="ui button primary submit mini fluid custom">Войти</div>
          </div>
        </form>
      </div>

      <div class="auth-modal_forgot transition hidden">
        <a href="javascript:void(null)" class="auth-modal__back js-auth-forgot-back"><?=icon('back')?></a>
        <div class="auth-modal__header">Восстановление<br>пароля</div>
        <form class="ui form small auth-modal__form js-user-validate" action="javascript:void(null)" data-action="user.forgot.email">
          <p>На данный адрес придёт ссылка<br>
            для смены пароля</p>
          <div class="field custom">
            <input type="text" name="email" data-validate="email-required" placeholder="Email">
          </div>
          <div class="field custom">
            <div class="ui button mini fluid gray submit custom">Получить письмо</div>
          </div>
        </form>
      </div>
      <div class="auth-modal_forgot-success transition hidden">
        <a href="javascript:void(null)" class="auth-modal__back js-auth-forgot-back"><?=icon('back')?></a>
        <div class="auth-modal__header">Восстановление<br>пароля</div>
        <div class="auth-modal_flex-center">
          <?=icon('message')?>
          <p>Мы отправили вам электронное
            сообщение. Если вы не получили его,
            возможно, вы не зарегистрированы
            на нашем веб-сайте.</p>
        </div>
      </div>
    </div>
    <div class="auth-modal__socials">
      <p class="auth-modal__socials-text">Вход через социальные сети</p>
      <div class="socials" data-ulogin="display=buttons">
        <a class="auth-modal__social" data-uloginbutton="googleplus">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0ZM18.6546 17.3462C18.1074 20.9409 15.4273 23.0017 11.9575 23.0017C8.05832 23.0017 4.89707 19.8404 4.89707 15.9412C4.89707 12.043 8.05832 8.88175 11.9575 8.88175C13.8437 8.88175 15.4964 9.55485 16.6994 10.7233L14.6731 12.7495C13.9869 12.0582 13.0478 11.6897 11.9575 11.6897C9.66228 11.6897 7.80146 13.6469 7.80146 15.9412C7.80146 18.2375 9.66228 20.1379 11.9575 20.1379C13.8813 20.1379 15.3381 19.208 15.7583 17.3462H11.9575V14.5108H18.6374C18.7175 14.9677 18.7581 15.4458 18.7581 15.9412C18.7581 16.4325 18.7225 16.9005 18.6546 17.3462ZM27.1332 16.4671H24.6592V18.94H22.902V16.4671H20.428V14.7088H22.902V12.2348H24.6592V14.7088H27.1332V16.4671Z" fill="black"/>
          </svg>
        </a>
        <a class="auth-modal__social" data-uloginbutton="yandex">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M16.852 6.80111C16.5032 6.85807 13.9756 7.42592 13.9756 11.8222C13.9756 13.8291 14.4791 15.038 15.0575 15.7648C15.6146 16.4645 16.4784 16.8488 17.3726 16.8488H18.9374V6.79541H16.9263C16.9013 6.79541 16.877 6.79689 16.852 6.80111Z" fill="black"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0ZM21.3534 27.0769H19.0034V18.8072H17.3494L13.6064 27.0769H10.6466L15.3473 18.5895C15.3473 18.5895 11.3869 16.7617 11.3869 12.235C11.3869 7.70891 14.5429 4.92308 17.1918 4.92308H21.3534V27.0769Z" fill="black"/>
          </svg>
        </a>
        <a class="auth-modal__social" data-uloginbutton="facebook">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M32 16C32 7.16479 24.8352 0 16 0C7.16479 0 0 7.16479 0 16C0 24.8352 7.16479 32 16 32C16.094 32 16.1875 31.998 16.2812 31.9963V19.541H12.8438V15.5349H16.2812V12.5869C16.2812 9.16797 18.3684 7.30713 21.4182 7.30713C22.8787 7.30713 24.134 7.41602 24.5 7.4646V11.0378H22.397C20.7378 11.0378 20.4165 11.8264 20.4165 12.9834V15.5349H24.3838L23.8667 19.541H20.4165V31.3818C27.1042 29.4646 32 23.3032 32 16Z" fill="black"/>
          </svg>

        </a>
        <a class="auth-modal__social" data-uloginbutton="vkontakte">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0C7.16374 0 0 7.16342 0 16C0 24.8366 7.16374 32 16 32C24.8363 32 32 24.8366 32 16C32 7.16342 24.8363 0 16 0ZM24.1161 17.7305C24.8618 18.4588 25.6507 19.1443 26.3202 19.948C26.6168 20.3036 26.8964 20.6712 27.1092 21.0846C27.413 21.6742 27.1386 22.3208 26.6109 22.3558L23.333 22.3552C22.4864 22.4252 21.8127 22.0838 21.2447 21.505C20.7913 21.0437 20.3707 20.551 19.934 20.0741C19.7555 19.8783 19.5676 19.694 19.3437 19.549C18.8969 19.2583 18.5086 19.3473 18.2526 19.8141C17.9917 20.2891 17.9321 20.8155 17.9072 21.3442C17.8715 22.1172 17.6385 22.3191 16.8629 22.3555C15.2058 22.4331 13.6335 22.1816 12.1721 21.3462C10.8829 20.6093 9.88514 19.5693 9.01565 18.3917C7.32252 16.0962 6.02582 13.5768 4.86073 10.9851C4.59851 10.4014 4.79034 10.0891 5.43427 10.077C6.5041 10.0563 7.57393 10.059 8.64376 10.076C9.07916 10.0829 9.36724 10.332 9.53453 10.7428C10.1127 12.1652 10.8214 13.5186 11.7095 14.7737C11.9462 15.1079 12.1878 15.4412 12.5319 15.6772C12.9116 15.9378 13.201 15.8517 13.3801 15.4278C13.4947 15.1583 13.5441 14.8703 13.569 14.5815C13.6541 13.5926 13.6642 12.6039 13.5169 11.6189C13.4253 11.0024 13.0786 10.6044 12.4641 10.4878C12.1512 10.4286 12.197 10.3127 12.3492 10.1339C12.6134 9.8249 12.8609 9.63372 13.3555 9.63372L17.059 9.63306C17.6427 9.74764 17.7737 10.0095 17.8529 10.5975L17.8562 14.7131C17.8493 14.9406 17.9704 15.615 18.379 15.764C18.7063 15.872 18.9224 15.6095 19.1182 15.4022C20.0063 14.4597 20.6391 13.3474 21.2058 12.196C21.4559 11.6883 21.6716 11.1628 21.8811 10.6364C22.0369 10.2472 22.2792 10.0557 22.7185 10.0622L26.2848 10.0665C26.3899 10.0665 26.4967 10.0675 26.6008 10.0855C27.2018 10.1883 27.3665 10.4469 27.1805 11.0332C26.8879 11.9544 26.3192 12.7218 25.7634 13.4911C25.1675 14.3147 24.5321 15.1096 23.9422 15.9368C23.4001 16.693 23.443 17.0738 24.1161 17.7305Z" fill="black"/>
          </svg>

        </a>
        <a class="auth-modal__social" data-uloginbutton="odnoklassniki">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.0151 13.2609C17.5429 13.2553 18.7597 12.028 18.7541 10.497C18.7486 8.96521 17.5291 7.74643 16.0007 7.74512C14.4594 7.74348 13.2285 8.98485 13.2403 10.5303C13.2518 12.0559 14.4823 13.2665 16.0151 13.2609Z" fill="black"/>
            <path d="M16 0C7.16342 0 0 7.16374 0 16C0 24.8363 7.16342 32 16 32C24.8366 32 32 24.8363 32 16C32 7.16374 24.8366 0 16 0ZM16.0229 4.86564C19.1394 4.87415 21.6408 7.42334 21.6248 10.5742C21.6091 13.6525 19.0668 16.1473 15.9555 16.1368C12.8759 16.1264 10.3503 13.5732 10.3687 10.4888C10.386 7.37064 12.9129 4.85713 16.0229 4.86564ZM22.3352 18.3511C21.6461 19.0586 20.8165 19.5706 19.8953 19.9277C19.0245 20.2652 18.0703 20.4352 17.1255 20.5484C17.2685 20.7036 17.3357 20.7799 17.4247 20.8689C18.7067 22.1577 19.9948 23.44 21.2726 24.7325C21.7079 25.1731 21.7986 25.7192 21.559 26.2308C21.2974 26.7906 20.7105 27.1586 20.1346 27.119C19.7699 27.0938 19.4855 26.9127 19.2331 26.6584C18.2654 25.6848 17.2793 24.7286 16.3319 23.7366C16.0553 23.4479 15.9231 23.5029 15.6792 23.7537C14.7059 24.7561 13.7166 25.7424 12.7218 26.7235C12.2749 27.1642 11.7433 27.244 11.2247 26.992C10.6738 26.7255 10.3235 26.1627 10.3503 25.5967C10.3693 25.215 10.5572 24.9224 10.8198 24.6611C12.089 23.3945 13.3542 22.124 14.6198 20.8545C14.704 20.7704 14.7819 20.681 14.904 20.5501C13.1778 20.3697 11.6212 19.9448 10.2885 18.9028C10.1228 18.7731 9.95225 18.6481 9.80166 18.5027C9.21796 17.9426 9.15937 17.3006 9.62062 16.64C10.0158 16.0743 10.6783 15.9231 11.3674 16.2481C11.501 16.3107 11.628 16.3899 11.7498 16.4737C14.2332 18.1803 17.645 18.2271 20.1382 16.55C20.3851 16.3614 20.6492 16.2062 20.9553 16.128C21.5498 15.9748 22.105 16.1938 22.4239 16.7143C22.7889 17.3091 22.7847 17.8892 22.3352 18.3511Z" fill="black"/>
          </svg>

        </a>
        <a class="auth-modal__social" data-uloginbutton="instagram">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M19.0625 16C19.0625 17.6914 17.6914 19.0625 16 19.0625C14.3086 19.0625 12.9375 17.6914 12.9375 16C12.9375 14.3086 14.3086 12.9375 16 12.9375C17.6914 12.9375 19.0625 14.3086 19.0625 16Z" fill="black"/>
            <path d="M23.1621 10.5813C23.0149 10.1824 22.78 9.82129 22.4749 9.5249C22.1785 9.21973 21.8176 8.98486 21.4185 8.83765C21.0947 8.71191 20.6084 8.56226 19.7126 8.52148C18.7437 8.47729 18.4531 8.46777 16 8.46777C13.5466 8.46777 13.2561 8.47705 12.2874 8.52124C11.3916 8.56226 10.905 8.71191 10.5815 8.83765C10.1824 8.98486 9.82129 9.21973 9.52515 9.5249C9.21997 9.82129 8.98511 10.1821 8.83765 10.5813C8.71191 10.905 8.56226 11.3916 8.52148 12.2874C8.47729 13.2561 8.46777 13.5466 8.46777 16C8.46777 18.4531 8.47729 18.7437 8.52148 19.7126C8.56226 20.6084 8.71191 21.0947 8.83765 21.4185C8.98511 21.8176 9.21973 22.1785 9.5249 22.4749C9.82129 22.78 10.1821 23.0149 10.5813 23.1621C10.905 23.2881 11.3916 23.4377 12.2874 23.4785C13.2561 23.5227 13.5464 23.532 15.9998 23.532C18.4534 23.532 18.7439 23.5227 19.7124 23.4785C20.6082 23.4377 21.0947 23.2881 21.4185 23.1621C22.2197 22.853 22.853 22.2197 23.1621 21.4185C23.2878 21.0947 23.4375 20.6084 23.4785 19.7126C23.5227 18.7437 23.532 18.4531 23.532 16C23.532 13.5466 23.5227 13.2561 23.4785 12.2874C23.4377 11.3916 23.2881 10.905 23.1621 10.5813ZM16 20.7175C13.3943 20.7175 11.282 18.6055 11.282 15.9998C11.282 13.394 13.3943 11.282 16 11.282C18.6055 11.282 20.7178 13.394 20.7178 15.9998C20.7178 18.6055 18.6055 20.7175 16 20.7175ZM20.9043 12.198C20.2954 12.198 19.8018 11.7043 19.8018 11.0955C19.8018 10.4866 20.2954 9.99292 20.9043 9.99292C21.5132 9.99292 22.0068 10.4866 22.0068 11.0955C22.0066 11.7043 21.5132 12.198 20.9043 12.198Z" fill="black"/>
            <path d="M16 0C7.16479 0 0 7.16479 0 16C0 24.8352 7.16479 32 16 32C24.8352 32 32 24.8352 32 16C32 7.16479 24.8352 0 16 0ZM25.1321 19.7878C25.0876 20.7659 24.9321 21.4336 24.7051 22.0181C24.2278 23.2522 23.2522 24.2278 22.0181 24.7051C21.4338 24.9321 20.7659 25.0874 19.7881 25.1321C18.8083 25.1768 18.4954 25.1875 16.0002 25.1875C13.5049 25.1875 13.1921 25.1768 12.2122 25.1321C11.2344 25.0874 10.5664 24.9321 9.98218 24.7051C9.3689 24.4744 8.81372 24.1128 8.35474 23.6453C7.88745 23.1865 7.52588 22.6311 7.29517 22.0181C7.06812 21.4338 6.9126 20.7659 6.86816 19.7881C6.823 18.8081 6.8125 18.4951 6.8125 16C6.8125 13.5049 6.823 13.1919 6.86792 12.2122C6.91235 11.2341 7.06763 10.5664 7.29468 9.98193C7.52539 9.3689 7.88721 8.81348 8.35474 8.35474C8.81348 7.88721 9.3689 7.52563 9.98193 7.29492C10.5664 7.06787 11.2341 6.9126 12.2122 6.86792C13.1919 6.82324 13.5049 6.8125 16 6.8125C18.4951 6.8125 18.8081 6.82324 19.7878 6.86816C20.7659 6.9126 21.4336 7.06787 22.0181 7.29468C22.6311 7.52539 23.1865 7.88721 23.6455 8.35474C24.1128 8.81372 24.4746 9.3689 24.7051 9.98193C24.9324 10.5664 25.0876 11.2341 25.1323 12.2122C25.177 13.1919 25.1875 13.5049 25.1875 16C25.1875 18.4951 25.177 18.8081 25.1321 19.7878Z" fill="black"/>
          </svg>
        </a>
        <a class="auth-modal__social" data-uloginbutton="mailru">
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="16" cy="16" r="16" fill="black"/>
            <path d="M18.8874 15.9999C18.8874 17.5921 17.5921 18.8874 15.9999 18.8874C14.4077 18.8874 13.1124 17.5921 13.1124 15.9999C13.1124 14.4077 14.4077 13.1124 15.9999 13.1124C17.5921 13.1124 18.8874 14.4077 18.8874 15.9999ZM15.9999 6.3999C10.7062 6.3999 6.3999 10.7062 6.3999 15.9999C6.3999 21.2936 10.7062 25.5999 15.9999 25.5999C17.9391 25.5999 19.809 25.0225 21.4074 23.9302L21.4349 23.911L20.1416 22.4079L20.1197 22.4216C18.8895 23.2136 17.4646 23.6319 15.9999 23.6319C11.7917 23.6319 8.3679 20.2081 8.3679 15.9999C8.3679 11.7917 11.7917 8.3679 15.9999 8.3679C20.2081 8.3679 23.6319 11.7917 23.6319 15.9999C23.6319 16.545 23.5709 17.097 23.4522 17.6401C23.2109 18.631 22.5169 18.9341 21.9965 18.8943C21.4726 18.8518 20.8596 18.4788 20.8554 17.5654V16.8694V15.9999C20.8554 13.3222 18.6776 11.1444 15.9999 11.1444C13.3222 11.1444 11.1444 13.3222 11.1444 15.9999C11.1444 18.6776 13.3222 20.8554 15.9999 20.8554C17.3007 20.8554 18.5206 20.3473 19.4401 19.4223C19.975 20.2548 20.8465 20.7766 21.8388 20.8561C21.9238 20.863 22.0109 20.8664 22.0966 20.8664C22.7953 20.8664 23.4872 20.6326 24.0454 20.2095C24.6207 19.772 25.0506 19.1405 25.2879 18.3814C25.3256 18.2586 25.3956 17.9782 25.3956 17.9761L25.3976 17.9658C25.5375 17.3569 25.5999 16.7501 25.5999 15.9999C25.5999 10.7062 21.2936 6.3999 15.9999 6.3999Z" fill="white"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="auth-modal__menu">
      <a class="auth-modal__menu-item active" data-tab="phone">Войти по<span class="mobile hidden"> коду</span> СМС</a>
      <a class="auth-modal__menu-item" data-tab="default">Войти по паролю</a>
    </div>
  </div>
</div>
