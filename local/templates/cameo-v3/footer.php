<?php

use Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @global CMain $APPLICATION
 */
$options = new \Page\Options(array(
  "useTitle" => array(
    "on" => "Использовать заголовок",
    "off" => "Не использовать заголовок",
    "default" => false
  ),
  "titleClass" => array(
    "text" => "Класс заголовка",
    "prompt" => true,
    "rule" => "useTitle"
  ),
  "titleCenter" => array(
    "on" => "Отцентровать заголовок",
    "off" => "Прижать заголовок к левому краю",
    "rule" => "useTitle",
    "default" => true
  ),
  "titleUppercase" => array(
    "on" => "Заголовок заглавными буквами",
    "off" => "Заголовок строчными буквами",
    "default" => true,
    "rule" => "useTitle"
  ),
  "bodyClass" => array(
    "text" => "Класс тела страницы",
    "prompt" => true
  ),
  "modules" => array(
    "text" => "Модули",
    "checkbox" => array(
      "object-fit" => "Object fit",
      "jquery" => "jQuery",
      "semantic" => "Semantic UI",
      "lightgallery" => "Light Gallery",
      "lg-thumbnail" => "Light Gallery thumbnail",
      "lg-mousemove" => "Light Gallery mousemove",
      "lg-zoom" => "Light Gallery zoom",
      "lightslider" => "Light Slider",
      "velocity" => "Velocity",
      "pageScroll" => "Page Scroll",
      "wow" => "Wow book",
      "googleMaps" => "Google Maps"
    ),
    "default" => "object-fit,jquery,semantic"
  ),
  "googleMapsKey" => array(
    'text' => "Google Maps API key",
    "prompt" => true,
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  ),
  "head" => array(
    "text" => "Скрипты шапки",
    "textarea" => true,
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  ),
  "body" => array(
    "text" => "Скрипты тела страницы",
    "textarea" => true,
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  ),
  "footer" => array(
    "text" => "Скрипты футера",
    "textarea" => true,
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  ),
  "sale" => array(
    "on" => "Включить распродажу",
    "off" => "Выключить распродажу",
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  ),
  "daemonCollector" => array(
    "on" => "Включить DaemonCollector",
    "off" => "Выключить DaemonCollector",
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  ),
  "daemonCollectorSiteKey" => array(
    "text" => "Ключ сайта daemonCollector",
    "prompt" => true,
    "rule" => $APPLICATION->GetCurDir() === SITE_DIR,
    "page" => false,
    "clean" => false
  )
));

$asset = Asset::getInstance();
$modules = $options->get("modules", true);
//region css
if ($modules['semantic']) {
    $asset->addCss(SITE_TEMPLATE_PATH . '/semantic/semantic.min.css');
}
if ($modules['lightgallery']) {
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/lib/lightgallery.min.css');
}
//if ($modules['lightslider']) {
$asset->addCss(SITE_TEMPLATE_PATH . '/css/lib/lightslider.min.css');
//}
if ($modules['wow']) {
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/lib/wow_book.min.css');
}
$asset->addCss(SITE_TEMPLATE_PATH . '/fonts/meta/stylesheet.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/css/inputmask.min.css');

if ($version == 4) {
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/v4/index.css');
    $asset->addCss(SITE_TEMPLATE_PATH . '/modals/styles.css');
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/v4/personal-area.css');
} else {
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/style.css');
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/shapes.min.css');
}
$asset->addCss(SITE_TEMPLATE_PATH . '/css/talkme.min.css');
//endregion
//region js
if ($modules['object-fit']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/picturefill.js');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/object-fit-cover.js');
}
if ($modules['jquery']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/jquery.min.js');
}
if ($modules['semantic']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/semantic/semantic.min.js');
}
if ($modules['lightgallery']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/lightgallery.min.js');
}
if ($modules['lg-thumbnail']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/lg-thumbnail.min.js');
}
if ($modules['lg-mousemove']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/lg-mousemove.min.js');
}
if ($modules['lg-zoom']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/lg-zoom.min.js');
}
//if ($modules['lightslider']) {
$asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/lightslider.min.js');
//}
if ($modules['googleMaps']) {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= $options->get("googleMapsKey") ?>&callback=initMap"
            async defer></script>
    <?
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/silverStyle.js');
}
if ($modules['velocity']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/velocity.min.js');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/velocity.ui.min.js');
}
if ($modules['pageScroll']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/page-scroll.min.js');
}
if ($modules['product']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/product.min.js');
}
if ($modules['wow']) {
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/pdf.combined.min.js');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/wow_book.min.js');
}
//suggesions
$asset->addCss(SITE_TEMPLATE_PATH . '/css/suggestions.min.css');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/suggestions.min.js');

$asset->addJs(SITE_TEMPLATE_PATH . '/js/jquery.serialize-object.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/lib/jquery.inputmask.bundle.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/config.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/functions.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/Content.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/cart.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/favorites.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/Form.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/scripts.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/user-sizes.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/shapes.min.js');
//endregion

$debug = false;
if ($debug) {
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/ruler.css');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/html2canvas.js');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/ruler.js');
}
$authOpen = $GLOBALS['activeSide'] == 'auth';
if (!SocialMeta::$exists) {
    new SocialMeta($arResult['ID'], !$GLOBALS['canonicalUsed'], true);
}
if ($GLOBALS['activeSide'] && $GLOBALS['activeSide'] != 'main') {
    ?>
    <script>
      window.defaultSide = '.<?=$GLOBALS['activeSide']?>';
      $(function() {
        $('.ui.shape').shape('set next side', ".<?=$GLOBALS['activeSide']?>").shape('slide bottom');
        $(".<?=$GLOBALS['activeSide']?>").find('.close').remove();
      });
    </script>
    <?
}
?>
<?
if (false) {
?>
<html>
<body>
<div class="pusher">
    <div class="ui shape">
        <div class="sides">
            <div class="side active main-side break-close">
                <div class="ui container fluid main-content">
                    <?
                    }
                    $content = "";
                    if ($options->get("useTitle")) {
                        $titleClass = $options->get("titleClass");
                        if ($options->get("titleCenter")) {
                            $titleClass .= " center aligned";
                        }
                        if ($options->get("titleUppercase")) {
                            $titleClass .= " uppercase";
                        }
                        $content .= "<h1 class='ui header normal $titleClass'>" . $APPLICATION->GetTitle() . "</h1>";
                    }
                    $APPLICATION->SetPageProperty("top-content", $content);
                    ?>
                </div>
            </div>
            <div class="side write-side">
                <div class="ui container fluid">
                    <div class="write-to-us">
                        <div class="h1 ui header center aligned uppercase normal">Напишите нам</div>
                        <? icon("close") ?>
                        <? $APPLICATION->IncludeComponent(
                          "bitrix:form.result.new",
                          "",
                          Array(
                            "CACHE_TIME" => "3600",
                            "CACHE_TYPE" => "A",
                            "CHAIN_ITEM_LINK" => "",
                            "CHAIN_ITEM_TEXT" => "",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO",
                            "EDIT_URL" => "result_edit.php",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "LIST_URL" => "result_list.php",
                            "SEF_MODE" => "N",
                            "SUCCESS_URL" => "",
                            "USE_EXTENDED_ERRORS" => "N",
                            "VARIABLE_ALIASES" => Array(
                              "RESULT_ID" => "RESULT_ID",
                              "WEB_FORM_ID" => "WEB_FORM_ID"
                            ),
                            "WEB_FORM_ID" => "4"
                          )
                        ); ?>
                    </div>
                </div>
            </div>
            <div class="side city-side">
                <div class="ui container fluid">
                    <div class="choose-city side-container">
                        <div class="h1 ui header center aligned uppercase normal">Выбор города</div>
                        <? icon("close") ?>
                        <form class="ui form">
                            <div class="ui search fluid" id="citySearch">
                                <div class="ui icon input fluid">
                                    <input type="text" class="prompt" value="">
                                    <? icon("search") ?>
                                </div>
                                <div class="results"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="side basket-edit-side break-close">
                <div class="ui container fluid main-content">

                </div>
            </div>
            <div class="side size-side">
                <div class="ui container fluid">
                    <div class="side-container">
                        <div class="h1 ui header center aligned uppercase normal">Таблица размеров</div>
                        <? icon("close") ?>
                        <?
                        //Dynamic::begin("size-table");
                        include "views/v" . $version . '/size-table.php';
                        //Dynamic::end("centered") ?>
                    </div>
                </div>
            </div>
            <? //Dynamic::begin("isAuthMainShape"); ?>
            <? //if (!CUser::isAuthorized()): ?>
            <?
            include "shape_includes/authorization-side.php";
            include "shape_includes/registration-side.php";
            include "shape_includes/forgot-password-side.php";
            ?>
            <? //endif; ?>
            <? //Dynamic::end(false); ?>

            <?
            include "shape_includes/search-side.php";

            include "modals/auth.php";
            echo $APPLICATION->GetPageProperty("additionalSide");
            ?>
        </div>
    </div>
    <?
    include "views/v" . $version . '/footer.php';
    ?>
</div>
<div class="hide">
    <?
    Dynamic::begin("isAuthULoginComponent");
    if (CUser::isAuthorized()) {

    } else {
        $APPLICATION->IncludeComponent(
          "ulogin:auth",
          "",
          Array(
            "GROUP_ID" => array(3, 4),
            "LOGIN_AS_EMAIL" => "N",
            "SEND_EMAIL" => "N",
            "SOCIAL_LINK" => "N",
            "ULOGINID1" => "",
            "ULOGINID2" => ""
          )
        );
    }
    Dynamic::end(false);
    ?>
</div>
<?
echo $options->get("footer");
?>
<?
if (!News::$loaded) {
    News::show();
}
Dynamic::begin("banner-block-bottom");
if ($USER->IsAdmin() && false) {
    global $location;
    ?>
    <div class="bottom-banners" data-city="<?= $location['id'] ?>">
        <?
        $rs = CIBlockElement::GetList(array(), array(
          "ACTIVE" => "Y",
          "IBLOCK_ID" => 78,
          "!=PROPERTY_not_in_city" => $location['id'],
          "ACTIVE_DATE" => "Y",
          array(
            "LOGIC" => "OR",
            array(
              "PROPERTY_in_city" => $location['id'],
            ),
            array(
              "PROPERTY_in_city" => false,
            )
          )
        ), false, false, array(
          "ID",
          "PREVIEW_PICTURE",
          "PREVIEW_TEXT",
          "PROPERTY_coupon",
          "PROPERTY_in_city",
          "PROPERTY_not_in_city"
        ));
        while ($result = $rs->Fetch()) {
            if (in_array($location['id'], $result['PROPERTY_NOT_IN_CITY_VALUE'])) {
                continue;
            }
            $result['PREVIEW_PICTURE'] = CFile::GetFileArray($result['PREVIEW_PICTURE']);
            if (!$_COOKIE['banner-' . $result['ID']]) {
                ?>
                <div class="action-block"
                     style="background-image:url('<?= $result['PREVIEW_PICTURE']['SRC'] ?>');background-size:cover;"
                     data-banner="<?= $result['ID'] ?>"
                     data-city="<?= implode(",", $result['PROPERTY_IN_CITY_VALUE']) ?>"
                     data-exclude="<?= implode(",", $result['PROPERTY_NOT_IN_CITY_VALUE']) ?>">
                    <div class="ui container">
                        <div class="flex">
                            <div class="action-title"
                                 style="line-height: 30px; margin: 0 auto; text-align: center; font-size: 15pt; padding: 0 70px 0 70px">
                                <?= $result['PREVIEW_TEXT'] ?>
                            </div>
                            <span class="close"><? icon("close") ?></span>
                            <?
                            if ($result['PROPERTY_COUPON_VALUE']) {
                                ?>
                                <div class="action-coupon">
                                    <a class="ui tiny inverted button" href="#!"
                                       data-copy="<?= $result['PROPERTY_COUPON_VALUE'] ?>"
                                       data-toggle="tooltip"
                                       title="Нажмите, чтобы скопировать" data-delay="0">Промокод:
                                        <b><?= $result['PROPERTY_COUPON_VALUE'] ?></b></a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?
            }
        }
        $APPLICATION->AddHeadScript("/include/js/v2/scripts.js");
        $APPLICATION->SetAdditionalCSS("/include/css/v2/style.css");

        ?>
    </div>
    <input type="text" id="copier"/>
    <?
}
Dynamic::end();

$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/chat.css");

?>
</body>
</html>
