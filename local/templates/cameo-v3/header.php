<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 */
global $location;

use Page\Options; ?>
<!doctype html>
<html lang="<?= LANGUAGE_ID ?>" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="cmsmagazine" content="b5df5d239bc002e95590270277e5e387"/>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta name="theme-color" content="#059856">
    <style>
        .hide {
            display: none;
        }
    </style>
    <script data-skip-moving="true">
        window.cameo = {
          version: <?=$version?>
        };
      document.documentElement.classList.remove('no-js');
      var hasWebP = (function() {
        var images = {
          basic: 'data:image/webp;base64,UklGRjIAAABXRUJQVlA4ICYAAACyAgCdASoCAAEALmk0mk0iIiIiIgBoSygABc6zbAAA/v56QAAAAA==',
          lossless: 'data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAQAAAAfQ//73v/+BiOh/AAA=',
        };

        return function(feature, callback) {
          var img = new Image();
          img.onload = function() {
            callback(!!(img.height > 0 && img.width > 0));
          };
          img.onerror = function() {
            callback(false);
          };
          img.src = images[feature || 'basic'];
        };
      })();
      hasWebP('basic', function(result) {
        if (!result) {
          document.documentElement.classList.add('no-webp');
        }
      });
    </script>
    <?
    //CJSCore::Init(array("fx"));
    $APPLICATION->ShowHead();
    \Page\Options::show("head-before");
    \Page\Options::show("head");
    ?>
    <meta name="robots" content="noyaca"/>

    <script data-skip-moving="true">!function() {
        var t = document.createElement('script');
        t.type = 'text/javascript', t.async = !0, t.src = 'https://vk.com/js/api/openapi.js?160', t.onload = function() {
          VK.Retargeting.Init('VK-RTRG-367779-32dXl'), VK.Retargeting.Hit();
        }, document.head.appendChild(t);
      }();</script>
    <script data-skip-moving="true">
      !function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
          n.callMethod ?
              n.callMethod.apply(n, arguments) : n.queue.push(arguments);
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s);
      }(window, document, 'script',
          'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1368697846602791');
      fbq('track', 'PageView');
    </script>
    <? Options::show("head-script") ?>
</head>
<body class="pushable <? \Page\Options::show("bodyClass") ?>">
<noscript><img src="https://vk.com/rtrg?p=VK-RTRG-367779-32dXl" style="position:fixed; left:-999px;" alt=""/></noscript>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1368697846602791&ev=PageView&noscript=1" alt=""
    /></noscript>
<?

\Page\Options::show("body");
?>
<? Dynamic::begin("isAdmin"); ?>
<?
$list = array_map(function ($letter) { return str_replace("G", "", $letter); },
  unserialize(COption::GetOptionString('main', 'show_panel_for_users')));
if ($USER->IsAdmin() || CSite::InGroup($list)) {
    $APPLICATION->ShowPanel();
}
?>
<? Dynamic::end(false); ?>
<?
include 'views/v' . $version . '/header.php';
include 'views/v' . $version . '/sidebar.php';
?>
<div class="pusher">
    <div class="ui shape">
        <div class="sides">
            <div class="side active main-side">
                        <div class="ui container fluid main-content">
                            <? $APPLICATION->ShowProperty("top-content") ?>
