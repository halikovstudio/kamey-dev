class Favorite
  constructor: ()->
    window.favorite = @
    $this = @
    @dom = $("#favorite-small")
    @trigger = $(".sidebar-trigger[data-target='favorite-small'],.favorite-button")
    @indicator = $(".sidebar-trigger[data-target='favorite-small'] .indicator,.favorite-button .indicator")
    @cards = @dom.find(".middle-content")
    @list = []

    @trigger.on('click', (e)->
      if $(@).hasClass('disabled')
        e.preventDefault();
    )

    $(document).on("click", ".favorite-trigger", (e) ->
      e.preventDefault();
      $this.toggle($(@));
    ).on("click", ".favorite-small .clear,.favorite .clear", (e) ->
      e.preventDefault()
      $(this).api({
        on: "now"
        url: "favorite.clear"
        onSuccess: () =>
          $(".catalog-list").find(".favorite-column").transition({
            animation: "fade out"
            interval: 200
            reverse: true
            onComplete: () =>
              $this.update({
                list: [],
                html: ""
              }, false);
              $(".favorite-trigger.active").removeClass("active")
          })
        onFailure: () =>
          $(this).addClass("error")
      })
    ).on("click", ".favorite-delete", (e) ->
      e.preventDefault();
      $button = $(@)
      id = $button.data("target")

      $button.closest(".favorite-column").transition("fade out", () ->
        $this.remove(id, (success) ->
          $(".favorite-trigger[data-target='#{id}']").removeClass("active") if success
        );
      )
    )
    @load()
    @get(@update.bind(this));
  save: (data) ->
    window.sessionStorage?.setItem("favorite-data", JSON.stringify(data))
  load: () ->
    jsonData = window.sessionStorage?.getItem('favorite-data')
    if jsonData?
      data = JSON.parse(jsonData)
      @update(data)
  get: (callback) ->
    $.get("/api/favorite.get/",{version: cameo.version},(data) =>
      callback?(data);
      @save(data)
    , "json")
  update: (data, setClass = true) ->
    @trigger.removeClass("loading")
    if data
      @list = data.list or [] if data.list?
      @html = data.html or "" if data.html?
    @cards.html(@html)
    if !$.isArray(@list)
      @list = []
    @list.forEach((id) ->
      $(".favorite-trigger[data-target='#{id}']").addClass("active")
    ) if setClass
    @updateIndicator()
  updateIndicator: () ->
    if @list.length > 0
      @trigger.popup("destroy")
      @trigger.removeClass("disabled")
    else
      @trigger.popup({
        html: @trigger.data("prevent")
        on: "hover",
        position: "bottom right"
      })
      @trigger.addClass("disabled")
    @indicator.text(if @list.length > 0 then @list.length else "")
  toggle: ($item) ->
    id = $item.data("target")

    if !$item.hasClass("active")
      @add(id, (success) =>
        $(".favorite-trigger[data-target='#{id}']").addClass("active") if success
      );
    else
      @remove(id, (success) =>
        $(".favorite-trigger[data-target='#{id}']").removeClass("active") if success
      )
  add: (id, callback) ->
    $.post("/api/favorite.add/", {
      id
    }, (data) =>
      @update(data)
      callback?(data.status == 200)
    );
  remove: (id, callback) ->
    $.post("/api/favorite.remove/", {
      id
    }, (data) =>
      @update(data)
      callback?(data.status == 200);
    );
