class GooglePay
  constructor: (@orderId, @amount, @instant = false, @container = '.button-payments') ->
    @baseRequest = {
      apiVersion: 2,
      apiVersionMinor: 0
    }
    @allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];
    @allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];
    @tokenizationSpecification = {
      type: 'PAYMENT_GATEWAY',
      parameters: {
        'gateway': 'sberbank',
        'gatewayMerchantId': 'kamey'
      }
    }
    @baseCardPaymentMethod = {
      type: 'CARD',
      parameters: {
        allowedAuthMethods: @allowedCardAuthMethods,
        allowedCardNetworks: @allowedCardNetworks
      }
    }
    @cardPaymentMethod = Object.assign(
      {},
      @baseCardPaymentMethod,
      {
        tokenizationSpecification: @tokenizationSpecification
      }
    )
    @onGooglePayLoaded();
  getGoogleIsReadyToPayRequest: () ->
    return Object.assign(
      {},
      @baseRequest,
      {
        allowedPaymentMethods: [@baseCardPaymentMethod]
      }
    );
  getGooglePaymentDataRequest: ()->
    paymentDataRequest = Object.assign({}, @baseRequest);
    paymentDataRequest.allowedPaymentMethods = [@cardPaymentMethod];
    paymentDataRequest.transactionInfo = @getGoogleTransactionInfo();
    paymentDataRequest.merchantInfo = {
      merchantId: '16387654798080459426',
      merchantName: 'ООО "Торговый Дом "Камея"'
    };

    paymentDataRequest.callbackIntents = ["PAYMENT_AUTHORIZATION"];

    return paymentDataRequest;
  getGooglePaymentsClient: ()->
    if (!@paymentsClient?)
      @paymentsClient = new google.payments.api.PaymentsClient({
        environment: 'PRODUCTION', #PRODUCTION
        paymentDataCallbacks: {
          onPaymentAuthorized: (paymentData)=>
            return @onPaymentAuthorized(paymentData)
        }
      });

    return @paymentsClient;

  onPaymentAuthorized: (paymentData) ->
    console.log('google pay: on payment authorized', paymentData);
    return new Promise((resolve, reject)=>
      @processPayment(paymentData).then((data)=>
        if (data? && data.acsUrl)
          form = document.createElement('form');
          @addInput(form, 'PaReq', data.paReq);
          @addInput(form, 'MD', data.orderId);
          @addInput(form, 'TermUrl', data.termUrl);
          form.method = 'POST';
          form.action = data.acsUrl;
          document.body.appendChild(form);
          form.submit();
          resolve({
            transactionState: 'SUCCESS'
          });
        else
          $(".payment-success").modal('show')
          console.log('google pay: success');
          resolve({
            transactionState: 'SUCCESS'
          });
      ).catch((e)=>
        console.log('google pay: error', e);
        resolve({
          transactionState: 'ERROR',
          error: {
            intent: 'PAYMENT_AUTHORIZATION',
            message: e,
            reason: 'PAYMENT_DATA_INVALID'
          }
        });
      )
    )
  onGooglePayLoaded: () ->
    console.log('google pay: init');
    paymentsClient = @getGooglePaymentsClient();
    paymentsClient.isReadyToPay(@getGoogleIsReadyToPayRequest()).then((response) =>
      console.log('google pay: isReadyToPay', response);
      if (response.result)
        @addGooglePayButton();
    ).catch((err) ->
# show error in developer console for debugging
      console.error(err);
    );
  addGooglePayButton: () ->
    paymentsClient = @getGooglePaymentsClient();
    button =
      paymentsClient.createButton({
        onClick: ()=>
          @onGooglePaymentButtonClicked();
      });
    console.log("google pay: add button");
    $(@container).append(button);
    if @instant
      @onGooglePaymentButtonClicked();
  getGoogleTransactionInfo: () ->
    return {
#      displayItems: [
#        {
#          label: "Subtotal",
#          type: "SUBTOTAL",
#          price: "11.00",
#        },
#        {
#          label: "Tax",
#          type: "TAX",
#          price: "1.00",
#        }
#      ],
      countryCode: 'RU',
      currencyCode: "RUB",
      totalPriceStatus: "FINAL",
      totalPrice: @amount + ".00",
      totalPriceLabel: "Total"
    };
  onGooglePaymentButtonClicked: () ->
    console.log("google pay: button clicked");
    paymentDataRequest = @getGooglePaymentDataRequest();
    paymentDataRequest.transactionInfo = @getGoogleTransactionInfo()
    paymentsClient = @getGooglePaymentsClient();
    console.log("google pay: load payment data");
    paymentsClient.loadPaymentData(paymentDataRequest);
  addInput: (form, name, value)->
    i = document.createElement('input');
    i.name = name;
    i.value = value;
    form.appendChild(i);
  processPayment: (paymentData)->
    console.log(paymentData);
    return new Promise((resolve, reject)=>
      _data = new FormData()
      _data.append('token', paymentData.paymentMethodData.tokenizationData.token);
      _data.append('orderId', @orderId);
      fetch('/api/payment.google', {
        method: 'post',
        body: _data
      }).then((response)->
        return response.json();
      ).then((response)->
        console.log('google pay: process payment gateway', response);
        if (response.status == 200)
          resolve(response.data);
        else
          reject(response.message)
      )
    )
