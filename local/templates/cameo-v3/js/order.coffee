$.fn.form.settings.prompt.empty = "Поле \"{name}\" должно быть заполнено"
$.fn.form.settings.prompt.checked = "{name} должно быть отмечено"
class Order
  constructor: (data, cb) ->
    window.order = this
    @data = data
    @inputs = {}
    @tab = {
      buttons: {
        container: $(".basket_step-block")
        basket: $(".basket_step-block [data-tab='list']")
        order: $(".basket_step-block [data-tab='ordering']")
      }
    }
    @sending = false
    @updateBasket = false
    @form = $("#ORDER_FORM")
    @content = @form.find("#order_form_content")
    @ajaxResult = @ajaxResult.bind(this)
    @token = "603780ba9dc89ca3a01bdc215b0d5314db1598a1"
    @googlePayClient = new google.payments.api.PaymentsClient({
      environment: 'PRODUCTION', #PRODUCTION
    });
    @initPlugins(true);

    @setViews();
    @needScrollUp = false
    $(document).on('click','.back-order-button', (e)=>
      e.preventDefault();
      @form.find('[name=step]').val(1)
      @submit()
    )
    $(window).on('load',()=>
      try
        @form.find('[name=track]').val(window.b24Tracker.guest.getTrace())

      catch e
        
    );

    @successModal = $("#order-success");
    @cityData = {}

    @form.submit((e)->
      e.preventDefault();
    );
    @setViewsData()

    @initForm()
    @fields = []
    @addValidateFields()
    @initCoupon();
    @initBonusCard();
    @markers = []
    cb?(this);
  initCoupon: () ->
    coupon = null
    if @data.COUPON_LIST.length > 0
      coupon = @data.COUPON_LIST[0].COUPON
    window.coupon = new Coupon(coupon, @data.COUPON_LIST[0])
  initBonusCard: () ->
    window.bonus_card = new BonusCard(@form)

  setViews: () ->
    @views = {
      items: $(".order-count")
      totalPrice: $(".order-total")
      discount: $(".order-discount .value")
      discountContainer: $(".order-discount")
      delivery: $(".order-delivery")
      total: $(".order-price .value, .order-price-value"),
      basket: $(".basket__product"),
      totalPriceWithoutDiscount: $(".order-real-total")
    }
  process: () ->
    if @tab.buttons.order.is(".active")
      @form.form("validate form")
    else
      @tab.buttons.order.click();


  submit: (value = "N", scrollUp = false) ->
    if @sending
      return
    @sending = true;
    @form.addClass("loading")
    @setErrorState(false)
    if value != "Y"
      @form.find("#confirmorder").val("N")
    else
      if (@form.find('[data-code=not_me]').is(':checked'))
        if (!@form.find('[data-code=not_me_email]').val())
          @form.find('[data-code=not_me_email]').val(@form.find('[data-code=email]').val())
        if (!@form.find('[data-code=not_me_phone]').val())
          @form.find('[data-code=not_me_phone]').val(@form.find('[data-code=phone]').val())
    $.post(@form[0].action, @form.serialize(), @ajaxResult);

  ajaxResult: (result) ->
    try
      json = JSON.parse(result)
      if json.error or json.success == "N"

      else if json.redirect
        @successModal.modal("show")
        @successModal.find("a").attr("href", json.redirect)
        window.top.location.href = json.redirect
    catch error
      @content.html(result);
      if @needScrollUp
        @needScrollUp = false
        $('html,body').animate({scrollTop:0})
    finally
      @sending = false;
      @form.removeClass("loading")
      @inputs = {}
      @initPlugins()
      @loadMap();


  updateData: (data) ->
    @data = data;
    @setViews();
    @setViewsData();
    @initForm();
    @initCoupon();
    @initBonusCard()
    @addValidateFields();

  setFinal: () ->
#    html = $("[data-tab='ordering'].ui.tab").html()
#    $(".basket-page").html(html)
#window.location.assign("/personal-area/basket/complete/");

  setViewsData: () ->
    if (!@data.GRID.ROWS)
      @data.GRID.ROWS = []
    count = 0
    Object.keys(@data.GRID.ROWS).forEach((key)=>
      column = @data.GRID.ROWS[key];
      count += column.data.QUANTITY;
    );
    @views.items.text(count)
    @views.totalPrice.text(@data.TOTAL.ORDER_TOTAL_PRICE_FORMATED)
    if @data.TOTAL.DISCOUNT_PRICE > 0 && @views.discountContainer.hasClass("hidden")
      @views.discountContainer.transition("scale in")
    else if @views.discountContainer.hasClass("visible") && @data.TOTAL.DISCOUNT_PRICE == 0
      @views.discountContainer.transition("scale out")
    @views.discount.text("-" + @data.TOTAL.DISCOUNT_PRICE_FORMATED)
    @views.delivery.text(@data.TOTAL.DELIVERY_PRICE_FORMATED)
    @views.total.text(@data.TOTAL.ORDER_PRICE_FORMATED)
    @views.totalPriceWithoutDiscount.text(@data.TOTAL.PRICE_WITHOUT_DISCOUNT)
    if @data.BASKET_HTML? && @updateBasket
      @updateBasket = false
      @views.basket.html(@data.BASKET_HTML)
      window.favorite?.update();

  initPlugins: (phoneOnly = false) ->
    $self = @
    @form.find(".ui.accordion").accordion()
    window.init.sticky(@form)
    isRussia = $("#countryName").val() == 'Россия'
    onSelectNothing = () ->
      @value = '' if isRussia
    if isRussia
      @form.find("[data-code='phone'],[data-code='not_me_phone']").inputmask("+7 (999) 999-99-99");
    else
      @form.find("[data-code='phone'],[data-code='not_me_phone']").inputmask("+9{1,2} (999) 999-99-99");
    region = @form.find('#regionName').val();
    constraints = {}
    if !region
      constraints = {
        deletable: false,
        label: false,
        locations: {
          city: @form.find("#cityName").val()
        }
      }
    @form.find("[data-code='street']").suggestions({
      token: @token,
      type: "ADDRESS",
      count: 5,
      autoSelectFirst: true,
      bounds: "street",
      constraints: constraints,
      restrict_value: true,
      onSelect: (suggestion, changed) ->
        @value = if suggestion.data.street_type == 'ул' then suggestion.data.street else suggestion.data.street_with_type
        $self.getInput("zip").val(suggestion.data.postal_code) if changed
      onSelectNothing
    });

    @form.find("[data-code='house']").suggestions({
      token: @token,
      type: "ADDRESS",
      hint: false,
      bounds: "house",
      constraints: @form.find("[data-code='street']")
      onSelect: (suggestion, changed) ->
        @value = suggestion.data.house
        $self.getInput("corps").val(suggestion.data.block)
        $self.getInput("zip").val(suggestion.data.postal_code) if changed
      onSelectNothing
    });
    @form.find('[data-code="full-address"]').suggestions({
      token: @token,
      type:'ADDRESS',
      count: 5,
      autoSelectFirst: true,
      constraints: constraints,
      restrict_value: true,
      onSelect: (suggestion, changed) ->
        $self.getInput("street").val(suggestion.data.street)
        $self.getInput("house").val(suggestion.data.house)
        $self.getInput("flat").val(suggestion.data.flat)
        $self.getInput("corps").val(suggestion.data.block)
        $self.getInput("zip").val(suggestion.data.postal_code) if changed
      onSelectNothing: () ->
        $self.getInput("street").val(@value)
        $self.getInput("house").val('')
        $self.getInput("flat").val('')
        $self.getInput("corps").val('')
    })
    @form.find("[data-tab]:not(.tab)").tab({
      onVisible: (tabPath) ->
        $("##{tabPath}").attr('checked', true)
        if($(this).find(".radio-simple").length != 0 && $(this).find(".radio-simple input:checked").length == 0)
          $(this).find(".radio-simple input:radio").first().attr('checked', true)
        $self.submit()
    });

    @form.find("[data-code='inn'],[data-code='company'],[data-code='kpp'],[data-code='company_adr']").suggestions({
      token: @token
      type: "PARTY",
      onSelect: (suggestion, changed) =>
        @getInput("inn").val(suggestion.data.inn);
        @getInput("kpp").val(suggestion.data.kpp);
        @getInput("company").val(suggestion.value);
        @getInput("company_adr").val(suggestion.data.address.unrestricted_value);

      onSelectNothing
    });

    @form.find('.ui.search.locations').each(() ->
      $(this).search({
        apiSettings: {
          url: "locations.get/{query}"
        },
        fields: {
          title: "name",
          description: "text"
        },
        errors: {
          noResults: "По Вашему запросу ничего не найдено"
        },
        onSelect: (result, response) ->
          value = result.value
          oldValue = $("#cityId").val()
          if oldValue == value
            return
          $self.cityData[oldValue] = {
            id: $(".useraddress select").val(),
            street: $self.form.find("[data-code='street']").val(),
            corps: $self.form.find("[data-code='corps']").val(),
            house: $self.form.find("[data-code='house']").val(),
            zip: $self.form.find("[data-code='ZIP']").val(),
            flat: $self.form.find("[data-code='flat']").val()
          }
          if $self.cityData[value]
            $(".useraddress select").val($self.cityData[value].id)
            $self.form.find("[data-code='street']").val($self.cityData[value].street)
            $self.form.find("[data-code='corps']").val($self.cityData[value].corps)
            $self.form.find("[data-code='house']").val($self.cityData[value].house)
            $self.form.find("[data-code='flat']").val($self.cityData[value].flat)
            $self.form.find("[data-code='ZIP'],[data-code='zip']").val($self.cityData[value].zip)
          else
            $(".useraddress select").val("")
            $self.form.find("[data-code='street']").val("")
            $self.form.find("[data-code='corps']").val("")
            $self.form.find("[data-code='house']").val("")
            $self.form.find("[data-code='flat']").val("")
            $self.form.find("[data-code='ZIP'],[data-code='zip']").val("")
          $("#cityId").val(value)
          $self.submit()
      });
    )

    @form.find('.ui.dropdown.useraddress').each(() ->
      $(this).dropdown({
        onChange: (value) ->
          if(window.userAddreses instanceof Array)
            cur = window.userAddreses.findIndex((item) ->
              item.id == value
            )
            currentObj = window.userAddreses[cur]
            if currentObj
              $self.form.find("[data-code='street']").val(currentObj.street)
              $self.form.find("[data-code='corps']").val(currentObj.corpse)
              $self.form.find("[data-code='house']").val(currentObj.house)
              $self.form.find("[data-code='flat']").val(currentObj.flat)
              $self.form.find("[data-code='ZIP']").val(currentObj.index)
            else
              $self.form.find("[data-code='street']").val("")
              $self.form.find("[data-code='corps']").val("")
              $self.form.find("[data-code='house']").val("")
              $self.form.find("[data-code='flat']").val("")
              $self.form.find("[data-code='ZIP']").val("")
          $self.submit()
      })
    )

    @form.find('.ui.dropdown.organization').each(() ->
      $(this).dropdown({
        onChange: (value) ->
          if(window.organizations instanceof Array)
            cur = window.organizations.findIndex((item) ->
              item.id == value
            )
            currentObj = window.organizations[cur]
            if currentObj
              $self.form.find("[data-code='COMPANY']").val(currentObj.name)
              $self.form.find("[data-code='INN']").val(currentObj.tin)
              $self.form.find("[data-code='KPP']").val(currentObj.msrn)
              $self.form.find("[data-code='CONTACT_PERSON']").val(currentObj.person)
              $self.form.find("[data-code='COMPANY_ADR']").val(currentObj.address)
              $self.form.find("[data-code='EMAIL']").val(currentObj.email)
              $self.form.find("[data-code='PHONE']").val(currentObj.phone)
# ОГРН, фактический адрес
            else
              $self.form.find("[data-code='COMPANY']").val("")
              $self.form.find("[data-code='INN']").val("")
              $self.form.find("[data-code='KPP']").val("")
              $self.form.find("[data-code='CONTACT_PERSON']").val("")
              $self.form.find("[data-code='COMPANY_ADR']").val("")
              $self.form.find("[data-code='EMAIL']").val("")
              $self.form.find("[data-code='PHONE']").val("")
          $self.submit()
      })
    )

    @googlePayClient.isReadyToPay({
      apiVersion: 2,
      apiVersionMinor: 0,
      allowedPaymentMethods: [{
        type: 'CARD',
        parameters: {
          allowedAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
          allowedCardNetworks: ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"]
        }
      }]
    }).then((response)=>
      if (response.result)
        @form.find('.check-google-pay').removeClass('hide')
    ).catch((e)=>

    );

    if window.ApplePaySession? && ApplePaySession.canMakePayments()
      @form.find('.check-apple-pay').removeClass('hide')

    if phoneOnly
      return

    @form.find("select:not(.locations):not(.useraddress)").each(()->
      $(@).dropdown($(@).data())
    )

  loadMap: () ->
    if @form.find("#pickPointMap").length
      if ($(".modals.page > .pickPointModal").length)
        $(".modals.page > .pickPointModal").remove()
      @pickModal = @form.find('.pickPointModal');
      @pickModal.find('.radio-group .item').tab({
        onVisible: (p,v,q) =>
          @pickModal.modal('refresh')
          if p == 'pick_map' && @moveMap
            @moveMap = false
            bound = new google.maps.LatLngBounds()
            for i of @markers
              bound.extend @markers[i].getPosition()
            @map.fitBounds bound
      })
      @moveMap = true
      @pickModal.modal();
      @pickModal.find('tbody tr').click((e)->
        $(".pickPointModal").modal('hide')
        $tr = $(this)
        if $tr.hasClass('active')
          return;
        window.order.setSelected($tr.data('point'))
      );
      @form.find(".pickPointTrigger .value").click((e)=>
        e.preventDefault();
        @pickModal.modal("show");
      )
      center = {lat: 55.751574, lng: 37.573856};
      @map = map = new google.maps.Map(document.getElementById('pickPointMap'), {
        zoom: 9,
        center: center,
        mapTypeControlOptions: {
          mapTypeIds: ['Silver']
        },
        fullscreenControl: false
      });
      mapType = new google.maps.StyledMapType(silverStyle, {name: "Карта"});
      map.mapTypes.set('Silver', mapType);
      map.setMapTypeId('Silver');
      iconAnchor = new google.maps.Point(-.5, -1)
      iconSize = new google.maps.Size(24, 32)
      icon = {
        url: '/local/templates/cameo-v3/images/map-marker.svg',
        anchor: iconAnchor,
        size: iconSize
      }
      iconActive = {
        url: '/local/templates/cameo-v3/images/map-marker-active.svg',
        anchor: iconAnchor,
        size: iconSize
      };
      window.mapIconActive = iconActive;
      window.mapIcon = icon;
      $(window).trigger("pvzReady")
  setSelected: (value, pan = true, setInSelect = false, closeInfoWindow = false) ->
    marker = @markers.filter((item)->
      item.setIcon(window.mapIcon);
      return item.id == value;
    )[0];
    if marker
      marker.setIcon(window.mapIconActive);
      if pan
        @map.panTo(marker.getPosition());
      if setInSelect
        $(".pvz-dropdown").dropdown("set selected", value)
      $(".pvz-value").val(value)
      $(".pickPointTrigger .value").val(marker.data.Address)
      $(".pickPointModal tr").removeClass("active").filter('[data-point="'+value+'"]').addClass('active')
      if closeInfoWindow
        new google.maps.event.trigger(marker, 'click');
  loadPvz: (list) ->
    @markers = []
    $.each(list, (key, item) =>
      marker = new google.maps.Marker({
        position: {
          lat: parseFloat(item.coordY)
          lng: parseFloat(item.coordX)
        },
        map: @map,
        icon: window.mapIcon,
      });
      marker.id = key;
      marker.data = item;
      infowindow = new google.maps.InfoWindow();
      string = [
        '<address>',
        "<strong>#{item.Name}</strong>",
        '<br/>',
        item.Address,
        '<br/>',
        item.WorkTime,
        '</address>',
        "<div class='ui primary mini button' onclick='window.order.setSelected(\"#{item.Code}\", false, true, true);'>Выбрать</div>"
      ].join('')
      infowindow.setContent(string)

      isOpen = false
      marker.addListener('click', () =>
        if !isOpen
          infowindow.open(@map, marker);
        else
          infowindow.close()
        isOpen = !isOpen
      );
      @markers.push(marker)
    );
    bound = new google.maps.LatLngBounds()
    for i of @markers
      bound.extend @markers[i].getPosition()
    @map.fitBounds bound

  getInput: (code) ->
    if !@inputs[code]
      @inputs[code] = @form.find("[data-code='#{code}']")
    return @inputs[code]

  setErrorState: (state = false) ->
    if state
      @form.addClass("error")
    else
      @form.removeClass("error")

  initForm: () ->
    @form.form({
      fields: {
        public_offer: {
          rules: [{type:"checked"}]
        },
        data_processing: {
          rules: [{type:"checked"}]
        }
        code: {
          rules:[{type:"not[qwerty]"}]
        },
        pick_point: {
          rules:[{type:'empty'}]
        },
        "full-address":{
          rules:[{type:'empty'}]
        },
        cityLine:{
          rules:[{type:'empty'}]
        }
      },
      inline: true
      onSuccess: () =>
        if (!$("[name=DELIVERY_ID]:checked").length)
          @form.form('add errors',['Отсутствует доставка'])
          return
        if @data.STEP
          if @data.STEP == 1
            @form.find("[name=step]").val(2);
            @needScrollUp = true;
            @submit()
          else
            if (!$("[name=PAY_SYSTEM_ID]:checked").length)
              @form.form('add errors',['Отсутствует способ оплаты'])
              return
            @submit('Y')
        else
          @submit("Y");
      onFailure: () =>
        if @form.find(".field.error").eq(0)
          $("html,body").stop().animate({
            scrollTop: @form.find(".field.error").eq(0).offset().top - $("header").height() - 40
          })
    })
  addValidateFields: () ->
    @fields = []
    object = {}

    @data.ORDER_PROP.properties.forEach((prop) =>
      if prop.REQUIRED == "Y"
        field = "ORDER_PROP_" + prop.ID
        object[field] = {
          rules: [
            {type:"empty"}
          ]
        }
      else if prop.CODE.match("not_me_fio")
        object["ORDER_PROP_" + prop.ID] = {
          depends: 'not_me',
          rules: [
            type:'empty'
          ]
        }
    )
    @form.form("add fields", object);
  reset: () ->
    this.updateData({
      TOTAL: {
        PRICE_WITHOUT_DISCOUNT: 0,
        DISCOUNT_PRICE: 0,
        DISCOUNT_PRICE_FORMATED: 0,
        DELIVERY_PRICE_FORMATED: 0,
        ORDER_PRICE_FORMATED: 0,
        ORDER_TOTAL_PRICE_FORMATED: 0
      },
      GRID: {
        ROWS: []
      },
      ORDER_PROP: {
        properties: []
      }
    });
    @content.html("<div class='ui error message'>В вашей корзине больше нет товаров, оформление заказа невозможно</div>")
    @form.addClass("error")
    @form.find(".submit").addClass("disabled")


class BonusCard
  constructor: (form) ->
    @form = form
    @input = @form.find('[name=bonus_withdraw_amount]')
    @send_trigger = @form.find('[name=bonus_withdraw_agreement]');
    @resend_trigger = @form.find('.resend-code');
    @before = @form.find('.withdrawal_before');
    @after = @form.find('.withdrawal_after');
    @code_inputs = @form.find('.code-input input');
    @amount_text = @form.find('.withdraw_amount')
    @submit_button = @form.find('.submit-button')
    @submit_button_text = @form.find('.submit-button-text')
    @cancel_button = @form.find('.cancel_withdraw')
    @accept_button = @form.find('.accept_withdraw')
    @version = +@form.find('.order-bonus-block').data('version')
    @amount = 0
    @form.find('.set-bonus-value').click((e)=>
      e.preventDefault();
      @input.val($(e.target).data('set'));
    );
    @send_trigger.change((e)=>
      if e.target.checked
        if !@input.val()
          @input.popup_error('Укажите количество бонусов для списания');
          @input.focus()
          @send_trigger.prop('checked', false)
          return
        max = +@input.prop('max')
        amount = +@input.val()
        if amount > max
          amount = max
          @input.val(amount)
        @input.prop('readonly', true);
        @submit_button.addClass("loading")
        $.post('/api/bonus.withdrawal_request/', {}, (response)=>
          @send_trigger.prop('readonly', false);
          @submit_button.removeClass('loading');
          if response.status != 200
            @input.prop('readonly', false);
            @input.popup_error(response.message)
            @send_trigger.prop('checked', false)
          else
            @amount_text.text(amount + " " + @pluralize(amount, 'бонус', "бонуса", "бонусов"))
            @submit_button.addClass("fluid").text('Списать бонусы и оформить заказ');
            @submit_button_text.text('Списать бонусы и оформить заказ');
            @before.transition('scale out', () =>
              @after.transition('scale in', () =>
                @code_inputs.val("").eq(0).focus();
              )
            )
        , 'json');
      else
        @input.prop('readonly', false);
    );
    @resend_trigger.click(()=>
      $.post('/api/bonus.withdrawal_request/', {return_error: true}, (response)=>
        if response.status != 200
          @resend_trigger.popup_error(response.message);
        else
          @resend_trigger.popup_success("Новый код успешно отправлен");
      , 'json')
    );
    @input.on('keydown', (e) =>
      if e.keyCode == 13
        @send_trigger.prop('checked', true).trigger("change")
        e.preventDefault()
        e.stopPropagation()
        e.stopImmediatePropagation()
        return false;
    )
    @code_inputs.on('input', (e) =>
      if e.target.value.length > 1
        e.target.value = e.target.value.slice(0, 1);
      if ["0", "1", '2', '3', '4', '5', '6', '7', '8', '9'].indexOf(e.target.value) == -1
        e.target.value = '';
        return
      next = $(e.target).next()
      if next.length
        next.focus()
    ).on("focus", () ->
      this.setSelectionRange(0, this.value.length)
    ).on('keydown', (e) ->
      if e.keyCode == 13
        e.preventDefault()
        e.stopPropagation();
        e.stopImmediatePropagation();
        next = $(e.target).next()
        if next.length
          next.focus()
      else if e.keyCode == 8
        e.target.value = ''
        prev = $(e.target).prev()
        if prev.length
          prev.focus()
    )
    @cancel_button.click((e)=>
      e.preventDefault();
      @code_inputs.val("")
      @send_trigger.prop('checked', false).trigger('change')
      @submit_button_text.text('Оформить заказ')
      if @version == 2
        @submit_button.text('Оформить заказ')
      else
        @submit_button.removeClass("fluid").text('Оформить заказ')
      @after.transition('scale out', () =>
        @before.transition('scale in', () =>

        )
      )
    )
    @accept_button.click((e)=>
      e.preventDefault();
      @code_inputs.val("")
      @send_trigger.prop('checked', true).trigger('change')
    )
  pluralize: (number, one, two, five) ->
    n = Math.abs(number)
    n %= 100
    if n >= 5 and n <= 20
      return five
    n %= 10
    if n == 1
      return one
    if n >= 2 and n <= 4
      return two
    five

$ ->
  window.onBasketUpdateSuccess = () ->
    order.submit();
# $("[data-modal='basket']").modal("hide")
#    order.updateBasket = true;
#    order.views.basket.append("<div class='ui active inverted dimmer'><div class='ui loader'></div></div>");

#  $(document).on("click", ".product_item .remove-from-basket", (e) ->
#    e.preventDefault()
#    if confirm("Вы уверены?")
#      $.post("/api/cart.remove/", {id: $(@).data("id")}, () =>
#        order.submit();
#        $(@).closest(".row").transition("scale out", () ->
#          $(@).remove();
#        )
#      );
#  )
#  $basketModal = $(".ui.modal[data-modal='basket']")
#  $basketModal.modal({
#    onHidden: () ->
#      $(@).find(".content").html("")
#  })
#  $(document).on("click", ".ui.bag .edit", (e) ->
#    e.preventDefault();
#    $basketModal.addClass("loading")
#    $basketModal.modal("show")
#    productId = $(@).data("product")
#    id = $(@).data("id")
#    link = $(@).data("link")
#    $.get(link, {
#      ajaxCall: "Y",
#      basketId: id
#    }, (data) ->
#      $basketModal.find(".content").html(data)
#      setTimeout(window.initJsForProducts, 0);
#      window.initTabs();
#      window.initDrift();
#      window.initForm($basketModal.find("form.ui.form"));
#
#      #$(`.ui.modal .heart-trigger[data-product-id="${id}"]`).addClass("active");
#      $basketModal.removeClass("loading")
#    );
#  )
#  timeout = null


