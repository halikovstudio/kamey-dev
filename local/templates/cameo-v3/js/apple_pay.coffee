#console.log = (any) ->
#  $(".console-log").append("<pre>"+JSON.stringify(any, null, 2)+"</pre>")
class ApplePay
  constructor: (@orderId, @amount, @instant = false, @container = '.apple-pay-button') ->
    @mdOrder = null;
    if window.ApplePaySession? && ApplePaySession.canMakePayments()
      @init();
  init: () ->
    $(@container).removeClass('hide').on('click', ()=>
      @startSession({
        currencyCode: 'RUB',
        countryCode: 'RU',
        merchantCapabilities: [
          'supports3DS',
        ],
        supportedNetworks: ['amex', 'masterCard', 'maestro', 'visa', 'mada'],
        total: {
          label: "Cameo",
          amount: @amount,
          type: 'final'
        }
      });
    );
    if @instant
      $(@container).click();
  startSession: (data) ->
    applePaySession = new ApplePaySession(3, data);
    applePaySession.onvalidatemerchant = (event)=>
      @validateSession(event.validationURL, (merchantSession) =>
        applePaySession.completeMerchantValidation(merchantSession)
      );
    applePaySession.onpaymentauthorized = (event) =>
      @performTransaction(event.payment, (outcome)=>
        if (outcome? && outcome.approved)
          applePaySession.completePayment(ApplePaySession.STATUS_SUCCESS);
          if (outcome.redirectUrl)
            location.href = outcome.redirectUrl;
        else
          applePaySession.completePayment(ApplePaySession.STATUS_FAILURE)
      )
    applePaySession.begin();
  validateSession: (appleUrl, callback) ->
    $.post('/api/payment.apple.validate', {appleUrl, orderId: @orderId}, (response) =>
      callback(response.data);
      @mdOrder = response.mdOrder;
    )
  performTransaction: (details, callback) ->
    console.log(details.token.paymentData);
    $.post('/api/payment.apple.pay', {
      token: JSON.stringify(details.token.paymentData),
      orderId: @orderId,
      mdOrder: @mdOrder
    }, (response)=>
      console.log(response);
      callback(response.data)
    , 'json').fail((xhr)=>
      callback({})
    );

