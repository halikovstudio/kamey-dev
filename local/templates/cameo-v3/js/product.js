// Generated by CoffeeScript 1.12.5
var product;

product = (function() {
  function product(params) {
    var $product, ref, ref1;
    this.version = ((ref = window.cameo) != null ? ref.version : void 0) || 3;
    this.selected = params.OFFER_SELECTED;
    this.offers = params.OFFERS || {};
    this.info = params.PRODUCT;
    this.offerProps = params.TREE_PROPS;
    this.tree = (ref1 = this.offers[this.selected]) != null ? ref1.TREE : void 0;
    this.colorPropName = params.colorPropName ? params.colorPropName : "PROP_669";
    this.findCache = {};
    this.updateCache = {};
    this.product = $(".catalog-detail[data-uid='" + this.info.UID + "']");
    this.offer = this.offers[this.selected];
    this.firstEmit = true;
    this.isAjax = params.is_ajax;
    this.updateViewCount(this.info.ID);
    this.find(".param input[type='radio']").change(this.processChange.bind(this));
    this.find(".param :checked").eq(0).trigger("change");
    this.find(".sub-trigger").change(this.processSubChange.bind(this));
    this.images = this.find(".images");
    this.imagesSlider = null;
    this.window = $(window);
    this.setImages();
    this.window.on("type", (function(_this) {
      return function(e, type) {
        _this.resetImages();
        return _this.setImages();
      };
    })(this));
    if (this.offer != null) {
      this.setDelivery();
      this.setTransit();
    } else {
      this.hideDelivery();
    }
    this.content = new Content(this.product);
    if (!window.content) {
      window.content = {};
    }
    window.content[this.info.UID] = this.content;
    $product = this;
    this.find(".content-trigger").click(function(e) {
      var data;
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      data = $(this).data();
      return $product.content.toggle(data.target, data.list, data.transition);
    });
    this.product.on("content-visible", (function(_this) {
      return function(e, target) {
        if (target === 'catalog-filter') {
          return _this.find(".cart-edit").text("Сохранить");
        }
      };
    })(this)).on("content-hidden", (function(_this) {
      return function(e, target) {
        if (target === 'catalog-filter') {
          return _this.find(".cart-edit").text("Изменить");
        }
      };
    })(this));
    this.window.on('refreshImages', (function(_this) {
      return function() {
        _this.resetImages();
        return _this.setImages();
      };
    })(this));
    this.detectSize();
    this.window.on('refreshSize', (function(_this) {
      return function() {
        return _this.detectSize();
      };
    })(this));
    this.updateFavorite();
  }

  product.prototype.updateFavorite = function() {
    var ref, uid;
    uid = this.info.ID + ':color:' + this.tree[this.colorPropName];
    this.product.find('.info .favorite-trigger').removeClass('active').attr('data-target', uid).data('target', uid);
    return (ref = window.favorite) != null ? ref.update() : void 0;
  };

  product.prototype.detectSize = function() {
    var nearestSize, values;
    if ((window.size_info != null)) {
      values = {};
      this.find('[data-code="RAZMER_ODEZHDA"] .ui.checkbox:not(.hide)').each(function() {
        return values[+this.innerText] = $(this).find('input');
      });
      nearestSize = this.closest(+window.size_info.ROSSIYA, Object.keys(values));
      values[nearestSize].prop("checked", true).trigger('change');
      values = {};
      this.find('[data-code="ROST"] .ui.checkbox:not(.hide)').each(function() {
        return values[+this.innerText] = $(this).find('input');
      });
      nearestSize = this.closest(+window.size_info.height, Object.keys(values));
      return values[nearestSize].prop("checked", true).trigger('change');
    }
  };

  product.prototype.closest = function(x, arr) {
    var indexArr, min;
    indexArr = arr.map(function(k) {
      return Math.abs(k - x);
    });
    min = Math.min.apply(Math, indexArr);
    return arr[indexArr.indexOf(min)];
  };

  product.prototype.setImages = function() {
    this.images = this.find(".images", true);
    this.images.lightGallery({
      selector: ".fit-container",
      download: false,
      zoom: this.window.width() < 992
    });
    if (this.window.width() < 992 && !this.imagesSlider) {
      this.current = 1;
      if (this.version === 3) {
        this.imagesSlider = this.images.lightSlider({
          item: 1,
          slideMargin: 0,
          vertical: true,
          verticalHeight: (this.window.width() - 48) * 1.33,
          onBeforeSlide: (function(_this) {
            return function(el) {
              if (_this.current === el.getTotalSlideCount() && (el.getTotalSlideCount() < el.getCurrentSlideCount() || el.getTotalSlideCount() === el.getCurrentSlideCount())) {
                _this.showRelated();
              }
              return _this.current = el.getCurrentSlideCount();
            };
          })(this)
        });
      } else {
        this.images.attr('data-state', 'begin');
        this.imagesSlider = this.images.lightSlider({
          item: 1.15,
          slideMargin: 20,
          onBeforeSlide: (function(_this) {
            return function(el) {
              var state;
              state = 'middle';
              if (el.getCurrentSlideCount() === 1) {
                state = 'begin';
              } else if (el.getTotalSlideCount() === el.getCurrentSlideCount()) {
                state = 'end';
              }
              _this.images.attr('data-state', state);
              return _this.current = el.getCurrentSlideCount();
            };
          })(this)
        });
      }
      return this.images.data("lightSlider", this.imagesSlider);
    } else if (this.imagesSlider) {
      this.imagesSlider.destroy();
      return this.imagesSlider = null;
    }
  };

  product.prototype.resetImages = function() {
    var ref, ref1, ref2;
    if ((ref = this.images.data("lightSlider")) != null) {
      ref.destroy();
    }
    this.images.data("lightSlider", null);
    if ((ref1 = this.imagesSlider) != null) {
      ref1.destroy();
    }
    this.imagesSlider = null;
    return (ref2 = this.images.data("lightGallery")) != null ? ref2.destroy(true) : void 0;
  };

  product.prototype.showRelated = function() {
    if (this.find("#see-also").length) {
      return this.content.toggle("see-also", '.content-trigger[data-target="catalog-info"],.lSSlideOuter,.navigation.mobile-only,.cart-add,.info>.sticky>.content', 'fade up');
    }
  };

  product.prototype.find = function(selector, resetCache) {
    if (resetCache == null) {
      resetCache = false;
    }
    if (typeof selector === "string") {
      if (!this.findCache[selector] || resetCache) {
        this.findCache[selector] = this.product.find(selector);
      }
      return this.findCache[selector];
    }
    return this.product.find(selector);
  };

  product.prototype.setDelivery = function() {
    var price, ref, ref1, ref2, weight;
    weight = +this.info.WEIGHT;
    price = (ref = this.offer) != null ? (ref1 = ref.ITEM_PRICES[0]) != null ? ref1.PRICE : void 0 : void 0;
    if (price) {
      return $.get("/api/delivery.product/", {
        weight: weight,
        price: price,
        id: (ref2 = this.offer) != null ? ref2.ID : void 0
      }, (function(_this) {
        return function(data) {
          _this.find("#delivery-list").html(data.html);
          if (!data.error) {
            _this.find('.delivery-info').transition('fade in');
            _this.find('.delivery-info .value').text(data.cityCase);
            _this.find('.delivery-info .delivery-text').text(data.deliveryText);
          }
          if (!data.html) {
            return _this.hideDelivery();
          }
        };
      })(this));
    }
  };

  product.prototype.hideDelivery = function() {
    return this.find("#delivery-list").html("<tr><td colspan='3'>Нет доступных служб доставки</td></tr>");
  };

  product.prototype.setTransit = function() {
    this.checkInterval = setInterval((function(_this) {
      return function() {
        return _this.setTransitView();
      };
    })(this), 60000);
    return this.setTransitView();
  };

  product.prototype.setTransitView = function() {
    var day, ekatTime, ekatTimeZoneOffset, howDelivery, month, nextWorkDate, now, nowUTC, ref, tomorrowDay;
    now = new Date;
    nowUTC = new Date(now.getTime() + now.getTimezoneOffset() * 60 * 1000);
    ekatTimeZoneOffset = 5;
    ekatTime = void 0;
    howDelivery = '';
    ekatTime = new Date(nowUTC.getTime() + ekatTimeZoneOffset * 3600 * 1000);
    tomorrowDay = ekatTime.getDay() + 1;
    if (tomorrowDay === 6 || tomorrowDay === 7) {
      if (tomorrowDay === 6) {
        nextWorkDate = new Date(now.getTime() + 3 * 24 * 3600 * 1000);
        month = nextWorkDate.getMonth() + 1;
        if (month < 10) {
          month = "0" + month;
        }
        day = nextWorkDate.getDate();
        if (day < 10) {
          day = "0" + day;
        }
        if (((ref = window.cameo) != null ? ref.version : void 0) === 4) {
          howDelivery += day + '.' + month;
        } else {
          howDelivery += day + '.' + month + '.' + nextWorkDate.getFullYear();
        }
      } else {
        howDelivery += 'завтра';
      }
      clearInterval(this.checkInterval);
    } else if (ekatTime.getHours() < 12) {
      howDelivery += 'сегодня';
    } else {
      howDelivery += 'завтра';
      clearInterval(this.checkInterval);
    }
    return this.find(".send-value").transition("fade in").text(howDelivery);
  };

  product.prototype.updateViewCount = function(productId, parentId) {
    if (this.updateCache[productId + ":" + parentId]) {
      return false;
    }
    return $.post("/bitrix/components/bitrix/catalog.element/ajax.php", {
      AJAX: "Y",
      SITE_ID: "s2",
      PRODUCT_ID: productId,
      PARENT_ID: parentId
    }, (function(_this) {
      return function() {
        return _this.updateCache[productId + ":" + parentId] = true;
      };
    })(this));
  };

  product.prototype.processChange = function(e) {
    var $input, $param, code, id, value;
    $input = $(e.target);
    value = $input.val();
    $param = $input.closest('.param');
    code = $param.data('code');
    id = $param.data("id");
    if (code === 'COLOR') {
      $param.attr('data-title', $input.next('label').prop('title'));
    }
    return this.findOffer(id, value);
  };

  product.prototype.processSubChange = function(e) {
    var $input, value;
    $input = $(e.target);
    value = $input.val();
    return this.find(".param [value='" + value + "']").click();
  };

  product.prototype.getStrName = function(i) {
    return "PROP_" + this.offerProps[i].ID;
  };

  product.prototype.getShowValues = function(arFilter, strName) {
    var arValues, i, j, oneSearch, search;
    arValues = [];
    i = 0;
    j = 0;
    search = false;
    oneSearch = true;
    if (arFilter.length === 0) {
      this.offers.forEach((function(_this) {
        return function(offer) {
          var value;
          value = offer.TREE[strName];
          if (!~arValues.indexOf(value)) {
            return arValues.push(value);
          }
        };
      })(this));
      search = true;
    } else {
      this.offers.forEach((function(_this) {
        return function(offer) {
          oneSearch = true;
          for (j in arFilter) {
            if (arFilter[j] !== offer.TREE[j]) {
              oneSearch = false;
              break;
            }
          }
          if (oneSearch) {
            if (!~arValues.indexOf(offer.TREE[strName])) {
              arValues.push(offer.TREE[strName]);
            }
            return search = true;
          }
        };
      })(this));
    }
    if (search) {
      return arValues;
    } else {
      return false;
    }
  };

  product.prototype.findOffer = function(id, value) {
    var allValues, arFilter, i, index, showList, showValues, strName;
    id = id.toString();
    value = value.toString();
    index = -1;
    arFilter = {};
    this.offerProps.forEach((function(_this) {
      return function(item, i) {
        if (item.ID === id) {
          return index = i;
        }
      };
    })(this));
    if (index > -1) {
      i = 0;
      while (i < index) {
        strName = this.getStrName(i);
        arFilter[strName] = this.tree[strName];
        i++;
      }
      strName = this.getStrName(index);
      arFilter[strName] = value;
      i = index + 1;
      showList = {};
      while (i < this.offerProps.length) {
        strName = this.getStrName(i);
        showValues = this.getShowValues(arFilter, strName);
        if (!showValues) {
          break;
        }
        allValues = [];
        if (this.tree[strName] && ~showValues.indexOf(this.tree[strName])) {
          arFilter[strName] = this.tree[strName];
        } else {
          arFilter[strName] = showValues[0];
        }
        showList[this.offerProps[i].ID] = showValues;
        i++;
      }
      this.tree = arFilter;
      return this.getOffer(showList);
    }
  };

  product.prototype.getOffer = function(showList) {
    var id, previousTree, prop;
    previousTree = this.offer.TREE;
    this.offers.every((function(_this) {
      return function(offer, index) {
        var all, key;
        all = true;
        for (key in offer.TREE) {
          if (_this.tree[key].toString() !== offer.TREE[key].toString()) {
            all = false;
          }
        }
        if (all === true) {
          _this.selected = index;
          return false;
        } else {
          return true;
        }
      };
    })(this));
    this.offer = this.offers[this.selected];
    for (id in showList) {
      this.find(".param[data-id='" + id + "']").find(".checkbox").addClass("hide");
      showList[id].forEach((function(_this) {
        return function(value) {
          return _this.find("[value='" + value + "']").closest(".checkbox").removeClass("hide");
        };
      })(this));
    }
    for (prop in this.tree) {
      this.find("[value='" + this.tree[prop] + "']").prop("checked", true);
    }
    return this.updateData(previousTree[this.colorPropName] !== this.tree[this.colorPropName]);
  };

  product.prototype.appendImages = function() {
    this.offer.SLIDER.forEach((function(_this) {
      return function(image, index) {
        _this.images.append("<div class='column'><a href='" + image.SRC + "' class='fit-container transition hidden'><img src='" + image.RESIZE + "'/></a></div>");
        return _this.images.find("img:last-child").one("load", function() {
          return $(this).parent().transition("fade in");
        });
      };
    })(this));
    return this.setImages();
  };

  product.prototype.updateGallery = function() {
    var $this;
    this.resetImages();
    $this = this;
    if ($(window).width() < 992) {
      this.images.find(".column").remove();
      this.offer.SLIDER.forEach((function(_this) {
        return function(image, index) {
          return _this.images.append("<div class='column'><a href='" + image.SRC + "' class='fit-container'><img src='" + image.RESIZE + "'/></a></div>");
        };
      })(this));
      return this.setImages();
    } else {
      return this.images.find(".fit-container").transition({
        interval: 100,
        duration: 200,
        animation: "fade out",
        reverse: true,
        onComplete: function() {
          if ($(this).parent().is(":first-child")) {
            $this.images.find(".column").remove();
            return $this.appendImages();
          }
        }
      });
    }
  };

  product.prototype.updateForm = function() {
    return this.find("form").find('[name="id"]').val(this.offer.ID);
  };

  product.prototype.updatePrices = function() {
    if (this.offer.ITEM_PRICES[0]) {
      return this.find(".price").not(".see-also .price").text(this.offer.ITEM_PRICES[0].PRINT_PRICE);
    }
  };

  product.prototype.updateData = function(updateGallery) {
    var link;
    if (updateGallery == null) {
      updateGallery = true;
    }
    this.find(".offer-properties tbody").html(this.offer.DISPLAY_PROPERTIES);
    console.log(this.info);
    link = this.offer.DETAIL_PAGE_URL ? this.offer.DETAIL_PAGE_URL : this.info.LINK;
    if (link && (window.history != null)) {
      window.history.replaceState({}, '', link);
    }
    this.updateViewCount(this.offer.ID, this.info.ID);
    if (!this.firstEmit && updateGallery) {
      this.updateGallery();
    } else {
      this.firstEmit = false;
    }
    this.updateForm();
    this.updatePrices();
    if (this.find(".cart-add").hasClass("added")) {
      this.find(".cart-add").removeClass("added").text(this.find(".cart-add").data("default"));
    }
    return this.updateFavorite();
  };

  return product;

})();
