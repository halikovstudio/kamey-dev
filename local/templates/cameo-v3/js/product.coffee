class product
  constructor: (params)->
    @version = window.cameo?.version || 3
    @selected = params.OFFER_SELECTED
    @offers = params.OFFERS || {}
    @info = params.PRODUCT
    @offerProps = params.TREE_PROPS
    @tree = @offers[@selected]?.TREE
    @colorPropName = if params.colorPropName then params.colorPropName else "PROP_669"
    @findCache = {}
    @updateCache = {}
    @product = $(".catalog-detail[data-uid='#{@info.UID}']")
    @offer = @offers[@selected]
    @firstEmit = true
    @isAjax = params.is_ajax;
    @updateViewCount(@info.ID)
    @find(".param input[type='radio']").change(@processChange.bind(@))
    @find(".param :checked").eq(0).trigger("change");
    @find(".sub-trigger").change(@processSubChange.bind(@));
    @images = @find(".images");
    @imagesSlider = null
    @window = $(window)
    @setImages();
    @window.on("type", (e, type) =>
      @resetImages();
      @setImages();
    )
    if @offer?
      @setDelivery()
      @setTransit()
    else
      @hideDelivery();
    @content = new Content(@product);
    if !window.content
      window.content = {}
    window.content[@info.UID] = @content;
    $product = @
    @find(".content-trigger").click((e)->
      e.preventDefault();
      e.stopPropagation()
      e.stopImmediatePropagation();
      data = $(@).data()
      $product.content.toggle(data.target, data.list, data.transition);
    )
    @product.on("content-visible", (e, target) =>
      if target == 'catalog-filter'
        @find(".cart-edit").text("Сохранить")
    ).on("content-hidden", (e, target) =>
      if target == 'catalog-filter'
        @find(".cart-edit").text("Изменить")
    )
    @window.on('refreshImages', ()=>
#console.log('refreshImages: ', @imagesSlider);
#$('.copyright').text('refreshImages: '+(if @imagesSlider != null then 'have' else "not have"));
      @resetImages();
      @setImages();
#@imagesSlider?.refresh()
    )
    #$.getScript("https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b7834d1b98fa42")
    @detectSize();
    @window.on('refreshSize', () =>
      @detectSize();
    )
    @updateFavorite()

  updateFavorite: () ->
    uid = @info.ID+':color:'+@tree[@colorPropName]
    @product.find('.info .favorite-trigger').removeClass('active').attr('data-target',uid).data('target',uid);
    window.favorite?.update()

  detectSize: () ->
    if (window.size_info?)
      values = {};
      @find('[data-code="RAZMER_ODEZHDA"] .ui.checkbox:not(.hide)').each(()->
        values[+this.innerText] = $(this).find('input');
      );
      nearestSize = @closest(+window.size_info.ROSSIYA, Object.keys(values));
      values[nearestSize].prop("checked", true).trigger('change');
      values = {};
      @find('[data-code="ROST"] .ui.checkbox:not(.hide)').each(()->
        values[+this.innerText] = $(this).find('input');
      );
      nearestSize = @closest(+window.size_info.height, Object.keys(values));
      values[nearestSize].prop("checked", true).trigger('change');

  closest: (x, arr) ->
    indexArr = arr.map((k)->
      return Math.abs(k - x);
    );
    min = Math.min.apply(Math, indexArr);
    return arr[indexArr.indexOf(min)];

  setImages: () ->
    @images = @find(".images", true)
    @images.lightGallery({
      selector: ".fit-container"
      download: false,
      zoom: @window.width() < 992
    })
    if @window.width() < 992 && !@imagesSlider
      @current = 1
      if @version == 3
        @imagesSlider = @images.lightSlider({
          item: 1
          slideMargin: 0,
          vertical: true,
#verticalHeight: @window.height() - 137 - 169 - 20 + 40
          verticalHeight: (@window.width() - 48) * 1.33,
          onBeforeSlide: (el) =>
            if @current == el.getTotalSlideCount() and (el.getTotalSlideCount() < el.getCurrentSlideCount() or el.getTotalSlideCount() == el.getCurrentSlideCount())
              @showRelated()
            @current = el.getCurrentSlideCount();
        })
      else
        @images.attr('data-state', 'begin')
        @imagesSlider = @images.lightSlider({
          item: 1.15
          slideMargin: 20,
          onBeforeSlide: (el) =>
            state = 'middle'
            if el.getCurrentSlideCount() == 1
              state = 'begin'
            else if el.getTotalSlideCount() == el.getCurrentSlideCount()
              state = 'end'
            @images.attr('data-state', state)
            @current = el.getCurrentSlideCount();
        })
      @images.data("lightSlider", @imagesSlider)
    else if @imagesSlider
      @imagesSlider.destroy()
      @imagesSlider = null;
  resetImages: () ->
    @images.data("lightSlider")?.destroy();
    @images.data("lightSlider", null)
    @imagesSlider?.destroy();
    @imagesSlider = null;
    @images.data("lightGallery")?.destroy(true);
  showRelated: () ->
    if @find("#see-also").length
      @content.toggle("see-also", '.content-trigger[data-target="catalog-info"],.lSSlideOuter,.navigation.mobile-only,.cart-add,.info>.sticky>.content', 'fade up');
  find: (selector, resetCache = false) ->
    if typeof selector == "string"
      if !@findCache[selector] || resetCache
        @findCache[selector] = @product.find(selector)
      return @findCache[selector]
    return @product.find(selector)

  setDelivery: () ->
    weight = +@info.WEIGHT
    price = @offer?.ITEM_PRICES[0]?.PRICE
    if price
      $.get("/api/delivery.product/", {
        weight,
        price,
        id: @offer?.ID
      }, (data) =>
        @find("#delivery-list").html(data.html)
        if !data.error
          @find('.delivery-info').transition('fade in');
          @find('.delivery-info .value').text(data.cityCase);
          @find('.delivery-info .delivery-text').text(data.deliveryText);
        if !data.html
          @hideDelivery()
      )
  hideDelivery: () ->
    @find("#delivery-list").html("<tr><td colspan='3'>Нет доступных служб доставки</td></tr>")
  setTransit: () ->
    @checkInterval = setInterval(()=>
      @setTransitView()
    , 60000)
    @setTransitView()

  setTransitView: () ->
    now = new Date
    nowUTC = new Date(now.getTime() + now.getTimezoneOffset() * 60 * 1000)
    ekatTimeZoneOffset = 5
    ekatTime = undefined
    howDelivery = ''
    ekatTime = new Date(nowUTC.getTime() + ekatTimeZoneOffset * 3600 * 1000)
    tomorrowDay = ekatTime.getDay() + 1
    if tomorrowDay == 6 or tomorrowDay == 7
      if tomorrowDay == 6
        nextWorkDate = new Date(now.getTime() + 3 * 24 * 3600 * 1000)
        month = nextWorkDate.getMonth() + 1;
        if month < 10
          month = "0" + month;
        day = nextWorkDate.getDate()
        if day < 10
          day = "0" + day
        if window.cameo?.version == 4
          howDelivery += day + '.' + month
        else
          howDelivery += day + '.' + month + '.' + nextWorkDate.getFullYear()
      else
        howDelivery += 'завтра'
      clearInterval @checkInterval
    else if ekatTime.getHours() < 12
      howDelivery += 'сегодня'
    else
      howDelivery += 'завтра'
      clearInterval @checkInterval
    @find(".send-value").transition("fade in").text(howDelivery)

  updateViewCount: (productId, parentId) ->
    if @updateCache[productId + ":" + parentId]
      return false
    $.post("/bitrix/components/bitrix/catalog.element/ajax.php", {
      AJAX: "Y",
      SITE_ID: "s2",
      PRODUCT_ID: productId,
      PARENT_ID: parentId
    }, () =>
      @updateCache[productId + ":" + parentId] = true
    );
  processChange: (e) ->
    $input = $(e.target)
    value = $input.val()
    $param = $input.closest('.param')
    code = $param.data('code')
    id = $param.data("id");
    if code == 'COLOR'
      $param.attr('data-title', $input.next('label').prop('title'))
    @findOffer(id, value);
  processSubChange: (e) ->
    $input = $(e.target)
    value = $input.val()
    @find(".param [value='#{value}']").click();
  getStrName: (i) ->
    return "PROP_" + @offerProps[i].ID
  getShowValues: (arFilter, strName) ->
    arValues = []
    i = 0
    j = 0
    search = false
    oneSearch = true
    if arFilter.length == 0
      @offers.forEach (offer) =>
        value = offer.TREE[strName]
        if !~arValues.indexOf(value)
          arValues.push(value)
      search = true
    else
      @offers.forEach (offer) =>
        oneSearch = true
        for j of arFilter
          if arFilter[j] != offer.TREE[j]
            oneSearch = false
            break;
        if oneSearch
          if !~arValues.indexOf(offer.TREE[strName])
            arValues.push(offer.TREE[strName])
          search = true
    return if search then arValues else false
  findOffer: (id, value) ->
    id = id.toString()
    value = value.toString()
    index = -1
    arFilter = {}
    @offerProps.forEach((item, i)=>
      if item.ID == id
        index = i
    );
    if index > -1
      i = 0
      while (i < index)
        strName = @getStrName(i);
        arFilter[strName] = @tree[strName]
        i++
      strName = @getStrName(index)
      arFilter[strName] = value
      i = index + 1
      showList = {}
      while (i < @offerProps.length)
        strName = @getStrName(i)
        showValues = @getShowValues(arFilter, strName)
        if !showValues
          break
        allValues = []
        if @tree[strName] && ~showValues.indexOf(@tree[strName])
          arFilter[strName] = @tree[strName]
        else
          arFilter[strName] = showValues[0]
        showList[@offerProps[i].ID] = showValues
        i++
      @tree = arFilter
      @getOffer(showList);
  getOffer: (showList) ->
    previousTree = @offer.TREE
    @offers.every (offer, index) =>
      all = true;

      for key of offer.TREE
        if @tree[key].toString() != offer.TREE[key].toString()
          all = false
      if all == true
        @selected = index;
        return false
      else
        return true;
    @offer = @offers[@selected]
    for id of showList
      @find(".param[data-id='#{id}']").find(".checkbox").addClass("hide")
      showList[id].forEach (value) =>
        @find("[value='#{value}']").closest(".checkbox").removeClass("hide")
    for prop of @tree
      @find("[value='#{@tree[prop]}']").prop("checked", true);
    @updateData(previousTree[@colorPropName] != @tree[@colorPropName]);
  appendImages: () ->
    @offer.SLIDER.forEach (image, index) =>
      @images.append("<div class='column'><a href='#{image.SRC}' class='fit-container transition hidden'><img src='#{image.RESIZE}'/></a></div>");
      @images.find("img:last-child").one("load", () ->
        $(@).parent().transition("fade in")
      )
    @setImages()

  updateGallery: () ->
    @resetImages();
    $this = @
    if $(window).width() < 992
      @images.find(".column").remove();
      @offer.SLIDER.forEach (image, index) =>
        @images.append("<div class='column'><a href='#{image.SRC}' class='fit-container'><img src='#{image.RESIZE}'/></a></div>");
      @setImages()
    else
      @images.find(".fit-container").transition({
        interval: 100,
        duration: 200,
        animation: "fade out",
        reverse: true
        onComplete: () ->
          if ($(@).parent().is(":first-child"))
            $this.images.find(".column").remove();
            $this.appendImages();
      })
  updateForm: () ->
    @find("form").find('[name="id"]').val(@offer.ID)
  updatePrices: () ->
    if @offer.ITEM_PRICES[0]
      @find(".price").not(".see-also .price").text(@offer.ITEM_PRICES[0].PRINT_PRICE)
  updateData: (updateGallery = true) ->
    @find(".offer-properties tbody").html(@offer.DISPLAY_PROPERTIES)
    console.log(@info);
    link = if @offer.DETAIL_PAGE_URL then @offer.DETAIL_PAGE_URL else @info.LINK;
    if link && window.history?
      window.history.replaceState({},'', link)
    @updateViewCount(@offer.ID, @info.ID)
    if !@firstEmit && updateGallery
      @updateGallery();
    else
      @firstEmit = false;
    @updateForm()
    @updatePrices();
    if @find(".cart-add").hasClass("added")
      @find(".cart-add").removeClass("added").text(@find(".cart-add").data("default"))
    @updateFavorite()
