function _template(string, values) {
  return string.replace(/{ *([^} ]+) *}/g, function(b, a) {
    b = values;
    a.replace(/[^.]+/g, function(a) {b = b[a];});
    return b;
  });
}

function getCookie(name) {
  
  var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ))
  return matches ? decodeURIComponent(matches[1]) : undefined
}

$(function() {
  var $document = $(document);
  if ($.fn.hasOwnProperty('calendar')) {
    $.fn.calendar.settings.ampm = false;
    $.fn.calendar.settings.text = {
      days: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
      months: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'],
      monthsShort: [
        'Янв',
        'Фев',
        'Мар',
        'Апр',
        'Май',
        'Июнь',
        'Июль',
        'Авг',
        'Сен',
        'Окт',
        'Ноя',
        'Дек'],
      now: 'Сейчас',
      today: 'Сегодня',
    };
    $.fn.calendar.settings.parser = {
      date: function(text, settings) {
        var formatted = text.replace(',', '').split(' '),
            sorted = [
              formatted[1],
              settings.text.months.indexOf(formatted[0]),
              formatted[2]];
        
        return new Date(sorted[2], sorted[1], sorted[0]);
      },
    };
  }
  
  $('.ui.form.user-data').each(function() {
    var $this = $(this);
    $this.form({
      inline: true,
      fields: {
        name: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            }],
        },
        oldPassword: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
            {
              type: 'minLength[6]',
              prompt: 'Это поле должно содержать как минимум {ruleValue} символов',
            },
          ],
        },
        password: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
            {
              type: 'minLength[6]',
              prompt: 'Это поле должно содержать как минимум {ruleValue} символов',
            },
          ],
        },
        confirmPassword: {
          rules: [
            {
              type: 'match[password]',
              prompt: 'Пароли не совпадают',
            }],
        },
      },
    }).api({
      url: $this.attr('action'),
      serializeForm: true,
      onFailure: function(response) {
        if (response.hasOwnProperty('field')) {
          $this.form('add prompt', response.field, response.message);
        } else if (response.hasOwnProperty('form-errors')) {
          Object.keys(response['form-errors']).forEach(function(key) {
            $this.form('add prompt', key, response['form-errors'][key]);
          });
        } else $this.form('add errors', [response.message]);
      },
      onSuccess: function() {
        $(this).
            find('[name="birthday"]').
            prop('disabled', true);
      },
    });
  });
  
  $('.js-password-toggler').on('click', function(e) {
    e.preventDefault();
    var $this = $(this),
        $field = $this.siblings('input').first();
    $field.attr('type',
        ($field.attr('type') === 'password' ? 'text' : 'password'));
    $this.toggleClass('active');
  });
  
  var im = new Inputmask('+7(999)99-99-999');
  var datemask = new Inputmask('99.99.9999');
  datemask.mask('[type="date"]');
  im.mask('[data-validate="phone"], [data-validate="phone-required"]');
  var emailRegexp = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/i;
  
  $('.js-tab-button').
      each(function(i, button) {$(button).tab($(button).data('tabdata'));});
  
  $.fn.form.settings.rules.login = function(value) {
    if (typeof value !== 'string') return false;
    var escapedValue = value.replace(/[()\-_+]+/g, '');
    if (emailRegexp.test(value)) {
      return true;
    } else if ((/^((7|8)+([0-9]){10})$/gm).test(escapedValue)) {
      if (escapedValue.charAt(0) == '7' || escapedValue.charAt(0) ==
          '8') escapedValue = escapedValue.substr(1);
      $(this).val(escapedValue);
      im.mask($(this));
      var result = $(this).inputmask('isComplete');
      $(this).inputmask('remove');
      return result;
    } else {
      return false;
    }
    if (isString(value[0])) {
      return $.fn.form.settings.rules.email(arguments);
    } else {
      return (/^((\+7|7|8)+([0-9]){10})$/gm).test(
          value.replace(/[()\-_\+]+/g, ''));
    }
  };
  var timeInterval = false;
  var $block = false;
  var isSend = false;
  $('.ui.form.js-user-validate').each(function(i, form) {
    var $form = $(form);
    $form.form({
      inline: true,
      fields: {
        'username': {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
            {
              type: 'login',
              prompt: 'Некорректный телефон / email',
            },
          ],
        },
        'text-required': {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            }],
        },
        'email': {
          optional: true,
          rules: [
            {
              type: 'email',
              prompt: 'Некорректный Email',
            }],
        },
        'phone': {
          optional: true,
          rules: [
            {
              type: 'inputmask',
              prompt: 'Некорректный телефон',
            }],
        },
        'email-required': {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
            {
              type: 'email',
              prompt: 'Некорректный Email',
            },
          ],
        },
        'phone-required': {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
            {
              type: 'inputmask',
              prompt: 'Некорректный телефон',
            },
          ],
        },
        'password': {
          rules: [
            {
              type: 'minLength[6]',
              prompt: 'Это поле должно содержать как минимум 6 символов',
            }],
        },
        'confirm-password': {
          rules: [
            {
              type: 'match[password]',
            }],
        },
        'checkbox-required': {
          rules: [
            {
              type: 'checked',
              prompt: 'Чтобы продолжить, Вы должны отметить этот пункт',
            }],
        },
      },
    }).api({
      url: $form.data('action'),
      cache: false,
      serializeForm: true,
      onFailure: function(response) {
        if (response.hasOwnProperty('form-errors')) {
          Object.keys(response['form-errors']).forEach(function(field) {
            $form.form('add prompt', field, response['form-errors'][field]);
          });
        } else if (response.hasOwnProperty('field')) {
          $form.form('add prompt', response.field, response.message);
        } else if (response.hasOwnProperty('replay')) {
          clearInterval(timeInterval);
          $visibleForm = $form.is(':visible') ? $form : $(
              '.auth-modal_auth:visible, .auth-modal_reg:visible').find('form');
          $visibleForm.find('.auth-modal__replay').html(response.replay);
          $tiktiok = $visibleForm.find('.tiktok');
          timeInterval = setInterval(function() {
            newVal = $tiktiok.text() - 1;
            if (newVal <= 9) {
              newVal = '0' + newVal;
            }
            $tiktiok.text(newVal);
            if (parseInt($tiktiok.text()) <= 0) {
              clearInterval(timeInterval);
              $visibleForm.find('.auth-modal__replay').html('');
            }
          }, 1000);
          $visibleForm.find('.ui.error.message').html('');
        } else {
          $form.form('add errors', [
            response.message
                ? response.message
                : 'При отправке формы возникла ошибка. Повторите попытку позже']);
        }
      },
      onSuccess: function(response) {
        if ($form.data('action') == 'user.forgot.email') {
          $('.auth-modal_forgot').transition('fade out', function() {
            $('.auth-modal_forgot-success').transition('fade in');
          });
        } else if (response.hasOwnProperty('email')) {
          $form.find('.js-email-target').text(response.email);
        }
        else if ($form.data('action') == 'order.protect') {
            $form.find('.ui.success.message').text(response.message);
            ym(21960151,'reachGoal','protective-suit');
        } else if (response.hasOwnProperty('phone')) {
          $('.send-phone-code-side .js-phone-target').
              text(response.phone).
              val(response.phone);
          $(window).scrollTop();
          $('.ui.shape').
              shape('set next side', '.send-phone-code-side').
              shape('slide left');
        } else if ($form.data('action') === 'register') {
          window.location.assign('/catalog/');
        } else if (!$form.hasClass('js-not-reload-after-submit')) {
          window.location.reload();
        } else if ($form.data('action') == 'user.auth-phone' &&
            response.hasOwnProperty('phoneCode')) {
          var $block = $('.auth-modal_auth');
          if (response.registration) {
            $block = $('.auth-modal_reg');
          }
          if ($('.auth-modal_start, .auth-modal__socials').is(':visible')) {
            $('.auth-modal_start, .auth-modal__socials').
                transition('fade out', function() {
                  $block.find('[name="phone"]').val(response.phoneCode);
                  $block.transition('fade in', function(){
                    $(this).find('input:visible').focus()
                  });
                });
          } else {
            $block.find('.auth-modal__replay').html('Код успешно отправлен');
          }
        }
      },
    });
  });
  $('.js-get-code-again').click(function() {
    $('.auth-modal_start form').trigger('submit');
  });
  $('.js-forgot-paswrd').click(function() {
    $('.auth-modal_default, .auth-modal__socials').
        transition('fade out', function() {
          $('.auth-modal_forgot').transition('fade in');
        });
  });
  $('.js-auth-forgot-back').click(function() {
    $('.auth-modal_forgot:visible, .auth-modal_forgot-success:visible').
        transition('fade out', function() {
          $('.auth-modal_default, .auth-modal__socials').transition('fade in');
        });
  });
  $(document).ready(function() {
    $('[data-modal]').click(function() {
      $('.ui.modal#' + $(this).data('modal')).modal('show');
    });
    $('.auth-modal__menu .auth-modal__menu-item').tab();
  });
  
  $('.js-subscribes-validate').each(function() {
    var $this = $(this);
    $this.form().api({
      on: 'submit',
      url: $this.attr('action'),
      serializeForm: true,
      onFailure: function(response) {
        $this.form('add errors', [response.message]);
      },
    });
  });
  $('.personal').find('.radio-group .item').tab();
  
  $(document).on('click', '.js-remove-organization-trigger', function(e) {
    e.preventDefault();
    var $this = $(this),
        $id = $this.data('value');
    if (!confirm('Вы действительно хотите удалить организацию?')) {
      return;
    }
    $this.api({
      on: 'now',
      url: 'user.removeorgaddress',
      data: {
        id: $id,
      },
      onSuccess: function(response) {
        var $content = $this.closest('.content'),
            $title = $content.prev('.title'),
            $animate = [$title];
        $content.hasClass('active')
            ? $animate.push($content)
            : $content.remove();
        $($animate).transition({
          animation: 'scale out',
          duration: '300',
          onComplete: function() {
            $(this).remove();
          },
        });
      },
    });
  });
  
  $('.js-cancel-order-trigger').each(function() {
    var $this = $(this),
        $popup = $('.ui.js-cancel-order-popup.popup'),
        $field = $popup.find('[name="orderid"]');
    $this.popup({
      inline: true,
      movePopup: true,
      popup: $popup,
      on: 'click',
      position: 'top center',
      lastResort: 'top right',
      onShow: function() {
        $field.val($this.data('order-id'));
      },
      onHide: function() {
        $field.val(null);
        $popup.removeClass('error');
        $popup.removeClass('success');
        $popup.form('refresh');
      },
    });
  });
  $('.ui.js-cancel-order-popup.popup').form({inline: true}).api({
    url: 'user.cancel-order',
    serializeForm: true,
    onSuccess: function(response) {
      window.location.reload();
      // var $container = $('.js-cancel-order-trigger[data-order-id="'+response["orderid"]+'"]').closest(".content.row.active");
      // $([$container, $container.prev(".title.row.active")]).transition({
      // animation: "scale out",
      // duration: 300,
      // onComplete: function () {
      // 	$(this).remove();
      // }
      // });
      // $(this).popup("hide");
    },
    onFailure: function(response) {
      $(this).form('add errors', [response.message]);
    },
  });
  
  $(document).on('change', '.js-make-organization-default', function(e) {
    e.preventDefault();
    var $this = $(this),
        $id = $this.attr('value');
    if (!$this.is(':checked')) return;
    $this.api({
      on: 'now',
      url: 'user.changedefaultorgaddress',
      data: {
        id: $id,
      },
    });
  });
  
  $(document).on('change', '.js-make-user-address-default', function(e) {
    e.preventDefault();
    var $this = $(this),
        $id = $this.attr('value');
    if (!$this.is(':checked')) return;
    $this.api({
      on: 'now',
      url: 'user.changedefaultaddress',
      data: {
        id: $id,
      },
    });
  });
  
  $('.ui.calendar').each(function() {
    var $this = $(this);
    $this.calendar($this.data('calendar'));
  });
  
  function buildAddressesForm() {
    var $this = $(this),
        $url = $this.attr('action'),
        token = '603780ba9dc89ca3a01bdc215b0d5314db1598a1';
    im.mask($this.find('[data-validate="phone"]'));
    $this.find('[data-dadata="address"]').suggestions({
      token: token,
      type: 'ADDRESS',
      count: 5,
      onSelect: function(suggestion) {
        var $form = $(this).closest('.ui.form');
        if (suggestion.data.city) $form.find('[data-suggestion="city"]').
            val(suggestion.data.city);
        if (suggestion.data.postal_code) $form.find(
            '[data-suggestion="postal_code"]').val(suggestion.data.postal_code);
        if (suggestion.data.street) $form.find('[data-suggestion="street"]').
            val(suggestion.data.street);
        if (suggestion.data.country) $form.find('[data-suggestion="country"]').
            val(suggestion.data.country);
        
      },
      onSelectNothing: function() {
        var $form = $(this).closest('.ui.form');
        $form.find('[data-suggestion="city"]').val('');
      },
    });
    $this.find('[data-dadata="party"]').suggestions({
      token: token,
      type: 'PARTY',
      count: 5,
      onSelect: function(suggestion) {
        var $form = $(this).closest('.ui.form');
        if (suggestion.data.name.short || suggestion.data.name.full) $form.find(
            '[data-suggestion="name"]').
            val(suggestion.data.name.full || suggestion.data.name.short);
        if (suggestion.data.inn) $form.find('[data-suggestion="inn"]').
            val(suggestion.data.inn);
        if (suggestion.data.kpp) $form.find('[data-suggestion="kpp"]').
            val(suggestion.data.kpp);
        if (suggestion.data.ogrn) $form.find('[data-suggestion="ogrn"]').
            val(suggestion.data.ogrn);
        if (suggestion.data.address) $form.find(
            '[data-suggestion="legal-address"]').
            val(suggestion.data.address.value);
      },
    });
    $this.find('[data-dadata="bank"]').suggestions({
      token: token,
      type: 'BANK',
      count: 5,
      onSelect: function(suggestion) {
        var $form = $(this).closest('.ui.form');
        if (suggestion.data.name.payment) $form.find('[name="bank_name"]').
            val(suggestion.data.name.payment);
        if (suggestion.data.bic) $form.find('[name="bik"]').
            val(suggestion.data.bic);
        if (suggestion.data.correspondent_account) $form.find('[name="ks"]').
            val(suggestion.data.correspondent_account);
      },
    });

    $this.form({
      inline: true,
      fields: {
        street: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
          ],
        },
        city: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
          ],
        },
        name: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
          ],
          optional: $this.data('action') !== 'user.changeorgaddress',
        },
        inn: {
          rules: [
            {
              type: 'minLength[10]',
              prompt: 'Это поле должно содержать не менее {ruleValue} символов',
            },
            {
              type: 'maxLength[12]',
              prompt: 'Это поле должно содержать не более {ruleValue} символов',
            },
            {
              type: 'number',
              prompt: 'Это поле должно состоять только из цифр',
            },
          ],
        },
        kpp: {
          optional: true,
          rules: [
            {
              type: 'exactLength[9]',
              prompt: 'Это поле должно содержать {ruleValue} символов',
            },
          ],
        },
        ogrn: {
          optional: true,
          rules: [
            {
              type: 'exactLength[13]',
              prompt: 'Это поле должно содержать {ruleValue} символов',
            },
          ],
        },
        phone: {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
            {
              type: 'inputmask',
              prompt: 'Некорректный номер телефона',
            },
          ],
        },
        email: {
          optional: true,
          rules: [
            {
              type: 'email',
              prompt: 'Некорректный Email',
            },
          ],
        },
        'legal-address': {
          rules: [
            {
              type: 'empty',
              prompt: 'Это поле не может быть пустым',
            },
          ],
        },
      },
    }).api({
      url: $url,
      serializeForm: true,
      onSuccess: function(response) {
        if (response.item) {
          var $title = $('[data-organization=\'' + response.item.id + '\']');
          $title.find('span').
              text(response.item.name +
                  (response.item.tin ? ', ИНН: ' + response.item.tin : ''));
          if (response.item.default) {
						$title.find('.check').removeClass('hide');
					} else {
						$title.find('.check').addClass('hide');
					}
          if (response.item.default) {
            $('[data-organization]').not($title).each(function() {
              $(this).find('.check').addClass('hide');
              $(this).
                  next('.content').
                  find('[name=default]').
                  prop('checked', false);
            });
          }
        }
        if (!$this.hasClass('js-insert-after-submit') ||
            !response.hasOwnProperty('item')) return;
        var $accordion = $this.closest('.accordion');
        var $formContent = $($this.clone());
        var item = response.item;
        $formContent.find('.ui.success.message').text('Изменения сохранены');
        var inputId = $formContent.find('[name="default"]').attr('id');
        $formContent.find('#' + inputId).
            attr('id', inputId.replace('new', item.id));
        $formContent.find('[for="' + inputId + '"]').
            attr('for', inputId.replace('new', item.id));
        $formContent.find('.primary.button').text('Сохранить');
        $formContent.find('.js-remove-organization-trigger').
            removeClass('hide').
            attr('data-value', item.id);
        var template = '<div class="title personal-accord__title" data-organization="{id}"><div class="personal__text personal-accord__org"><svg class="icon check{hide}"><use xlink:href="/local/templates/cameo-v3/images/icons.svg#check-icon" class="use-check"></use></svg><span>{name}</span></div><div class="personal-accord__show"><svg class="icon show"><use xlink:href="/local/templates/cameo-v3/images/icons.svg#show-icon" class="use-show"></use></svg></div></div><div class="content organizations__accord-content"><div class="mb16"></div><form action="{action}" class="ui form organizations__form js-addresses-validate"><input type="hidden" name="id" value="{id}"/>{form}</form></div>';
        $(_template(template, {
          name: item.name + (item.tin ? ', ИНН: ' + item.tin : ''),
          id: item.id,
          action: 'changeorgaddress',
          hide: item.default ? '' : ' hide',
          form: $formContent.html(),
        })).insertBefore($accordion.find('.title').last());
        var ourForm = $accordion.find(
            '.ui.form [name="id"][value="' + item.id + '"]').
            closest('.ui.form');
        buildAddressesForm.call(ourForm);
        ourForm.form('set values', $this.form('get values'));
        $accordion.find('.title.transition.hidden').transition({
          animation: 'scale in',
          duration: 300,
        });
        $this.form('set values', {
          inn: '',
          kpp: '',
          name: '',
          phone: '',
          checkpoint: '',
          email: '',
          ogrn: '',
          person: '',
          'address': '',
          'real-address': '',
          'rs': '',
          'ks': '',
          'bank_name': '',
          bik: '',
        });
				isSend = false;
      },
      onFailure: function(response) {
        $this.form('add errors', [response.message]);
      },
    });
  }
  
  if ($.fn.suggestions != null) {
  $(".register-full-name").suggestions({
    token: '603780ba9dc89ca3a01bdc215b0d5314db1598a1',
    type: 'NAME',
    autoSelectFirst: true,
    onSelect: function(suggestion, changed) {
      var $form = $(this).closest('form');
      Object.keys(suggestion.data).forEach(function(key) {
        var element = $form.find('[name="' + key + '"]');
        if (element.length) {
          element.val(suggestion.data[key]);
        }
      });
    },
    onSelectNothing: function() {
      this.value = '';
    },
  });
  }

  
  $('.js-addresses-validate').each(buildAddressesForm);
  
  $document.on('click', '.js-logout-trigger', function(e) {
    e.preventDefault();
    var $this = $(this);
    $this.addClass('loading');
    $.post('/api/user.logout', function(res) {
      res.status === 200
          ? window.location.reload()
          : $this.removeClass('loading').addClass('error');
    });
  });
  
  $('#catalogSearch').search('destroy').search({
    apiSettings: {
      url: 'search?query={query}',
      method: 'GET',
    },
    type: 'standard',
    searchFields: ['title'],
    minCharacters: 3,
    maxResults: 10,
    cache: false,
    showNoResults: true,
    searchOnFocus: false,
    selectFirstResult: true,
    onSelect: function(selected) {
      //console.log(selected);
      if (selected.url) window.location.assign(selected.url);
      return false;
    },
    templates: {
      standard: function(response) {
        var count = response.results.length;
        var message = 'найден 1 результат';
        if (count % 10 < 5 && count % 10 > 1) message = 'найдено ' + count +
            ' результата';
        if (count % 10 > 5 || count % 10 === 0) message = 'найдено ' + count +
            ' результатов';
        message = '<p class="gray-color prolog">По запросу "' + response.query +
            '" ' + message + '</p>';
        var $html = $('<div></div>');
        $html.append(message);
        response.results.forEach(function(item) {
          var $item = $(
              '<div class="search card result" onclick="window.location.assign(\'' +
              item.url + '\');"></div>');
          var $content = $('<div class="content"></div>');
          var $title = $('<div class="uppercase title"></div>').
              html(item.saleName);
          var $description = $('<div class="gray-color description"></div>').
              html(item.path);
          $content.append($title).append($description);
          if (item.image) $item.append(
              '<div class="picture fit-container"><img class="image" src="' +
              item.image + '"/></div>');
          $item.append($content);
          if (item.price) $item.append(
              '<div class="price">' + item.price + '</div>');
          if (!item.image && !$item.price) $item.addClass('bordered');
          $html.append($item);
        });
        return $html;
      },
    },
  });
  
  /*personal*/
  
  var windowWidth = $(window).width();
  if (windowWidth < 992) {
    $('.getback__reason td[colspan="3"]').attr('colspan', 5);
    $('.cart .ui.table .icon.dropdown').on('click', function() {
      $tr = $(this).closest('tr');
      $tds = $tr.find('.color-param, .size-param, .column-stepper');
      if ($(this).hasClass('opened')) {
        $(this).removeClass('opened');
        $tds.removeClass('opened');
        $tds.animate({height: '0px'}, 300, 'linear');
      } else {
        $(this).addClass('opened');
        $tds.animate({height: '42px'}, 300, 'linear', function() {
          $tds.addClass('opened');
        });
      }
    });
  }
  
  $('.transition-yourparams').click(function() {
    $('.yourparams__item_active').removeClass('yourparams__item_active');
    $(this).addClass('yourparams__item_active');
    var $target = $(this).data('target');
    if ($('.yourparams__edit.visible').length) {
      $('.yourparams__edit.visible').transition('fade out', function() {
        $('.yourparams__edit' + $target).transition('fade in');
      });
    } else {
      $('.yourparams__edit' + $target).transition('fade in');
    }
  });
  $('.yourparams__edit .basic.button').
      click(function() {
        $(this).
            closest('.yourparams__edit').
            transition('fade out');
        $('.yourparams__item_active').removeClass('yourparams__item_active');
      });
  $('[data-checkbox]').click(function() {
    var checkbox = $(this).data('checkbox');
    if ($(this).hasClass('active')) {
      $('#' + checkbox).prop('checked', false).trigger('change');
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
      $('#' + checkbox).prop('checked', true).trigger('change');
    }
    
  });
  $('.getback-textarea-toggle').on('change', function() {
    animate = this.checked ? 'in' : 'out';
    $(this).
        closest('tr').
        find('.getback-textarea').
        transition('fade ' + animate);
  });
  var santimetersMask = new Inputmask('9{2,3} см');
  santimetersMask.mask('.input-yourparams');
  if (getCookie("BITRIX_CAMEO_detect_timezone") === 'Y') {
      var offset = new Date().getTimezoneOffset(), o = Math.abs(offset),
          timezone = (offset < 0 ? "" : "-") + (Math.floor(o / 60)) + "." + ((o % 60));
    $.post('/api/user.update',{timezone: timezone});
  }

  $('.js-auto-submit').keyup(function () {
    var val = $(this).val();
    console.log(val)
    if(val && val.length == 4){
      $(this).closest('form').trigger('submit');
    }
  });

  $('.js-auto-disabled').keyup(function () {
    $form = $(this).closest('form');
    if($form.form('is valid')){
      $form.find('.submit').removeClass('gray disabled')
      $form.find('.submit').addClass('primary')
    }else{
      $form.find('.submit').addClass('gray disabled')
      $form.find('.submit').removeClass('primary')
    }
  });

  $('.modal .button.close').click(function(e) {
    e.preventDefault();
    $(this).closest('.modal').modal('hide')
  })
});
