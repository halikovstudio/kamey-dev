if !window.cameo
  window.cameo = {version: 3}
$.fn.dropdown.settings.templates.dropdown = (select) ->
  placeholder = select.placeholder or false
  values = select.values or {}
  html = ''
  if window.cameo.version == 3
    html += '<svg class="icon dropdown"><use class="use-dropdown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/local/templates/cameo-v3/images/icons.svg#dropdown-icon"></use></svg>'
  else
    html += '<i class="icon dropdown"></i>'
  if select.placeholder
    html += '<div class="default text">' + placeholder + '</div>'
  else
    html += '<div class="text"></div>'
  html += '<div class="menu">'
  $.each select.values, (index, option) ->
    html += if option.disabled then '<div class="disabled item" data-value="' + option.value + '">' + option.name + '</div>' else '<div class="item" data-value="' + option.value + '">' + option.name + '</div>'
    return
  html += '</div>'
  return html
$.fn.api.settings.successTest = (response) ->
  return response.status == 200

$.fn.api.settings.base = "/api/"

$.fn.form.settings.keyboardShortcuts = false

$.fn.api.settings.api = {
  "cart add": "cart.add",
  "cart update params": "cart.update-params"
}

$.fn.api.settings.method = "post"

$.fn.search.settings.templates.message = (message, type) ->
  html = ''
  if message != undefined and type != undefined
    html += '' + '<div class="message ' + type + '">'
    # message type
    if type == 'empty'
      html += '' + '<div class="header">По Вашему запросу ничего не найдено</div class="header">' + '<div class="description">' + message + '</div class="description">'
    else
      html += ' <div class="description">' + message + '</div>'
    html += '</div>'
  return html

$.fn.form.settings.rules.inputmask = (value)->
  return $(this).inputmask("isComplete");
$.fn.form.settings.prompt.empty = "Поле \"{name}\" должно быть заполнено"
$.fn.form.settings.prompt.checked = "{name} должно быть отмечено"
$.fn.form.settings.prompt.inputmask = "Введите корректный номер телефона"
