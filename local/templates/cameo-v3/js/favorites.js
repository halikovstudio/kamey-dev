// Generated by CoffeeScript 1.12.5
var Favorite;

Favorite = (function() {
  function Favorite() {
    var $this;
    window.favorite = this;
    $this = this;
    this.dom = $("#favorite-small");
    this.trigger = $(".sidebar-trigger[data-target='favorite-small'],.favorite-button");
    this.indicator = $(".sidebar-trigger[data-target='favorite-small'] .indicator,.favorite-button .indicator");
    this.cards = this.dom.find(".middle-content");
    this.list = [];
    this.trigger.on('click', function(e) {
      if ($(this).hasClass('disabled')) {
        return e.preventDefault();
      }
    });
    $(document).on("click", ".favorite-trigger", function(e) {
      e.preventDefault();
      return $this.toggle($(this));
    }).on("click", ".favorite-small .clear,.favorite .clear", function(e) {
      e.preventDefault();
      return $(this).api({
        on: "now",
        url: "favorite.clear",
        onSuccess: (function(_this) {
          return function() {
            return $(".catalog-list").find(".favorite-column").transition({
              animation: "fade out",
              interval: 200,
              reverse: true,
              onComplete: function() {
                $this.update({
                  list: [],
                  html: ""
                }, false);
                return $(".favorite-trigger.active").removeClass("active");
              }
            });
          };
        })(this),
        onFailure: (function(_this) {
          return function() {
            return $(_this).addClass("error");
          };
        })(this)
      });
    }).on("click", ".favorite-delete", function(e) {
      var $button, id;
      e.preventDefault();
      $button = $(this);
      id = $button.data("target");
      return $button.closest(".favorite-column").transition("fade out", function() {
        return $this.remove(id, function(success) {
          if (success) {
            return $(".favorite-trigger[data-target='" + id + "']").removeClass("active");
          }
        });
      });
    });
    this.load();
    this.get(this.update.bind(this));
  }

  Favorite.prototype.save = function(data) {
    var ref;
    return (ref = window.sessionStorage) != null ? ref.setItem("favorite-data", JSON.stringify(data)) : void 0;
  };

  Favorite.prototype.load = function() {
    var data, jsonData, ref;
    jsonData = (ref = window.sessionStorage) != null ? ref.getItem('favorite-data') : void 0;
    if (jsonData != null) {
      data = JSON.parse(jsonData);
      return this.update(data);
    }
  };

  Favorite.prototype.get = function(callback) {
    return $.get("/api/favorite.get/", {
      version: cameo.version
    }, (function(_this) {
      return function(data) {
        if (typeof callback === "function") {
          callback(data);
        }
        return _this.save(data);
      };
    })(this), "json");
  };

  Favorite.prototype.update = function(data, setClass) {
    if (setClass == null) {
      setClass = true;
    }
    this.trigger.removeClass("loading");
    if (data) {
      if (data.list != null) {
        this.list = data.list || [];
      }
      if (data.html != null) {
        this.html = data.html || "";
      }
    }
    this.cards.html(this.html);
    if (!$.isArray(this.list)) {
      this.list = [];
    }
    if (setClass) {
      this.list.forEach(function(id) {
        return $(".favorite-trigger[data-target='" + id + "']").addClass("active");
      });
    }
    return this.updateIndicator();
  };

  Favorite.prototype.updateIndicator = function() {
    if (this.list.length > 0) {
      this.trigger.popup("destroy");
      this.trigger.removeClass("disabled");
    } else {
      this.trigger.popup({
        html: this.trigger.data("prevent"),
        on: "hover",
        position: "bottom right"
      });
      this.trigger.addClass("disabled");
    }
    return this.indicator.text(this.list.length > 0 ? this.list.length : "");
  };

  Favorite.prototype.toggle = function($item) {
    var id;
    id = $item.data("target");
    if (!$item.hasClass("active")) {
      return this.add(id, (function(_this) {
        return function(success) {
          if (success) {
            return $(".favorite-trigger[data-target='" + id + "']").addClass("active");
          }
        };
      })(this));
    } else {
      return this.remove(id, (function(_this) {
        return function(success) {
          if (success) {
            return $(".favorite-trigger[data-target='" + id + "']").removeClass("active");
          }
        };
      })(this));
    }
  };

  Favorite.prototype.add = function(id, callback) {
    return $.post("/api/favorite.add/", {
      id: id
    }, (function(_this) {
      return function(data) {
        _this.update(data);
        return typeof callback === "function" ? callback(data.status === 200) : void 0;
      };
    })(this));
  };

  Favorite.prototype.remove = function(id, callback) {
    return $.post("/api/favorite.remove/", {
      id: id
    }, (function(_this) {
      return function(data) {
        _this.update(data);
        return typeof callback === "function" ? callback(data.status === 200) : void 0;
      };
    })(this));
  };

  return Favorite;

})();
