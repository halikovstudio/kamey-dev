$(function() {
  (function() {
    var $auto = $('.yourparams__right [type=checkbox]'),
        filled = function() {
          var filled = true;
          $('.yourparams__edit input').each(function() {
            if (!this.value) {
              filled = false;
            }
          });
          return filled;
        },
        calculate = function() {
          if (!filled()) {
            if ($auto.prop('disabled') !== true) {
              $.post('/api/user.update', {
                auto_size: 0,
              });
            }
            $auto.prop('disabled', true).prop('checked', true);
          } else {
            if ($auto.prop('disabled') !== false) {
              $.post('/api/user.update', {
                auto_size: 1,
              });
              $auto.prop('checked', false);
            }
            $auto.prop('disabled', false);
          }
          $.get('/api/user.size.table',{type: window.size_type}, function(data) {
            $('.yourparams__table').html(data.html);
            if (data.size) {
              window.size_info = data.size;
              $(window).trigger('refreshSize')
            }
          }, 'json');
        },
        
        $modal = $('.find-modal'),
        $modalForm = $modal.find('form'),
        set_sizes = function() {
          var data = {
            auto_size: 1,
          };
          $('.size-empty').transition('fade out', function() {
            $('.size-filled').transition('fade in');
          });
          $modalForm.find('.input-yourparams').each(function() {
            var name = $(this).attr('name'),
                value = $(this).inputmask('unmaskedvalue');
            data[name] = value;
            $('.value-' + name).text(value);
            $('.form-size [name=' + name + ']').val(value);
          });
          $('.yourparams__checkbox input').
              prop('checked', false).
              prop('disabled', false);
          $.post('/api/user.update', data, function() {
            calculate();
          });
        };
    $('.form-size').each(function() {
      var $form = $(this),
          $input = $form.find('.input-yourparams');
      $form.api({
        url: 'user.update',
        serializeForm: true,
        beforeSend: function(settings) {
          settings.data[$input.attr('name')] = $input.inputmask(
              'unmaskedvalue');
          return settings;
        },
        onSuccess: function() {
          $(this).popup_success('Данные сохранены');
          var value = $input.inputmask('unmaskedvalue');
          $('.value-' + $input.attr('name')).text(value ? value : '-');
          calculate();
        },
        onFailure: function(data) {
          $(this).popup_error(data.message);
        },
      });
    });
    $('.yourparams__checkbox input').on('change', function() {
      $.post('/api/user.update', {
        auto_size: !this.checked ? 1 : 0,
      });
    });
    $('.size-clear').click(function(e) {
      e.preventDefault();
      $('.yourparams__value').text('-');
      $('.yourparams__checkbox input').prop('checked', true);
      $('.input-yourparams').val('');
      $modalForm.get(0).reset();
      $modal.modal('hide');
      $modal.find('.transition').removeClass('visible').addClass('hidden').attr('style','');
      $('.find-chest-side').removeClass('hidden').addClass('visible');
      $('.size-filled').transition('fade out', function() {
        $('.size-empty').transition('fade in');
      });
      $('.find-modal__load').css('right', '100%');
      $('.yourparams__right [type=checkbox]').prop('disabled', true);
      $.post('/api/user.update', {
        auto_size: 0,
        height: '',
        waist: '',
        hip: '',
        chest: '',
      });
    });
    
    $('.transition-target').click(function() {
      var $target = $(this).data('target');
      var load = $(this).data('load');
      if ($(this).
          closest('.transition').
          find('input').
          inputmask('isComplete') || $(this).hasClass('find-modal__back')) {
        $('.find-modal__load').animate({'right': load + '%'}, 500);
        if (!load) {
          set_sizes();
        }
        $(this).closest('.transition').transition('fade out', function() {
          $('.' + $target).transition('fade in', function() {
            $(this).find('input:visible').focus();
          });
        });
      } else {
        $(this).popup_error('Заполните поле');
      }
    });
    $modalForm.on('submit', function(e) {
      e.preventDefault();
      $(this).
          find('.transition.visible .transition-target:not(.find-modal__back)').
          click();
    });
  })();
  
});
