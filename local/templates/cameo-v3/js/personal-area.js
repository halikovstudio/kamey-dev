$(function() {
  function onFormFailure(data) {
    if (data.field) {
      return $(this).form('add prompt', data.field, data.message);
    } else if (data.message) {
      return $(this).form('add errors', [data.message]);
    }
  }
  
  (function() {
    var $form = $('.form-change-phone'),
        $input = $form.find('input[name=phone]'),
        $button = $form.find('.button'),
        $modal = $('#accept-phone'),
        old = $input.val(),
        $confirmForm = $modal.find('form'),
        $codeInput = $modal.find('[name=code]');
    $input.on('input', function(e) {
      if ($input.inputmask('isComplete') && $input.val() !== old) {
        if (!$button.transition('is visible')) {
          $button.transition('fade in');
        }
      } else {
        if ($button.transition('is visible')) {
          $button.transition('fade out');
        }
      }
    });
    $form.form({
      inline: true,
      fields: {
        phone: [
          'empty',
          'inputmask',
          'not[' + old + ']',
        ],
      },
    }).api({
      serializeForm: true,
      url: 'user.change.phone.send',
      onSuccess: function() {
        $modal.find('[name=phone]').val($input.val());
        $codeInput.val('');
        $modal.modal('show');
      },
      onFailure: onFormFailure,
    });
    $confirmForm.find('.js-get-code-again').api({
      url: 'user.change.phone.send',
      beforeSend: function(settings) {
        settings.data = {
          replay: true,
          phone: $input.val(),
        };
        return settings;
      },
      onFailure: function(data) {
        $(this).popup_error(data.message);
      },
      onSuccess: function(data) {
        $(this).popup_success('Код успешно выслан');
      },
    });
    $confirmForm.form({
      inline: true,
      fields: {
        code: ['exactLength[6]'],
      },
    }).api({
      url: 'user.change.phone.confirm',
      serializeForm: true,
      onFailure: onFormFailure,
      onSuccess: function() {
        $input.popup_success('Номер телефона успешно изменен');
        $button.transition('fade out');
        old = $input.val();
        $modal.modal('hide');
      },
    });
    $codeInput.inputmask('999999', {
      oncomplete: function() {
        $confirmForm.submit();
      },
    });
  })();
  
  (function() {
    $('.form-user-field').each(function() {
      var $form = $(this),
          $input = $form.find('.field input'),
          $button = $form.find('.button'),
          old = $input.val();
      
      $input.on('input', function() {
        if ($input.inputmask('isComplete') && $input.val() !== old) {
          if (!$button.transition('is visible')) {
            $button.transition('fade in');
          }
        } else {
          if ($button.transition('is visible')) {
            $button.transition('fade out');
          }
        }
      });
      $form.form({
        inline: true,
        fields: {
          email: ['email', 'empty'],
          name: 'empty',
          birthday: 'empty',
        },
      }).api({
        serializeForm: true,
        url: 'user.update',
        beforeSend: function(settings) {
          if ($input.val() === old) {
            return;
          }
          return settings;
        },
        onSuccess: function(data) {
          $button.transition('fade out');
          old = $input.val();
          if (data.email) {
            $('#accept-email').modal('show').find('.value').text(data.email);
          } else {
            $(this).popup_success('Данные сохранены');
          }
        },
        onFailure: function(data) {
          $(this).popup_error(data.message);
        },
      });
    });
  })();
  $(document).on('click', '.accordion-close', function(e) {
    e.preventDefault();
    $(this).closest('.content').prev().click();
  });
  $(document).on('click', '.form-reset', function(e) {
    e.preventDefault();
    $(this).closest('form').get(0).reset();
  });
  $('.subscription-form').form().api({
    on: 'change',
    url: 'user.subscribe',
    serializeForm: true,
    onFailure: onFormFailure
  });
});
