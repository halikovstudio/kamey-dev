<div class="side registration-side">
    <div class="ui container fluid">
        <div class="side-container">
            <div class="h1 ui header center aligned uppercase normal">Регистрация</div>
            <? icon("close") ?>
            <div class="right-floated gadget hidden">
                <div class="ui vertical menu catalog-menu fluid">
                    <a href="javascript:void(null)" class="item active shape-trigger" data-target="registration-side">Регистрация</a>
                    <a href="javascript:void(null)" class="item shape-trigger" data-target="authorization-side">Вход</a>
                </div>
            </div>
            <form class="ui form js-user-validate" action="javascript:void(null)" data-action="user.register">
                <div class="field">
                    <input type="text" name="name" placeholder="ФИО" value="" data-validate="text-required" id="registration-fio">
                    <input type="hidden" name="_name">
                    <input type="hidden" name="_last_name">
                    <input type="hidden" name="_second_name">
                    <input type="hidden" name="_gender">
                </div>
                <div class="field">
                    <input type="tel" name="phone" placeholder="Номер мобильного телефона" value="" data-validate="phone-required">
                </div>
                <div class="field">
                    <input type="email" name="email" placeholder="Адрес электронной почты" value="" data-validate="email-required">
                </div>
                <div class="field relative">
                    <button type="button" class="js-password-toggler eye-button" tabindex="-1"><?icon("eye")?></button>
                    <input type="password" name="password" placeholder="Пароль" value="" data-validate="password">
                </div>
                <div class="field relative">
                    <button type="button" class="js-password-toggler eye-button" tabindex="-1"><?icon("eye")?></button>
                    <input type="password" name="confirmPassword" placeholder="Повторите пароль" value="" data-validate="confirm-password">
                </div>
                <div class="ui error message"></div>
                <div class="ui stackable grid add-top-padding-x1">
                    <div class="nine wide column">
                        <div class="field ui checkbox">
                            <input id="form_registration_field_agree" type="checkbox" name="agree" value="Y" data-validate="checkbox-required">
                            <label for="form_registration_field_agree">Я даю согласие на обработку персональных данных</label>
                        </div>
                        <div class="field ui checkbox">
                            <input id="form_registration_field_subscribe" type="checkbox" name="subscribe" value="Y" data-validate="checkbox">
                            <label for="form_registration_field_subscribe">Подписаться на новости и рассылку</label>
                        </div>
                    </div>
                    <div class="seven wide column flat end-justified">
                        <button type="submit" class="ui primary fluid button">Зарегистрироваться</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?
    Dynamic::begin('registration-script');
    if (!CUser::IsAuthorized()) {
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/suggestions.min.css');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/suggestions.min.js');
        ?>
        <script>
          $(function(){
            $("#registration-fio").suggestions({
              token: "603780ba9dc89ca3a01bdc215b0d5314db1598a1",
              type: "NAME",
              count: 5,
              autoSelectFirst: true,
              onSelect: function(suggestion, changed) {
                $("[name=_name]").val(suggestion.data.name);
                $("[name=_last_name]").val(suggestion.data.surname);
                $("[name=_second_name]").val(suggestion.data.patronymic);
                $("[name=_gender]").val(suggestion.data.gender !== 'MALE' ? 'F' : "M");
                return suggestion.value;
              },
            });
          })
        </script>
        <?
    } Dynamic::end(); ?>
</div>