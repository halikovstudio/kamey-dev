<div class="side authorization-side">
    <div class="ui container fluid">
        <div class="side-container">
            <div class="h1 ui header center aligned uppercase normal">Вход</div>
            <? icon("close") ?>
            <div class="right-floated gadget hidden">
                <div class="ui vertical menu catalog-menu fluid">
                    <a href="javascript:void(null)" class="item shape-trigger" data-target="registration-side">Регистрация</a>
                    <a href="javascript:void(null)" class="item active shape-trigger" data-target="authorization-side">Вход</a>
                </div>
            </div>
            <form class="ui form js-user-validate" action="javascript:void(null)" data-action="user.authorize">
                <div class="field">
                    <input type="text" name="login" placeholder="Телефон / Почта" value="" data-validate="username" data-autofocus>
                </div>
                <div class="field relative">
                    <button type="button" class="js-password-toggler eye-button"><?icon("eye")?></button>
                    <input type="password" name="password" placeholder="Пароль" value="" data-validate="password">
                </div>
                <div class="ui error message"></div>
                <div class="ui stackable grid add-top-padding-x1 middle aligned">
                    <div class="seven wide column flat end-justified">
                        <button type="submit" class="ui primary fluid button">Войти</button>
                    </div>
                    <div class="one wide column mobile hidden"></div>
                    <div class="seven wide column">
                        <div class="ui checkbox">
                            <input id="form_authirization_field_remember" type="checkbox" name="remember" value="Y" data-validate="checkbox" checked>
                            <label for="form_authirization_field_remember">Запомнить меня<br> на этом устройстве</label>
                        </div>
                    </div>
                </div>
                <div class="divided-adds">
                    <a href="javascript:void(null)" class="shape-trigger black underline" data-target="forgot-password-side" data-animation="slide left">Забыли пароль?</a>
					<br class="mobile only tablet only">
<br class="mobile only tablet only">
  <a href="javascript:void(null)" class="black underline shape-trigger mobile only tablet only" data-target="registration-side">Регистрация</a>
                    <p class="gray-color">Вход через социальные сети</p>
                    <div class="socials" data-ulogin="display=buttons">
                        <button class="ui circular vk icon button" type="button" data-uloginbutton="vkontakte"><i class="ui icon vk"></i></button>
                        <button class="ui circular facebook icon button" type="button" data-uloginbutton="facebook"><i class="ui icon facebook f"></i></button>
                        <button class="ui circular orange icon button" type="button" data-uloginbutton="odnoklassniki"><i class="ui icon odnoklassniki"></i></button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
