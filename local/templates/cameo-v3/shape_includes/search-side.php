<div class="side search-side">
    <div class="ui container fluid">
        <div class="choose-city side-container">
            <div class="h1 ui header center aligned uppercase normal">Поиск</div>
            <? icon("close") ?>
            <form class="ui form fullscreen" onsubmit="event.preventDefault()">
                <div class="ui search fluid" id="catalogSearch">
                    <div class="ui icon input fluid">
                        <input type="text" class="prompt" data-autofocus name="query" placeholder="Поиск по каталогу">
                        <? icon("search".($version == 4 ? '-bold' : '')) ?>
                    </div>
                    <div class="results"></div>
                </div>
            </form>
        </div>
    </div>
</div>
