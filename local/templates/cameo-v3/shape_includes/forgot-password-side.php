<div class="side forgot-password-side">
    <div class="ui container fluid">
        <div class="side-container">
            <div class="h1 ui header center aligned uppercase normal">Забыли пароль?</div>
            <? icon("close") ?>
            <div class="right-floated gadget hidden">
                <div class="ui vertical menu catalog-menu fluid">
                    <a href="javascript:void(null)" class="item shape-trigger" data-target="registration-side">Регистрация</a>
                    <a href="javascript:void(null)" class="item shape-trigger" data-target="authorization-side">Вход</a>
                </div>
            </div>
            <div class="left-floated gadget hidden">
                <button type="button" class="back-shape-button shape-trigger" data-target="authorization-side" data-animation="slide right">
                    <?icon("arrow-left")?>
                    <span class="uppercase black underline">Назад</span>
                </button>
            </div>
            <form class="ui form js-user-validate" action="javascript:void(null)" data-action="user.forgot.v3">
                <div class="field">
                    <input type="text" name="login" placeholder="Телефон / Почта" value="" data-validate="username">
                </div>
                <div class="ui error message"></div>
                <div class="center-aligned vertical-nested">
                    <button class="ui primary button" type="submit">Отправить</button>
                </div>
                <div class="ui success message replacing">
                    <p class="uppercase">Письмо с кодом активации отправлено по адресу <span class="js-email-target"></span></p>
                    <a href="javascript:void(null)" class="ui primary button shape-trigger" data-target="authorization-side">Вход</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="side send-phone-code-side">
    <div class="ui container fluid">
        <div class="side-container">
            <div class="h1 ui header center aligned uppercase normal">Забыли пароль?</div>
            <? icon("close") ?>
            <div class="right-floated gadget hidden">
                <div class="ui vertical menu catalog-menu fluid">
                    <a href="javascript:void(null)" class="item shape-trigger" data-target="registration-side">Регистрация</a>
                    <a href="javascript:void(null)" class="item shape-trigger" data-target="authorization-side">Вход</a>
                </div>
            </div>
            <div class="left-floated gadget hidden">
                <button type="button" class="back-shape-button shape-trigger" data-target="authorization-side" data-animation="slide right">
                    <?icon("arrow-left")?>
                    <span class="uppercase black underline">Назад</span>
                </button>
            </div>
            <p class="uppercase center-aligned">
                Код активации выслан на номер <span class="js-phone-target"></span>
            </p>
            <form class="ui form js-user-validate js-not-reload-after-submit" action="javascript:void(null)" data-action="user.restore.phone">
                <input type="hidden" name="phone" value="" class="js-phone-target">
                <div class="field">
                    <input type="text" name="code" placeholder="Код активации" value="" data-validate="text-required">
                </div>
                <div class="field relative">
                    <button type="button" class="js-password-toggler eye-button" tabindex="-1"><?icon("eye")?></button>
                    <input type="password" name="password" placeholder="Новый пароль" value="" data-validate="password">
                </div>
                <div class="field relative">
                    <button type="button" class="js-password-toggler eye-button" tabindex="-1"><?icon("eye")?></button>
                    <input type="password" name="confirmPassword" placeholder="Повторите пароль" value="" data-validate="confirm-password">
                </div>
                <div class="ui error message"></div>
                <div class="ui success message">
                    <p class="uppercase">Пароль обновлен</p>
                </div>
                <div class="center-aligned vertical-nested">
                    <button class="ui primary button" type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
