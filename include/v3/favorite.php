<div class="ui grid favorite">
    <div class="column eleven wide computer sixteen wide tablet sixteen wide mobile">
        <div class="ui grid three column stackable doubling catalog-list">
          <?
          $APPLICATION->IncludeFile("/include/v3/favorite-component.php", array(
            "FAVORITE_CLASS" => "column favorite-column",
            "UNWRAP" => false
          ), array("MODE" => "php", 'SHOW_BORDER' => false));
          $list = \Product\Favorite::getList();
          if (empty($list)) {
              ?>
              <div class='ui header normal big'>Вы еще не добавили товары в избранное</div>
            <?
          }
          ?>
        </div>
    </div>
    <div class="column five wide computer sixteen wide tablet info sixteen wide mobile">
        <div class="top-content">
            <a href="/api/favorite.clear/" class="black icon clear">
              <? icon("trash") ?>
                Очистить избранное
            </a>
        </div>
        <div class="questions mobile hidden">
            <p class="gray-color">
                Появились вопросы<br>или предложения?<br>Напишите нам
            </p>
            <div class="write-us">
              <? icon("email") ?>Написать нам
            </div>
        </div>
    </div>
</div>