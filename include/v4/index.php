<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$menu = $GLOBALS['catalogMenu'];
?>
<div class="ui container">
    <div class="cd-section visible">
        <?
        Dynamic::begin("v3-index");

        if (true) {
            ?>
            <div class="primary-block clear august">
                <div class="image-gallery-container">
                    <div class="image-gallery break-gallery dark-controls" data-count="1" data-pager="true"
                         data-margin="0"
                         data-controls="true">
                        <?
                        if (true) {
                            ?>
                            <div class="item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/images/slider/flower.png" alt=""
                                     class="gadget only slider-background">
                                <div class="ui container">
                                    <div class="ui grid">
                                        <div class="column eight wide large screen five wide computer six wide hd content-down tablet hidden mobile hidden">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/slider/flower.png" alt=""
                                                 class="slider-image">
                                        </div>
                                        <div class="column eight wide large screen eleven wide computer ten wide hd sixteen wide mobile sixteen wide tablet">
                                            <div class="ui header h1 normal uppercase">День<br>медицинского<br>работника
                                            </div>
                                            <div class="ui header h3 normal uppercase">Скидка на весь ассортимент 20%
                                            </div>
                                            <div class="hint gray-color">Только для участников <a href="/bonus-system/">бонусной
                                                    программы</a></div>
                                            <p class="gray-color">Срок проведения акции<br>с 16.06.19 по 18.06.19</p>
                                            <?
                                            if (!BonusCard::isset()) {
                                                ?>
                                                <a class="ui primary button" href="/bonus-system/registration/">
                                                    Получить бонусную карту
                                                </a><?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                        } else {
                            ?>
                            <div class="item">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/images/slider/august.png?v=2" alt=""
                                     class="slider-background">
                                <div class="ui container">
                                    <div class="ui grid">
                                        <div class="column eight wide large screen eleven wide computer eight wide hd sixteen wide mobile sixteen wide tablet">
                                            <div class="ui header h1 normal uppercase">Белый<br> тебе к лицу
                                            </div>
                                            <br>
                                            <div class="hint gray-color">Для держателей <a href="/bonus-system/">бонусных
                                                    карт</a> весь август действует <br>скидка 50% на каждый 3-ий товар в
                                                корзине
                                            </div>
                                            <p class="gray-color"><a href="/conditions/event-white-to-you/">Полные
                                                    условия
                                                    акции</a></p>
                                            <?
                                            if (!BonusCard::isset()) {
                                                ?>
                                                <a class="ui primary button" href="/bonus-system/registration/">
                                                    Получить бонусную карту
                                                </a><?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?
        } else {
            if (false) {
                ?>
                <div class="warning-korona-container">
                    <div class="ui container">
                        <div class="warning-korona">
                            <div class="ui grid bottom aligned">
                                <div class="column eight wide large screen sixteen wide computer sixteen wide tablet sixteen wide mobile">
                                    <h2 class="ui header">Дорогие друзья,<br>
                                        мы продолжаем работать для вас!</h2>
                                    <p>В это непростое время мы стараемся делать все возможное, чтобы обеспечить
                                        медицинской одеждой врачей и медперсонал, людей, которые находятся «на
                                        передовой». Конечно же,
                                        на первом месте для нас стоит безопасность и здоровье наших покупателей и
                                        сотрудников.
                                        В связи с этим мы внесли некоторые изменения в сервисы согласно текущей
                                        ситуации.
                                    </p>
                                    <p>
                                        Наш интернет-магазин продолжает работу. Вы можете заказать понравившиеся
                                        модели
                                        на сайте. Менеджер интернет-магазина свяжется с вами по телефону для
                                        уточнения
                                        заказа.
                                        Мы благодарны за то, что вы остаетесь с нами. Надеемся на скорое возвращение
                                        в привычный режим работы!
                                    </p>
                                    <h2 class="ui header">Спасибо за поддержку и понимание!</h2>
                                    <p>Со всеми правилами работы интернет-магазина, информацией о способах доставки
                                        вы
                                        можете ознакомиться на странице <a href="/faq/">“ВАЖНО”</a>.
                                        Если у вас возникнут дополнительные вопросы, наши менеджеры будут рады вам
                                        помочь.</p>
                                </div>
                                <div class="column eight wide large screen sixteen wide computer sixteen wide tablet sixteen wide mobile">
                                    <a href="/faq/" class="ui primary button">Частые вопросы</a>
                                    <div onclick="$('.warning-korona-container').transition('fade out', function(){$('.primary-block').transition('fade in')})"
                                         class="ui primary button">Закрыть
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="primary-block transition hidden">
                    <h1 class="ui header normal uppercase">Особое внимание <br/>к деталям</h1>
                    <p class="gray-color">Мы моделируем, производим<br class="mobile only"/> и продаем качественные
                        <br/>и
                        удобные
                        вещи для врачей.</p>
                </div>
                <?
            } else {
                if (true) {
                    $asset = \Bitrix\Main\Page\Asset::getInstance();
                    $asset->addCss('/protection/protection-index.css');
                    ?>
                    <div class="primary-block with-protection">
                        <h1 class="ui header normal uppercase"><span>Ценности остаются.<br>Мы меняем только форму</span><br><br>Комплекты<br>медицинские<br>COVID-19</h1>
                        <p class="gray-color">Многоразового применения<br>
                            от производителя</p>
                        <div>
                        <a href="/protection/" class="ui primary custom button">Подробнее</a>
                        </div>
                        <div class="protection__top-reg mobile hidden">
                            <div class="protection__top-reg-icon">
                                <?=icon('protect-register')?>
                            </div>
                            <div class="protection__top-reg-text">
                                Имеет регистрационное
                                <br>
                                удостоверение на мед. изделия
                            </div>
                        </div>
                    </div>
                    <?
                } else {
                    ?>
                    <div class="primary-block">
                        <h1 class="ui header normal uppercase">Особое внимание <br/>к деталям</h1>
                        <p class="gray-color">Мы моделируем, производим<br class="mobile only"/> и продаем качественные
                            <br/>и
                            удобные
                            вещи для врачей.</p>
                    </div>
                    <?
                }
            }
        }
        Dynamic::end(null, null, true);
        ?>
    </div>
    <div class="block-container">
        <div class="collection primary block">
            <div class="ui grid">
                <div class="column eleven wide computer sixteen wide mobile sixteen wide tablet main">
                    <div class="fit-container top">
                        <div class="header">Новая <br/>коллекция</div>
                        <picture>
                            <source srcset="<?= SITE_TEMPLATE_PATH ?>/images/collection-main-1.webp" type="image/webp">
                            <source srcset="<?= SITE_TEMPLATE_PATH ?>/images/collection-main-1.jpg" type="image/jpeg">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/collection-main-1.jpg" alt="Cameo"/>
                        </picture>
                    </div>
                </div>
                <div class="column additional five wide computer sixteen wide tablet sixteen wide mobile">
                    <div class="fit-container top"><img src="<?= SITE_TEMPLATE_PATH ?>/images/collection-main-2.jpg"
                                                        alt="Cameo"/>
                    </div>
                    <div class="fit-container top"><img src="<?= SITE_TEMPLATE_PATH ?>/images/collection-main-3.jpg"
                                                        alt="Cameo"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <h2 class="ui header uppercase">Новинки</h2>
            <div class="product-slider-container">
                <div class="product-slider dark-controls ui grid three column">
                    <? $APPLICATION->IncludeComponent(
                      "bitrix:catalog.section",
                      "",
                      array(
                        "CLASS" => "column",
                        "ACTION_VARIABLE" => "action",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_TO_BASKET_ACTION" => "ADD",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BACKGROUND_IMAGE" => "-",
                        "BASKET_URL" => "/personal/basket.php",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "COMPATIBLE_MODE" => "Y",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "CONVERT_CURRENCY" => "N",
                        "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[{\"CLASS_ID\":\"CondIBProp:38:217\",\"DATA\":{\"logic\":\"Equal\",\"value\":288}}]}",
                        "DETAIL_URL" => "",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "FILTER_NAME" => "arrFilter",
                        "HIDE_NOT_AVAILABLE" => "Y",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                        "IBLOCK_ID" => "38",
                        "IBLOCK_TYPE" => "catalog",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "LABEL_PROP" => "-",
                        "LINE_ELEMENT_COUNT" => "3",
                        "MESSAGE_404" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_COMPARE" => "Сравнить",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "OFFERS_CART_PROPERTIES" => array(),
                        "OFFERS_FIELD_CODE" => array("", ""),
                        "OFFERS_LIMIT" => "0",
                        "OFFERS_PROPERTY_CODE" => array(0 => "COLOR",),
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => "18",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array(
                          0 => "Интернет [Базовая]",
                        ),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_DISPLAY_MODE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPERTIES" => array(),
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "PROPERTY_CODE" => array(
                          0 => "",
                          1 => "CML2_ARTICLE",
                          2 => "CML2_ATTRIBUTES",
                          3 => "POL",
                          4 => "NEWPRODUCT",
                          5 => "SALELEADER",
                          6 => "SPECIALOFFER",
                          7 => "MODEL",
                          8 => "",
                        ),
                        "SECTION_CODE" => "",
                        "SECTION_ID" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => "",
                        "SECTION_USER_FIELDS" => array("", ""),
                        "SEF_MODE" => "N",
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "SHOW_CLOSE_POPUP" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "TEMPLATE_THEME" => "blue",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N"
                      )
                    ); ?>
                </div>
            </div>
        </div>

        <div class="block">
            <div class="sale">
                <div class=" collections">
                    <div class="collection">
                        <div class="header">Женская коллекция</div>
                        <div class="ui vertical menu catalog-menu fluid">
                            <?
                            foreach ($menu['619']['ITEMS'] as $arItem) {
                                ?>
                                <a href="<?= $arItem['LINK'] ?>" class="item"><?= $arItem['TEXT'] ?></a>
                                <?
                            }
                            if (\Page\Options::getNow("sale")) {
                                ?><a class="item" href="<?= $menu['619']['LINK'] ?>?sale=true">
                                    Распродажа</a><?
                            }
                            ?>
                        </div>
                        <div class="fit-container"><img
                                    src="<?= SITE_TEMPLATE_PATH ?>/images/index/collection-woman.jpg" alt="Cameo"/>
                        </div>
                    </div>
                    <div class="collection">
                        <div class="header">Мужская коллекция</div>
                        <div class="ui vertical menu catalog-menu fluid">
                            <?
                            foreach ($menu['624']['ITEMS'] as $arItem) {
                                ?>
                                <a href="<?= $arItem['LINK'] ?>" class="item"><?= $arItem['TEXT'] ?></a>
                                <?
                            }
                            if (\Page\Options::getNow("sale")) {
                                ?><a class="item"
                                     href="<?= $menu['624']['LINK'] ?>?sale=true">Распродажа</a><?
                            }
                            ?>
                        </div>
                        <div class="fit-container"><img
                                    src="<?= SITE_TEMPLATE_PATH ?>/images/index/collection-man.jpg" alt="Cameo"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">

            <h2 class="ui header uppercase small-margin">Об одежде</h2>
            <div class="main-information">
                <?
                if ($GLOBALS['importantCity']) {
                    $rsDescription = CIBlockElement::GetList(array(), array(
                      "IBLOCK_ID" => 94,
                      "PROPERTY_city" => $GLOBALS['importantItemId'],
                      "PROPERTY_type_VALUE" => "index",
                      "ACTIVE" => "Y"
                    ), false, false, array("PREVIEW_TEXT", "ID"))->Fetch();
                    if ($rsDescription) {
                        echo $rsDescription['PREVIEW_TEXT'];
                        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(94, $rsDescription['ID']);
                        $propertyValues = $ipropValues->getValues();
                        if ($propertyValues['ELEMENT_PAGE_TITLE']) {
                            $APPLICATION->SetTitle($propertyValues['ELEMENT_PAGE_TITLE']);
                        }
                        if ($propertyValues['ELEMENT_META_TITLE']) {
                            $APPLICATION->SetPageProperty('title', $propertyValues['ELEMENT_META_TITLE']);
                        }
                        if ($propertyValues['ELEMENT_META_KEYWORDS']) {
                            $APPLICATION->SetPageProperty('keywords', $propertyValues['ELEMENT_META_KEYWORDS']);
                        }
                        if ($propertyValues['ELEMENT_META_DESCRIPTION']) {
                            $APPLICATION->SetPageProperty('description', $propertyValues['ELEMENT_META_DESCRIPTION']);
                        }
                    }
                } else {
                    $APPLICATION->IncludeComponent(
                      "bitrix:main.include",
                      "",
                      array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/v3/main/bottom"
                      )
                    );
                } ?>
            </div>
        </div>
    </div>
</div>
