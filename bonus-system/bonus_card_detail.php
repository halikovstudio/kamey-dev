<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var bool $IS_CRM
 */
?>
<script src="https://apis.google.com/js/platform.js" type="text/javascript"></script>
<div class="text--center personal__header">Бонусная карта</div>
<div class="ui grid">
    <div class="<?=$IS_CRM ? 'column sixteen wide' : 'seven wide computer seven wide tablet sixteen wide mobile column'?>">
        <img src="<?= SITE_TEMPLATE_PATH ?>/images/bonus-system/bonus_card_<?=BonusCard::percent()?>.png" alt=""
             class="ui image drop-shadow">
        <div class="passes-container">
            <div class="google-jwt"></div>
            <a href="/api/bonus.apple.bind/" class="apple-save-button">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/add-to-wallet.svg"
                     alt="Add to wallet" class="bonuscard__apple">
            </a>
        </div>
    </div>
    <div class="eight wide computer eight wide tablet sixteen wide mobile column right floated">
        <table class="ui very basic table bonuscard__table">
            <thead>
            <tr>
                <th>Карта</th>
                <th>Баланс</th>
            </tr>
            </thead>
            <tbody>
            <tr class="unborder">
                <td>
                    <div class="mobile only personal__th">Карта</div>
                    №<?= BonusCard::$number ?>
                </td>
                <td>
                    <div class="mobile only personal__th">Баланс</div>
                    <span class="bonuscard__count"><?=BonusCard::amount()?></span>
                    <span class="uppercase text--big text--medium mobile only">Детализация</span>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="bonuscard__dashed"></div>
        <table class="ui very basic table bonuscard__table unstackable">
            <thead>
            <tr>
                <th>Накопленная сумма</th>
                <th>Начисления</th>
            </tr>
            </thead>
            <tbody>
            <?
            foreach (BonusCard::matrix() as $sum => $percent) {
                $active = $percent == BonusCard::percent();
                ?>
                <tr<?= ($active ? ' class="actived"' : '') ?>>
                    <td><b><?= (CurrencyFormat($active ? BonusCard::accumulated() : $sum, 'RUB')) ?></b></td>
                    <td><?= $percent ?>% от
                        <span class="mobile hidden">совершённой </span>
                        покупки.</td>
                </tr>
                <?
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<div class="personal__dashed"></div>
<div class="text--center personal__header">Детализация</div>
<table class="ui very basic table bonuscard__table unstackable">
    <thead class="mobile hidden">
    <tr>
        <th>
            <span class="uppercase text--medium text--small">Дата</span>
        </th>
        <th>
            <span class="uppercase text--medium text--small">Основание</span>
        </th>
        <th>
            <span class="uppercase text--medium text--small">Кол-во бонусов</span>
        </th>
        <th>
            <span class="uppercase text--medium text--small">Итого</span>
        </th>
    </tr>
    </thead>
    <tbody>
    <?
    foreach (BonusCard::history_list() as $item) {
        if ($item['Charge'] && $item['WriteOff']) {
            $item['Charge'] -= $item['WriteOff'];
            if ($item['Charge'] < 0) {
                $item['WriteOff'] = abs($item['WriteOff']);
            }
        }
        ?>
        <tr>
            <td class="mobile hidden"><?= date('d.m.Y', strtotime($item['TransactionDate'])) ?></td>
            <td><div class="mobile only personal__th"><?= date('d.m.Y', strtotime($item['TransactionDate'])) ?></div><?= $item['Comment'] ?></td>
            <td>
                <span<?= ($item['Charge'] ? '' : ' class="bonuscard__draw"') ?>><?= ($item['Charge'] ? '+'.$item['Charge'] : '-' . $item['WriteOff']) ?></span>
            </td>
            <td><?= $item['EndingBalance'] ?></td>
        </tr>
        <?
    }
    ?>
    </tbody>
</table>
<script>
  $(function() {
    $.get('/api/bonus.google.jwt/', function(response) {
      if (response.jwt) {
        var saveToAndroidPay = document.createElement('g:savetoandroidpay');
        saveToAndroidPay.setAttribute('jwt', response.jwt);
        saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
        saveToAndroidPay.setAttribute('onfailure', 'failureHandler');
        saveToAndroidPay.setAttribute('height', 'standard');
        saveToAndroidPay.setAttribute('size', 'matchparent');
        document.querySelector('.passes-container .google-jwt').appendChild(saveToAndroidPay);
        var script = document.createElement('script');
        script.src = 'https://apis.google.com/js/plusone.js';
        document.head.appendChild(script);
      }
    });
  });
</script>
