<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var bool $INCLUDE_FORM
 */
global $location;

use Bitrix\Main\Page\Asset; ?>
<div class="bonuscard__container">
    <div class="text--center personal__header">Бонусная карта</div>
    <div class="mb56">
        <img src="<?= SITE_TEMPLATE_PATH ?>/images/bonuscard.png" alt=""
             class="ui image drop-shadow bonuscard__image">
    </div>
    <div class="bonuscard__advantages">
        <div class="ui grid doubling  four column">
            <div class="column">
                <div class="ui image mb24">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/bonus-system/advantages/1.svg">
                </div>
                <div class="personal__text mbm24">Приветственные 300 бонусов являются несгораемыми
                </div>
            </div>
            <div class="column">
                <div class="ui image mb24">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/bonus-system/advantages/2.svg">
                </div>
                <div class="personal__text mbm24">5% от Вашей покупки
                    будут зачислены на Вашу карту
                </div>
            </div>
            <div class="column">
                <div class="ui image mb24">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/bonus-system/advantages/3.svg">
                </div>
                <div class="personal__text mbm24">Бонусами можно
                    оплатить 100% Вашей покупки
                </div>
            </div>
            <div class="column">
                <div class="ui image mb24">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/bonus-system/advantages/4.svg">
                </div>
                <div class="personal__text mbm24">Один бонус равен
                    одному рублю
                </div>
            </div>
        </div>
    </div>
    <?
    if ($location['city'] == 'Уфа' && $INCLUDE_FORM) {
        ?>
        <div class="bonuscard__snap">
            <div class="uppercase text--medium text--small mb16">Привязать свою карту</div>
            <div class="ui grid">
                <div class="nine wide computer nine wide tablet sixteen wide mobile column">
                    <form class="ui form add-card-form">
                        <input type="hidden" name="send_code" value="N">
                        <div class="fields custom">
                            <div class="ten wide field custom transition visible">
                                <input type="text" name="number" placeholder="Номер карты">
                            </div>
                            <div class="ten wide field transition hidden code-field">
                                <div class="code-input">
                                    <input type="text" name="code[0]">
                                    <input type="text" name="code[1]">
                                    <input type="text" name="code[2]">
                                    <input type="text" name="code[3]">
                                    <input type="text" name="code[4]">
                                </div>
                                <br>
                                <p class="gray-color">Введите код из смс</p>
                            </div>
                            <div class="six wide field custom personal__data-button">
                                <button class="ui mini basic button custom">Подтвердить</button>
                            </div>
                        </div>
                        <div class="ui error message"></div>
                    </form>
                </div>
                <div
                        class="seven wide computer seven wide tablet sixteen wide mobile column right aligned personal__data-button">
                    <div class="uppercase text--medium text--small mb16 text--left mobile only">Выпустить новую
                        карту
                    </div>
                    <button class="ui mini primary button custom" data-modal="get-card">Выпустить карту</button>
                </div>
            </div>
        </div>
    <? } ?>
    <div class="bonuscard__description">
        <div class="ui grid">
            <div class="twelve wide computer twelve wide tablet sixteen wide mobile column">
                <div class="personal__text personal__mute">
                    <p>Бонусную карту Вы можете получить при покупке от 3000 р. Бонусы в размере 5% от Вашей
                        покупки сразу
                        будут зачислены на Вашу карту. Один бонус равен одному рублю. Бонусами Вы можете
                        оплатить до 100%
                        Вашей
                        следующей покупки.
                    </p>
                    <p>
                        По мере накопления покупок сумма бонусов, начисляемая
                        на Вашу карту, увеличивается:
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="ui grid">
        <div class="eleven wide computer eleven wide tablet sixteen wide mobile column">
            <table class="ui very basic table bonuscard__table unstackable">
                <thead>
                <tr>
                    <th>Накопленная сумма</th>
                    <th>Начисления</th>
                </tr>
                </thead>
                <tbody class="available">
                <tr>
                    <td>
                        <b>10 000 р.</b>
                    </td>
                    <td>7% от
                        <span class="mobile hidden">совершённой </span>
                        покупки.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>40 000 р.</b>
                    </td>
                    <td>10% от
                        <span class="mobile hidden">совершённой </span>
                        покупки.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>70 000 р.</b>
                    </td>
                    <td>12% от
                        <span class="mobile hidden">совершённой </span>
                        покупки.
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?
if ($INCLUDE_FORM) {
    $email = $_GET['email'] ? $_GET['email'] : $USER->GetEmail();
    if ($email) {
        $arUser = CUser::GetList($by, $order, ['=EMAIL' => $email])->Fetch();
        BonusCard::explode_name($arUser);
        $full_name = implode(" ", array_filter([$arUser['LAST_NAME'], $arUser['NAME'], $arUser['SECOND_NAME']]));
    } else {
        $full_name = '';
        $arUser = [];
    }
    $asset = Asset::getInstance();
    $asset->addCss(SITE_TEMPLATE_PATH . '/css/suggestions.min.css');
    $asset->addJs(SITE_TEMPLATE_PATH . '/js/suggestions.min.js');
    ?>
    <div class="ui mini modal auth-modal" id="get-card">
        <div class="close">
            <? icon("close") ?>
        </div>
        <div class="modal-content auth-modal__content auth-modal__content_nonheight">
            <div class="">
                <div class="auth-modal__header">получение
                    <br>
                    бонусной карты
                </div>
                <form class="ui form small auth-modal__form bonus-registration-form">
                    <?= bitrix_sessid_post() ?>
                    <div class="field custom">
                        <input type="text" name="full_name" placeholder="ФИО держателя карты"
                               value="<?= $full_name ?>">
                        <input type="hidden" name="name" value="<?= $arUser['NAME'] ?>">
                        <input type="hidden" name="surname" value="<?= $arUser['LAST_NAME'] ?>">
                        <input type="hidden" name="patronymic" value="<?= $arUser['SECOND_NAME'] ?>">
                        <input type="hidden" name="gender"
                               value="<?= $arUser['PERSONAL_GENDER'] == 'M' ? 'MALE' : 'FEMALE' ?>">
                        <input type="hidden" name="email" value="<?= $email ?>">
                        <input type="hidden" name="send_code" value="N">
                    </div>
                    <div class="field custom">
                        <input type="date" name="birthday" placeholder="Дата вашего рождения"
                               value="<?= BonusCard::reverse_birthday($arUser['PERSONAL_BIRTHDAY'], 'Y-m-d') ?>">
                    </div>
                    <div class="field custom">
                        <input type="tel" name="phone" placeholder="Номер телефона"
                               value="<?= BonusCard::normalizePhone($arUser['PERSONAL_PHONE']) ?>">
                    </div>
                    <div class="field code-field transition hidden">
                        <div class="code-input">
                            <input type="text" name="code[0]">
                            <input type="text" name="code[1]">
                            <input type="text" name="code[2]">
                            <input type="text" name="code[3]">
                            <input type="text" name="code[4]">
                        </div>
                        <br>
                        <p class="gray-color">Введите код из смс</p>
                    </div>
                    <div class="ui error message"></div>
                    <div class="ui success message">Вы успешно зарегистрированы в бонусной программе</div>
                    <div class="field custom">
                        <button class="ui button mini fluid primary custom">Получить</button>
                    </div>
                </form>
                <div class="auth-modal__meta">Нажимая кнопку «Получить»,
                    вы принимаете согласие на <a
                            href="/legal-information/public_offer.php">обработку персональных данных</a>
                </div>
            </div>
        </div>
    </div>
    <script>
      $(function() {
        var form = document.querySelector('.bonus-registration-form');
        $(form).form({
          fields: {
            full_name: 'empty',
            phone: 'inputmask',
            birthday: 'empty',
          },
          inline: true,
        }).api({
          serializeForm: true,
          onSuccess: function(data) {
            if (data.enter_code) {
              $(this).removeClass('success');
              $(this).find('.code-field').transition('scale in');
              $(this).find('[name="send_code"]').val('Y');
            } else {
              $(this).find('input,button').addClass('disabled').prop('disabled', true);
            }
          },
          onFailure: function(data) {
            if (data.field) {
              return $(this).form('add prompt', data.field, data.message);
            } else if (data.message) {
              return $(this).form('add errors', [data.message]);
            }
          },
          url: 'bonus.register',
        });
        $('[name=full_name]').suggestions({
          token: '603780ba9dc89ca3a01bdc215b0d5314db1598a1',
          type: 'NAME',
          autoSelectFirst: true,
          onSelect: function(suggestion, changed) {
            Object.keys(suggestion.data).forEach(function(key) {
              var element = form.querySelector('[name="' + key + '"]');
              if (element) {
                element.value = suggestion.data[key];
              }
            });
          },
          onSelectNothing: function() {
            this.value = '';
          },
        });
      });
      $('[name=phone]').inputmask('+7 (999) 999-99-99');
    </script>
    <?
}
?>
