<?php
if (!$_REQUEST['path'] || !$_REQUEST['importantId']) {
    die();
}

$initialPath = $_REQUEST['path'];
$_REQUEST['path'] = explode("?", $_REQUEST['path'])[0];

$path = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['path'];
$chunk = $_REQUEST['path'];
$city = explode('/', $_SERVER['REQUEST_URI'])[1];
$GLOBALS['cityChunk'] = "/" . $city;
$GLOBALS['importantCity'] = $city;
$GLOBALS['importantCityId'] = $_GET['importantId'];
$GLOBALS['importantItemId'] = $_GET['importantItemId'];
unset($_GET['importantId'], $_GET['path'], $_GET['importantItemId']);
$_SERVER['REQUEST_URI'] = str_replace("importantId=" . $_GET['importantId'], "", $_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI'] = str_replace("path=" . $_GET['path'] . '&', "", $_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI'] = str_replace("importantItemId=" . $_GET['importantItemId'], "", $_SERVER['REQUEST_URI']);
if (!isset($_GET['bxrand']) && $initialPath != "/" && false) {
    foreach (array(
               "REQUEST_URI",
               "REDIRECT_URL",
               "SCRIPT_URL",
               "SCRIPT_URI",
               "REDIRECT_SCRIPT_URI",
               "REDIRECT_SCRIPT_URL"
             ) as $key) {
        $_SERVER[$key] = str_replace("/" . $city, "", $_SERVER[$key]);
    }
} else {
    $GLOBALS['serverReplace'] = array(
      "original" => array(),
      "replace" => array()
    );
    foreach (array(
               "REQUEST_URI",
               "REDIRECT_URL",
               "SCRIPT_URL",
               "SCRIPT_URI",
               "REDIRECT_SCRIPT_URI",
               "REDIRECT_SCRIPT_URL"
             ) as $key) {
        $GLOBALS['serverReplace']['original'][$key] = $_SERVER[$key];
        $GLOBALS['serverReplace']['replace'][$key] = str_replace("/" . $city, "", $_SERVER[$key]);
    }
    $GLOBALS['rewriteChunk'] = $GLOBALS['cityChunk'];
}

if (file_exists($path)) {
    $info = pathinfo($path);
    if ($info['extension'] && $_SERVER['DOCUMENT_ROOT'] . '/' != $path && !in_array($info['extension'],
        array("php", "html"))) {
        CHTTP::SetStatus("301 Moved permanently");

        header("Location: " . $initialPath);
        //LocalRedirect($initialPath,false,"301 Moved permanently");
        die();
    }
    if (is_dir($path)) {
        $index = "index.php";
        if ($chunk == "/about/") {
            $index = "about.php";
        }
        require($path . $index);
    } else {
        require($path);
    }
} else {
    $levels = substr_count($chunk, "/") - 1;
    $count = 1;
    $included = false;
    while ($levels > 1) {
        $levelPath = explode("/", $path);
        $levelPath = array_splice($levelPath, 0, count($levelPath) - $count - 1);
        $levelPath[] = "";
        $levelPath = implode("/", $levelPath);
        if (file_exists($levelPath)) {
            if (is_dir($levelPath)) {
                $index = "index.php";
                if ($chunk == "/about/") {
                    $index = "about.php";
                }
                $included = true;
                require($levelPath . $index);
            } else {
                $included = true;
                require($levelPath);
            }
            die();
            break;
        }
        $levels--;
        $count++;
    }
    if (!isset($_GET['bxrand'])) {
        require($_SERVER['DOCUMENT_ROOT'] . '/404.php');
    }
}
