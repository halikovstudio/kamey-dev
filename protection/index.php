<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Медицинские защитные комплекты");
$asset = \Bitrix\Main\Page\Asset::getInstance();
$asset->addCss('/protection/protection.css');
$asset->addJs('/protection/protection.js')
?>
<div class="ui container">
  <div class="ui grid protection__top">
    <div class="four wide computer four wide tablet sixteen wide mobile middle aligned column">
      <div class="protection__top-mobile mobile only">
        <img src="/include/protectmobile.png" alt="">
      </div>
      <h1 class="protection__top-header">Медицинские
        защитные
        комплекты
      </h1>
      <div class="protection__top-sub">Многоразового применения
        от производителя
      </div>
      <a href="#order" class="ui primary custom button mobile hidden">Сделать оптовый заказ</a>
      <div class="protection__top-reg mobile hidden">
        <div class="protection__top-reg-icon">
            <?=icon('protect-register')?>
        </div>
        <div class="protection__top-reg-text">
          Имеет регистрационное
          <br>
          удостоверение на мед. изделия
        </div>
      </div>
    </div>
    <div class="four wide column right floated mobile hidden">
      <div class="protection__top-right">
        <div class="protection__top-right-image">
          <img src="/include/protecttop.png" alt="">
        </div>
        <div class="protection__top-right-text">
          Внешний вид и цвета
          <br>
          могут отличаться от фото
        </div>
      </div>
    </div>
  </div>
  <div class="protection-block">
    <div class="ui grid">
      <div class="five wide widescreen six wide computer six wide tablet sixteen wide mobile column">
        <div class="protection-block__sub">Внешний комплект</div>
        <div class="protection-block__header">Комбинезон
          <br>
          с капюшоном и бахилы
        </div>
        <div class="protection-block__text">Данный комплект применяется при работе
          с патогенными биологическими агентами (ПБА) I-II
          или III-IV групп патогенности по ГОСТ Р 52905
        </div>
        <div class="protection-block__table yourparams">
          <div class="yourparams__item">
            <div class="yourparams__name">Ткань</div>
            <div class="yourparams__value">Nortsea</div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Плетение</div>
            <div class="yourparams__value">полотняное</div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Плотность</div>
            <div class="yourparams__value">155 г/м</div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Состав</div>
            <div class="yourparams__value">100% Микрополиэфир,
              <br>
              PU-мембрана
            </div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Отделка</div>
            <div class="yourparams__value">DWR (устойчивая
              <br>
              водоотталкивающая пропитка)
            </div>
          </div>
        </div>
      </div>
      <div class="five wide widescreen six wide computer six wide tablet sixteen wide mobile column mobile-first">
        <div class="protection-block__image-block">
          <div class="protection-block__image">
            <img src="/include/protectblock-1.png" alt="">
          </div>
          <div class="protection-block__image-text">
            Внешний вид и цвета
            <br>
            могут отличаться от фото
          </div>
          <div class="protection-block__popup js-protect-popup">
            <?=icon('plus')?>
          </div>
          <div class="ui custom popup protection-block__popup-content">
            <div class="protection-block__popup-content-text">
                Изделия для комплектации:<br>
                комбинезон,<br>
                бахилы (мягкие,либо с жесткой подошвой)<br>
                шлем,<br>
                халат,<br>
                фартук,<br>
                нарукавники.
            </div>
          </div>
        </div>
      </div>
      <div class="four wide computer four wide tablet sixteen wide mobile column right floated">
        <div class="protection-block__advantages">
          <div class="protection-block__item">
            <div class="protection-block__item-icon">
                <?=icon('water-resistant')?>
            </div>
            <div class="protection-block__item-text">
              Водоне-
              <br>
              проницаемость
            </div>
          </div>
          <div class="protection-block__item">
            <div class="protection-block__item-icon">
                <?=icon('quick-dry')?>
            </div>
            <div class="protection-block__item-text">
              Быстрое высыхание
              <br>
              после обработки
            </div>
          </div>
          <div class="protection-block__item">
            <div class="protection-block__item-icon">
                <?=icon('ecology')?>
            </div>
            <div class="protection-block__item-text">
              От 50 циклов
              <br>
              обработки
            </div>
          </div>
          <div class="protection-block__item">
            <div class="protection-block__item-icon">
                <?=icon('breathable')?>
            </div>
            <div class="protection-block__item-text">
              «Дышащие»
              <br>
              свойства PU-мембраны
            </div>
          </div>
        </div>
        <div class="protection__top-reg">
          <div class="protection__top-reg-icon">
              <?=icon('protect-register')?>
          </div>
          <div class="protection__top-reg-text">
            Имеет регистрационное
            <br>
            удостоверение на мед. изделия
          </div>

        </div>
        <a href="#order" class="protection-block__button ui primary custom fluid button mobile only">Сделать оптовый заказ</a>
      </div>
    </div>
  </div>
  <div class="protection-block">
    <div class="ui grid">
      <div class="five wide widescreen six wide computer six wide tablet sixteen wide mobile column">
        <div class="protection-block__sub">Внутренний комплект</div>
        <div class="protection-block__header">фуфайка и брюки
          <br>
          (конструкция унисекс)
        </div>
        <div class="protection-block__text">Данный костюм применяется
          под наружный защитный комплект
        </div>
        <div class="protection-block__table yourparams">
          <div class="yourparams__item">
            <div class="yourparams__name">Ткань</div>
            <div class="yourparams__value">Трикатажное полотно</div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Плетение</div>
            <div class="yourparams__value">Кулирка</div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Плотность</div>
            <div class="yourparams__value">145-150 г/м</div>
          </div>
          <div class="yourparams__item">
            <div class="yourparams__name">Состав</div>
            <div class="yourparams__value">Хлопок 100%
              <br>
              (или хлопок 95%, лайкра 5%).
            </div>
          </div>
        </div>
      </div>
      <div class="five wide widescreen six wide computer six wide tablet sixteen wide mobile column mobile-first">
        <div class="protection-block__image-block">
          <div class="protection-block__image">
            <img src="/include/protectblock-2.png" alt="">
          </div>
          <div class="protection-block__image-text">
            Внешний вид и цвета
            <br>
            могут отличаться от фото
          </div>
          <div class="protection-block__popup js-protect-popup">
              <?=icon('plus')?>
          </div>
          <div class="ui custom popup protection-block__popup-content">
            <div class="protection-block__popup-content-text">
                Изделия для комплектации:<br>
                комбинезон,<br>
                бахилы (мягкие,либо с жесткой подошвой)<br>
                шлем,<br>
                халат,<br>
                фартук,<br>
                нарукавники.
            </div>
          </div>
        </div>
      </div>
      <div class="four wide computer four wide tablet sixteen wide mobile column right floated">
        <div class="protection-block__advantages">
          <div class="protection-block__item">
            <div class="protection-block__item-icon">
                <?=icon('water-resistant')?>
            </div>
            <div class="protection-block__item-text">
              Воздухо-
              <br>
              проницаемость
            </div>
          </div>
          <div class="protection-block__item">
            <div class="protection-block__item-icon">
                <?=icon('feather')?>
            </div>
            <div class="protection-block__item-text">
              Гигиеничность
              <br>
              и комфорт
            </div>
          </div>
        </div>
        <div class="protection__top-reg">
          <div class="protection__top-reg-icon">
              <?=icon('protect-register')?>
          </div>
          <div class="protection__top-reg-text">
            Имеет регистрационное
            <br>
            удостоверение на мед. изделия
          </div>
        </div>
        <a href="#order" class="protection-block__button ui primary custom fluid button mobile only">Сделать оптовый заказ</a>
      </div>
    </div>
  </div>
  <div class="protection-block" id="order">
    <div class="ui grid">
      <div class="four wide widescreen five wide computer five wide tablet sixteen wide mobile column">
        <div class="protection-block__header protection-block__header_marged">Оформление
          <br>
          оптового заказа
        </div>
        <div class="protection-block__text">Заполните форму и наши менеджеры
          <br>
          свяжуться с вами в ближайшее время
        </div>
        <div class="protection-block__form">
          <form class="ui small form js-user-validate" action="javascript:void(null)" data-action="order.protect">
              <input type="hidden" name="trace">
            <div class="field custom">
              <input type="text" name="fio" placeholder="ФИО" data-validate="text-required">
            </div>
            <div class="field custom">
              <input type="tel" name="phone" placeholder="Номер телефона" data-validate="phone-required">
            </div>
            <div class="field custom">
              <input type="email" name="email" placeholder="Электронная почта" data-validate="email-required">
            </div>
            <div class="field custom">
              <div class="ui button primary submit mini fluid custom">Оформить</div>
            </div>
            <div class="ui success message"></div>
          </form>
          <div class="protection-block__form-meta">Нажимая кнопку «Оформить», вы принимаете согласие на обработку
            <a target="_blank" href="">персональных данных</a>
          </div>
        </div>
      </div>
      <div class="twelve wide widescreen eleven wide computer column mobile hidden">
        <div class="protection-block__order-image">
          <img src="/include/protectorder.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
