$(document).ready(function () {
  $('.js-protect-popup').each(function () {
    var $target = $(this).siblings('.ui.popup')
    $(this).popup(
      {
        popup : $target,
        on    : 'click',
        position: 'right center'
      }
    )
  });
  $(window).on('load', function(){
    $(".protection-block__form [name=trace]").val(window.b24Tracker.guest.getTrace());
  })
})
