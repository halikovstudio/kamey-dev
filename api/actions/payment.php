<?
/**
 * @var Api $api
 **/

use Bitrix\Sale\BusinessValue;
use Bitrix\Sale\Order;

$api->space('payment', function () {
    CModule::IncludeModule('sale');
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== "off" ? 'https://' : 'http://';
    $domain_name = $_SERVER['HTTP_HOST'];
    $logger = function ($url, $method, $data, $response, $title) {
        $objDateTime = new DateTime();
        $file = $_SERVER['DOCUMENT_ROOT'] . '/local/sberbank-pay.log';
        $logContent = '';

        if (file_exists($file)) {
            $logSize = filesize($file) / 1000;
            if ($logSize < 5000) {
                $logContent = file_get_contents($file);
            }
        }
        $logContent .= $title . "\n";
        $logContent .= '----------------------------' . "\n";
        $logContent .= "DATE: " . $objDateTime->format("Y-m-d H:i:s") . "\n";
        $logContent .= 'URL ' . $url . "\n";
        $logContent .= 'METHOD ' . $method . "\n";
        $logContent .= "DATA: \n" . print_r($data, true) . "\n";
        $logContent .= "RESPONSE: \n" . print_r($response, true) . "\n";
        $logContent .= "\n\n";
        file_put_contents($file, $logContent);
    };

    if (SITE_DIR == '/' || strlen(SITE_DIR) == 0) {
        $site_dir = '/';
    } else {
        if (substr(SITE_DIR, 0, 1) != '/') {
            $site_dir = '/' . SITE_DIR;
        }
        if (substr(SITE_DIR, -1, 1) != '/') {
            $site_dir = SITE_DIR . '/';
        }
    }
    global $siteUrl;
    $siteUrl = $protocol . $domain_name . $site_dir;

    function _makePaymentRequest($orderId, $url, $data, $useAmount = true, $auth = true, $json = false, $repeat = 30)
    {
        global $siteUrl;
        $order = Order::load($orderId);
        $payment = BonusCard::getAnotherPayment($order->getPaymentCollection());
        $system = $payment->getPaySystem();
        $login = BusinessValue::getValueFromProvider($payment, "SBERBANK_GATE_LOGIN", $system->getConsumerName());
        $password = BusinessValue::getValueFromProvider($payment, "SBERBANK_GATE_PASSWORD", $system->getConsumerName());
        $orderNumber = BusinessValue::getValueFromProvider($payment, "SBERBANK_ORDER_NUMBER",
          $system->getConsumerName());
        $amount = BusinessValue::getValueFromProvider($payment, "SBERBANK_ORDER_AMOUNT", $system->getConsumerName());
        $isTest = BusinessValue::getValueFromProvider($payment, "SBERBANK_GATE_TEST_MODE",
            $system->getConsumerName()) == 'Y';
        $returnUrl = $siteUrl . 'sberbank/result.php' . '?PAYMENT=SBERBANK&ORDER_ID=' . $payment->getField('ORDER_ID') . '&PAYMENT_ID=' . $payment->getField('ID');

        if ($auth) {
            $data['userName'] = $login;
            $data['password'] = $password;
        }
        if ($useAmount) {
            $data['amount'] = $amount * 100;
            $data['returnUrl'] = $returnUrl;
            $data['failUrl'] = $returnUrl;
        }

        $response = null;
        $saveResponse = null;
        for ($i = 0; $i < $repeat; $i++) {
            if (isset($data['orderNumber'])) {
                $data['orderNumber'] = $orderNumber . '_' . $i;
            }

            $headers = [
              'x-original-domain-name: www.kamey.ru'
            ];

            if ($json) {
                $headers[] = 'Content-Type: application/json';
            }

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json ? json_encode($data) : $data);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            $res = curl_exec($curl);
            curl_close($curl);

            $saveResponse = $res;
            $response = json_decode($res, true);
            if (!$response['success'] && ($response['error']['code'] == 10 && $response['error']['message'] == 'Заказ с таким номером уже зарегистрирован' || $response['errorCode'] == 1 && $response['errorMessage'] == 'Заказ с таким номером уже обработан')) {
                continue;
            } else {
                break;
            }
        }

        return [
          'response' => $response,
          'saveResponse' => $saveResponse,
          'data' => $data
        ];
    }

    $this->action("google", function () use ($siteUrl, $logger) {
        /** @var Api $this */
        /** @var Order $order */
        $url = 'https://securepayments.sberbank.ru/payment/google/payment.do';

        $result = _makePaymentRequest($this->data['orderId'], $url, [
          'merchant' => 'kamey',
          'ip' => $_SERVER['REMOTE_ADDR'],
          'paymentToken' => base64_encode($_POST['token']),
          'preAuth' => false,
          'orderNumber' => ''
        ], true, false, true);

        $logger($url, 'POST', $result['data'], $result['response'], 'REGISTER GOOGLE PAY PAYMENT');
        if ($result['response']['success']) {
            $this->success([
              'data' => $result['response']['data']
            ]);
        } else {
            $this->error($result['response']['error']['message']);
        }
    });

    $this->action("apple.validate", function () use ($logger) {
        /** @var Api $this */
        if (!$this->data['appleUrl']) {
            $this->error("Необходима ссылка валидации");
        }
        if (!$this->data['orderId']) {
            $this->error('Укажите номер заказа');
        }
        $url = 'https://securepayments.sberbank.ru/payment/rest/register.do';
        $result = _makePaymentRequest($this->data['orderId'], $url, ['orderNumber' => '']);
        $logger($url, 'POST', $result['data'], $result['response'], 'REGISTER SBERBANK ORDER');
        $orderId = $result['response']['orderId'];
        $url = 'https://securepayments.sberbank.ru/payment/applepay/validateMerchant.do';
        $result = _makePaymentRequest($this->data['orderId'], $url,
          ['mdOrder' => $orderId, 'validationUrl' => $this->data['appleUrl']], false, false, true, 1);
        $logger($url, 'POST', $result['data'], $result['response'], 'VALIDATE APPLE SESSION');
        if ($result['response']['success']) {
            $this->success([
              'data' => $result['response']['data'],
              'mdOrder' => $orderId
            ]);
        }
        $this->error('Произошла ошибка');
    });

    $this->action("apple.pay", function () use ($siteUrl, $logger) {
        /** @var Api $this */

        $url = "https://securepayments.sberbank.ru/payment/applepay/paymentOrder.do";
        $result = _makePaymentRequest($this->data['orderId'], $url, [
          'mdOrder' => $this->data['mdOrder'],
          'paymentToken' => base64_encode($this->data['token'])
        ], false, false, true, 1);
        $logger($url, 'POST', $result['data'], $result['response'], 'REGISTER APPLE PAY PAYMENT');
        if ($result['response']['success']) {
            $this->success([
              'data' => [
                'approved' => true,
                'redirectUrl' => $result['response']['data']['redirectUrl']
              ]
            ]);
        } else {
            $this->error($result['response']['error']['message']);
        }
    });
});
