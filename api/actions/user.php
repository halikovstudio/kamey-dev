<?php
/** @var Api $api */

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\ObjectException;
use Bitrix\Main\UserConsent\Consent;
use Rarus\Sms4b\Sms4bException;
use View\Subscribe;
use bonus\google\Loyalty;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use CodeItNow\BarcodeBundle\Utils\QrCode;

$api->space("user", function () {
    /** @var Api $this */
    global $USER;
    $this->action("getinfo", function () {
        /** @var Api $this */
        $list = [];
        if (CUser::IsAuthorized()) {
            Loader::includeModule("sale");
            $arFilter = Array("USER_ID" => CUser::GetID(), "LID" => SITE_ID);
            $count = 0;
            $db_sales = CSaleOrder::GetList(array(), $arFilter, array());
            $arUser = CUser::GetByID(CUser::GetID())->Fetch();
            $list['orders'] = $db_sales;
            $list['username'] = CUser::GetFullName() ?: "Инкогнито";
            $list['phone'] = $arUser['PERSONAL_PHONE'];
        }
        $this->success(array(
          "list" => $list
        ));
    });

    $this->action("test", function () use ($USER) {
        /** @var Api $this */
        if ($USER->IsAdmin()) {
            $rs = CUser::GetList($by, $order);
            $phones = [];
            while ($ob = $rs->Fetch()) {
                if ($ob['PERSONAL_PHONE']) {
                    $phones[substr(BonusCard::simplify_phone($ob['PERSONAL_PHONE']), 1)][] = $ob['ID'];
                }
            }
            $phones = array_filter($phones, function ($item) {
                return count($item) > 1;
            });
            echo "<pre>";
            print_r($phones);
            echo "</pre>";
            die();
        }
    });

    $this->action("change.phone.send", function () {
        /** @var Api $this */
        $phone = $this->data['phone'];
        if (!$phone) {
            $this->error('Укажите номер телефона', 'phone');
        }
        $sessid = $this->data['sessid'];
        $user = BonusCard::find_user_by_phone($phone, false);
        if ($user) {
            $this->error('Указанный номер телефона занят', 'phone');
        }

        $iblockCode = 100;
        $arSelect = ["ID", "NAME", "CODE", "XML_ID", "TIMESTAMP_X"];
        $arFilter = [
          "IBLOCK_ID" => $iblockCode,
          [
            "LOGIC" => "OR",
            ["NAME" => $phone],
            ["XML_ID" => $sessid]
          ]
        ];
        $exist = CIBlockElement::GetList(['ID' => 'DESC'], $arFilter, false, ["nTopCount" => 1], $arSelect)->Fetch();
        if ($exist) {
            if ($this->data['replay']) {
                $time = time() - strtotime($exist["TIMESTAMP_X"]);
                if ($time < 60) {
                    $invertTime = 60 - $time;
                    $textTime = $invertTime <= 9 ? '0' . $invertTime : $invertTime;
                    $this->error([
                      'replay' => 'Повторная отправка доступна через 00:<span class="tiktok">' . $textTime . '</span>',
                    ]);
                }
            } else {
                $this->success();
            }
        }
        $code = mt_rand(1000, 9999);
        $mes = "Код подтверждения: " . $code;
        $arLoad = [
          'IBLOCK_ID' => $iblockCode,
          "NAME" => $phone,
          "CODE" => $code,
          "XML_ID" => $sessid
        ];
        $el = new CIBlockElement;
        if (!$el->Add($arLoad)) {
            $this->error("Произошла ошибка. Попробуйте позже", 'phone');
        }
        if (CModule::IncludeModule("smsc.sms")) {
            if (!SMSC_Send::Send_SMS(BonusCard::normalizePhone($phone), $mes)) {
                $this->error('Смс не отправлено', 'phone');
            }
        } else {
            $this->error('Модуль не подключился', 'phone');
        }
    });

    $this->action("change.phone.confirm", function () {
        /** @var Api $this */
        $result = $this->call('user.auth-code', $this->data);
        if ($result['status'] == Api::ERROR) {
            $this->error($result);
        }
        $phone = $this->data['phone'];
        $user = BonusCard::find_user_by_phone($phone, false);
        if ($user) {
            $this->error('Указанный номер телефона занят', 'phone');
        }
        global $USER;
        $USER->Update(CUser::GetID(), ['PERSONAL_PHONE' => BonusCard::normalizePhone($this->data['phone'])]);
    });

    $this->action("auth-phone", function () {
        /** @var Api $this */
        $phone = str_replace(array("(", ")", "-", " "), "", $this->data['phone']);
        $sessid = $this->data['sessid'];
        $registration = true;
        $curUser = false;
        if (!$phone) {
            $this->error(array(
              "form-errors" => array("phone-required" => "Неободимо заполнить телефон")
            ));
        }
//        $obuser = Bitrix\Main\UserTable::getList(Array(
//            "select" => Array("PERSONAL_PHONE"),
//            "filter" => array(
//                array(
//                    "LOGIC" => "OR",
//                    Array(
//                        "=LOGIN" => $phone,
//                    ),
//                    Array(
//                        "PERSONAL_PHONE" => (str_replace(array("(", ")", "-", " "), "", $phone))
//                    )
//                )
//            )
//        ));
//        while ($user = $obuser->fetch()){
//            $userPhone = $user['PERSONAL_PHONE'];
//            if($userPhone) {
//                $phoneDecode = str_replace(array("(", ")", "-", " "), "", $userPhone);
//                if($phoneDecode == $phone){
//                    $registration = false;
//                    break;
//                }
//            }
//        }
        $user = BonusCard::find_user_by_phone($phone, false);
        if ($user) {
            $registration = false;
        }
        $phone = $user['PERSONAL_PHONE'] ?: $phone;

        $iblockCode = 100;
        $arSelect = ["ID", "NAME", "CODE", "XML_ID", "TIMESTAMP_X"];
        $arFilter = [
          "IBLOCK_ID" => $iblockCode,
          [
            "LOGIC" => "OR",
            ["NAME" => $phone],
            ["XML_ID" => $sessid]
          ]
        ];
        $exist = CIBlockElement::GetList(['ID' => 'DESC'], $arFilter, false, ["nTopCount" => 1], $arSelect)->Fetch();
        if ($exist) {
            $time = time() - strtotime($exist["TIMESTAMP_X"]);
            if ($time < 60) {
                $invertTime = 60 - $time;
                $textTime = $invertTime <= 9 ? '0' . $invertTime : $invertTime;
                $this->error([
                  'replay' => 'Повторная отправка доступна через 00:<span class="tiktok">' . $textTime . '</span>',
                ]);
            }
        }

        $code = mt_rand(1000, 9999);
        $mes = "$code — ваш код аутентификации для " . $_SERVER['SERVER_NAME'];
        if (CModule::IncludeModule("smsc.sms")) {
            if (!SMSC_Send::Send_SMS(str_replace(array("(", ")", "-", " "), "", $phone), $mes)) {
                $this->error('Смс не отправлено');
            }
        } else {
            $this->error('Модуль не подключился');
        }
        $arLoad = [
          'IBLOCK_ID' => $iblockCode,
          "NAME" => $phone,
          "CODE" => $code,
          "XML_ID" => $sessid
        ];
        $el = new CIBlockElement;
        /**todo*/
        try {
            $el->Add($arLoad);
        } catch (Exception $e) {
        }
        $this->success([
          'phoneCode' => $user['PERSONAL_PHONE'] ?: $phone,
          'registration' => $registration,
          'user' => $user
        ]);

        $this->error('Технические неполадки');
    });
    $this->action("auth-code", function () use ($USER) {
        /** @var Api $this */
        if ($USER->IsAuthorized()) {
            $this->success("Authorized");
        }
        $phone = $this->data['phone'];
        $code = $this->data['code'];
        if (!$phone || !$code) {
            $this->error("Нет данных");
        }
        $user = BonusCard::find_user_by_phone($phone, false);
        if ($user) {
            $exist = getAccessCode($phone);
            if ($exist['CODE'] == $code) {
                if ($USER->Authorize($user['ID'], true)) {
                    $this->success(array("custom" => "param"));
                } else {
                    $this->error(array(
                      "form-errors" => array("code" => "Учетная запись не найдена")
                    ));
                }
            } else {
                $this->error(array(
                  "form-errors" => array("code" => "Код недействителен")
                ));
            }
        } else {
            $this->error(array(
              "form-errors" => array("code" => "Учетная запись не найдена")
            ));
        }
        $this->error('Технические неполадки');
    });

    $this->action("reg-code", function () use ($USER) {
        /** @var Api $this */
        if ($USER->IsAuthorized()) {
            $this->success("Authorized");
        }
        $phone = $this->data['phone'];
        $code = $this->data['code'];
        if (!$phone) {
            $this->error("Ошибка при регистрации");
        }

        $exist = getAccessCode($phone);
        if ($exist['CODE'] == $code) {
            $rsUser = $USER->GetList($by, $order, ['=EMAIL' => $this->data['email']])->Fetch();
            if ($rsUser) {
                if ($rsUser['PERSONAL_PHONE']) {
                    mail('office@kamey.ru', 'Необходимо связаться с клиентом',
                      'Необходимо связаться с клиентом ' . $this->data['email'] . ' для уточнения корректности ранее введенного телефона ' . $rsUser['PERSONAL_PHONE'] . '. Вводил ' . $this->data['phone']);
                    $this->error('Указанный email занят');
                } else {
                    $user = new CUser();
                    $user->Update($rsUser['ID'], ['PERSONAL_PHONE' => BonusCard::normalizePhone($this->data['phone'])]);
                    if ($USER->Authorize($rsUser['ID'], true)) {
                        $this->success(array("custom" => "param"));
                    }
                }
            }
            $password = generate_password(6);
            $email_code = randString(12);
            $arrFields = [
              'UF_EMAIL' => $this->data['email'],
              'UF_EMAIL_CODE' => $email_code,
              'PERSONAL_PHONE' => BonusCard::normalizePhone($this->data['phone']),
              'LOGIN' => $this->data['phone'],
              'PASSWORD' => $password,
              'CONFIRM_PASSWORD' => $password,
              'NAME' => $this->data['full_name'],
              'GROUP_ID' => array(9, 3, 4),
              'PERSONAL_GENDER' => $this->data['gender'] == 'MALE' ? 'M' : 'F'
            ];
            $user = new CUser;
            $ID = $user->Add($arrFields);
            if (!$ID) {
                $message = strpos($user->LAST_ERROR,
                  "с таким e-mail") !== false ? "Указанный email занят" : $user->LAST_ERROR;
                $this->error($message);
            }
            $data = [
              'PhoneNumber' => BonusCard::simplify_phone($phone),
              'Name' => $this->data['name'],
              'Surname' => $this->data['surname'],
              'Patronymic' => $this->data['patronymic'],
              'Email' => $this->data['email'],
              'StartBonuses' => 300,
              'Gender' => $this->data['gender'] == 'MALE' ? 'Male' : 'Female'
            ];
            $result = BonusCard::request('CreateDiscountCard', $data);
            if ($result && $result['CardGUID']) {
                $user->Update($ID, ['UF_CARD_GUID' => $result['CardGUID']]);
                if ($result['NewCard']) {
                    BonusCardTable::add([
                      'PhoneNumber' => BonusCard::simplify_phone($phone),
                      'CardGUID' => $result['CardGUID'],
                      'CardNumber' => $result['CardNumber']
                    ]);
                }
                $jwt = BonusCard::jwt(true);
                CEvent::SendImmediate("BONUS_CARD_NEW", "s2", [
                  "EMAIL" => $this->data['email'],
                  "GUID" => $result['CardGUID'],
                  'NUMBER' => $result['CardNumber'],
                  "JWT" => $jwt
                ]);
            }
            CEvent::SendImmediate("CUSTOM_CONFIRM_EMAIL", SITE_ID, [
              'USER_ID' => $ID,
              'EMAIL' => $this->data['email'],
              'CODE' => $email_code,
            ]);
            $loginResult = $USER->Login($arrFields["LOGIN"], $password, 'Y');
            $loginResult['MESSAGE'] = str_replace("<br>", "", $loginResult['MESSAGE']);
            if ($ID !== false && (!$loginResult['TYPE'] || $loginResult['TYPE'] != 'ERROR')) {
                if ($this->data['subscribe']) {
                    Subscribe::init();
                    Subscribe::subscribe(Subscribe::$idList);
                }
                Consent::addByContext(1);
                $this->success($result);
            } else {
                $this->error($result);
            }
        } else {
            $this->error(array(
              "form-errors" => array("code" => "Код недействителен")
            ));
        }
    });

    $this->action("authorize", function () use ($USER) {
        /** @var Api $this */
        if ($USER->IsAuthorized()) {
            $this->success("Authorized");
        }
        if (SITE_TEMPLATE_ID === "cameo-v3") {
            $filter = array(
              "LOGIC" => "OR",
              Array(
                "=EMAIL" => $this->data["login"],
              ),
            );
            foreach (BonusCard::get_phone_formats($this->data['login']) as $phone) {
                $filter[] = Array(
                  "PERSONAL_PHONE" => str_replace("'", '', $phone)
                );
            }
            $user = Bitrix\Main\UserTable::getList(Array(
              "select" => Array("LOGIN", "EMAIL", "PERSONAL_PHONE", "UF_FAVORITE"),
              "filter" => array(
                $filter
              )
            ))->fetch();
            if ($user) {
                $login = $user["LOGIN"];
                $result = $USER->Login($login, $this->data['password'], $this->data['remember']);
                if (!$result['TYPE'] || $result['TYPE'] != 'ERROR') {
                    $this->success(array("custom" => "param"));
                } else {
                    $this->error(array(
                      "form-errors" => array("password" => "Неверный пароль"),
                    ));
                }
            } else {
                $this->error(array(
                  "form-errors" => array("login" => "Учетная запись не найдена"),
                ));
            }

            return;
        }
        if (isset($this->data["email"])) {
            $by = "timestamp_x";
            $order = "desc";
            $rsUser = CUser::GetList(
              $by,
              $order,
              array("EMAIL" => $this->data["email"]),
              array("select" => ["LOGIN"])
            );
            if ($u = $rsUser->Fetch()) {
                $login = $u["LOGIN"];
                $result = $USER->Login($login, $this->data['password'], $this->data['remember']);
                $result['MESSAGE'] = str_replace("<br>", "", $result['MESSAGE']);
                if (!$result['TYPE'] || $result['TYPE'] != 'ERROR') {
                    $this->success();
                } else {
                    $this->error(["field" => "password", "message" => "Неверный пароль"]);
                }
            } else {
                $this->error(['field' => "email", 'message' => "Неверный адрес электронной почты"]);
            }
        } elseif (isset($this->data["phone"])) {
            $by = "timestamp_x";
            $order = "desc";
            $rsUser = CUser::GetList(
              $by,
              $order,
              array("PERSONAL_PHONE" => $this->data["phone"]),
              array("select" => ["LOGIN"])
            );
            if ($u = $rsUser->Fetch()) {
                $login = $u["LOGIN"];
                $result = $USER->Login($login, $this->data['password'], $this->data['remember']);
                $result['MESSAGE'] = str_replace("<br>", "", $result['MESSAGE']);
                if (!$result['TYPE'] || $result['TYPE'] != 'ERROR') {
                    $this->success();
                } else {
                    $this->error(["field" => "phone", "message" => "Неверный пароль"]);
                }
            } else {
                $this->error(['field' => "phone", 'message' => "Неверный номер телефона"]);
            }
        } else {
            $this->error(['field' => "login", 'message' => "Введите данные для авторизации", "data" => $this->data]);
        }
    });

    $this->action("register", function () use ($USER) {
        /** @var Api $this */
        $arrFields = [
          'EMAIL' => $this->data['email'],
          'PERSONAL_PHONE' => $this->data['phone'],
          'PASSWORD' => $this->data['password'],
          'CONFIRM_PASSWORD' => $this->data['confirmPassword'],
          'NAME' => $this->data['name'],
          'GROUP_ID' => array(9, 3, 4),
          'LAST_NAME' => $this->data['lastName'],
        ];
        if ($this->data['_name']) {
            $arrFields['NAME'] = $this->data['_name'];
            $arrFields['LAST_NAME'] = $this->data['_last_name'];
            $arrFields['SECOND_NAME'] = $this->data['_second_name'];
            $arrFields['PERSONAL_GENDER'] = $this->data['_gender'];
        }
        if (!empty($this->data['email'])) {
            $arrFields["LOGIN"] = $this->data['email'];
        } elseif (!empty($this->data['phone'])) {
            $arrFields['LOGIN'] = $this->data['phone'];
        } else {
            if (SITE_TEMPLATE_ID === "cameo-v3") {
                $this->error(array(
                  "form-errors" => array(
                    "email" => "Введите номер телефона или Email",
                    "phone" => "Введите номер телефона или Email"
                  )
                ));
            } else {
                $this->error(['field' => "email", 'message' => "Введите номер телефона или Email"]);
            }
        }
        if ($this->data['phone']) {
            $rs = CUser::GetList($by, $order, array("PERSONAL_PHONE" => $this->data['phone']), array(
              "SELECT" => array("ID"),
              "FIELDS" => array("ID"),
            ))->Fetch();
            if ($rs) {
                $this->error("Указанный номер телефона занят", 'phone');
            }
        }
        $user = new CUser;
        $ID = $user->Add($arrFields);
        if (!$ID) {
            $message = strpos($user->LAST_ERROR,
              "с таким e-mail") !== false ? "Указанный email занят" : $USER->LAST_ERROR;
            $this->error($message, "email");
        }
        $result = $USER->Login($arrFields["LOGIN"], $this->data['password'], $this->data['remember']);
        $result['MESSAGE'] = str_replace("<br>", "", $result['MESSAGE']);
        if ($ID !== false && (!$result['TYPE'] || $result['TYPE'] != 'ERROR')) {
            if ($this->data['subscribe']) {
                Subscribe::init();
                Subscribe::subscribe(Subscribe::$idList);
            }
            Consent::addByContext(1);
            $this->success($result);
        } else {
            $this->error($result);
        }
    });

    /*$this->action('forgot', function () {
      $by = "timestamp_x";
      $order = "desc";
      if (isset($this->data["email"])) {
        $rsUser = CUser::GetList(
          $by,
          $order,
          array("EMAIL" => $this->data["email"]),
          array("select" => ["LOGIN"])
        );
        if ($u = $rsUser->Fetch()) {
          $login = $u["LOGIN"];
          $result = CUser::SendPassword($login, $this->data['email']);
          $result['MESSAGE'] = str_replace("<br>", "", $result['MESSAGE']);
          if (!$result['TYPE'] || $result['TYPE'] != 'ERROR') {
            $this->success($result);
          } else {
            $this->error($result);
          }
        } else {
          $this->error(['field' => "email", 'message' => "Неверный адрес электронной почты"]);
        }
      } elseif (isset($this->data["phone"])) {
        $this->error(['field' => "phone", 'message' => "Неверный номер телефона"]);
      } else {
        $this->error(['field' => "login", 'message' => "Введите данные", "data" => $this->data]);
      }
    });*/

    $this->action("size.table", function () {
        /** @var Api $this */
        ob_start();
        SizeTable::getUserTable();
        $content = ob_get_contents();
        ob_end_clean();
        $sizes = null;
        if ($this->data['type']) {
            $sizes = SizeTable::getSizes($this->data['type']);
        }
        $this->success(['html' => $content, 'size' => $sizes]);
    });

    $this->action("update", function () use ($USER) {
        /** @var Api $this */
        if (SITE_TEMPLATE_ID === 'cameo-v3' && $this->data["password"] && $this->data["oldPassword"] && $this->data["confirmPassword"]) {
            $this->call("user.chpass", $this->data);

            return;
        }

        $currentUser = CUser::GetByID(CUser::GetID())->Fetch();
        $arUser = array();
        if ($this->data['name']) {
            $arUser["NAME"] = $this->data["name"];
        }

        if ($this->data['email']) {
            $rsUser = $USER->GetList($by, $order, ['=EMAIL' => $this->data['email']])->Fetch();
            if ($rsUser) {
                $this->error('Указанный email занят');
            }
            $email_code = randString(12);
            $user = new CUser;
            $user->Update($USER->GetId(), [
              'UF_EMAIL' => $this->data['email'],
              'UF_EMAIL_CODE' => $email_code
            ]);
            $send_result = CEvent::SendImmediate("CUSTOM_CONFIRM_EMAIL", SITE_ID, [
              'USER_ID' => CUser::GetID(),
              'EMAIL' => $this->data['email'],
              'CODE' => $email_code,
            ]);
            if ($send_result == 'F') {
                $this->error("Произошла ошибка при отправке письма. Попробуйте позже.");
            }
            $this->success(['email' => $this->data['email'], 'result' => $send_result]);
        }

        if (isset($this->data["lastName"])) {
            $arUser["LAST_NAME"] = $this->data["lastName"];
        }
        if (isset($this->data["sizeTrousers"])) {
            $arUser["UF_THROUSERS"] = $this->data["sizeTrousers"];
        }
        if (isset($this->data["sizeBathrobe"])) {
            $arUser["UF_BATHROBE"] = $this->data["sizeBathrobe"];
        }
        if (isset($this->data["sizeBlouse"])) {
            $arUser["UF_BLOUSE"] = $this->data["sizeBlouse"];
        }
        if (isset($this->data["size"])) {
            $arUser["UF_COSTUME"] = $this->data["size"];
        }
        if (isset($this->data["growth"])) {
            $arUser["UF_HEIGHT"] = $this->data["growth"];
        }
        if (isset($this->data["height"])) {
            $arUser["UF_HEIGHT"] = $this->data["height"];
        }
        if (isset($this->data["chest"])) {
            $arUser["UF_CHEST"] = $this->data["chest"];
        }
        if (isset($this->data["hip"])) {
            $arUser["UF_HIP"] = $this->data["hip"];
        }
        if (isset($this->data["waist"])) {
            $arUser["UF_WAIST"] = $this->data["waist"];
        }

        if (isset($this->data["auto_size"])) {
            $arUser["UF_AUTO_SIZE"] = $this->data["auto_size"];
        }

        if (isset($this->data["birthday"]) && !$currentUser['PERSONAL_BIRTHDAY']) {
            $arUser["PERSONAL_BIRTHDAY"] = date('d.m.Y', strtotime($this->data['birthday']));
        }
        if (isset($this->data["gender"])) {
            $arUser["PERSONAL_GENDER"] = $this->data["gender"];
        }
        if (isset($this->data["timezone"])) {
            $arUser["UF_TIMEZONE"] = $this->data["timezone"];
            global $APPLICATION;
            $APPLICATION->set_cookie("detect_timezone", 'N', -1);
        }

//    $arUser = [
//      "NAME" => $this->data["name"],
//      "LAST_NAME" => $this->data["lastName"],
//      "PERSONAL_PHONE" => $this->data["phone"],
//      "EMAIL" => $this->data["email"],
//      "UF_THROUSERS" => $this->data["sizeTrousers"],
//      "UF_BATHROBE" => $this->data["sizeBathrobe"],
//      "UF_BLOUSE" => $this->data["sizeBlouse"],
//      "UF_COSTUME" => $this->data["size"],
//      "UF_HEIGHT" => $this->data["growth"],
//      "UF_CHEST" => $this->data['chest'],
//      "UF_HIP" => $this->data['hip'],
//      "UF_WAIST" => $this->data['waist'],
//      "PERSONAL_BIRTHDATE" => $this->data["birthdate"],
//      "PERSONAL_GENDER" => $this->data["gender"],
//    ];
        if (CUser::IsAuthorized()) {
            $user = new CUser;
            $user->Update($USER->GetId(), $arUser);
        } else {
            $_SESSION['arUser'] = array_merge($_SESSION['arUser'] ?: [], $arUser);
        }
        $this->success(['arUser'=>$arUser]);
    });

    $this->action("subscribe", function () use ($USER) {
        /** @var Api $this */

        foreach ($this->data['subscription'] as $key => $value) {
            $key = str_replace("_", "", $key);
            if ($value == "Y") {
                $subscribeList[] = $key;
            } else {
                $unsubscribeList[] = $key;
            }
        }
        if (!empty($unsubscribeList)) {
            Subscribe::unsubscribe($unsubscribeList);
        }
        if (!empty($subscribeList)) {
            Subscribe::subscribe($subscribeList);
        }

        $user = new CUser;
        $data = [
          'UF_PUSH_SUBSCRIBES' => $this->data['push_subscribes'] ?: [],
          'UF_SMS_SUBSCRIBES' => $this->data['sms_subscribes'] ?: [],
          'UF_SMS_FROM' => $this->data['smsFrom'],
          'UF_PUSH_FROM' => $this->data['smsFrom'],
          'UF_SMS_TO' => $this->data['smsTo'],
          'UF_PUSH_TO' => $this->data['smsTo'],
          'UF_TIMEZONE' => $this->data['timezone']
        ];

        $res = $user->Update($USER->GetId(), $data);
        if ($res) {
            $this->success();
        } else {
            $this->error(["message" => "Не удалось сохранить изменения"]);
        }
    });

    $this->action("login", function () use ($USER) {
        /** @var Api $this */
        if ($USER->IsAuthorized()) {
            $this->success("Authorized");
        }
        $result = $USER->Login($this->data['login'], $this->data['password'], $this->data['remember']);
        $result['MESSAGE'] = str_replace("<br>", "", $result['MESSAGE']);
        if (!$result['TYPE'] || $result['TYPE'] != 'ERROR') {
            $this->success();
        } else {
            $this->error($result);
        }
    });

    $this->action('chpass', function () use ($USER) {
        /** @var Api $this */
        $this->check(array(
          "password" => array(
            "name" => "Новый пароль",
            "rules" => "minLength[6]"
          ),
          "confirmPassword" => array(
            "name" => "Подтвердите пароль",
            "rules" => "match[password]"
          )
        ));
        global $USER;
        $result = $USER->Update(CUser::GetID(), array(
          "PASSWORD" => $this->data['password']
        ));
        if (!$result) {
            $this->error($USER->LAST_ERROR);
        }
    });
    $this->action('logout', function () use ($USER) {
        /** @var Api $this */
        $USER->Logout();
    });
    /*$this->action('forgot', function () {
      $result = CUser::SendPassword($this->data['login'], $this->data['email']);
      if ($result['TYPE'] == 'ERROR') {
        $this->error("Логин или email не найдены");
      } else {
        $this->success($result);
      }
    });*/
    $this->action('restore', function () use ($USER) {
        $result = $USER->ChangePassword($this->data['login'], $this->data['checkword'], $this->data['password'],
          $this->data['password-repeat']);
        if ($result['TYPE'] == 'OK') {
            $USER->Login($this->data['login'], $this->data['password'], 'Y');
        }
        $this->success($result);
    });

    $this->action('cancel-order', function () use ($USER) {
        if (!$this->data["orderid"]) {
            $this->error("Заказа не существует");
        }
        CModule::IncludeModule("sale");
        CSaleOrder::CancelOrder($this->data["orderid"], "Y", $this->data["reason"]);
        $this->success(array("orderid" => $this->data["orderid"]));
    });

    $this->space("forgot", function () {
        /** @var Api $this */
        $this->action("v3", function () {
            $user = Bitrix\Main\UserTable::getList(Array(
              "select" => Array("LOGIN", "EMAIL", "PERSONAL_PHONE"),
              "filter" => array(
                array(
                  "LOGIC" => "OR",
                  Array(
                    "=LOGIN" => $this->data["login"],
                  ),
                  Array(
                    "=EMAIL" => $this->data["login"],
                  ),
                  Array(
                    "PERSONAL_PHONE" => $this->data["login"]
                  )
                )
              )
            ))->fetch();
            if ($user) {
                $login = $user["LOGIN"];
                if (strtolower($user["EMAIL"]) == strtolower($this->data["login"])) {
                    CUser::SendPassword(
                      $user["LOGIN"],
                      $user["EMAIL"],
                      SITE_ID
                    );
                    $this->success(array("login" => $login, "email" => $user["EMAIL"]));
                } else {
                    $this->call("user.forgot.phone", array("phone" => $this->data["login"]));
                    $this->success(array("phone" => $this->data["login"]));
                }
            } else {
                $this->error(array(
                  "form-errors" => array("login" => "Учетная запись не найдена")
                ));
            }
        });
        $this->action("email", function () {
            /** @var Api $this */
            $rsUser = CUser::GetList(
              $by,
              $order,
              array("=EMAIL" => $this->data["email"]),
              array("select" => ["LOGIN"])
            );
            if ($u = $rsUser->Fetch()) {
                $login = $u["LOGIN"];
                $result = CUser::SendPassword($login, $this->data['email']);
                $result['MESSAGE'] = str_replace("<br>", "", $result['MESSAGE']);
                if (!$result['TYPE'] || $result['TYPE'] != 'ERROR') {
                    $this->success($result);
                } else {
                    $this->error($result);
                }
            } else {
                $this->error(['field' => "email", 'message' => "Неверный адрес электронной почты"]);
            }
        });
        $this->action("phone", function () {
            /** @var Api $this */
            $this->check(array(
              "phone" => array(
                "name" => "Номер телефона",
                "rules" => "empty"
              )
            ));
            $by = "timestamp_x";
            $order = "desc";
            $rsUser = CUser::GetList(
              $by,
              $order,
              array("PERSONAL_PHONE" => $this->data["phone"]),
              array("SELECT" => ["UF_TIME"], "FIELDS" => array("ID", "UF_TIME"))
            );
            if ($u = $rsUser->Fetch()) {
                $diff = $u['UF_TIME'] + 180 - time();
                if ($u['UF_TIME'] && $diff > 0) {
                    $this->error("Повторная отправка возможна через " . pluralize($diff, "секунда"), "phone");
                }
                $code = mt_rand(1000, 9999);
                CModule::IncludeModule("rarus.sms4b");
                $SMS4B = new Csms4b();
                try {
                    send_sms_ext(str_replace(array("(", ")", "-", " "), "", $this->data['phone']), $code);
//                    $list = array(
//                      str_replace(array("(", ")", "-", " "), "", $this->data['phone']) => ($code)
//                    );
                    //$result = $SMS4B->SendSmsSaveGroup($list)[0];
                    //if ($result['Result'] != 1) {
                    //  $this->error("Произошла ошибка при отправке смс. Попробуйте позже", 'phone');
                    //}
                    global $USER;
                    $USER->Update($u['ID'], array(
                      'UF_CODE' => $code,
                      'UF_TIME' => time()
                    ));
                    $this->success(array(
                      "phone" => $this->data["phone"],
                        //"result" => $result
                    ));
                } catch (ArgumentException $e) {
                    $this->error("Произошла ошибка: " . $e->getMessage());
                } catch (ObjectException $e) {
                    $this->error("Произошла ошибка: " . $e->getMessage());
                } catch (Sms4bException $e) {
                    $this->error("Произошла ошибка: " . $e->getMessage());
                }
            } else {
                $this->error(['field' => "phone", 'message' => "Неверный номер телефона"]);
            }
        });
    });

    $this->space("restore", function () {
        /** @var Api $this */
        $this->action("phone", function () {
            /** @var Api $this */
            $this->check(array(
              "phone" => array(
                "name" => "Номер телефона",
                "rules" => "empty"
              ),
              "code" => array(
                "name" => "Код подтверждения",
                "rules" => "length[5]"
              ),
              "password" => array(
                "name" => "Пароль",
                "rules" => "minLength[6]"
              ),
              "confirmPassword" => array(
                "name" => "Подтверждение пароля",
                "rules" => "match[password]"
              )
            ));
            $by = "timestamp_x";
            $order = "desc";
            $rsUser = CUser::GetList(
              $by,
              $order,
              array("PERSONAL_PHONE" => $this->data["phone"]),
              array("SELECT" => ["UF_CODE"], "FIELDS" => array("ID", "UF_CODE"))
            );
            if ($u = $rsUser->Fetch()) {
                if ($this->data['code'] != $u['UF_CODE']) {
                    $this->error("Неверный код подтверждения", "code");
                }
                global $USER;
                $result = $USER->Update($u['ID'], array(
                  "PASSWORD" => $this->data['password'],
                  'UF_CODE' => false,
                  "UF_TIME" => false
                ));
                if ($result == false) {
                    $this->error($USER->LAST_ERROR, "phone");
                }
            } else {
                $this->error(['field' => "phone", 'message' => "Неверный номер телефона"]);
            }
        });
    });

    global $rootApi;
    $rootApi = $this;

    $this->action('addaddress', function () use ($USER) {
        $address = new userAddressesData();
        $city = "";

        if (is_int($this->data['city'])) {
            Loader::includeModule("sale");
            $city = CSaleLocation::GetList(array(),
              array("ID" => $this->data['city'], "LID" => LANGUAGE_ID))->fetch()["CITY_NAME"];
        } else {
            $city = $this->data["city"];
        }

        $arFields = [
          "user" => CUSER::GetID(),
          "name" => $this->data["name"],
          "country" => $this->data["country"],
          "city" => $city,
          "city_id" => $this->data["city_id"] ?: (is_int($this->data["city"]) ? $this->data["city"] : null),
          "street" => $this->data["street"],
          "index" => $this->data["index"],
          "flat" => $this->data["flat"],
          "house" => $this->data["house"],
          "courpse" => $this->data["corpse"],
          "default" => $this->data["default"] === "Y"
        ];

        if (!$arFields['city_id']) {
            Loader::includeModule("sale");
            $arFields['city_id'] = CSaleLocation::GetList(array(),
              array("CITY_NAME" => $city, "LID" => LANGUAGE_ID))->fetch()['CITY_ID'];
        }

        $res = $address->add($arFields);
        $arFields["id"] = $res->getId();
        $arFields["id"] ? $this->success(array("item" => $arFields)) : $this->error();
    });
    $this->action('changeaddress', function () use ($USER) {
        $address = new userAddressesData();
        $city = "";

        if (is_int($this->data['city'])) {
            Loader::includeModule("sale");
            $city = CSaleLocation::GetList(array(),
              array("ID" => $this->data['city'], "LID" => LANGUAGE_ID))->fetch()["CITY_NAME"];
        } else {
            $city = $this->data["city"];
        }
        $arFields = [
          "user" => CUSER::GetID(),
          "name" => $this->data["name"],
          "country" => $this->data["country"],
          "city" => $city,
          "city_id" => $this->data["city_id"] ?: (is_int($this->data["city"]) ? $this->data["city"] : null),
          "street" => $this->data["street"],
          "index" => $this->data["index"],
          "flat" => $this->data["flat"],
          "house" => $this->data["house"],
          "corpse" => $this->data["corpse"],
          "default" => $this->data["default"] === "Y"
        ];

        if (!$arFields['city_id']) {
            Loader::includeModule("sale");
            $arFields['city_id'] = CSaleLocation::GetList(array(),
              array("CITY_NAME" => $city, "LID" => LANGUAGE_ID))->fetch()['CITY_ID'];
        }

        $res = $address->update($this->data["id"], $arFields);
        $this->success($res);
    });
    $this->action('changedefaultaddress', function () use ($USER) {
        $address = new userAddressesData();
        $arFields = [
          "default" => true
        ];
        $res = $address->update($this->data["id"], $arFields);
        $this->success($res);
    });
    $this->action('removeaddress', function () use ($USER) {
        $this->check(array(
          "id" => array(
            "rules" => array(
              "empty" => "Не указан id дреса"
            )
          )
        ));
        $address = new userAddressesData();
        $res = $address->delete($this->data["id"]);
        $this->success($res);
    });

    $this->action('addorgaddress', function () use ($USER) {
        $address = new orgAddressesData();
        $arFields = array(
          "user" => CUSER::GetID(),
          "name" => $this->data["name"],
          "tin" => $this->data["tin"],
          "checkpoint" => $this->data["checkpoint"],
          "msrn" => $this->data["msrn"],
          "person" => $this->data["person"],
          "phone" => $this->data["phone"],
          "email" => $this->data["email"],
          "address" => $this->data["address"],
          "actual_address" => $this->data["actual_address"],
          "default" => $this->data["default"],
          "bik" => $this->data['bik'],
          'bank_name' => $this->data['bank_name'],
          'rs' => $this->data['rs'],
          'ks' => $this->data['ks']
        );

        $res = $address->add($arFields);
        $arFields["id"] = $res->getId();
        $arFields["id"] ? $this->success(array("item" => $arFields)) : $this->error();
    });
    $this->action('changeorgaddress', function () use ($USER) {
        $address = new orgAddressesData();
        $arFields = array(
          "user" => CUSER::GetID(),
          "name" => $this->data["name"],
          "tin" => $this->data["tin"],
          "checkpoint" => $this->data["checkpoint"],
          "msrn" => $this->data["msrn"],
          "person" => $this->data["person"],
          "phone" => $this->data["phone"],
          "email" => $this->data["email"],
          "address" => $this->data["address"],
          "actual_address" => $this->data["actual_address"],
          "default" => $this->data["default"],
          "bik" => $this->data['bik'],
          'bank_name' => $this->data['bank_name'],
          'rs' => $this->data['rs'],
          'ks' => $this->data['ks'],
        );

        $address->update($this->data["id"], $arFields);
        $arFields['id'] = $this->data['id'];
        $this->success(['item' => $arFields]);
    });
    $this->action('changedefaultorgaddress', function () use ($USER) {
        $address = new orgAddressesData();
        $arFields = [
          "default" => true
        ];
        $address->update($this->data["id"], $arFields);
    });
    $this->action('removeorgaddress', function () use ($USER) {
        $address = new orgAddressesData();
        $address->delete($this->data["id"]);
    });
});
