<?
/**
 * @var Api $api
 **/

use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

$api->space('favorite', function () {
    global $USER, $request;
    $list = array();
    if (!$this->isCall()) {
        $list = \Product\Favorite::getList();
    }
    $this->action("get", function () use ($list) {
        /** @var Api $this */
        $this->success(array(
            "list" => $list,
            "html" => (!$this->data['version'] || $this->data['version'] == 3) ? \Product\Favorite::view($list) : null
        ));
    });
    $this->action("add", function () use ($USER, $list) {
        /** @var Api $this */
        $id = $this->data['id'];
        if (!in_array($id, $list)) {
            $list[] = $id;
        }
        $this->call("favorite.save", array(
            "list" => array_values($list)
        ));
        $this->success(array(
            "list" => array_values($list),
            "html" => \Product\Favorite::view($list)
        ));
    });
    $this->action("remove", function () use ($USER, $list) {
        /** @var Api $this */
        $id = $this->data['id'];
        $list = array_filter($list, function ($value) use ($id) {
            return $value != $id;
        });
        $this->call("favorite.save", array(
            "list" => array_values($list),
        ));
        $this->success(array(
            "list" => array_values($list),
            "html" => \Product\Favorite::view($list)
        ));
    });

    $this->action("clear", function () use ($list) {
        /** @var Api $this */
        $this->call("favorite.save", array(
            "list" => []
        ));
    });

    $this->action("save", function () {
        /** @var Api $this */
        $this->check(array("list" => "empty"));
        $list = array_unique($this->data['list']);
        global $USER;
        if ($USER->IsAuthorized()) {
            $USER->Update(CUser::GetID(), array(
                'UF_FAVORITE' => $list
            ));
        } else {
            $cookie = new Cookie("favorite", serialize($list));
            Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
        }
    });
});
