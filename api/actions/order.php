<?
/**
 * @var Api $api
 **/

use Bitrix\Sale\Order;

$api->space('order', function () {
    $this->action("payment", function () {
        /** @var Api $this */
        $this->check([
          'order_id' => 'empty',
          'sum' => 'empty',
          'payed' => 'empty',
          'number' => 'empty',
          'date' => 'empty'
        ]);

        return;
        CModule::IncludeModule('sale');
        $order = Order::load($this->data['order_id']);
        $payment = BonusCard::getAnotherPayment($order->getPaymentCollection());
        $payment->setField('SUM', $this->data['sum']);
        $payment->setPaid($this->data['payed'] ? 'Y' : 'N');
        $payment->setField('PAY_VOUCHER_NUM', $this->data['number']);
        $payment->setField('PAY_VOUCHER_DATE', $this->data['date']);
        $result = $order->save();
        if (!$result->isSuccess()) {
            $this->error(implode(", ", $result->getErrorMessages()));
        }
    });
    $this->action("return", function () {
        /** @var Api $this */
        require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/ProductReturn.php';
        $return = new ProductReturn($this->data);
        $link = $return->make();
        ob_start();
        B24Sync::sendOrderMessage($this->data['order_id'], 'Оформлена заявка на возврат: https://www.kamey.ru' . $link);
        ob_end_clean();
        global $USER;
        CEvent::SendImmediate("ORDER_RETURN", SITE_ID, [
          'EMAIL' => $USER->GetEmail(),
          'ORDER_ID' => $this->data['order_id'],
          'ORDER_ACCOUNT_NUMBER' => $return->order->getField('ACCOUNT_NUMBER')
        ], 'Y', '', [
          $_SERVER['DOCUMENT_ROOT'] . $link
        ]);

        $this->success([
          'link' => $link
            //'result'=>$return->saveResult
        ]);
    });
    $this->action("test", function(){
        /** @var Api $this */
        $result = B24Sync::component()->Send("crm.lead.add",[
          'fields'=>['TITLE'=>"TESt"]
        ]);
        $this->success(['result'=>$result]);
    });
    $this->action("protect", function () {
        $data = $this->data;
        if (!$data['phone']) {
            $this->error('Укажите номер телефона', 'phone');
        }
        if (!$data['email']) {
            $this->error('Укажите Электронную почту', 'email');
        }
        $fields = [
          "TITLE" => "Оптовый заказ изолирующих комплектов",
          "NAME" => $data['fio'],
//            "STATUS_ID"=> "NEW",
//            "OPENED"=> "Y",
          "ASSIGNED_BY_ID" => 103,
          "PHONE" => [["VALUE" => $data['phone'], "VALUE_TYPE" => "HOME"]],
          "EMAIL" => [["VALUE" => $data['email'], "VALUE_TYPE" => "HOME"]],
          "UF_CRM_1575617476921" => 49
        ];
        $result = B24Sync::component()->Send("crm.lead.add",[
          'fields'=>$fields
        ]);
        $leadId = $result['result'];
        if ($leadId && $this->data['trace']) {
            B24Sync::component()->Send('crm.tracking.trace.add', [
              'ENTITIES' => [
                [
                  'TYPE' => 'LEAD',//COMPANY, CONTACT, DEAL, LEAD, QUOTE
                  'ID' => $leadId
                ]
              ],
              'TRACE' => $this->data['trace']
            ]);
        }
        mail('isolated_kits@kamey.ru', 'Оптовый заказ изолирующих комплектов',
          'Номер телефона: ' . $data['phone'] . "\n" . 'Email: ' . $data['email'], "From: store@kamey.ru");

        $this->success('Ваша заявка успешно отправлена');
    });
});
