<?php
require "Validator.php";
/**
 * MIT License
 *
 * Copyright (c) 2017 safindr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/** Api Class
 * <code>
 * Тестовый путь запроса к Api
 * http://site.ru/api/foo.bar.baz/546/custom:value/
 *
 * При данном пути сформируется следующий массив данных:
 * $this->data = array(
 *   "action" => "foo.bar.baz",
 *   "id" => 546,
 *   "custom" => "value"
 * );
 * </code>
 * ```
 * $api->space("foo", function(){
 *   $api->space("bar", function(){
 *     $api->get("baz", function(){
 *       Это действие сработает только при запросе с методом GET.
 *     });
 *     $api->put("baz", function(){
 *       Это действие сработает только при запросе с методом PUT.
 *     });
 *     $api->action("baz", function(){
 *       Это действие сработает, когда не были задействованы действия
 *       для остальных методов. Порядок имеет значение. Метод $api->action
 *       является действием по умолчанию и может отрабатывать когда 7
 *       не указаны привязки к методам
 *     });
 *     Действия, указанные ниже выполнены не будут,
 *     т.к. выполнится действие $api->action("baz");
 *     $api->post("baz", function(){
 *       Это действие сработает только при переходе методом POST.
 *     });
 *     $api->delete("baz", function(){
 *       Это действие сработает только при переходе методом DELETE.
 *     });
 *   Для пространства действий ($this->space()) возможно указать
 *   проверку на принадлежность к массиву групп пользователей,
 *   если пользователь не сосотоит ни в одной группе, то выполнение
 *   данного пространства прекращается.
 *   }, array(1, 2, 5));
 * Так же возможно указать свою проверку на доступ к пространству
 * путем указания callback функции
 * }, function(){
 *   if (1 == 2) {
 *     return false;
 *   }
 *   return true;
 * });
 * ```
 * ```
 * Так же доступно следующее использование:
 * $this->action("foo/{bar}/{baz}/");
 * При запросе по ссылке http://site.ru/api/foo/546/value/
 * Сформируется следующий массив:
 * $this->data = array(
 *   "action" => "foo",
 *   "bar" => 546,
 *   "baz" => "value"
 * );
 * ```
 */
class Api
{
  /**
   * Constant for message "Access denied"
   */
  const ACCESS_DENIED = 403;
  /**
   * Constant for message "Action not found"
   */
  const NOT_FOUND = 404;
  /**
   * Constant for message "Action not valid"
   */
  const NOT_VALID = 405;
  /**
   * Constant for validate failure message
   */
  const MISSING = 411;
  /**
   * Constant for success status
   */
  const SUCCESS = 200;
  /**
   * Constant for error status
   */
  const ERROR = 400;
  /** @var array $data $_GET or $_POST data */
  public $data;
  /** @var array $files $_FILES data */
  public $files;
  /** @var array $options */
  public $options;
  /** @var string $action Current action */
  public $action;
  /**
   * @var array $validate Validator data contains here
   */
  public $validate = array(
    "errors" => array(
      "single" => array(),
      "multiple" => array(),
      "all" => array(),
      "important" => array(),
      "result" => array(
        "all" => "",
        "important" => ""
      )
    ),
    "list" => array()
  );
  /**
   * @var array List of variables that can be set via options
   */
  protected $validOptionsList = array(
    "actionsPath",
    "paramsDelimiter",
    "spaceDelimiter",
    "messageAccess",
    "messageNotFound",
    "messageNotValid",
    "messageMissing",
    "die",
    "skipDispatch",
    "importantMethod",
    "importantAction",
    "importantData",
    "detectSiteDir",
    "validateGlue",
    "validateResultType",
    "validateFieldPrepend"
  );
  /** @var string $actionsPath Folder to find api actions */
  private $actionsPath = 'actions';
  /**
   * @var string Message for success status
   */
  private $successStatus = 'ok';
  /**
   * @var string Message for error status
   */
  private $errorStatus = 'error';
  /**
   * @var array List of parse fields from url
   */
  private $dispatchList = array(
    'action'
  );
  /**
   * @var string $validateGlue Glue for string of errors
   */
  private $validateGlue = ", ";
  /**
   * @var string $validateResultType Validate result type for message
   */
  private $validateResultType = "important";
  /**
   * @var bool $validateFieldPrepend Prepend "Field" to prompt message in list
   */
  private $validateFieldPrepend = true;
  /**
   * @var int Number of skipping field in url
   */
  private $skipDispatch = 0;
  /**
   * @var array Permissions of current action space
   */
  private $permission = array();
  /**
   * @var string Access denied message
   */
  private $messageAccess = 'Access denied';
  /**
   * @var string Action not found message
   */
  private $messageNotFound = 'Action not found';
  /**
   * @var string Action not valid message
   */
  private $messageNotValid = 'Action is not valid';
  /**
   * @var string Missing fields message
   */
  private $messageMissing = 'Missing fields: {fields}';
  /**
   * @var string Delimiter to parse url params
   */
  private $paramDelimiter = "/";
  /**
   * @var string Delimiter to parse space of action
   */
  private $spaceDelimiter = ".";
  /**
   * @var array Current spaces
   */
  private $space = array();
  /** @var string Current request method */
  private $method;
  private $url;
  private $splitData;
  private $dispatchKeys;
  private $die = true;
  private $importantAction = null;
  private $importantMethod = null;
  private $importantData = null;
  private $emitData = null;
  private $detectSiteDir = true;

  /**
   * Api constructor.
   *
   * @param array $options {
   *
   * @var string $recaptcha_secret
   * @var string $actionsPath
   * @var string $paramsDelimiter
   * @var string $spaceDelimiter
   * @var string $messageAccess
   * @var string $messageNotFound
   * @var string $messageNotValid
   * @var string $messageMissing
   * @var string $die
   * @var int $skipDispatch
   * @var string $importantAction
   * @var string $importantMethod
   * @var string $importantData
   * @var bool $detectSiteDir
   * @var string $validateGlue
   * @var string $validateResultType
   * @var bool $validateFieldPrepend
   * }
   */
  function __construct($options = array())
  {
    $this->getData();
    $this->files = $_FILES;
    $this->options = $options;
    $this->detectOptionVariables();
    $this->prepareDispatchList();
    $this->detectUrl();
    $this->dispatch();
    $this->action = $this->importantAction ?: $this->data['action'];
    $this->method = $this->importantMethod ? strtolower($this->importantMethod) : strtolower($_SERVER['REQUEST_METHOD']);
    if (is_array($this->importantData)) {
      $this->data = array_merge($this->data, $this->importantData);
    }
    $this->detectActions();
  }

  private function getData()
  {
    $request = file_get_contents('php://input');
    if ($_SERVER['CONTENT_TYPE'] == 'application/json') {
      $this->data = json_decode($request, true);

      return true;
    }
    switch ($_SERVER['REQUEST_METHOD']) {
      case 'PUT':
      case 'DELETE':
      case 'PATCH':
        parse_str($request, $this->data);
        break;
      case 'GET':
        $this->data = $_GET;
        break;
      case 'POST':
        $this->data = $_POST;
        break;
    }

    return true;
  }

  /**
   * Detect all variables that are applied via options
   */
  private function detectOptionVariables()
  {
    foreach ($this->options as $name => $value) {
      $this->setOption($name);
    }
  }

  /**
   * Set variable via option
   *
   * @param $name
   */
  private function setOption($name)
  {
    if (in_array($name, $this->validOptionsList) && isset($this->options[$name])) {
      $this->$name = $this->options[$name];
    }
  }

  /**
   * Prepare dispatch list by $this->skipDispatch value
   */
  private function prepareDispatchList()
  {
    $list = $this->dispatchList;
    $this->dispatchList = array();
    $i = $this->skipDispatch;
    foreach ($list as $field) {
      $this->dispatchList[++$i] = $field;
    }
  }

  private function detectUrl()
  {
    $url = $_SERVER['REQUEST_URI'];
    if (defined("BX_ROOT") && $this->detectSiteDir) {
      $dir = SITE_DIR;
      if (strpos($dir, "/") === 0) {
        $dir = substr($dir, 1);
      }
      if ($length = strlen($dir)) {
        $url = substr($url, $length);
      }
    }
    $this->url = $url;
  }

  /**
   * Parse url params into $this->data
   *
   * @param null $list
   */
  private function dispatch($list = null)
  {
    if ($list == null) {
      $list = $this->dispatchList;
    }
    // Split the URL into its constituent parts.
    $parse = parse_url($this->url);

    // Remove the leading forward slash, if there is one.
    $path = ltrim($parse['path'], $this->paramDelimiter);

    // Put each element into an array.
    $elements = explode($this->paramDelimiter, $path);

    // Create a new empty array.
    $args = array();

    foreach ($elements as $k => $value) {
      if ($this->skipDispatch == $k) {
        continue;
      }
      $field = $list[$k];
      $pair = explode(":", $value);
      if ($pair[1]) {
        $field = $pair[0];
        $value = $pair[1];
      } elseif (!$field) {
        $field = 'param' . $k;
      }
      $args[$field] = $value;
    }
    $this->dispatchKeys = array_keys($args);
    $this->data = array_merge($this->data, $args);
  }

  /**
   * Find and execute valid action
   */
  private function detectActions()
  {
    global $api;
    $api = $this;
    foreach (glob($this->actionsPath . "/*.php") as $filename) {
      include $filename;
    }
    if ($this->action) {
      $this->error(self::NOT_VALID);
    } else {
      $this->error(self::NOT_FOUND);
    }
  }

  /**
   * Execute with error status
   *
   * @param string|array $message
   * @param array $assign
   */
  public function error($message, $assign = array())
  {
    $this->execute(false, $message, $assign);
  }

  /**
   * Execute result of action
   *
   * @param bool $type
   * @param string $message
   * @param array $assign
   *
   * @return bool
   */
  public function execute($type = true, $message = '', $assign = array())
  {
    if ($this->emitData != null) {
      return false;
    }
    if (is_string($assign)) {
      $assign = array(
        'field' => $assign
      );
    }
    if (is_array($type)) {
      $assign = $type;
      $type = true;
    }
    if (is_array($message)) {
      $assign = $message;
      $message = null;
    }
    $status = $type === true ? $this->successStatus : ($type === false ? $this->errorStatus : $type);
    $result = array(
      "type" => $status,
      "message" => $message
    );
    $this->fieldToLower($assign, 'TYPE');
    $this->fieldToLower($assign, 'MESSAGE');
    $this->fieldToLower($assign, 'FIELD');
    if (is_int($result['message'])) {
      $result['status'] = $result['message'];
    }
    $result = array_merge($result, $assign);
    $result['message'] = $this->getMessageByConst($result['message']);
    $result['type'] = strtolower($result['type']);
    $result['message'] = str_replace("<br>", "\r\n", $result['message']);
    $this->replaceMessage($result['message']);
    if (!isset($result['status'])) {
      if ($result['type'] == $this->successStatus) {
        $result['status'] = self::SUCCESS;
      } elseif ($result['type'] == $this->errorStatus) {
        $result['status'] = self::ERROR;
      }
    }
    if ($this->die) {
      echo json_encode($result);
      require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
      die();
    } else {
      $this->emitData = $result;
    }

    return true;
  }

  /**
   * Convert array field to lower case
   *
   * @param $object
   * @param $field
   */
  private function fieldToLower(&$object, $field)
  {
    if (isset($object[$field])) {
      $object[strtolower($field)] = $object[$field];
      unset($object[$field]);
    }
  }

  /**
   * Return message by constant
   *
   * @param $const
   *
   * @return string
   */
  private function getMessageByConst($const)
  {
    switch ($const) {
      case self::ACCESS_DENIED:
        return $this->messageAccess;
        break;
      case self::NOT_FOUND:
        return $this->messageNotFound;
        break;
      case self::NOT_VALID:
        return $this->messageNotValid;
        break;
      case self::MISSING:
        return $this->validate['errors']['result'][$this->validateResultType];
        break;
    }

    return $const;
  }

  /**
   * Replace in message {field} to $this->data[field]
   *
   * @param $message
   */
  public function replaceMessage(&$message)
  {
    foreach ($this->data as $key => $value) {
      $message = str_replace('{' . $key . '}', $value, $message);
    }
  }

  public static function errorHandler()
  {
    $last_error = error_get_last();
    if ($last_error['type'] === E_ERROR) {
      ob_end_clean();
      global $api;
      $api->error("$last_error[message] in $last_error[file] on line $last_error[line]", array(
        'file' => $last_error['file'],
        'line' => $last_error['line']
      ));
    }
  }

  public function getValidateFieldPrepend()
  {
    return $this->validateFieldPrepend;
  }

  /**
   * Execute with success status
   *
   * @param string|array $message
   * @param array $assign
   */
  public function success($message = '', $assign = array())
  {
    $this->execute(true, $message, $assign);
  }

  /**
   * Validate Google recaptcha
   *
   * @param null $secret
   */
  public function grecaptcha($secret = null)
  {
    if ($this->data['g-recaptcha-response']) {
      if ($secret == null) {
        if (function_exists('o') && o('recaptcha_secret')) {
          $secret = o('recaptcha_secret');
        } elseif ($this->options['recaptcha_secret']) {
          $secret = $this->options['recaptcha_secret'];
        }
      }
      $response = $this->fetch("https://www.google.com/recaptcha/api/siteverify", array("post" => array(
        'secret' => $secret,
        'response' => $_POST['g-recaptcha-response'],
        'remoteip' => $_SERVER['REMOTE_ADDR']
      )));
      $response = json_decode($response, true);
      if (!$response['success']) {
        $this->error('Captcha invalid', $response);
      }
    } else {
      $this->error('Where captcha?');
    }
  }

  /**
   * Fetch url
   *
   * @param string $url
   * @param array $options {
   *
   * @var string $useragent
   * @var array $post
   * @var string $refer
   * @var int $timeout
   * @var string $cookie
   * @var array $headers
   * @var bool $verbose
   * @var array $get
   * }
   * @return mixed
   */
  public function fetch($url, $options = array())
  {
    $ch = curl_init();

    $useragent = isset($options['useragent']) ? $options['useragent'] : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2';

    if (isset($options['get'])) {
      if (strpos($url, "?") !== false) {
        $url = substr($url, 0, strpos($url, "?") - 1);
      }
      $url .= "?" . http_build_query($options['get']);
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POST, isset($options['post']));

    if (isset($options['post'])) curl_setopt($ch, CURLOPT_POSTFIELDS, $options['post']);
    if (isset($options['refer'])) curl_setopt($ch, CURLOPT_REFERER, $options['refer']);

    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, (isset($options['timeout']) ? $options['timeout'] : 5));
    if (isset($options['cookie'])) {
      curl_setopt($ch, CURLOPT_COOKIEJAR, $options['cookie']);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $options['cookie']);
    }

    if (isset($options['headers'])) {
      curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);
    }
    if (isset($options['verbose'])) {
      curl_setopt($ch, CURLOPT_VERBOSE, $options['verbose']);
    }
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
  }

  /**
   * The action is triggered only by the method get
   * @uses Api::action()
   *
   * @param $action
   * @param $callback
   */
  public function get($action, $callback)
  {
    $this->action($action, $callback, 'get');
  }

  /**
   * Check action to valid and execute it
   * <code>
   * $api->action("foo", function(){
   *   //do stuff
   * });
   * </code>
   *
   * @param string $action
   * @param callable|Closure $callback
   * @param null $method
   */
  public function action($action, $callback, $method = null)
  {
    if (!empty($this->space)) {
      $action = implode($this->spaceDelimiter, $this->space) . $this->spaceDelimiter . $action;
    }
    $action = $this->splitAction($action);
    if ($this->isValid($action, $method)) {
      $this->executeSplit();
      $callback->bindTo($this, $this);
      $callback();
      $this->execute();
    }
  }

  private function splitAction($action)
  {
    $explode = explode($this->paramDelimiter, $action);
    $action = $explode[0];
    unset($explode[0]);
    $this->splitData = implode($this->paramDelimiter, $explode);

    return $action;
  }

  /**
   * Check action to valid
   *
   * @param $action
   * @param null $method
   *
   * @return bool
   */
  private function isValid($action, $method = null)
  {
    if (($method != null ? $this->method == $method : true) && $action == $this->action && $this->checkPermission()) {
      return true;
    }

    return false;
  }

  /**
   * Check permissions to current space
   * @return bool
   */
  private function checkPermission()
  {
    if (empty($this->permission)) {
      return true;
    } else {
      foreach ($this->permission as $item) {
        if (is_array($item) && class_exists('CSite') && !CSite::InGroup($item)) {
          $this->error(self::ACCESS_DENIED);
        } elseif (is_callable($item)) {
          /** @var callable|Closure $item */
          $item->bindTo($this, $this);
          $result = $item();
          if ($result === false) {
            $this->error(self::ACCESS_DENIED);
          }
        }
      }
    }

    return true;
  }

  private function executeSplit()
  {
    if (strlen($this->splitData)) {
      $list = array(
        $this->skipDispatch + 1 => array_values($this->dispatchList)[0]
      );
      preg_match_all("/{(.*?)}/", $this->splitData, $match);
      foreach ($match[1] as $key) {
        $list[] = $key;
      }
      if (count($list) == 1) {
        $this->splitData = ltrim($this->splitData, $this->paramDelimiter);
        $explode = explode($this->paramDelimiter, $this->splitData);
        foreach ($explode as $field) {
          $list[] = $field;
        }
      }
      $this->clearDispatch();
      $this->dispatch($list);
      if (is_array($this->importantData)) {
        $this->data = array_merge($this->data, $this->importantData);
      }
    }
  }

  private function clearDispatch()
  {
    foreach ($this->dispatchKeys as $key) {
      unset($this->data[$key]);
    }
  }

  /**
   * The action is triggered only by the method post
   * @uses Api::action()
   *
   * @param $action
   * @param $callback
   */
  public function post($action, $callback)
  {
    $this->action($action, $callback, 'post');
  }

  /**
   * The action is triggered only by the method put
   * @uses Api::action()
   *
   * @param $action
   * @param $callback
   */
  public function put($action, $callback)
  {
    $this->action($action, $callback, 'put');
  }

  /**
   * The action is triggered only by the method delete
   * @uses Api::action()
   *
   * @param $action
   * @param $callback
   */
  public function delete($action, $callback)
  {
    $this->action($action, $callback, 'delete');
  }

  /**
   * Space of actions
   * <code>
   * $api->space("foo", function(){
   *   // Action is "foo.bar"
   *   $api->action("bar");
   *
   *   $api->space("baz", function(){
   *     // Action is "foo.baz.bax"
   *     $api->action("bax");
   *   }, array(11, 2, 3));
   * }, function(){
   *   if ($this->data['bar'] == 2) {
   *     return true;
   *   }
   *   return false;
   * });
   * </code>
   *
   * @param $name
   * @param callable|Closure $actions
   * @param null|array|callable|Closure $permission Array of user groups or function
   */
  public function space($name, $actions, $permission = null)
  {
    $this->spaceOpen($name, $permission);
    if (strpos($this->action, $name) !== false) {
      $actions->bindTo($this, $this);
      $actions();
    }
    $this->spaceClose();
  }

  /**
   * Open space
   *
   * @param $name
   * @param $permission
   */
  public function spaceOpen($name, $permission)
  {
    $this->space[] = $name;
    $this->permission[] = $permission;
  }

  /**
   * End space
   */
  public function spaceClose()
  {
    array_pop($this->space);
    array_pop($this->permission);
  }

  /**
   * Close all spaces
   */
  public function spaceCloseAllSpaces()
  {
    $this->space = array();
    $this->permission = array();
  }

  /**
   * Check fields in $this->data
   *
   * @param $list
   *
   * @return bool
   */
  public function check($list)
  {
    Validator::init($this);
    $this->validate['list'] = $list;
    $isValid = true;
    foreach ($list as $field => $params) {
      if (!Validator::validate($field, $params)) {
        $isValid = false;
      }
    }
    if (!$isValid) {
      $this->validate['errors']['result']['all'] = implode($this->validateGlue, $this->validate['errors']['all']);
      $this->validate['errors']['result']['important'] = implode($this->validateGlue, $this->validate['errors']['important']);
      $this->error(self::MISSING, array(
        "errors" => $this->validate['errors']
      ));
    }

    return true;
  }

  /**
   * Return edit link of element
   *
   * @param $id
   *
   * @return string
   */
  public function getElementAdminLink($id)
  {
    CModule::IncludeModule('iblock');
    $iblockID = CIBlockElement::GetIBlockByID($id);
    $iblockType = CIBlock::GetByID($iblockID)->Fetch();
    $iblockType = $iblockType['IBLOCK_TYPE_ID'];
    $url = "https://" . $_SERVER['SERVER_NAME'] . "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=$iblockID&type=$iblockType&ID=$id&lang=ru&find_section_section=-1&WF=Y";

    return $url;
  }

  /**
   * Call another action in action without print data and die, return result of action
   * <code>
   * $api->action("add", function(){
   *   //do stuff
   *   $data = $api->call('get');
   *   $api->success($data);
   * });
   * $api->action("get", function(){
   *   $api->success(array("some data"));
   * });
   * <code>
   *
   * @param string $action
   * @param null|array|string $data When specifying a string, it can be used as a method, if it is not specified
   * @param null|string $method
   *
   * @return array|null
   */
  public function call($action, $data = null, $method = null)
  {
    if (is_null($method) && is_string($data)) {
      $method = $data;
    }
    $options = $this->options;
    $options['importantAction'] = $action;
    $options['importantMethod'] = $method;
    $options['importantData'] = $data;
    $options['die'] = false;
    $api = new Api($options);

    return $api->emitData;
  }

  /**
   * @return bool
   */
  public function isCall()
  {
    return !$this->die;
  }
}

