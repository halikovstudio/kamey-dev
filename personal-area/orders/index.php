<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/personal-area.js');
$APPLICATION->SetTitle("Заказы"); ?>
<h1 class="ui header center aligned uppercase personal__h1">Мой кабинет</h1>
<div class="ui container">
    <div class="personal">
        <div class="form-container user-type text--center">
            <?$APPLICATION->IncludeComponent(
              "bitrix:menu",
              "personal",
              Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(""),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "personal",
                "USE_EXT" => "N"
              )
            );?>
        </div>
        <div class="text--center personal__header">Заказы</div>
        <? $APPLICATION->IncludeComponent(
          "safin:sale.personal.order.list",
          "v4",
          array(
            "COMPONENT_TEMPLATE" => "my-orders",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_GROUPS" => "Y",
            "PATH_TO_DETAIL" => "",
            "PATH_TO_COPY" => "",
            "PATH_TO_CANCEL" => "",
            "PATH_TO_PAYMENT" => "/personal-area/order/payment.php",
            "PATH_TO_BASKET" => "/personal-area/basket/",
            "PATH_TO_CATALOG" => "/catalog/",
            "ORDERS_PER_PAGE" => "100",
            "ID" => ($_GET['COPY_ORDER'] == 'Y' ? $_GET['ID'] : null),
            "SET_TITLE" => "N",
            "SAVE_IN_SESSION" => "N",
            "NAV_TEMPLATE" => "",
            "HISTORIC_STATUSES" => array("A"),
            "RESTRICT_CHANGE_PAYSYSTEM" => array(
              0 => "0",
            ),
            "DEFAULT_SORT" => "DATE_INSERT",
            "ALLOW_INNER" => "N",
            "ONLY_INNER_FULL" => "N",
            "STATUS_COLOR_C" => "gray",
            "STATUS_COLOR_F" => "gray",
            "STATUS_COLOR_N" => "green",
            "STATUS_COLOR_P" => "yellow",
            "STATUS_COLOR_S" => "gray",
            "STATUS_COLOR_SA" => "gray",
            "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO"
          ),
          false
        ); ?>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
