<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/personal-area.js');
$APPLICATION->SetTitle("Подписки"); ?>
<h1 class="ui header center aligned uppercase personal__h1">Мой кабинет</h1>
<div class="ui container">
    <div class="personal">
        <div class="form-container user-type text--center">
            <? $APPLICATION->IncludeComponent(
              "bitrix:menu",
              "personal",
              Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(""),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "personal",
                "USE_EXT" => "N"
              )
            ); ?>
        </div>
        <?
        require_once '../parts/subscriptions.php'
        ?>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
