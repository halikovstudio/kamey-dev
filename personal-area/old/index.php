<?

use View\Subscribe;

define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Личная информация");
CModule::IncludeModule('subscribe');

$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/suggestions.min.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/suggestions.min.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/lib/calendar.min.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/lib/calendar.min.js');
$arUser = CUser::GetByID(CUser::GetID())->fetch();
$organizations = (new orgAddressesData())->get();
$userAddresses = (new userAddressesData())->get();
$APPLICATION->SetTitle("Личный кабинет");
global  $version;
?>
<div class="ui grid very relaxed block-container personal-area">
    <div class="column sixteen wide computer sixteen wide gadget thirteen wide large screen thirteen wide widescreen">
        <div class="<? //ui accordion legal-accordion?>"
             data-selector="<?= htmlspecialchars(json_encode(array(
               "trigger" => "h2.i.header",
               "title" => "h2.i.header"
             ))) ?>"
             data-animate-children="false" data-exclusive="false">
            <div class="block" id="my-orders">
                <h2 class="ui header h1 uppercase normal">
                    Мои заказы
                </h2>
                <div class="content my-orders">
                    <? $APPLICATION->IncludeComponent(
                      "safin:sale.personal.order.list",
                      "my-orders",
                      array(
                        "COMPONENT_TEMPLATE" => "my-orders",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_GROUPS" => "Y",
                        "PATH_TO_DETAIL" => "",
                        "PATH_TO_COPY" => "",
                        "PATH_TO_CANCEL" => "",
                        "PATH_TO_PAYMENT" => "/personal-area/order/payment.php",
                        "PATH_TO_BASKET" => "/personal-area/basket/",
                        "PATH_TO_CATALOG" => "/catalog/",
                        "ORDERS_PER_PAGE" => "100",
                        "ID" => ($_GET['COPY_ORDER'] == 'Y' ? $_GET['ID'] : null),
                        "SET_TITLE" => "N",
                        "SAVE_IN_SESSION" => "N",
                        "NAV_TEMPLATE" => "",
                        "HISTORIC_STATUSES" => array("A"),
                        "RESTRICT_CHANGE_PAYSYSTEM" => array(
                          0 => "0",
                        ),
                        "DEFAULT_SORT" => "DATE_INSERT",
                        "ALLOW_INNER" => "N",
                        "ONLY_INNER_FULL" => "N",
                        "STATUS_COLOR_C" => "gray",
                        "STATUS_COLOR_F" => "gray",
                        "STATUS_COLOR_N" => "green",
                        "STATUS_COLOR_P" => "yellow",
                        "STATUS_COLOR_S" => "gray",
                        "STATUS_COLOR_SA" => "gray",
                        "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                      ),
                      false
                    ); ?>
                </div>
            </div>
            <?
            //   if ($_SERVER['REMOTE_ADDR'] == '188.234.148.65' || $_SERVER['REMOTE_ADDR'] == '92.50.158.6') {
            ?>
            <div class="block about bonus-system" id="my-bonus">
                <h2 class="ui header h1 uppercase normal">
                    Бонусная карта
                </h2>
                <div class="content my-bonus">
                    <?
                    if (!BonusCard::isset()) {
                        $APPLICATION->IncludeFile("/bonus-system/bonus_card.php", ['INCLUDE_FORM' => true]);
                    } else {
                        $APPLICATION->IncludeFile("/bonus-system/bonus_card_detail.php");
                    }
                    ?>
                </div>
            </div>
            <? // } ?>
            <div class="block" id="my-data">
                <h2 class="ui header h1 uppercase normal">
                    Персональные<br>данные
                </h2>
                <div class="content my-data">
                    <div class="ui grid">
                        <div class="column thirteen wide hd sixteen wide gadget three offset hd sixteen wide computer">
                            <form class="ui two column stackable grid form user-data" action="user.update">
                                <div class="row">
                                    <div class="column">

                                        <div class="field">
                                            <input type="text" name="name" placeholder="ФИО"
                                                   value="<?= $arUser["NAME"] ?>" data-validate="text-required"
                                                   title="ФИО">
                                        </div>
                                        <div class="field">
                                            <input type="tel" name="phone" placeholder="Номер телефона"
                                                   value="<?= $arUser["PERSONAL_PHONE"] ?>" data-validate="phone"
                                                   title="Номер телефона">
                                        </div>
                                        <div class="field">
                                            <input type="email" name="email" placeholder="Адрес электронной почты"
                                                   value="<?= $arUser["EMAIL"] ?>" data-validate="email" title="Email">
                                        </div>
                                        <div class="field">
                                            <input type="date" name="birthday" placeholder="Дата рождения"
                                                   <?=($arUser["PERSONAL_BIRTHDAY"] ? 'disabled' : '')?>
                                                   value="<?= $arUser["PERSONAL_BIRTHDAY"] ? date('Y-m-d', strtotime($arUser["PERSONAL_BIRTHDAY"])) : '' ?>" title="Дата рождения">
                                        </div>
                                        <div class="ui success message">Изменения сохранены</div>
                                        <div class="ui error message">Произошла ошибка</div>
                                        <button class="ui primary button" type="submit">Сохранить</button>
                                    </div>
                                    <div class="column">
                                        <div class="ui two column stackable centered grid">
                                            <div class="column">
                                                <p class="gray-color">
                                                    Вы можете использовать данные<br> из социальных сетей, чтобы<br>
                                                    ускорить процесс заполнения.
                                                </p>
                                                <div class="socials" data-ulogin="display=buttons">
                                                    <button class="ui circular vk icon button" type="button"
                                                            data-uloginbutton="vkontakte"><i class="ui icon vk"></i>
                                                    </button>
                                                    <button class="ui circular facebook icon button" type="button"
                                                            data-uloginbutton="facebook"><i
                                                                class="ui icon facebook f"></i></button>
                                                    <button class="ui circular orange icon button" type="button"
                                                            data-uloginbutton="odnoklassniki"><i
                                                                class="ui icon odnoklassniki"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <? // include("personal-data-block.php")?>
                            </form>
                            <form class="ui two column stackable grid form user-data" action="user.chpass">
                                <div class="row">
                                    <div class="column">
                                        <div class="nested-fields">
                                            <div class="ui header uppercase normal">Изменить пароль</div>
                                            <div class="nested-fields" style="margin-bottom: 1.5rem">
                                                <div class="field relative">
                                                    <button type="button"
                                                            class="js-password-toggler eye-button"><? icon("eye") ?></button>
                                                    <input type="password" name="oldPassword"
                                                           placeholder="Текущий пароль" value="">
                                                </div>
                                                <div class="field relative">
                                                    <button type="button"
                                                            class="js-password-toggler eye-button"><? icon("eye") ?></button>
                                                    <input type="password" name="password" placeholder="Новый пароль"
                                                           value="" data-validate="password">
                                                </div>
                                                <div class="field relative">
                                                    <button type="button"
                                                            class="js-password-toggler eye-button"><? icon("eye") ?></button>
                                                    <input type="password" name="confirmPassword"
                                                           placeholder="Повторите пароль" value=""
                                                           data-validate="confirm-password">
                                                </div>
                                            </div>
                                            <div class="ui success message">Изменения сохранены</div>
                                            <div class="ui error message">Произошла ошибка</div>
                                            <button class="ui primary button" type="submit">Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?
            if ($version == 3) {
            ?>
            <div class="block" id="my-addresses">
                <h2 class="ui header h1 uppercase normal">
                    Адреса<br>доставки
                </h2>
                <div class="content my-addresses">
                    <div class="ui grid">
                        <div class="column thirteen wide hd sixteen wide gadget three offset hd sixteen wide computer">
                            <div class="ui stackable grid">
                                <div class="twelve wide column">
                                    <div class="ui stackable grid">
                                        <div class="twelve wide column">
                                            <div class="ui accordion user-addresses"
                                                 data-selector="<?= htmlspecialchars(json_encode(array("trigger" => ".js-open-accordion-trigger"))) ?>">
                                                <? foreach ($userAddresses as $key => $address):
                                                    if ($address['name']) {
                                                        $subtitle = $address['formatted'];
                                                    } else {
                                                        $address['name'] = $address['formatted'];
                                                    }
                                                    ?>
                                                    <div class="title">
                                                        <div class="ui radio checkbox">
                                                            <input type="radio"
                                                                   name="address-default"<?= boolval($address["default"]) ? " checked" : "" ?>
                                                                   class="js-make-user-address-default"
                                                                   id="form_addresses_field_default_value_<?= $address["id"] ?>"
                                                                   value="<?= $address["id"] ?>">
                                                            <label for="form_addresses_field_default_value_<?= $address["id"] ?>"><?= $address["name"] ?><? if ($subtitle): ?>
                                                                    <br><span
                                                                            class="sub-label"><?= $subtitle ?></span><? endif; ?>
                                                            </label>
                                                        </div>
                                                        <div class="actions">
                                                            <button class="ui circular icon button js-open-accordion-trigger"
                                                                    type="button" title="Изменить">
                                                                <? icon("pencil") ?>
                                                            </button>
                                                            <button class="ui circular icon button js-remove-user-address"
                                                                    data-value="<?= $address["id"] ?>" type="button"
                                                                    title="Удалить">
                                                                <? icon("close") ?>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="content">
                                                        <form action="user.changeaddress"
                                                              class="ui form js-addresses-validate">
                                                            <input type="hidden" name="id"
                                                                   value="<?= $address["id"] ?>">
                                                            <div class="field">
                                                                <input type="text" placeholder="Название" name="name"
                                                                       value="<?= $address["name"] ?>">
                                                            </div>
                                                            <div class="fields stackable">
                                                                <div class="field ten wide">
                                                                    <input type="text" placeholder="Страна"
                                                                           data-dadata="address" name="country"
                                                                           value="<?= $address["country"] ?>"
                                                                           data-suggestion="country">
                                                                </div>
                                                                <div class="field six wide">
                                                                    <input type="text" placeholder="Индекс"
                                                                           data-dadata="address" name="index"
                                                                           value="<?= $address["index"] ?>"
                                                                           data-suggestion="postal_code">
                                                                </div>
                                                            </div>
                                                            <div class="fields stackable">
                                                                <div class="field ten wide">
                                                                    <input type="text" placeholder="Город*"
                                                                           data-dadata="address" name="city"
                                                                           value="<?= $address["city"] ?>"
                                                                           data-suggestion="city" data-validate="city">
                                                                </div>
                                                                <div class="field six wide">
                                                                    <input type="text" placeholder="Дом" name="house"
                                                                           value="<?= $address["house"] ?>">
                                                                </div>
                                                            </div>
                                                            <div class="fields stackable">
                                                                <div class="field ten wide">
                                                                    <input type="text" placeholder="Улица*"
                                                                           data-dadata="address" name="street"
                                                                           value="<?= $address["street"] ?>"
                                                                           data-suggestion="street"
                                                                           data-validate="street">
                                                                </div>
                                                                <div class="field six wide">
                                                                    <input type="text" placeholder="Корпус"
                                                                           name="corpse"
                                                                           value="<?= $address["corpse"] ?>">
                                                                </div>
                                                            </div>
                                                            <div class="fields stackable epilogue">
                                                                <div class="field ten wide">
                                                                    <div class="ui checkbox">
                                                                        <input id="make-default-user-address-<?= $address["id"] ?>"<?= boolval($address["default"]) ? " checked" : "" ?>
                                                                               type="checkbox" name="default" value="Y">
                                                                        <label for="make-default-user-address-<?= $address["id"] ?>">Сделать
                                                                            аресом по умолчанию</label>
                                                                    </div>
                                                                </div>
                                                                <div class="field six wide">
                                                                    <input type="text" name="flat"
                                                                           placeholder="Квартира"
                                                                           value="<?= $address["flat"] ?>">
                                                                </div>
                                                            </div>
                                                            <div class="ui success message">Изменения сохранены</div>
                                                            <div class="ui error message">При сохранении возникла ошибка
                                                            </div>
                                                            <button class="ui primary button" type="submit">Сохранить
                                                            </button>
                                                        </form>
                                                    </div>
                                                <? endforeach; ?>
                                                <div class="title add-new">
                                                    <div class="bold uppercase">Добавить адрес</div>
                                                    <div class="actions">
                                                        <button class="ui circular icon button js-open-accordion-trigger"
                                                                type="button">
                                                            <? icon("close") ?>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="content">
                                                    <form action="user.addaddress"
                                                          class="ui form js-addresses-validate  js-insert-after-submit">
                                                        <div class="fields stackable">
                                                            <div class="field ten wide">
                                                                <input type="text" data-dadata="address"
                                                                       placeholder="Страна" name="country" value=""
                                                                       data-suggestion="country">
                                                            </div>
                                                            <div class="field six wide">
                                                                <input type="text" data-dadata="address"
                                                                       placeholder="Индекс" name="index" value=""
                                                                       data-suggestion="postal_code">
                                                            </div>
                                                        </div>
                                                        <div class="fields stackable">
                                                            <div class="field ten wide">
                                                                <input type="text" data-dadata="address"
                                                                       placeholder="Город*" name="city" value=""
                                                                       data-suggestion="city" data-validate="city">
                                                            </div>
                                                            <div class="field six wide">
                                                                <input type="text" placeholder="Дом" name="house"
                                                                       value="">
                                                            </div>
                                                        </div>
                                                        <div class="fields stackable">
                                                            <div class="field ten wide">
                                                                <input type="text" data-dadata="address"
                                                                       placeholder="Улица*" name="street" value=""
                                                                       data-suggestion="street" data-validate="street">
                                                            </div>
                                                            <div class="field six wide">
                                                                <input type="text" placeholder="Корпус" name="corpse"
                                                                       value="">
                                                            </div>
                                                        </div>
                                                        <div class="fields stackable epilogue">
                                                            <div class="field ten wide">
                                                                <div class="ui checkbox">
                                                                    <input id="make-default-user-address-new"
                                                                           type="checkbox" name="default" value="Y">
                                                                    <label for="make-default-user-address-new">Сделать
                                                                        аресом по умолчанию</label>
                                                                </div>
                                                            </div>
                                                            <div class="field six wide">
                                                                <input type="text" name="flat" placeholder="Квартира"
                                                                       value="">
                                                            </div>
                                                        </div>
                                                        <div class="ui success message">Адрес добавлен</div>
                                                        <div class="ui error message">При сохранении возникла ошибка
                                                        </div>
                                                        <button class="ui primary button" type="submit">Сохранить
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <? } ?>
            <div class="block" id="my-organizations">
                <h2 class="ui header h1 uppercase normal">
                    Мои организации
                </h2>
                <div class="content my-organizations">
                    <div class="ui grid">
                        <div class="column thirteen wide hd sixteen wide gadget three offset hd sixteen wide computer">
                            <div class="ui stackable grid">
                                <div class="twelve wide column">
                                    <p class="grey-color">
                                        Для Вашего удобства Вы можете указать реквизиты Вашей организации и,
                                        <br>
                                        сохранив их при заказе, выбрав "покупка на юридическое лицо", реквизиты
                                        <br>
                                        будут указываться автоматически. Вы можете добавить несколько
                                        <br>
                                        организаций и выбирать их реквизиты при оформлении заказа.
                                    </p>
                                    <div class="ui accordion user-addresses"
                                         data-selector="<?= htmlspecialchars(json_encode(array("trigger" => ".js-open-accordion-trigger"))) ?>">
                                        <? foreach ($organizations as $key => $organization): ?>
                                            <div class="title">
                                                <div class="ui radio checkbox">
                                                    <input type="radio" name="default"
                                                           id="form_organizations_field_default_value_<?= $organization["id"] ?>"<?= $organization["default"] ? " checked" : "" ?>
                                                           value="<?= $organization["id"] ?>"
                                                           class="js-make-organization-default">
                                                    <label for="form_organizations_field_default_value_<?= $organization["id"] ?>"><?= $organization["name"] ?><? if ($organization["tin"]): ?>
                                                            <br><span class="sub-label">
                                                            ИНН: <?= $organization["tin"] ?></span><? endif; ?></label>
                                                </div>
                                                <div class="actions">
                                                    <button class="ui circular icon button js-open-accordion-trigger"
                                                            type="button" title="Изменить">
                                                        <? icon("pencil") ?>
                                                    </button>
                                                    <button class="ui circular icon button js-remove-organization-trigger"
                                                            data-value="<?= $organization["id"] ?>" type="button"
                                                            title="Удалить">
                                                        <? icon("close") ?>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="content">
                                                <form action="user.changeorgaddress"
                                                      class="ui form js-addresses-validate">
                                                    <input type="hidden" name="id" value="<?= $organization["id"] ?>">
                                                    <div class="ui stackable grid">
                                                        <div class="ten wide column">
                                                            <div class="field">
                                                                <input type="text" placeholder="Название организации*"
                                                                       data-dadata="party" name="name"
                                                                       data-validate="name"
                                                                       value="<?= $organization["name"] ?>"
                                                                       data-suggestion="name">
                                                            </div>
                                                            <div class="field">
                                                                <input type="text" placeholder="ИНН*"
                                                                       data-dadata="party"
                                                                       name="inn" data-validate="inn"
                                                                       value="<?= $organization["tin"] ?>"
                                                                       data-suggestion="inn">
                                                            </div>
                                                            <div class="field">
                                                                <input type="text" placeholder="КПП" data-dadata="party"
                                                                       name="kpp" data-validate="text"
                                                                       value="<?= $organization["checkpoint"] ?>"
                                                                       data-suggestion="kpp">
                                                            </div>
                                                            <div class="field">
                                                                <input type="text" placeholder="ОГРН"
                                                                       data-dadata="party"
                                                                       name="ogrn" data-validate="ogrn"
                                                                       value="<?= $organization["msrn"] ?>"
                                                                       data-suggestion="ogrn">
                                                            </div>
                                                            <div class="field">
                                                                <input type="text" placeholder="Контактное лицо"
                                                                       name="person"
                                                                       value="<?= $organization["person"] ?>">
                                                            </div>
                                                            <div class="field">
                                                                <input type="tel"
                                                                       placeholder="Номер телефона контактного лица*"
                                                                       name="phone" data-validate="phone"
                                                                       value="<?= $organization["phone"] ?>">
                                                            </div>
                                                            <div class="field">
                                                                <input type="text" placeholder="Юридический адрес"
                                                                       name="legal-address"
                                                                       value="<?= $organization["address"] ?>"
                                                                       data-suggestion="legal-address">
                                                            </div>
                                                            <div class="field">
                                                                <input type="text" placeholder="Фактический адрес"
                                                                       name="real-address"
                                                                       value="<?= $organization["actual_address"] ?>">
                                                            </div>
                                                            <div class="field">
                                                                <input type="email"
                                                                       placeholder="Адрес электронной почты"
                                                                       name="email" data-validate="email"
                                                                       value="<?= $organization["email"] ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fields epilogue">
                                                        <div class="ten wide field">
                                                            <div class="ui checkbox">
                                                                <input id="make-default-organization-<?= $organization["id"] ?>"
                                                                       type="checkbox" name="default" value="Y"
                                                                       data-validate="checkbox"<?= $organization["default"] ? " checked" : "" ?>>
                                                                <label for="make-default-organization-<?= $organization["id"] ?>">Сделать
                                                                    адресом по умолчанию</label>
                                                            </div>
                                                        </div>
                                                        <div class="six wide right aligned field">
                                                            <button class="ui primary button" type="submit">Сохранить
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="ui success message">Изменения сохранены</div>
                                                    <div class="ui error message">При сохранении возникла ошибка</div>
                                                </form>
                                            </div>
                                        <? endforeach; ?>
                                        <div class="title add-new">
                                            <div class="bold uppercase">Добавить Организацию</div>
                                            <div class="actions">
                                                <button class="ui circular icon button js-open-accordion-trigger"
                                                        type="button">
                                                    <? icon("close") ?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <form action="user.addorgaddress"
                                                  class="ui form  js-addresses-validate js-insert-after-submit">
                                                <div class="ui stackable grid">
                                                    <div class="ten wide column">
                                                        <div class="field">
                                                            <input type="text" data-dadata="party"
                                                                   placeholder="Название организации*" name="name"
                                                                   data-validate="name" data-suggestion="name"
                                                                   title="Название организации*">
                                                        </div>
                                                        <div class="field">
                                                            <input type="text" data-dadata="party" placeholder="ИНН*"
                                                                   name="inn" data-validate="inn" data-suggestion="inn"
                                                                   title="ИНН*">
                                                        </div>
                                                        <div class="field">
                                                            <input type="text" placeholder="КПП" name="kpp"
                                                                   data-validate="kpp" data-suggestion="kpp"
                                                                   title="КПП">
                                                        </div>
                                                        <div class="field">
                                                            <input type="text" data-dadata="party" placeholder="ОГРН"
                                                                   name="ogrn" data-validate="ogrn"
                                                                   data-suggestion="ogrn" title="ОГРН">
                                                        </div>
                                                        <div class="field">
                                                            <input type="text" placeholder="Контактное лицо"
                                                                   name="person" title="Контактное лицо">
                                                        </div>
                                                        <div class="field">
                                                            <input type="tel"
                                                                   placeholder="Номер телефона контактного лица*"
                                                                   name="phone" data-validate="phone"
                                                                   title="Номер телефона контактного лица*">
                                                        </div>
                                                        <div class="field">
                                                            <input type="text" placeholder="Юридический адрес"
                                                                   name="legal-address" data-suggestion="legal-address"
                                                                   title="Юридический адрес">
                                                        </div>
                                                        <div class="field">
                                                            <input type="text" placeholder="Фактический адрес"
                                                                   name="real-address" title="Фактический адрес">
                                                        </div>
                                                        <div class="field">
                                                            <input type="email" placeholder="Адрес электронной почты"
                                                                   name="email" data-validate="email"
                                                                   title="Адрес электронной почты">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields epilogue">
                                                    <div class="field ten wide">
                                                        <div class="ui checkbox">
                                                            <input id="make-default-organization-new" type="checkbox"
                                                                   name="default" data-validate="checkbox" value="Y">
                                                            <label for="make-default-organization-new">Сделать
                                                                организацией по умолчанию</label>
                                                        </div>
                                                    </div>
                                                    <div class="six wide field right aligned">
                                                        <button class="ui primary button" type="submit">Сохранить
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="ui success message">Организация добавлена</div>
                                                <div class="ui error message">При сохранении возникла ошибка</div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block" id="my-subscribes">
                <h2 class="ui header h1 uppercase normal">
                    Подписки<br>и уведомления
                </h2>
                <div class="content my-subscribes">
                    <div class="ui grid">
                        <div class="column thirteen wide hd sixteen wide gadget three offset hd sixteen wide computer">
                            <div class="ui stackable doubling grid v-nested">
                                <div class="four column row">
                                    <div class="column js-tab-button active" data-tab="email"
                                         data-tabdata="<?= htmlspecialchars(json_encode(array("deactivate" => "siblings"))) ?>">
                                        <button class="uppercase bold tab-button">Электронная почта</button>
                                    </div>
                                    <div class="column js-tab-button" data-tab="sms"
                                         data-tabdata="<?= htmlspecialchars(json_encode(array("deactivate" => "siblings"))) ?>">
                                        <button class="uppercase bold tab-button">SMS сообщения</button>
                                    </div>
                                    <div class="column js-tab-button" data-tab="push"
                                         data-tabdata="<?= htmlspecialchars(json_encode(array("deactivate" => "siblings"))) ?>">
                                        <button class="uppercase bold tab-button">PUSH уведомления</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fluid column content">
                                        <div class="ui tab active" data-tab="email">
                                            <?
                                            Subscribe::show();
                                            ?>
                                        </div>
                                        <div class="ui tab" data-tab="sms">
                                            <form action="user.subscribe" class="ui form js-subscribes-validate">
                                                <input type="hidden" name="type" value="sms">
                                                <div class="ui normal uppercase header">Рубрика подписок</div>
                                                <div class="nested-fields">
                                                    <div class="four fields">
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="men"
                                                                       value="Y" <?= in_array(1,
                                                                  $arUser["UF_SMS_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_men_with_sms">
                                                                <label for="form_subscribe_field_men_with_sms"
                                                                       class="gray-color">Мужчинам</label>
                                                            </div>
                                                        </div>
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="women"
                                                                       value="Y" <?= in_array(2,
                                                                  $arUser["UF_SMS_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_women_with_sms">
                                                                <label for="form_subscribe_field_women_with_sms"
                                                                       class="gray-color">Женщинам</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="four fields">
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="lookbook"
                                                                       value="Y" <?= in_array(3,
                                                                  $arUser["UF_SMS_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_lookbook_with_sms">
                                                                <label for="form_subscribe_field_lookbook_with_sms"
                                                                       class="gray-color">Lookbook</label>
                                                            </div>
                                                        </div>
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="actions"
                                                                       value="Y" <?= in_array(4,
                                                                  $arUser["UF_SMS_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_action_with_sms">
                                                                <label for="form_subscribe_field_action_with_sms"
                                                                       class="gray-color">Новости и акции</label>
                                                            </div>
                                                        </div>
                                                        <div class="field two fields">
                                                            <div class="field">
                                                                <select class="ui dropdown" name="smsFrom"
                                                                        data-initial="<?= $arUser["UF_SMS_FROM"] ?>">
                                                                    <option <?= !$arUser["UF_SMS_FROM"] ? "selected" : "" ?>
                                                                            value="">С
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "00:00" ? "selected" : "" ?>
                                                                            value="00:00">00:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "01:00" ? "selected" : "" ?>
                                                                            value="01:00">01:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "02:00" ? "selected" : "" ?>
                                                                            value="02:00">02:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "03:00" ? "selected" : "" ?>
                                                                            value="03:00">03:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "04:00" ? "selected" : "" ?>
                                                                            value="04:00">04:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "05:00" ? "selected" : "" ?>
                                                                            value="05:00">05:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "06:00" ? "selected" : "" ?>
                                                                            value="06:00">06:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "07:00" ? "selected" : "" ?>
                                                                            value="07:00">07:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "08:00" ? "selected" : "" ?>
                                                                            value="08:00">08:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "09:00" ? "selected" : "" ?>
                                                                            value="09:00">09:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "10:00" ? "selected" : "" ?>
                                                                            value="10:00">10:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "11:00" ? "selected" : "" ?>
                                                                            value="11:00">11:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "12:00" ? "selected" : "" ?>
                                                                            value="12:00">12:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "13:00" ? "selected" : "" ?>
                                                                            value="13:00">13:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "14:00" ? "selected" : "" ?>
                                                                            value="14:00">14:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "15:00" ? "selected" : "" ?>
                                                                            value="15:00">15:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "16:00" ? "selected" : "" ?>
                                                                            value="16:00">16:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "17:00" ? "selected" : "" ?>
                                                                            value="17:00">17:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "18:00" ? "selected" : "" ?>
                                                                            value="18:00">18:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "19:00" ? "selected" : "" ?>
                                                                            value="19:00">19:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "20:00" ? "selected" : "" ?>
                                                                            value="20:00">20:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "21:00" ? "selected" : "" ?>
                                                                            value="21:00">21:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "22:00" ? "selected" : "" ?>
                                                                            value="22:00">22:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_FROM"] == "23:00" ? "selected" : "" ?>
                                                                            value="23:00">23:00
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="field">
                                                                <select class="ui dropdown" name="smsTo"
                                                                        data-initial="<?= $arUser["UF_SMS_TO"] ?>">
                                                                    <option <?= !$arUser["UF_SMS_TO"] ? "selected" : "" ?>
                                                                            value="">По
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "00:00" ? "selected" : "" ?>
                                                                            value="00:00">00:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "01:00" ? "selected" : "" ?>
                                                                            value="01:00">01:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "02:00" ? "selected" : "" ?>
                                                                            value="02:00">02:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "03:00" ? "selected" : "" ?>
                                                                            value="03:00">03:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "04:00" ? "selected" : "" ?>
                                                                            value="04:00">04:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "05:00" ? "selected" : "" ?>
                                                                            value="05:00">05:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "06:00" ? "selected" : "" ?>
                                                                            value="06:00">06:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "07:00" ? "selected" : "" ?>
                                                                            value="07:00">07:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "08:00" ? "selected" : "" ?>
                                                                            value="08:00">08:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "09:00" ? "selected" : "" ?>
                                                                            value="09:00">09:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "10:00" ? "selected" : "" ?>
                                                                            value="10:00">10:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "11:00" ? "selected" : "" ?>
                                                                            value="11:00">11:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "12:00" ? "selected" : "" ?>
                                                                            value="12:00">12:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "13:00" ? "selected" : "" ?>
                                                                            value="13:00">13:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "14:00" ? "selected" : "" ?>
                                                                            value="14:00">14:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "15:00" ? "selected" : "" ?>
                                                                            value="15:00">15:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "16:00" ? "selected" : "" ?>
                                                                            value="16:00">16:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "17:00" ? "selected" : "" ?>
                                                                            value="17:00">17:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "18:00" ? "selected" : "" ?>
                                                                            value="18:00">18:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "19:00" ? "selected" : "" ?>
                                                                            value="19:00">19:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "20:00" ? "selected" : "" ?>
                                                                            value="20:00">20:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "21:00" ? "selected" : "" ?>
                                                                            value="21:00">21:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "22:00" ? "selected" : "" ?>
                                                                            value="22:00">22:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_SMS_TO"] == "23:00" ? "selected" : "" ?>
                                                                            value="23:00">23:00
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ui success message twelve wide field">
                                                    Изменения сохранены
                                                </div>
                                                <div class="ui error message twelve wide field">

                                                </div>
                                                <button class="ui primary button" type="submit">Сохранить</button>
                                            </form>
                                        </div>
                                        <div class="ui tab" data-tab="push">
                                            <form action="user.subscribe" class="ui form js-subscribes-validate">
                                                <input type="hidden" name="type" value="push">
                                                <div class="ui normal uppercase header">Рубрика подписок</div>
                                                <div class="nested-fields">
                                                    <div class="four fields">
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="men"
                                                                       value="Y" <?= in_array(1,
                                                                  $arUser["UF_PUSH_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_men_with_push">
                                                                <label for="form_subscribe_field_men_with_push"
                                                                       class="gray-color">Мужчинам</label>
                                                            </div>
                                                        </div>
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="women"
                                                                       value="Y" <?= in_array(2,
                                                                  $arUser["UF_PUSH_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_women_with_push">
                                                                <label for="form_subscribe_field_women_with_push"
                                                                       class="gray-color">Женщинам</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="four fields">
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="lookbook"
                                                                       value="Y" <?= in_array(3,
                                                                  $arUser["UF_PUSH_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_lookbook_with_push">
                                                                <label for="form_subscribe_field_lookbook_with_push"
                                                                       class="gray-color">Lookbook</label>
                                                            </div>
                                                        </div>
                                                        <div class="field">
                                                            <div class="ui checkbox">
                                                                <input type="checkbox" name="actions"
                                                                       value="Y" <?= in_array(4,
                                                                  $arUser["UF_PUSH_SUBSCRIBES"]) ? "checked" : "" ?>
                                                                       id="form_subscribe_field_action_with_push">
                                                                <label for="form_subscribe_field_action_with_push"
                                                                       class="gray-color">Новости и акции</label>
                                                            </div>
                                                        </div>
                                                        <div class="field two fields">
                                                            <div class="field">
                                                                <select class="ui dropdown" name="pushFrom"
                                                                        data-initial="<?= $arUser["UF_PUSH_FROM"] ?>">
                                                                    <option <?= !$arUser["UF_PUSH_FROM"] ? "selected" : "" ?>
                                                                            value="">С
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "00:00" ? "selected" : "" ?>
                                                                            value="00:00">00:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "01:00" ? "selected" : "" ?>
                                                                            value="01:00">01:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "02:00" ? "selected" : "" ?>
                                                                            value="02:00">02:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "03:00" ? "selected" : "" ?>
                                                                            value="03:00">03:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "04:00" ? "selected" : "" ?>
                                                                            value="04:00">04:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "05:00" ? "selected" : "" ?>
                                                                            value="05:00">05:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "06:00" ? "selected" : "" ?>
                                                                            value="06:00">06:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "07:00" ? "selected" : "" ?>
                                                                            value="07:00">07:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "08:00" ? "selected" : "" ?>
                                                                            value="08:00">08:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "09:00" ? "selected" : "" ?>
                                                                            value="09:00">09:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "10:00" ? "selected" : "" ?>
                                                                            value="10:00">10:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "11:00" ? "selected" : "" ?>
                                                                            value="11:00">11:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "12:00" ? "selected" : "" ?>
                                                                            value="12:00">12:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "13:00" ? "selected" : "" ?>
                                                                            value="13:00">13:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "14:00" ? "selected" : "" ?>
                                                                            value="14:00">14:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "15:00" ? "selected" : "" ?>
                                                                            value="15:00">15:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "16:00" ? "selected" : "" ?>
                                                                            value="16:00">16:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "17:00" ? "selected" : "" ?>
                                                                            value="17:00">17:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "18:00" ? "selected" : "" ?>
                                                                            value="18:00">18:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "19:00" ? "selected" : "" ?>
                                                                            value="19:00">19:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "20:00" ? "selected" : "" ?>
                                                                            value="20:00">20:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "21:00" ? "selected" : "" ?>
                                                                            value="21:00">21:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "22:00" ? "selected" : "" ?>
                                                                            value="22:00">22:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_FROM"] == "23:00" ? "selected" : "" ?>
                                                                            value="23:00">23:00
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="field">
                                                                <select class="ui dropdown" name="pushTo">
                                                                    <option <?= !$arUser["UF_PUSH_TO"] ? "selected" : "" ?>
                                                                            value="">По
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "00:00" ? "selected" : "" ?>
                                                                            value="00:00">00:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "01:00" ? "selected" : "" ?>
                                                                            value="01:00">01:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "02:00" ? "selected" : "" ?>
                                                                            value="02:00">02:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "03:00" ? "selected" : "" ?>
                                                                            value="03:00">03:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "04:00" ? "selected" : "" ?>
                                                                            value="04:00">04:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "05:00" ? "selected" : "" ?>
                                                                            value="05:00">05:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "06:00" ? "selected" : "" ?>
                                                                            value="06:00">06:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "07:00" ? "selected" : "" ?>
                                                                            value="07:00">07:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "08:00" ? "selected" : "" ?>
                                                                            value="08:00">08:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "09:00" ? "selected" : "" ?>
                                                                            value="09:00">09:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "10:00" ? "selected" : "" ?>
                                                                            value="10:00">10:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "11:00" ? "selected" : "" ?>
                                                                            value="11:00">11:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "12:00" ? "selected" : "" ?>
                                                                            value="12:00">12:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "13:00" ? "selected" : "" ?>
                                                                            value="13:00">13:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "14:00" ? "selected" : "" ?>
                                                                            value="14:00">14:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "15:00" ? "selected" : "" ?>
                                                                            value="15:00">15:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "16:00" ? "selected" : "" ?>
                                                                            value="16:00">16:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "17:00" ? "selected" : "" ?>
                                                                            value="17:00">17:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "18:00" ? "selected" : "" ?>
                                                                            value="18:00">18:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "19:00" ? "selected" : "" ?>
                                                                            value="19:00">19:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "20:00" ? "selected" : "" ?>
                                                                            value="20:00">20:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "21:00" ? "selected" : "" ?>
                                                                            value="21:00">21:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "22:00" ? "selected" : "" ?>
                                                                            value="22:00">22:00
                                                                    </option>
                                                                    <option <?= $arUser["UF_PUSH_TO"] == "23:00" ? "selected" : "" ?>
                                                                            value="23:00">23:00
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ui success message twelve wide field">Изменения сохранены
                                                </div>
                                                <div class="ui error message twelve wide field"></div>
                                                <button class="ui primary button" type="submit">Сохранить</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="computer or lower hidden column three wide right aligned">
        <div class="ui sticky" data-context=".personal-area" data-break-mobile="true">
            <div class="ui vertical fluid menu spy">
                <a class="item" href="#my-orders">Мои заказы</a>
                <? //if ($_SERVER['REMOTE_ADDR'] == '188.234.148.65' || $_SERVER['REMOTE_ADDR'] == '92.50.158.6') {
                ?><a class="item" href="#my-bonus">Бонусная карта</a><?
                //} ?>
                <a class="item" href="#my-data">Персональные данные</a>
                <a class="item" href="#my-addresses">Адреса доставки</a>
                <a class="item" href="#my-organizations">Мои организации</a>
                <a class="item" href="#my-subscribes">Подписки и уведомления</a>

            </div>
            <div class="questions">
                <a href="javascript:void(null)" class="js-logout-trigger uppercase">Выход</a>
            </div>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

