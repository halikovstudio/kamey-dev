<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Подтверждение почты"); ?>
<div class="ui container">
    <div class="personal">
        <div class="text--center personal__header">Подтверждение почты</div>
        <div class="organizations__container">
            <?
            if (!$_GET['s']) {
                ?>
                <div class="ui error message">
                    Отсутствует уникальный код
                </div>
                <?
            } else {
                $arUser = CUser::GetList($by, $order, ['UF_EMAIL_CODE' => $_GET['s']])->Fetch();
                if (!$arUser) {
                    ?>
                    <div class="ui error message">Код не действителен</div>
                    <?
                } else {
                    global $USER;
                    $rsUser = CUser::GetList($by, $order, ['=EMAIL' => $arUser['UF_EMAIL']])->Fetch();
                    if ($rsUser) {
                        $USER->Update($arUser['ID'], [
                          'UF_EMAIL' => '',
                          'UF_EMAIL_CODE' => ''
                        ]);
                        ?>
                        <div class="ui error message">Почта <?= $arUser['UF_EMAIL'] ?> занята</div>
                        <?
                    } else {
                        $USER->Update($arUser['ID'], [
                          'EMAIL' => $arUser['UF_EMAIL'],
                          'UF_EMAIL' => '',
                          'UF_EMAIL_CODE' => ''
                        ]);
                        ?>
                        <div class="ui success message">
                            Почта подтверждена
                        </div>
                        <?
                    }
                }
            }
            ?>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
