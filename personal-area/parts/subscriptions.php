<?php

use View\Subscribe;

$timezones = [
  '' => 'Не указано',
  '-12' => '(GMT -12:00) Эниветок, Кваджалейн',
  '-11' => '(GMT -11:00) Остров Мидуэй, Самоа',
  '-10' => '(GMT -10:00) Гавайи',
  '-9' => '(GMT -9:00) Аляска',
  '-8' => '(GMT -8:00) Тихоокеанское время (США и Канада)',
  '-7' => '(GMT -7:00) Горное время (США и Канада)',
  '-6' => '(GMT -6:00) Центральное время (США и Канада), Мехико',
  '-5' => '(GMT -5:00) Восточное время (США и Канада), Богота, Лима',
  '-4' => '(GMT -4:00) Атлантическое время (Канада), Каракас, Ла-Пас',
  '-3.5' => '(GMT -3:30) Ньюфаундленд',
  '-3' => '(GMT -3:00) Бразилия, Буэнос-Айрес, Джорджтаун',
  '-2' => '(GMT -2:00) Срединно-Атлантического',
  '-1' => '(GMT -1:00) Азорские острова, острова Зеленого Мыса',
  '0' => '(GMT +0.0) Время Западной Европе, Лондон, Лиссабон, Касабланка',
  '1' => '(GMT +1:00) Брюссель, Копенгаген, Мадрид, Париж',
  '2' => '(GMT +2:00) Киев, Калининград, Южная Африка',
  '3' => '(GMT +3:00) Багдад, Эр-Рияд, Москва, Санкт-Петербург',
  '3.5' => '(GMT +3:30) Тегеран',
  '4' => '(GMT +4:00) Абу-Даби, Мускат, Баку, Тбилиси',
  '4.5' => '(GMT +4:30) Кабул',
  '5' => '(GMT +5:00) Екатеринбург, Исламабад, Карачи, Ташкент',
  '5.5' => '(GMT +5:30) Бомбей, Калькутта, Мадрас, Нью-Дели',
  '5.75' => '(GMT +5:45) Катманду',
  '6' => '(GMT +6:00) Алматы, Дакке, Коломбо',
  '7' => '(GMT +7:00) Бангкок, Ханой, Джакарта',
  '8' => '(GMT +8:00) Пекин, Перт, Сингапур, Гонконг',
  '9' => '(GMT +9:00) Токио, Сеул, Осака, Саппоро, Якутск',
  '9.5' => '(GMT +9:30) Аделаида, Дарвин',
  '10' => '(GMT +10:00) Восточная Австралия, Гуам, Владивосток',
  '11' => '(GMT +11:00) Магадан, Соломоновы острова, Новая Каледония',
  '12' => '(GMT +12:00) Окленд, Веллингтон, Фиджи, Камчатка',
];
$times = [
  '00:00',
  '01:00',
  '02:00',
  '03:00',
  '04:00',
  '05:00',
  '06:00',
  '07:00',
  '08:00',
  '09:00',
  '10:00',
  '11:00',
  '12:00',
  '13:00',
  '14:00',
  '15:00',
  '16:00',
  '17:00',
  '18:00',
  '19:00',
  '20:00',
  '21:00',
  '22:00',
  '23:00',
];
$isCrm = isset($userId);
$userId = isset($userId) ? $userId : CUser::GetID();
$arUser = CUser::GetByID($userId)->Fetch();
$types = [
  1 => 'Мужчинам',
  2 => 'Женщинам',
  3 => 'Lookbook',
  4 => 'Новости и акции'
];
Subscribe::init(); ?>
<div class="text--center personal__header">Подписки</div>
<form class="ui form subscription-form"<?= ($isCrm ? ' style="pointer-events:none;"' : '') ?>>
    <div class="ui grid">
        <div class="three wide computer five wide tablet sixteen wide mobile column">
            <div class="uppercase text--medium text--small mb24">Электронная почта</div>
            <div class="grouped fields custom">
                <?
                foreach (Subscribe::$list as $item) {
                    ?>
                    <div class="field">
                        <div class="ui checkbox custom">
                            <input type="hidden" name="subscription[_<?= $item['ID'] ?>]" value="N">
                            <input type="checkbox" name="subscription[_<?= $item['ID'] ?>]"
                                   class="hidden"
                                   value="Y" <?= in_array($item['ID'], Subscribe::$checked) ? "checked" : "" ?>
                                   id="form_subscription_email_<?= $item['ID'] ?>">
                            <label for="form_subscription_email_<?= $item['ID'] ?>"><?= $item['NAME'] ?></label>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
        <div class="three wide computer five wide tablet sixteen wide mobile column">
            <div class="uppercase text--medium text--small mb24">sms сообщения</div>
            <div class="grouped fields custom">
                <?
                foreach ($types as $type => $label) {
                    ?>
                    <div class="field">
                        <div class="ui checkbox custom">
                            <input type="checkbox" name="sms_subscribes[]" tabindex="0" class="hidden"
                                   value="<?= $type ?>" id="sms_<?= $type ?>"<?= (in_array($type,
                              $arUser['UF_SMS_SUBSCRIBES']) ? ' checked' : '') ?>>
                            <label for="sms_<?= $type ?>"><?= $label ?></label>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
        <div class="three wide computer five wide tablet sixteen wide mobile column">
            <div class="uppercase text--medium text--small mb24">push уведомления</div>
            <div class="grouped fields custom">
                <?
                foreach ($types as $type => $label) {
                    ?>
                    <div class="field">
                        <div class="ui checkbox custom">
                            <input type="checkbox" name="push_subscribes[]" tabindex="0" class="hidden"
                                   value="<?= $type ?>" id="push_<?= $type ?>"<?= (in_array($type,
                              $arUser['UF_PUSH_SUBSCRIBES']) ? ' checked' : '') ?>>
                            <label for="push_<?= $type ?>"><?= $label ?></label>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
        <div class="five wide computer twelve wide tablet sixteen wide mobile column right floated order-mobile-1">
            <div class="personal__text mb16">В какое время можно высылать Вам
                <br>
                уведомления от магазина Cameo.
            </div>
            <div class="subscriptions__hours mb32">
                <div class="subscriptions__clock">
                    <?= icon('clock') ?>
                </div>
                <div>
                    <div class="personal__text">Ваш часовой пояс:</div>
                    <select class="ui dropdown subscriptions__utc" name="timezone" id="timezone">
                        <?
                        foreach ($timezones as $timezone => $label) {
                            ?>
                            <option value="<?= $timezone ?>"<?= $timezone == $arUser['UF_TIMEZONE'] ? ' selected' : '' ?>><?= $label ?></option>
                            <?
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class=" fields subscriptions__time">
                <div class="field">
                    <select class="ui dropdown" name="smsFrom"
                            data-initial="<?= $arUser["UF_SMS_FROM"] ?>">
                        <option <?= !$arUser["UF_SMS_FROM"] ? "selected" : "" ?>
                                value="">Не указано
                        </option>
                        <?
                        foreach ($times as $time) {
                            ?>
                            <option value="<?= $time ?>"<?= ($arUser['UF_SMS_FROM'] == $time ? ' selected' : '') ?>>
                                с <?= $time ?></option>
                            <?
                        }
                        ?>
                    </select>
                </div>
                <div class="field">
                    <select class="ui dropdown" name="smsTo"
                            data-initial="<?= $arUser["UF_SMS_TO"] ?>">
                        <option <?= !$arUser["UF_SMS_TO"] ? "selected" : "" ?>
                                value="">Не указано
                        </option>
                        <?
                        foreach ($times as $time) {
                            ?>
                            <option value="<?= $time ?>"<?= ($arUser['UF_SMS_TO'] == $time ? ' selected' : '') ?>>
                                по <?= $time ?></option>
                            <?
                        }
                        ?>
                    </select>
                </div>
            </div>
            <br>
            <div class="mb32 mobile hidden"></div>
        </div>
    </div>
    <div class="ui error message"></div>
</form>
