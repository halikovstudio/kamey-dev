<?
$arUser = CUser::GetByID(CUser::GetID())->fetch();
$arUser['PERSONAL_PHONE'] = BonusCard::normalizePhone($arUser['PERSONAL_PHONE']);
$allSizes = !(!$arUser['UF_HEIGHT'] || !$arUser['UF_WAIST'] || !$arUser['UF_CHEST'] || !$arUser['UF_HIP']);
$someSizes = $arUser['UF_HEIGHT'] || $arUser['UF_WAIST'] || $arUser['UF_CHEST'] || $arUser['UF_HIP'];
?>
<div class="ui grid">
    <div class="eight wide computer ten wide tablet sixteen wide mobile column">
        <form class="ui form form-user-field">
            <?= bitrix_sessid_post() ?>
            <div class="fields custom">
                <div class="eleven wide field custom">
                    <input type="text" name="name" placeholder="Имя и фамилия" value="<?= $arUser["NAME"] ?>">
                </div>
                <div class="five wide field">
                    <button class="ui mini primary button custom transition hidden">Сохранить</button>
                </div>
            </div>
        </form>
        <form class="ui form form-user-field">
            <?= bitrix_sessid_post() ?>
            <div class="fields custom">
                <div class="eleven wide field custom">
                    <input type="email" name="email" placeholder="Email" value="<?= $arUser["EMAIL"] ?>">
                </div>
                <div class="five wide field personal__data-button">
                    <button class="ui mini primary button custom transition hidden">Подтвердить</button>
                </div>
            </div>
        </form>
        <form class="ui form form-change-phone">
            <?= bitrix_sessid_post() ?>
            <div class="fields custom">
                <div class="eleven wide field custom">
                    <input type="tel" name="phone" placeholder="" value="<?= $arUser["PERSONAL_PHONE"] ?>">
                </div>
                <div class="five wide field personal__data-button">
                    <button class="ui mini primary button custom transition hidden">Подтвердить</button>
                </div>
            </div>
        </form>
        <form class="ui form form-user-field">
            <?= bitrix_sessid_post() ?>
            <div class="fields custom">
                <div class="eleven wide field custom relative birthday-input">
                    <input type="<?=($arUser["PERSONAL_BIRTHDAY"] ? 'text' : 'date')?>" name="birthday" placeholder="Дата рождения"
                           title="Дата рождения" <?= ($arUser["PERSONAL_BIRTHDAY"] ? 'disabled' : '') ?>
                           value="<?= $arUser["PERSONAL_BIRTHDAY"]  ?>">
                    <button type="button" class="eye-button lock-button"><? icon("lock") ?></button>
                </div>
                <div class="five wide field">
                    <button class="ui mini primary button custom transition hidden">Сохранить</button>
                </div>
            </div>
        </form>
        <div class="ui custom basic button js-logout-trigger">Выйти</div>
    </div>
    <div class="five wide computer five wide tablet sixteen wide mobile column right floated">
        <div class="auth-modal__socials">
            <?
            $APPLICATION->IncludeComponent(
              "ulogin:sync",
              "",
              Array(
                "GROUP_ID" => array(3, 4),
                "LOGIN_AS_EMAIL" => "N",
                "SEND_EMAIL" => "N",
                "SOCIAL_LINK" => "N",
                "ULOGINID1" => "",
                "ULOGINID2" => ""
              )
            );
            ?>

            </div>
        </div>
    </div>
</div>
<div class="personal__dashed"></div>

<div class="uppercase text--medium text--small mb16 size-filled<?= (!$someSizes ? ' transition hidden' : '') ?>">Мои
    параметры
</div>
<div class="ui grid size-filled<?= (!$someSizes ? ' transition hidden' : '') ?>">
    <div class="eleven wide computer sixteen wide tablet sixteen wide mobile column">
        <div class="ui grid bottom aligned">
            <div
                    class="seven wide widescreen eight wide large screen eight wide computer eight wide tablet sixteen wide mobile column">
                <div class="yourparams">
                    <div class="yourparams__item transition-yourparams"
                         data-target=".yourparams-chest">
                        <span class="yourparams__name">Обхват груди, см</span>
                        <span class="yourparams__value value-chest"><?= $arUser['UF_CHEST'] ?: '-' ?></span>
                    </div>
                    <div class="yourparams__item transition-yourparams"
                         data-target=".yourparams-waist">
                        <span class="yourparams__name">Обхват талии, см</span>
                        <span class="yourparams__value value-waist"><?= $arUser['UF_WAIST'] ?: '-' ?></span>
                    </div>
                    <div class="yourparams__item transition-yourparams"
                         data-target=".yourparams-hips">
                        <span class="yourparams__name">Обхват бёдер, см</span>
                        <span class="yourparams__value value-hip"><?= $arUser['UF_HIP'] ?: '-' ?></span>
                    </div>
                    <div class="yourparams__item transition-yourparams"
                         data-target=".yourparams-height">
                        <span class="yourparams__name">Рост, см</span>
                        <span class="yourparams__value value-height"><?= $arUser['UF_HEIGHT'] ?: '-' ?></span>
                    </div>
                </div>
            </div>
            <div class="seven wide computer seven wide tablet sixteen wide mobile column right floated">
                <div class="transition hidden yourparams__edit yourparams-chest">
                    <form class="ui form form-size">
                        <div class="fields custom">
                            <div class="six wide field custom">
                                <label for="" class="mb16">Ваш обхват груди</label>
                                <input class="input-yourparams" type="text" value="<?= $arUser['UF_CHEST'] ?>"
                                       name="chest">
                            </div>
                        </div>
                        <div class="mb56"></div>
                        <div class="field">
                            <a class="ui mini basic button custom">Отмена</a>
                            <button class="ui mini primary button custom">Сохранить</button>
                        </div>
                    </form>
                </div>
                <div class="transition hidden  yourparams__edit yourparams-waist">
                    <form class="ui form form-size">
                        <div class="fields custom">
                            <div class="six wide field custom">
                                <label for="" class="mb16">Ваш обхват талии</label>
                                <input class="input-yourparams" type="text" value="<?= $arUser['UF_WAIST'] ?>"
                                       name="waist">
                            </div>
                        </div>
                        <div class="mb56"></div>
                        <div class="field">
                            <a class="ui mini basic button custom">Отмена</a>
                            <button class="ui mini primary button custom">Сохранить</button>
                        </div>
                    </form>
                </div>
                <div class="transition hidden yourparams__edit yourparams-hips">
                    <form class="ui form form-size">
                        <div class="fields custom">
                            <div class="six wide field custom">
                                <label for="" class="mb16">Ваш обхват бедер</label>
                                <input class="input-yourparams" type="text" value="<?= $arUser['UF_HIP'] ?>" name="hip">
                            </div>
                        </div>
                        <div class="mb56"></div>
                        <div class="field">
                            <a class="ui mini basic button custom">Отмена</a>
                            <button class="ui mini primary button custom">Сохранить</button>
                        </div>
                    </form>
                </div>
                <div class="transition hidden  yourparams__edit yourparams-height">
                    <form class="ui form form-size">
                        <div class="fields custom">
                            <div class="six wide field custom">
                                <label for="" class="mb16">Ваш рост</label>
                                <input class="input-yourparams" type="text" value="<?= $arUser['UF_HEIGHT'] ?>"
                                       name="height">
                            </div>
                        </div>
                        <div class="mb56"></div>
                        <div class="field">
                            <a class="ui mini basic button custom">Отмена</a>
                            <button class="ui mini primary button custom">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="mb48"></div>
        <div class="uppercase text--medium text--small mb24">Подходящие вам размеры</div>
        <table class="ui very basic table yourparams__table unstackable">
            <? SizeTable::getUserTable() ?>
        </table>
    </div>
    <div class="four wide computer sixteen wide tablet sixteen wide mobile column right floated">
        <div class="yourparams__right">
            <div class="uppercase text--medium text--small mb16 mobile only">Настройки</div>
            <div class="yourparams__text mb24">Автоматическая
                <br>
                рекомендация размера
            </div>
            <div class="ui toggle checkbox yourparams__checkbox mb56">
                <span>Вкл.</span>
                <input type="checkbox"
                       name="public"<?= (!$arUser['UF_AUTO_SIZE'] ? ' checked' : '') ?><?= (!$allSizes ? ' disabled' : '') ?> tabindex="-1"/>
                <label></label>
                <span>Выкл.</span>
            </div>
            <a class="ui mini basic button custom size-clear">Очистить размеры</a>
        </div>
    </div>
</div>
<div class="ui grid size-empty<?= ($someSizes ? ' transition hidden' : '') ?>">
    <div
            class="three wide widescreen four wide large screen four wide computer six wide tablet sixteen wide mobile column">
        <?= icon('yourparams') ?>
    </div>
    <div
            class="five wide widescreen six wide large screen eight wide computer eight wide tablet sixteen wide mobile column">
        <div class="uppercase text--medium text--big mb24">Найди свой размер</div>
        <div class="personal__text mb24">
            Измеряем полный обхват груди сантиметровой лентой по наиболее выступающим точками груди, строго
            горизонтально.
        </div>
        <a class="ui mini primary button custom" data-modal="find">Найти</a>
    </div>
</div>
<div class="personal__dashed"></div>
<div class="uppercase text--medium text--small mb16">Изменить пароль</div>
<div class="ui grid">
    <div class="eight wide computer ten wide tablet sixteen wide mobile column">
        <form class="ui form user-data" action="user.chpass">
            <div class="fields custom">
                <div class="eleven wide field custom relative">
                    <button type="button" class="js-password-toggler eye-button"><? icon("eye") ?></button>
                    <input type="password" name="password" placeholder="Новый пароль" value=""
                           data-validate="password">
                </div>
            </div>
            <div class="fields custom">
                <div class="eleven wide field custom relative">
                    <button type="button" class="js-password-toggler eye-button"><? icon("eye") ?></button>
                    <input type="password" name="confirmPassword" placeholder="Подтвердите пароль" value=""
                           data-validate="confirm-password">
                </div>
            </div>
            <div class="ui error message"></div>
            <div class="ui success message">Пароль успешно изменен</div>
            <div class="field">
                <button class="ui mini primary button custom">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<div class="ui mini modal auth-modal" id="accept-email">
    <div class="close">
        <? icon("close") ?>
    </div>
    <div class="modal-content auth-modal__content auth-modal__content_nonheight">
        <div class="">
            <div class="auth-modal__header">Подтвердите новую
                <br>
                Электронную почту
            </div>
            <div class="auth-modal_flex-center auth-modal_flex-center_nonheight">
                <?= icon('message') ?>
                <p>Мы отправили вам пиcьмо
                    по новому электронному адресу
                    <span class="value"></span> для потдверждения.
                </p>
            </div>
            <br>
            <br>
        </div>
    </div>
</div>
<div class="ui mini modal auth-modal" id="accept-phone">
    <div class="close">
        <? icon("close") ?>
    </div>
    <div class="modal-content auth-modal__content auth-modal__content_nonheight">
        <div class="">
            <div class="auth-modal__header">Подтвердите новый номер
                телефона
            </div>
            <form class="ui form small auth-modal__form js-user-validate form-confirm-phone">
                <?= bitrix_sessid_post() ?>
                <input type="hidden" name="phone">
                <div class="field custom">
                    <input type="text" name="code" data-validate="text-required" placeholder="Введите код из СМС">
                </div>
                <div class="auth-modal__replay"></div>
                <div class="ui error message"></div>
                <div class="field custom">
                    <div class="ui button mini fluid gray js-get-code-again custom">Получить смс повторно</div>
                </div>
            </form>
        </div>
    </div>
</div>
<?
global $APPLICATION;
$APPLICATION->IncludeFile('/personal-area/parts/find-modal.php',['arUser'=>$arUser]);
?>
