<?
$allSizes = !(!$arUser['UF_HEIGHT'] || !$arUser['UF_WAIST'] || !$arUser['UF_CHEST'] || !$arUser['UF_HIP']);
$someSizes = $arUser['UF_HEIGHT'] || $arUser['UF_WAIST'] || $arUser['UF_CHEST'] || $arUser['UF_HIP'];
?>
<div class="ui modal find-modal" id="find">
    <div class="close">
        <? icon("close") ?>
    </div>
    <div class="find-modal__load"></div>
    <div class="modal-content find-modal__content">
        <form class="ui form">
            <button style="position:absolute;opacity: 0;visibility: hidden;"></button>
            <div class="find-chest-side transition <?=!$allSizes ? 'visible' : 'hidden'?>">
                <div class="find-modal__step">1 из 4</div>
                <div class="uppercase text--medium text--center text--big mb32  mobile only">обхват груди, см</div>
                <div class="ui grid">
                    <div
                      class="six wide widescreen six wide large screen six wide computer six wide tablet sixteen wide mobile column mobile--center mbm40">
                        <?= icon('yourparams') ?>
                    </div>
                    <div
                      class="nine wide widescreen nine wide large screen nine wide computer nine wide tablet sixteen wide mobile column">
                        <div class="uppercase text--medium text--big mb24  mobile hidden">обхват груди, см</div>
                        <div class="personal__text mb24">
                            Измеряем полный обхват груди сантиметровой лентой по наиболее выступающим точками груди,
                            строго
                            горизонтально.
                        </div>
                        <div class="field custom find-modal__input">
                            <input type="text" name="chest" class="input-yourparams">
                        </div>
                        <br>
                        <br>
                        <div class="field custom">
                            <a class="ui mini primary button custom fluid transition-target"
                               data-target="find-waist-side" data-load="60">Далее
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="find-waist-side transition hidden">
                <a href="javascript:void(null)" class="find-modal__back transition-target" data-target="find-chest-side"
                   data-load="80"><?= icon('back') ?></a>
                <div class="find-modal__step">2 из 4</div>
                <div class="uppercase text--medium text--big text--center mb32 mobile only">обхват талии, см</div>
                <div class="ui grid">
                    <div
                      class="six wide widescreen six wide large screen six wide computer six wide tablet sixteen wide mobile column mobile--center mbm40">
                        <?= icon('waist') ?>
                    </div>
                    <div
                      class="nine wide widescreen nine wide large screen nine wide computer nine wide tablet sixteen wide mobile column">
                        <div class="uppercase text--medium text--big mb24 mobile hidden">обхват талии, см</div>
                        <div class="personal__text mb24">Ленту накладывают горизонтально вокруг туловища на уровне линии
                            талии и замыкают спереди.
                        </div>
                        <div class="field custom find-modal__input">
                            <input type="text" name="waist" class="input-yourparams">
                        </div>
                        <br>
                        <br>
                        <div class="field custom">
                            <a class="ui mini primary button custom fluid transition-target"
                               data-target="find-hips-side" data-load="40">Далее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="find-hips-side transition hidden">
                <a href="javascript:void(null)" class="find-modal__back transition-target" data-target="find-waist-side"
                   data-load="60"><?= icon('back') ?></a>
                <div class="find-modal__step">3 из 4</div>
                <div class="uppercase text--medium text--big mb32 text--center mobile only">обхват бёдер, см</div>
                <div class="ui grid">
                    <div
                      class="six wide widescreen six wide large screen six wide computer six wide tablet sixteen wide mobile column mobile--center mbm40">
                        <?= icon('hips') ?>
                    </div>
                    <div
                      class="nine wide widescreen nine wide large screen nine wide computer nine wide tablet sixteen wide mobile column">
                        <div class="uppercase text--medium text--big mb24  mobile hidden">обхват бёдер, см</div>
                        <div class="personal__text mb24">Сзади лента накладывается на наиболее выступающие назад точки
                            ягодичной области, сбоку и впереди идет строго горизонтально.
                        </div>
                        <div class="field custom find-modal__input">
                            <input type="text" name="hip" class="input-yourparams">
                        </div>
                        <br>
                        <br>
                        <div class="field custom">
                            <a class="ui mini primary button custom fluid transition-target"
                               data-target="find-height-side" data-load="20">Далее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="find-height-side transition hidden">
                <a href="javascript:void(null)" class="find-modal__back transition-target" data-target="find-hips-side"
                   data-load="40"><?= icon('back') ?></a>
                <div class="find-modal__step">4 из 4</div>
                <div class="uppercase text--medium text--big mb32 text--center  mobile only">Ваш рост, см</div>
                <div class="ui grid center aligned">
                    <div
                      class="nine wide widescreen nine wide large screen nine wide computer nine wide tablet sixteen wide mobile column">
                        <div class="uppercase text--medium text--big mb24  mobile hidden">Ваш рост, см</div>
                        <div class="personal__text mb24">Если у вас уже есть похожая модель
                            нашего бренда, размер которой вам подходит,
                            рекомендуем выбрать тот же размер.
                        </div>
                        <div class="field custom find-modal__input">
                            <input type="text" name="height" class="input-yourparams">
                        </div>
                        <br>
                        <br>
                        <div class="field custom">
                            <a class="ui mini primary button custom fluid transition-target"
                               data-target="find-params-side" data-load="0">Далее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="find-params-side transition <?=$allSizes ? 'visible' : 'hidden'?>">
                <div class="uppercase text--medium text--big mb16">Мои параметры</div>
                <div class="ui grid">
                    <div
                      class="eight wide widescreen eight wide large screen eight wide computer eight wide tablet sixteen wide mobile column mobile--center mbm40">
                        <div class="yourparams">
                            <div class="yourparams__item">
                                <span class="yourparams__name">Обхват груди, см</span>
                                <span class="yourparams__value value-chest"><?=$arUser['UF_CHEST']?></span>
                            </div>
                            <div class="yourparams__item">
                                <span class="yourparams__name">Обхват талии, см</span>
                                <span class="yourparams__value value-waist"><?=$arUser['UF_WAIST']?></span>
                            </div>
                            <div class="yourparams__item">
                                <span class="yourparams__name">Обхват бёдер, см</span>
                                <span class="yourparams__value value-hip"><?=$arUser['UF_HIP']?></span>
                            </div>
                            <div class="yourparams__item">
                                <span class="yourparams__name">Рост, см</span>
                                <span class="yourparams__value value-height"><?=$arUser['UF_HEIGHT']?></span>
                            </div>
                        </div>
                    </div>
                    <div class="seven wide computer sixteen wide tablet sixteen wide mobile column right floated">
                        <div class="yourparams__right">
                            <div class="uppercase text--medium text--small mb16 mobile only">Настройки</div>
                            <div class="yourparams__text mb24">Автоматическая
                                <br>
                                рекомендация размера
                            </div>
                            <div class="ui toggle checkbox yourparams__checkbox mb56">
                                <span>Вкл.</span>
                                <input type="checkbox" name="public" <?= (!$arUser['UF_AUTO_SIZE'] || !$allSizes ? ' checked' : '') ?> tabindex="-1">
                                <label></label>
                                <span>Выкл.</span>
                            </div>
                            <a class="ui mini basic button custom size-clear">Очистить размеры</a>
                        </div>
                    </div>
                    <div class="column sixteen wide">
                        <div class="mb28"></div>
                        <div class="uppercase text--medium text--small mb24">Подходящие вам размеры</div>
                        <table class="ui very basic table yourparams__table unstackable">
                            <? SizeTable::getUserTable() ?>
                        </table>
                    </div>
                    <div class="column sixteen wide">
                        <div class="ui grid two column stackable sizes-set-complete">
                            <div class="column">
                                <p>Введенные параметры сохранены. Каталог товаров оптимизирован под ваши параметры.</p>
                            </div>
                            <div class="column">
                                <div class="ui mini custom black button close">Закрыть</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
