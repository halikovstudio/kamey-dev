<?php
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/suggestions.min.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/suggestions.min.js');
$organizations = (new orgAddressesData())->get();
?>
<div class="organizations__container">
    <div class="text--center personal__header">Организации</div>
    <div class="ui accordion mb48">
        <?
        foreach ($organizations as $organization) {
            ?>
            <div class="title personal-accord__title" data-organization="<?=$organization['id']?>">
                <div class="personal__text personal-accord__org">
                    <?= icon('check',
                      $organization["default"] ? "" : " hide") ?><span><?= $organization["name"] ?><? if ($organization["tin"]): ?>, ИНН: <?= $organization["tin"] ?><? endif; ?></span>
                </div>
                <div class="personal-accord__show">
                    <svg class="icon show">
                        <use xlink:href="/local/templates/cameo-v3/images/icons.svg#show-icon"
                             class="use-show"></use>
                    </svg>
                </div>
            </div>
            <div class="content organizations__accord-content">
                <div class="mb16"></div>
                <form action="user.changeorgaddress" class="ui form organizations__form js-addresses-validate">
                    <input type="hidden" name="id" value="<?= $organization["id"] ?>">
                    <div class="fields custom">
                        <div class="four wide field custom">
                            <input type="text" placeholder="ИНН*"
                                   data-dadata="party"
                                   name="tin" data-validate="inn"
                                   value="<?= $organization["tin"] ?>"
                                   data-suggestion="inn">
                        </div>
                        <div class="six wide field custom">
                            <input type="text" placeholder="КПП" data-dadata="party"
                                   name="checkpoint" data-validate="text"
                                   value="<?= $organization["checkpoint"] ?>"
                                   data-suggestion="kpp">
                        </div>
                        <div class="six wide field custom">
                            <input type="text" name="bik" placeholder="БИК" value="<?= $organization["bik"] ?>"
                                   data-validate="text" data-dadata="bank">
                        </div>
                    </div>
                    <div class="field custom">
                        <input type="text" placeholder="Название организации*"
                               data-dadata="party" name="name"
                               data-validate="name"
                               value="<?= $organization["name"] ?>"
                               data-suggestion="name">
                    </div>
                    <div class="field custom">
                        <input type="text" name="address"
                               placeholder="Юридический адрес"
                               value="<?= $organization["address"] ?>"
                               data-suggestion="legal-address">
                    </div>
                    <div class="field custom">
                        <input type="text" name="bank_name" value="<?= $organization['bank_name'] ?>"
                               placeholder="Наименование банка" data-dadata="bank">
                    </div>
                    <div class="two column fields custom">
                        <div class="field custom">
                            <input type="text" name="rs" placeholder="Р/C" value="<?= $organization['rs'] ?>">
                        </div>
                        <div class="field custom">
                            <input type="text" name="ks" placeholder="К/C" value="<?= $organization['ks'] ?>"
                                   data-dadata="bank">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox radio custom">
                            <input id="make-default-organization-<?= $organization["id"] ?>"
                                   type="checkbox" name="default" value="Y"
                                   class="hidden"
                                   data-validate="checkbox"<?= $organization["default"] ? " checked" : "" ?>>
                            <label for="make-default-organization-<?= $organization["id"] ?>">Сделать
                                организацией по умолчанию</label>
                        </div>
                    </div>
                    <div class="ui success message">Изменения сохранены</div>
                    <div class="ui error message">При сохранении возникла ошибка</div>
                    <div class="personal__order-buttons">
                        <div class="personal__order-buttons-left">
                            <a class="ui mini basic button custom js-remove-organization-trigger"
                               data-value="<?= $organization['id'] ?>">Удалить</a>
                            <a class="ui mini basic button custom mobile only accordion-close form-reset">Отменить</a>
                        </div>
                        <div class="personal__order-buttons-right">
                            <a class="ui mini basic button custom mobile hidden accordion-close form-reset">Отменить</a>
                            <button class="ui mini primary button custom">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
            <?
        }
        ?>
        <div class="title personal-accord__title personal-accord-add-organization-title add-new">
            <div class="text--right personal__data-button">
                <a class="ui primary custom button">Добавить организацию</a>
            </div>
        </div>
        <div class="content organizations__accord-content">
            <div class="mb16"></div>
            <form action="user.addorgaddress"
                  class="ui form js-addresses-validate js-insert-after-submit organizations__form">
                <div class="fields custom">
                    <div class="four wide field custom">
                        <input type="text" placeholder="ИНН*"
                               data-dadata="party"
                               name="tin" data-validate="inn"
                               data-suggestion="inn">
                    </div>
                    <div class="six wide field custom">
                        <input type="text" placeholder="КПП" data-dadata="party"
                               name="checkpoint" data-validate="text"
                               data-suggestion="kpp">
                    </div>
                    <div class="six wide field custom">
                        <input type="text" name="bik" placeholder="БИК"
                               data-validate="text" data-dadata="bank">
                    </div>
                </div>
                <div class="field custom">
                    <input type="text" placeholder="Название организации*"
                           data-dadata="party" name="name"
                           data-validate="name"
                           data-suggestion="name">
                </div>
                <div class="field custom">
                    <input type="text" name="address"
                           placeholder="Юридический адрес"
                           data-suggestion="legal-address">
                </div>
                <div class="field custom">
                    <input type="text" name="bank_name"
                           placeholder="Наименование банка" data-dadata="bank">
                </div>
                <div class="two column fields custom">
                    <div class="field custom">
                        <input type="text" name="rs" placeholder="Р/C">
                    </div>
                    <div class="field custom">
                        <input type="text" name="ks" placeholder="К/C"
                               data-dadata="bank">
                    </div>
                </div>
                <div class="field">
                    <div class="ui checkbox radio custom">
                        <input id="make-default-organization-new"
                               type="checkbox" name="default" value="Y"
                               class="hidden"
                               data-validate="checkbox">
                        <label for="make-default-organization-new">Сделать
                            организацией по умолчанию</label>
                    </div>
                </div>
                <div class="personal__order-buttons">
                    <div class="personal__order-buttons-left">
                        <a class="ui mini basic button custom js-remove-organization-trigger hide">Удалить</a>
                        <a class="ui mini basic button custom mobile only accordion-close form-reset">Отменить</a>
                    </div>
                    <div class="personal__order-buttons-right">
                        <a class="ui mini basic button custom mobile hidden accordion-close form-reset">Отменить</a>
                        <button class="ui mini primary button custom">Добавить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
