<?
define('NEED_AUTH', true);
define('NEED_AUTH_TEXT','Для продолжения оформления заказа необходимо <a href="#!" data-modal="auth">авторизоваться</a>.');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("cameo-v3:option:modules", "object-fit,jquery,semantic,googleMaps");
$APPLICATION->SetPageProperty("cameo-v3:option:useTitle", "false");
$APPLICATION->SetTitle("Оформление заказа");
global $userAddresses;
global $organizations;
$userAddresses = [];
$organizations = [];
if (CUser::isAuthorized()) {
  $addresses = new userAddressesData();
  $userAddresses = $addresses ? $addresses->get() : [];
  $oAddresses = new orgAddressesData();
  $organizations = $oAddresses ? $oAddresses->get() : [];
  $currentCityId = null;
}
global $version;
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax", 
	"main".($version == 3 ? '' : '-v'.$version),
	array(
		"ACTION_VARIABLE" => "action",
		"ADDITIONAL_PICT_PROP_17" => "-",
		"ADDITIONAL_PICT_PROP_21" => "-",
		"ALLOW_APPEND_ORDER" => "Y",
		"ALLOW_AUTO_REGISTER" => "Y",
		"ALLOW_NEW_PROFILE" => "N",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"COMPATIBLE_MODE" => "Y",
		"DELIVERY_NO_AJAX" => "Y",
		"DELIVERY_NO_SESSION" => "Y",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"DISABLE_BASKET_REDIRECT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"PATH_TO_AUTH" => "/personal-area/",
		"PATH_TO_BASKET" => "/personal-area/basket/",
		"PATH_TO_PAYMENT" => "payment.php",
		"PATH_TO_PERSONAL" => "/personal-area/",
		"PAY_FROM_ACCOUNT" => "N",
		"PROPS_FADE_LIST_N" => array(
			0 => "LOGOTIP",
		),
		"PRODUCT_COLUMNS_VISIBLE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "PROPS",
		),
		"SEND_NEW_USER_NOTIFY" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",
		"SHOW_VAT_PRICE" => "Y",
		"TEMPLATE_LOCATION" => "popup",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "Y",
		"USE_PRELOAD" => "Y",
		"USE_PREPAYMENT" => "N",
		"COMPONENT_TEMPLATE" => "main",
		"SPOT_LOCATION_BY_GEOIP" => "Y",
		"ADDITIONAL_PICT_PROP_38" => "-",
		"ADDITIONAL_PICT_PROP_70" => "-",
		"ADDITIONAL_PICT_PROP_88" => "-",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"USE_PHONE_NORMALIZATION" => "N",
		"ADDITIONAL_PICT_PROP_90" => "-",
		"ADDITIONAL_PICT_PROP_91" => "-"
	),
	false
); ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
